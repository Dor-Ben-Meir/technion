
######################################################################################
#Python code for homework 3 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci

RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533


def E_two(e, Ni):
    E = np.arccos((e + np.cos(Ni))/(1 + e*np.cos(Ni)))
#    if(Ni > np.pi):
#        E = 2*np.pi - E
    return E

        
def t_one(a, e, E):
    t = np.sqrt((a**3)/mu) * (E - e*np.sin(E)) 
    return t

#Minimal semi-major axis    
def a_min(r1,r2):
    a = (LA.norm(r1) + LA.norm(r2) + LA.norm(r2 - r1))/4
    return a

    
def e_three(r1,r2,a):
    e = LA.norm(r1 + (r2 - r1)/LA.norm(r2 - r1)*(2*a - LA.norm(r1)))/(2*a)
    return e

def Ni_two(r,V,a,e):
    Ni = np.arccos((a/LA.norm(r)*(1 - e**2) - 1)/e);
    if(np.dot(V,r) < 0):
        Ni = 2*np.pi - Ni
    return Ni
    
def i(h_v, h_size):
    i = np.arccos(h_v[2]/h_size)
    return i
    
#Angle between given points
def eta_one(r1,r2):
    eta = np.arccos(np.dot(r1, r2)/(LA.norm(r1)*LA.norm(r2)))
    return eta
    
#F and G functions and derivatives
def F_two(r2,eta,p):
    F = 1 - LA.norm(r2)/p*(1 - np.cos(eta))
    return F

def G_two(r1,r2,eta,h):    
    G = LA.norm(r1) * LA.norm(r2)/h*np.sin(eta)
    return G

def Ft_two(r1,r2,eta,p):    
    Ft = np.sqrt(mu/p)*np.tan(eta/2)*((1 - np.cos(eta))/p - 1/LA.norm(r1) - 1/LA.norm(r2))
    return Ft

def Gt_two(r1,eta,p):    
    Gt = 1 - LA.norm(r1)/p*(1 - np.cos(eta))
    return Gt
    
#velocities at given points    
def V_r1(r1,r2,F,G):
    V1 = (r2 - F*r1)/G
    return V1
    
def V_r2(r1,Ft,Gt,V1):
    V2 = Ft*r1 + Gt*V1 
    return V2    

def Pr_one(a,e):
    Pr = a * (1 - e)
    return Pr

r1 = (10**3) * np.array([-5.5547228,0.6201831,-7.867960])  # [km]
r2 = (10**3) * np.array([-1.5501876, 4.96742911,-8.3046369])  # [km]

r1_size = LA.norm(r1)
    
a = a_min(r1,r2)
e = e_three(r1,r2,a)
p = a * (1 - e**2)
h = np.sqrt(mu*p)

eta = eta_one(r1,r2) 
F = F_two(r2,eta,p)
G = G_two(r1,r2,eta,h)
Ft = Ft_two(r1,r2,eta,p)
Gt = Gt_two(r1,eta,p)

V1 = V_r1(r1,r2,F,G)
V2 = V_r2(r1,Ft,Gt,V1)

Ni1 = Ni_two(r1,V1,a,e)
Ni2 = Ni_two(r2,V2,a,e)

E1 = E_two(e, Ni1)
E2 = E_two(e, Ni2)

delta_t = (t_one(a, e, E2) - t_one(a, e, E1)) * u.s 

h_v = np.cross(r1, V1) 
e_v = (np.cross(V1, h_v) / mu) - (r1 / LA.norm(r1))
i = i(h_v, h) 
n = np.cross([0,0,1],h_v)
n_size = LA.norm(n)
dot_ne = np.dot(n,e_v)
w =  np.arccos(dot_ne / (n_size * e)) 
if(e_v[2] < 0 ):
    w = (2 * np.pi - w)    
Omega = np.arccos(n[0]/n_size) 
if(n[1] <= 0 ):
    Omega = (2 * np.pi - Omega)    
    
    
ans1 = dict(a = a * u.km, e_v = e_v, e = e, i = i * u.rad, w = w * u.rad, Omega = Omega * u.rad)
ans2 = dict(V1 = V1 * (u.km / u.s), V2 = V2 * (u.km / u.s), delta_t = delta_t)    
ans3 = dict(Ni1 = Ni1 * u.rad, Ni2 = Ni2 * u.rad,r1_size = r1_size * u.km, RE = RE * u.km)   
    

