clear 
close 
clc 

T = 43200; 
i = 55; %[deg] 
Omega = 45; %[deg]
r = 26610.24; %[km] 
RA = 91.6636; %[deg] 
del = 23.45; %[deg] 
rs = [cos(deg2rad(del))*cos(deg2rad(RA))     cos(deg2rad(del))*sin(deg2rad(RA))      sin(deg2rad(del))]; 
DCM = @(Om,om,i) [cos(Om)*cos(om)-sin(Om)*sin(om)*cos(i) ;     sin(Om)*cos(om)+cos(Om)*sin(om)*cos(i);     sin(om)*sin(i)].*r;

tspan = 100; 
omega = linspace(0,360,tspan); 
rSat = DCM(deg2rad(Omega),deg2rad(omega),deg2rad(i));

cm = 8.63e-14; 
cs = 3.96e-14; %[1/s^2]
fm = zeros(3,100); fs = fm; fsm = fm;
for j = 1:tspan
        fm(:,j) = cm.*(-rSat(:,j)+3.*dot(rs,rSat(:,j))*rs); % moon perturbation  
        fs(:,j) = cs.*(-rSat(:,j)+3.*dot(rs,rSat(:,j))*rs); % sun perturbation (rs=rm)   
        fsm(:,j) = fs(:,j)+fm(:,j);
end



