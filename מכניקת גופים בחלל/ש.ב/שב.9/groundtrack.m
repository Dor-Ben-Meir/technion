function groundtrack
clc
clear
format long g
close all
a = 7098.092e3; % semimajor axis of a leo orbit [m]
e = 0.04507; % eccentricity
i = 1.4272*180/pi; % inclination [deg]
Omega = 0; % right ascension of ascending node [deg]
omega = 0; % argument of perigee [deg]
M0 = 0; % mean anomaly at t=0 [deg]
to = juliandate(2018,06,01,12,30,50);
tf = juliandate(2018,06,02,12,30,50);
Y = kep2cart(a,e,i,Omega,omega,M0,'mea');
step = 1;
span = 0:step:5951.454;
option = odeset('RelTol',1e-12,'AbsTol',1e-12,'NormControl','on');
[~,r] = ode45(@Deriv,span,Y,option);
n = length(span);
ref = zeros(n,3);
lamda = zeros(n,1);
phi = zeros(n,1);
height = zeros(n,1);
for i=1:n
 to = to+step/86400;
 U = R_z(gmst(to-2400000.5));
 ref(i,1:3) = U*r(i,1:3)';
 [lamda(i),phi(i),height(i)] = Geodetic(ref(i,:));
end
figure
geoshow('landareas.shp','FaceColor',[0.5 1 0.5]);
title('Satellite Ground Track')
hold on
plot(lamda*(180/pi),phi*(180/pi),'.r')
% animation
an = animatedline('Marker','*');
for k = 1:n
 addpoints(an,lamda(k)*(180/pi),phi(k)*(180/pi));
 drawnow
 pause(0.2);
 clearpoints(an);
end
end