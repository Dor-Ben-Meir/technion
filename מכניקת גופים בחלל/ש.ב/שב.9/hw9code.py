######################################################################################
#Python code for homework 9 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import astropy.units as u



mu = 398601.2 #[km^3/s^2]
TN = 2*24*60*60 #[s]
J2 = 1.083e-3
Re = 6378.16 # [km]
omegads = 1.99e-7# [rad/s]
rp = 400 + Re # [km]
N = 29
k = 2
w_e = 2 * np.pi / 86164

def a_two(mu,T):
    a = (mu * ((T / (2 * np.pi))**2))**(1/3)
    return a
    
      
T = np.linspace(96, 102, num = 10**5)*60 #[s]
a = a_two(mu,T)
e = 1 - rp / a 


n0 = np.sqrt(mu/a**3)
cosi = omegads/(-3/2*J2*np.sqrt(mu)*Re**2*a**(-7/2)*(1-e**2)**(-2))
w_dot = 3/4*J2*np.sqrt(mu)*Re**2*a**(-7/2)*(1-e**2)**-2*(5*cosi**2-1)
M_dot = 3/4*J2*np.sqrt(mu)*Re**2*a**(-7/2)*(1-e**2)**(-3/2)*(3*cosi**2-1)
n = N/k*(w_e - omegads) - (M_dot + w_dot)
error = abs(n - n0)



ind =  np.where(error == error.min())
a = a[ind]
e = e[ind]
cosi = cosi[ind]
i = np.arccos(cosi)

ans1 = dict(a = a*u.km, e = e, i = i*u.rad) 