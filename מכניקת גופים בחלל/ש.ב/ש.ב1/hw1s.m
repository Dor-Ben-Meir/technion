clear all
close all
clc

%Define Parameters, caculate initial values
R_E=6378; %[km], Earth radius
r_AN=[7650 7650 0]; %[km], Initial location

%Import
sol = importdata('sol.txt');
t = importdata('t.txt');
rp = importdata('rp.txt');
x=sol(:,1);y=sol(:,2);z=sol(:,3);
vx=sol(:,4);vy=sol(:,5);vz=sol(:,6);
t=t/3600; %convert to hours

%Part 1.1 - Satellite's orbit
figure(1)
plot3(x,y,z,'k','LineWidth',1); %Orbit
hold on ; [X,Y,Z]=sphere; surf(R_E.*X , R_E.*Y , R_E.*Z); %Earth
hold on; plot3 (r_AN(1),r_AN(2),r_AN(3),'or','MarkerSize',4,'LineWidth',1); %Ascending Node
hold on; plot3 (rp(1),rp(2),rp(3),'*','MarkerSize',4,'LineWidth',1); % Perigee Location
axis equal ;grid on; xlabel ('x [km]'); ylabel ('y [km]'); zlabel ('z [km]');
legend ('Orbit','Earth','Ascending Node','Perigee Location'); title ('Satellites Orbit Around Earth')
