######################################################################################
#Python code for homework 8 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
import astropy.units as u
from numpy import linalg as LA
from scipy.optimize import fsolve



def t_one(a, e, E):
    t = np.sqrt((a**3)/mu) * (E - e*np.sin(E)) 
    return t

def E_two(e, Ni):
    E = np.arccos((e + np.cos(Ni))/(1 + e*np.cos(Ni)))
    return E


mu = 3.986 * (10**5) 
R_E = 6378 
 
a_sat = 8059 
e_sat = 0.159
i_sat = np.deg2rad(102.3) 
Omega_sat = np.deg2rad(11.4)
omega_sat = np.deg2rad(62.42)
omega_avg = -3.6 * (10**-7)
delta = np.deg2rad(14.73)
e = 0.159

t = (0 - omega_sat) / omega_avg
t = t / 86400

RA = np.deg2rad(41.4)

rs = np.array([np.cos(delta)*np.cos(RA),np.cos(delta)*np.sin(RA),np.sin(delta)])
h = np.array([np.sin(i_sat)*np.sin(Omega_sat),-np.sin(i_sat)*np.cos(Omega_sat),np.cos(i_sat)])
beta = abs(np.arcsin(np.dot(h,rs)))

a_shadow = R_E/np.sin(beta)
b_shadow = R_E


ev1 = np.cos(omega_sat)*np.cos(Omega_sat) - np.sin(omega_sat)*np.cos(i_sat)*np.sin(Omega_sat)
ev2 = np.cos(omega_sat)*np.sin(Omega_sat)+np.sin(omega_sat)*np.cos(i_sat)*np.cos(Omega_sat)
ev3 = np.sin(omega_sat)*np.sin(i_sat)
e_vec = np.array([ev1, ev2, ev3])

alpha = np.arcsin(-np.dot((np.cross(h,rs)),e_vec)/np.cos(beta))

def equations(P):
    x, y = P
    eq1 = ((x+a_sat*e_sat)/a_sat)**2+(y/(a_sat*np.sqrt(1-(e_sat)**2)))**2 - 1
    eq2 = ((x*np.cos(alpha)+y*np.sin(alpha))/a_shadow)**2 + ((x*np.sin(alpha)- y*np.cos(alpha))/b_shadow)**2 - 1
    return (eq1, eq2)

P =  fsolve(equations, (3000, -7000))
x, y = P
theta_s = (math.atan(y/x))

theta= np.arange( 0, 2*np.pi + 0.1 ,0.1)       
x_E = R_E*np.cos(theta)
y_E = R_E*np.sin(theta)

x_sat=-e_sat*a_sat+a_sat*np.cos(theta)
b_sat=a_sat * np.sqrt(1-e_sat**2)
y_sat=b_sat*np.sin(theta)
x_sha=abs(R_E/np.sin(beta))*np.cos(theta)*np.cos(alpha)-R_E*np.sin(theta)*np.sin(alpha)
y_sha=abs(R_E/np.sin(beta))*np.cos(theta)*np.sin(alpha)+R_E*np.sin(theta)*np.cos(alpha)


theta_s2 = np.deg2rad(180 -20.7679)
theta_s1 = np.deg2rad(76.9939)

E1 = E_two(e, theta_s1)
E2 = E_two(e, theta_s2)
Delta_t = t_one(a_sat, e, E2) - t_one(a_sat, e, E1)


ans1 = dict(a = 8059* u.km, 
e = 0.159,
it = np.deg2rad(102.3) * u.rad ,
Omega = np.deg2rad(11.4) * u.rad,
omega = np.deg2rad(62.42) * u.rad,
omega_avg = -3.6 * (10**-7) * (u.rad/u.s),
delta = np.deg2rad(14.73) * u.rad,
t = t * u.d,
)
e = 0.159
ans2 = dict(
theta_s2 = np.deg2rad(180 -20.7679) * u.rad,
theta_s1 = np.deg2rad(76.9939) * u.rad,
E1 = E_two(e, theta_s1) * u.rad,
E2 = E_two(e, theta_s2) * u.rad,
Deta_t2T = Delta_t / 7200,
Delta_t = t_one(a_sat, e, E2) - t_one(a_sat, e, E1) ,
)


np.savetxt('x_sat.txt', (x_sat))
np.savetxt('y_sat.txt', (y_sat))  
np.savetxt('x_sha.txt', (x_sha))  
np.savetxt('y_sha.txt', (y_sha))
np.savetxt('x_E.txt', (x_E))  
np.savetxt('y_E.txt', (y_E))