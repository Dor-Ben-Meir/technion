clc
clear
close all

Cf1 = importdata('Cf1.txt');
f = importdata('f.txt');
Isp1 = importdata('Isp1.txt');



% Plotting
figure(1)
hold on
grid on
box on
for a=1:3
plot(f,Cf1(a,:),'LineWidth',2)
end
title('\fontsize{17} Thrust Coefficient Vs. Fuel to Air Ratio');
xlabel('\fontsize{14}  f');
ylabel('\fontsize{14}  C_{F}');
legend('A_{5}/A_{3} = 0.5','A_{5}/A_{3} = 0.6','A_{5}/A_{3} = 0.7','Location', 'Best')





figure(2)
hold on
grid on
box on
for a=1:3
plot(Isp1(a,:),Cf1(a,:),'LineWidth',2)
end
title('\fontsize{17} Thrust Coefficient Vs. Specific Impulse');
xlabel('\fontsize{14}  I_{SP}');
ylabel('\fontsize{14}  C_{F}');
legend('A_{5}/A_{3} = 0.5','A_{5}/A_{3} = 0.6','A_{5}/A_{3} = 0.7','Location', 'Best')



Cf2 = importdata('Cf2.txt');
Isp2 = importdata('Isp2.txt');


% Plotting
figure(3)
hold on
grid on
box on
for a=1:3
plot(f,Cf2(a,:),'LineWidth',2)
end
title('\fontsize{17} Thrust Coefficient Vs. Fuel to Air Ratio');
xlabel('\fontsize{14}  f');
ylabel('\fontsize{14}  C_{F}');
legend('T_{a}= 300','T_{a}= 260','T_{a}= 220','Location', 'Best')


figure(4)
hold on
grid on
box on
for a=1:3
plot(Isp2(a,:),Cf2(a,:),'LineWidth',2)
end
title('\fontsize{17} Thrust Coefficient Vs. Specific Impulse');
xlabel('\fontsize{14}  I_{SP}');
ylabel('\fontsize{14}  C_{F}');
legend('T_{a}= 300','T_{a}= 260','T_{a}= 220','Location', 'Best')