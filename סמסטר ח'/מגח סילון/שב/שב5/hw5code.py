######################################################################################
#Python code for homework 5 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
from scipy.optimize import fsolve

Ma = 2
Gamma_a = 1.4
Gamma_5 = 1.286
AitoA3 = 0.25
Cp_a = 1005 #[j/KgK]
Cp_e = Gamma_5 *287 /(Gamma_5-1) #j/KgK]
g = 9.81
qR = 46.5 * (10**6)
Ta = 300 #[K]
f = [0.02,0.03,0.04,0.05,0.06,0.067]
f = np.array(f) 
R = 287
DeltaT0 = [783, 1127, 1427, 1683, 1889, 1994]
DeltaT0 = np.array(DeltaT0)

def CFnja5_s(Gamma_a,Gamma_5,Ma,S,A5A3):
    AaA5 =  AitoA3 * (1 / A5A3)
    CFnja5 = (2/(Gamma_a*(Ma**2)))*(S*AaA5*(phi_a/phi_5)*((1+((Gamma_a-1)/2)*(Ma**2))**(Gamma_a/(Gamma_a-1)))*((2/(Gamma_5+1))**(Gamma_5/(Gamma_5-1)))*(1+Gamma_5)-1)-2*AaA5
    CFnja5 = A5A3 * CFnja5
    return CFnja5


def phi(M,gamma,R):
    phi = np.sqrt(gamma/R) * (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi


    
def PHI(M,gamma,R):
    PHI = phi(M,gamma,R) * ((1+((gamma-1)/2)*(M**2))**(-(gamma/(gamma-1))))
    return PHI
  


ccf = np.zeros((3,6)) 
A5toA3 = [0.5, 0.6, 0.7] 
A5toA3 = np.array(A5toA3) 
T0a = Ta*(1+((Gamma_a-1)/2)*(Ma**2))   
#T05 = (Cp_a * T0a + f * qR) / ((f + 1) * Cp_e)
#S = (f + 1) * np.sqrt(T05 / T0a)
S = (f + 1) * np.sqrt(1 + (DeltaT0/ T0a))



phi_a =  PHI(Ma,Gamma_a,R)
phi_5 =  PHI(1,Gamma_5,R)

CFnja5 = np.zeros((len(A5toA3),len(S)))
#CFnja5 = np.zeros((len(S),len(A5toA3)))
Isp = np.zeros((len(A5toA3),len(S)))
i = -1 

for a in A5toA3:
    i = i + 1
    CFnja5[i,:] = CFnja5_s(Gamma_a,Gamma_5,Ma,S,a) 
    ccf[i,:]  = CFnja5[i,:] / np.transpose(f)
    Isp[i,:] = (Ma* np.sqrt(Gamma_a*287*Ta)/(AitoA3*2*g)) * (ccf[i,:] )
    #Isp[i,:] = 8.17 * np.sqrt(Ta) * (CFnja5[i,:] / np.transpose(f) )
    
np.savetxt('f.txt', (f))    
np.savetxt('Cf1.txt', (CFnja5))
np.savetxt('Isp1.txt', (Isp))

    
DeltaT0 = np.zeros((3,6))
S = np.zeros((3,6))
ccf = np.zeros((3,6))
A5toA3 = 0.6
Ta = [300, 260, 220]
Ta = np.array(Ta)
T0a = Ta*(1+((Gamma_a-1)/2)*(Ma**2))
DeltaT0[0,:] = [783, 1127, 1427, 1683, 1889, 1994]
DeltaT0[1,:] = [789, 1133, 1444, 1700, 1905, 1967]
DeltaT0[2,:] =[800, 1144, 1461, 1722, 1928, 1983]

i = -1 
for T in T0a:
    i = i + 1
    #T05[i,:] = (Cp_a * T + f * qR) / ((f + 1) * Cp_e)
    #S[i,:] = (f + 1) * np.sqrt(T05[i,:] / T)
    S[i,:] = (f + 1) * np.sqrt(1 + (DeltaT0[i,:]/ T))
    
i = -1 
for T in Ta:
    i = i + 1
    CFnja5[i,:] = CFnja5_s(Gamma_a,Gamma_5,Ma,S[i,:],A5toA3) 
    ccf[i,:] = CFnja5[i,:] / f
    Isp[i,:] = (Ma* np.sqrt(Gamma_a*287*T)/(AitoA3*2*g)) * (ccf[i,:])


    
np.savetxt('Cf2.txt', (CFnja5))
np.savetxt('Isp2.txt', (Isp))    
    
    
    
    
    