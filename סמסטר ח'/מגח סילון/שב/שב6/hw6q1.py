######################################################################################
#Python code for homework 6 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
import astropy.units as u


Ta = 250
Ma = 2.5
M2 = 0.23
qR = 43 * (10**6)
Gamma_a = 1.4
R = 287
Cp = 1

def phi(M,gamma,R):
    phi = np.sqrt(gamma/R) * (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi


    
def PHI(M,gamma,R):
    PHI = phi(M,gamma,R) * ((1+((gamma-1)/2)*(M**2))**(-(gamma/(gamma-1))))
    return PHI






T0a = Ta*(1+((Gamma_a-1)/2)*(Ma**2))  
T03toTstar = 0.2224
T04 = T0a / T03toTstar


f = ((T04 / T0a) - 1) / ((qR / (Cp*T0a)) - (T04 / T0a))

phi_3 =  PHI(M2,Gamma_a,R)
phi_star =  PHI(1,Gamma_a,R)
P04toP02 = ((f + 1)**2) * np.sqrt(T04 / T0a) * (phi_3 / phi_star)

dic1 = dict(T0a = T0a * u.K, T04 = T04 * u.K, f = f * (10**3), P04toP02 = P04toP02 )