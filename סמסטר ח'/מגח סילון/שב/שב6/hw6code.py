######################################################################################
#Python code for homework 6 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
from scipy.optimize import fsolve


Ma = 2
Gamma_a = 1.4
Gamma_5 = 1.286
g = 9.8
AitoA3 = 0.3
A5A3 = 0.55
eta_C = 0.97
Ta = 300 #[K]
f = [0.02,0.03,0.04,0.05,0.06,0.067]
f = np.array(f) 
R = 287
DeltaT0 = [783, 1127, 1427, 1683, 1889, 1994]
DeltaT0 = np.array(DeltaT0)

P04toP03 = [0.958, 0.954, 0.951, 0.949, 0.947, 0.946]
P04toP03 = np.array(P04toP03) 

P03toP02 = [0.895, 0.9175, 0.925, 0.93, 0.935, 0.9375]
P03toP02 = np.array(P03toP02) 


M3 = [0.188, 0.165, 0.15, 0.138, 0.13, 0.12]
M3 = np.array(M3) 

def rd(M,gamma):
    C1 = 0.5*(gamma+1)*M**2
    C2 = 1 + 0.5*(gamma-1)*M**2
    C3 = (2*gamma/(gamma+1))*M**2
    C4 = (gamma-1)/(gamma+1)
    rd = ((C1/C2)**(gamma/(gamma-1)))*(C3-C4)**(1/(1-gamma))
    return rd


def CFnja5_s(Gamma_a,Gamma_5,Ma,S,A5A3,AaA5):
    CFnja5 = (2/(Gamma_a*(Ma**2)))*(S*AaA5*(phi_a/phi_5)*((1+((Gamma_a-1)/2)*(Ma**2))**(Gamma_a/(Gamma_a-1)))*((2/(Gamma_5+1))**(Gamma_5/(Gamma_5-1)))*(1+Gamma_5)-1)-2*AaA5
    CFnja5 = A5A3 * CFnja5
    return CFnja5


def phi(M,gamma,R):
    phi = np.sqrt(gamma/R) * (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi


    
def PHI(M,gamma,R):
    PHI = phi(M,gamma,R) * ((1+((gamma-1)/2)*(M**2))**(-(gamma/(gamma-1))))
    return PHI




phi_a =  PHI(Ma,Gamma_a,R)
phi_5 =  PHI(1,Gamma_5,R)

T0a = Ta*(1+((Gamma_a-1)/2)*(Ma**2))   
S = (f + 1) * np.sqrt((1 + (DeltaT0/ T0a)) * eta_C)

rd = rd(Ma,Gamma_a)
P05toP0a1 = P04toP03 * P03toP02 * rd

AaA51 =  AitoA3 * (1 / A5A3)
P05toP0a2 = S * AaA51 * (phi_a / phi_5)

P05toP0a = np.zeros(len(P05toP0a1))
AaA5 = np.zeros(len(P05toP0a1))

for i in range(len(P05toP0a1)):
    if (P05toP0a1[i] > P05toP0a2[i]):
        P05toP0a[i] = P05toP0a2[i]
        AaA5[i] = AaA51
    else:
        P05toP0a[i] = P05toP0a1[i]
        AaA5[i] = P05toP0a[i] * (phi_5 / phi_a) * (1 / S[i])
        
CFnja5 = CFnja5_s(Gamma_a,Gamma_5,Ma,S,A5A3,AaA5)  
Isp = (Ma* np.sqrt(Gamma_a*287*Ta)/(AitoA3*2*g)) * (CFnja5 / f )    


np.savetxt('f.txt', (f))    
np.savetxt('Cf.txt', (CFnja5))
np.savetxt('Isp.txt', (Isp))      


