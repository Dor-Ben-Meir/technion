######################################################################################
#Python code for homework 6 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
import astropy.units as u

gamma = 1.4
Ta = 220
R = 287
g = 9.81
f = 0.035
D3 = 0.52
Ai3 = 0.35
A3 = (np.pi/4)*D3**2
Ai = A3*Ai3
Pa = 0.185 * 101325 
Ma = 2

def phi(M,gamma,R):
    phi = np.sqrt(gamma/R) * (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi

def mdot(P, A, Phi, T0a, f):
    m = (f * P  * A * Phi) / np.sqrt(T0a)
    return m
    
def f_one(m_dot, T0a, P, A, phi):
    f = (m_dot * np.sqrt(T0a)) / (P * A * phi)
    return f
    
def A_one(m_dot, T0a, P, f, phi):
    A = (m_dot * np.sqrt(T0a)) / (P * f * phi)
    return A
    
Phi = phi(Ma,gamma,R)
T0a = (Ta*(1+((gamma-1)/2)*(Ma**2))) 
m_dotf = mdot(Pa, 0.96*Ai, Phi, T0a, f)


M = np.array([1.7, 1.8, 1.9])
f = np.array([0.045, 0.0425, 0.04])
Cfnj = np.array([0.67, 0.645, 0.625])
CD0 = np.array([0.64, 0.615, 0.57])
Phi = phi(M,gamma,R)
Aa_Ai = np.array([0.785,0.85,0.89])


np.savetxt('M.txt', (M))
np.savetxt('f.txt', (f))
np.savetxt('Cfnj.txt', (Cfnj))  
np.savetxt('CD0.txt', (CD0))  
np.savetxt('Aa_Ai.txt', (Aa_Ai)) 