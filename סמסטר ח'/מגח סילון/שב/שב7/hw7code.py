######################################################################################
#Python code for homework 6 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
import astropy.units as u

gamma = 1.4
R = 287
g = 9.81
f = 0.067



PaSL = 101325
Pa5 = 84297
Pa10 = 69671

P = []
Phi = []
Ta = []
T0a = []
Ma1 = [0.9, 0.95, 1, 1.05, 1.1, 1.15]
Ma2 = 1.2
Ma3 = [1.3, 1.4, 1.5, 1.6]

Mall = [0.9, 0.95, 1, 1.05, 1.1, 1.15, 1.2, 1.3, 1.4, 1.5, 1.6]

Cd1 = [0.21, 0.23, 0.3, 0.55, 0.57, 0.55,  0.54, 0.5, 0.48, 0.46, 0.45]

Cf1 = [0.565, 0.565, 0.59, 0.59, 0.633, 0.633,  0.66,  0.695, 0.705, 0.71, 0.715]
 
W0 = 600 * 9.81 #[N]
D3 = 0.48
Ai3 = 0.35
A3 = (np.pi/4)*D3**2
Ai = A3*Ai3
TaSL = 300
aSL = np.sqrt(gamma*R*TaSL)
Ta5 = 280
a5 = np.sqrt(gamma*R*Ta5)
Ta10 = 270
a10 = np.sqrt(gamma*R*Ta10)

Aa = [0.6*Ai,Ai*0.625, Ai*0.635, Ai*0.65, Ai*0.675, Ai*0.66, Ai*0.65,Ai*0.675,Ai*0.7, Ai*0.725, Ai*0.75]

m_dot1 = np.zeros(len(Aa))
m_dot2 = np.zeros(len(Aa))
f2 = np.zeros(len(Aa))

def phi(M,gamma,R):
    phi = np.sqrt(gamma/R) * (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi

def mdot(P, A, Phi, T0a, f):
    m = (f * P  * A * Phi) / np.sqrt(T0a)
    return m
    
def f_one(m_dot, T0a, P, A, phi):
    f = (m_dot * np.sqrt(T0a)) / (P * A * phi)
    return f
    
    

Pr = PaSL
T = TaSL
for M in Ma1:

    t = T*(1+((gamma-1)/2)*(M**2))
    p = phi(M,gamma,R)
    T0a.append(t)
    Phi.append(p)
    P.append(Pr)
    Ta.append(T)
    
Pr = Pa5
T = Ta5
M = Ma2
T0a.append(T*(1+((gamma-1)/2)*(M**2))) 
Phi.append(phi(M,gamma,R))
P.append(Pr)
Ta.append(T)



Pr = Pa10
T = Ta10
for M in Ma3:
    T0a.append(T*(1+((gamma-1)/2)*(M**2)))
    Phi.append(phi(M,gamma,R))
    P.append(Pr)
    Ta.append(T)
    


T0a = np.array(T0a)
Phi = np.array(Phi)
Aa = np.array(Aa)
P = np.array(P)
Ta = np.array(Ta)
Mall = np.array(Mall)
Cd1 = np.array(Cd1)
Cf1 = np.array(Cf1)



m_dot1 = mdot(P, Aa, Phi, T0a, f)


np.savetxt('Mall.txt', (Mall))
np.savetxt('Cd1.txt', (Cd1))  
np.savetxt('Cf1.txt', (Cf1))
np.savetxt('m_dot1.txt', (m_dot1))  

##########################################################################################



C1 = 1.43
C2 = -0.01
C3 = -0.35

Aa = [0.6*Ai,Ai*0.625, Ai*0.635, Ai*0.65, Ai*0.675, Ai*0.68, Ai*0.72,Ai*0.675,Ai*0.7, Ai*0.725, Ai*0.75]
Aa = np.array(Aa)

m_dot2 = C3 + C1*(Mall + C2*(300-Ta))
f2 = f_one(m_dot2, T0a, P, Aa, Phi)

Mall2 = [0.9,  1,  1.1,  1.2, 1.3, 1.4, 1.5, 1.6]
Cf_control = [0.57, 0.596,0.635, 0.638 ,0.685, 0.7, 0.707, 0.709]
Cf2 = [0.565,  0.59,  0.633,   0.66,  0.695, 0.705, 0.71, 0.715]

Mall2 = np.array(Mall2)
Cf_control = np.array(Cf_control)
Cf2 = np.array(Cf2)


C_error = (Cf2 - Cf_control) / Cf2

np.savetxt('Mall2.txt', (Mall2))
np.savetxt('m_dot2.txt', (m_dot2))
np.savetxt('f2.txt', (f2))  
np.savetxt('Cf2.txt', (Cf2))  
np.savetxt('Cf_control.txt', (Cf_control))
np.savetxt('C_error.txt', (C_error)) 
np.savetxt('C2.txt', ((300-Ta))) 

##########################################################################################

alphamax = np.arcsin(((0.5 * gamma * Pa5 * (1.2**2) * A3) / W0) * (0.66 - 0.54))
alpha = 0.75 * alphamax
Cfnj = 0.54 + (W0 / (0.5 * gamma * Pa5 * (1.2**2) * A3)) * np.sin(alpha)

ans1 = dict(alphamax = np.rad2deg(alphamax) * u.deg, alpha = np.rad2deg(alpha) * u.deg, Cfnj = Cfnj, Cf_control = Cf_control[3])