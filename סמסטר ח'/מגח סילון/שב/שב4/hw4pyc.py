######################################################################################
#Python code for homework 4 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from scipy.optimize import fsolve


#constants:
gamma = 1.4


def phi(M):
    phi = (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi
    
def PHI(M):
    PHI = phi(M) * ((1+((gamma-1)/2)*(M**2))**(-(gamma/(gamma-1))))
    return PHI

def rd(M):
    C1 = 0.5*(gamma+1)*M**2
    C2 = 1 + 0.5*(gamma-1)*M**2
    C3 = (2*gamma/(gamma+1))*M**2
    C4 = (gamma-1)/(gamma+1)
    rd = ((C1/C2)**(gamma/(gamma-1)))*(C3-C4)**(1/(1-gamma))
    return rd

def M2_sock(M):
    C1 = 2 + (gamma-1)*M**2
    C2 = 1-gamma + 2*gamma*M**2
    M2 = np.sqrt(C1/C2)
    return M2

def At_Ai(M):
    C = (2/(gamma+1))*(1+ 0.5*(gamma-1)*M**2)
    AtAi = M*(C**((gamma+1)/(2*(1-gamma))))
    return AtAi
    
       

           

Md =  np.arange( 1.5, 4 + 0.5, 0.5)

P02toP0a = np.zeros(len(Md))
P02toP0a = rd(Md)

np.savetxt('Md.txt', (Md))
np.savetxt('P02toP0a.txt', (P02toP0a))  

#######################################################################################
Mt = []
#Mt = np.zeros(len(Md))
#M = M2_sock(Md)

for m in Md:
    M = M2_sock(m)
    def Mt_solve(Mt):
        solve = PHI(Mt) - (1/At_Ai(M)) * PHI(m)
        return solve
    Mt.append(fsolve(Mt_solve, 1.1))
    
Mt = np.array(Mt)    
    
P02toP0a2 = np.zeros(len(Mt))
P02toP0a2 = rd(Mt)

np.savetxt('P02toP0a2.txt', (P02toP0a2))

#######################################################################################
#
Md2 =  np.arange( 1.1, 2, 0.1)
Ms =  np.arange( 0.01, 1.01, 0.01)

AtAi = np.zeros(len(Md))
As = np.zeros(len(Ms))

AtAi = At_Ai(Md2)
As =  At_Ai(Ms)
Mi =  np.interp(AtAi, As, Ms)
M0 = M2_sock(Mi)

np.savetxt('Md2.txt', (Md2))
np.savetxt('M0.txt', (M0)) 