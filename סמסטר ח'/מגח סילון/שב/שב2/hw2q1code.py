######################################################################################
#Python code for homework 2 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
import astropy.units as u
from scipy.optimize import fsolve

def A(d):
    A = (np.pi * (d**2)) / 4
    return A
    
def d(A):
    d = np.sqrt((4 * A) / np.pi)
    return d
    
#Q1:
gamma = 1.4
Ma = 2
Me = 1
d_e = 10 * (10**-2) # [m]
d_max =  14 * (10**-2) # [m]
Ae = A(d_e)
Amax = A(d_max)
rc = 0.9
rn = 1
Aa0 =  7 * (10**-2)
P0atoPa = ((1+((gamma-1)/2)*(Ma**2))**((gamma/(gamma-1))))
PetoP0e = ((1+((gamma-1)/2)*(Me**2))**((-gamma/(gamma-1))))
P02toP0a = 0.7209
P0etoP0a = P02toP0a * rc * rn
PetoPa = PetoP0e * P0etoP0a * P0atoPa 


def Aa_solve(Aa):
    Fnj = ((Ae / Aa) * PetoPa) * (1 + gamma * (Me**2)) - (gamma * (Ma**2)) - ((Ae / Aa)) 
    return Fnj

Aa = fsolve(Aa_solve, Aa0)
damax = d(Aa) * (10**2) * u.cm
ans1 = dict(P0atoPa = P0atoPa, PetoP0e = PetoP0e, P0etoP0a = P0etoP0a, PetoPa = PetoPa)