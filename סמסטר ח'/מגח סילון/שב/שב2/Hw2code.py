
import numpy as np
from scipy.optimize import fsolve


#constants:
gamma = 1.4;
#variables:
M1 = np.arange(0.1, 1 + 0.1, 0.1)
A1A2 = M1
M20 = 0.001
M2Vector = np.zeros((10, 10))
P02P01 = np.zeros((10, 10))

def phi(M):
    phi = (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi
    
def PHI(M):
    PHI = phi(M) * ((1+((gamma-1)/2)*(M**2))**(-(gamma/(gamma-1))))
    return PHI
    
def RHS(m1,a1a2):
    RHS = ((gamma*(m1**2)+(1/a1a2)))/(phi(m1))
    return RHS
 
def LHS(M2):
    LHS = (1+gamma*(M2**2))/(phi(M2))
    return LHS
           
def PtoP(M1,M2,a1a2):
    ptop = a1a2 * (PHI(M1) / PHI(M2))
    return ptop

    
i = -1
j = -1 

for a1a2 in A1A2:
        if(j != -1):
             j = -1
        i = i + 1
        for m1 in M1:
            j = j + 1
            def M2_solve(M2):
                solve = LHS(M2) - RHS(m1,a1a2)
                return solve
            M2 = fsolve(M2_solve, M20)
            P02_to_P01 = PtoP(m1,M2,a1a2)
            
            M2Vector[i,j] = M2
            P02P01[i,j] = P02_to_P01


np.savetxt('M1.txt', (M1))        
np.savetxt('A1A2.txt', (A1A2))
np.savetxt('M2Vector.txt', (M2Vector))
np.savetxt('P02P01.txt', (P02P01))       
    
    