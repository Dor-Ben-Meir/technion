clear all
close all
clc

%ploting the results:
M1 = importdata('M1.txt');
M2Vector = importdata('M2Vector.txt');
A1A2 = importdata('A1A2.txt');
P02P01 = importdata('P02P01.txt');

figure(1)
hold on
grid on
box on
for i=1:1:10
    yyaxis right
    plot(M1,M2Vector(i,:),'r-');
    yyaxis left
    plot(M1,P02P01(i,:),'b-')
end

title('\fontsize{17} Pressure & Mach Diagram');
xlabel('\fontsize{14}  M_{1}');
ylabel('\fontsize{14}  P_{02}/P_{01}');
yyaxis right
ylabel('\fontsize{14}  M_{2}');
