######################################################################################
#Python code for homework 3 in Scrajet is better than Ramjet 086414, Dor Ben Meir 305702490
######################################################################################

import numpy as np
from scipy.optimize import fsolve

#Preparing the CFnj1->5 coefficient map:

Gamma_a = 1.4 #Air at Intake is standard air
Gamma_5 = 1.286 #Common value for exuast gases.
Gamma_6 = 1.286
R_a = 287 #[J/kgK] Air at Intake is standard air
R_5 = 300 #[J/kgK] Common value for exuast gases.
Ma = 3.7
M5 = 1
eta_N = [1,1.02,1.04,1.06];
P05P0a =  np.arange( 0.1, 1 + 0.1, 0.1)             
S = np.arange( 1.2, 2.7 + 0.1, 0.1)                  
AaA5 =  np.arange( 0.01, 1.5 + 0.01, 0.01)       
A6A5 =  np.arange( 1, 2 + 0.05, 0.05)
A6A5_2 = [1,1.2,1.4,1.6,1.8,2]           
Lambda_P05P0a =  np.arange( 0, 2 + 0.05, 0.05)                                
                                    
M6 = np.zeros((1,len(A6A5)))
CFnja5_A5A3_P = np.zeros((len(P05P0a),len(AaA5)))                    
CFnja5_A5A3_S = np.zeros((len(S),len(AaA5))) 
Lambda_1 = np.zeros((len(eta_N),len(A6A5)))
Lambda_2 = np.zeros((len(P05P0a),len(Lambda_P05P0a)))
DeltaCFnj56_A5A3 = np.zeros((len(A6A5_2),len(Lambda_P05P0a)))

def phi(M,gamma,R):
    phi = np.sqrt(gamma/R) * (M*np.sqrt(1+((gamma-1)/2)*(M**2)))
    return phi
    
def PHI(M,gamma,R):
    PHI = phi(M,gamma,R) * ((1+((gamma-1)/2)*(M**2))**(-(gamma/(gamma-1))))
    return PHI
    
def CFnja5_A5A3_p(Gamma_a,Gamma_5,Ma,AaA5,P05P0a):  
    CFnja5_A5A3 = (2/(Gamma_a*(Ma**2)))*(P05P0a*((1+((Gamma_a-1)/2)*(Ma**2))**(Gamma_a/(Gamma_a-1)))*((2/(Gamma_5+1))**(Gamma_5/(Gamma_5-1)))*(1+Gamma_5)-1)-2*AaA5
    return CFnja5_A5A3
    
def CFnja5_A5A3_s(Gamma_a,Gamma_5,Ma,AaA5,S):
    CFnja5_A5A3 = (2/(Gamma_a*(Ma**2)))*(S*AaA5*(phi_a/phi_5)*((1+((Gamma_a-1)/2)*(Ma**2))**(Gamma_a/(Gamma_a-1)))*((2/(Gamma_5+1))**(Gamma_5/(Gamma_5-1)))*(1+Gamma_5)-1)-2*AaA5
    return CFnja5_A5A3
    
def LHS(M2,gamma,R):
    LHS = (1+gamma*(M2**2))/(phi(M2,gamma,R))
    return LHS
           
def RHS(m1,a1a2,gamma,R):
    RHS = ((gamma*(m1**2)+(a1a2)))/(phi(m1,gamma,R))
    return RHS

def Lambda(a6a5,C2,C3,eN,gamma,M6):
    lambdaf = C2*(a6a5*((1+eN*gamma*(M6**2))/((1+((gamma-1)/2)*(M6**2))**(gamma/(gamma-1))))-C3)
    return lambdaf
    
def DeltaCFnj56_A5A3f(C1,lambdaf,a6a5):
    DeltaCFnj56 = C1*(lambdaf - a6a5 + 1)
    return DeltaCFnj56
  
phi_a =  PHI(Ma,Gamma_a,R_a)
phi_5 =  PHI(M5,Gamma_5,R_5)
C1 = 2/(Gamma_a*Ma**2)
C2 = (1+((Gamma_a-1)/2)*(Ma**2))**(Gamma_a/(Gamma_a-1))
C3 = 1.2533


#calculation of M6 for every A6A5:
i = -1
for a6a5 in A6A5:
    i = i + 1
    def M6_solve(M6):
        solve = phi_5 * (a6a5**-1)   -  PHI(M6,Gamma_5,R_5)
        return solve
    M6[:,i] = fsolve(M6_solve, 1.001) 



#calculation of CFnjA-5/A5_A3 as a funcion of P05P0a:
i = -1
j = -1 

for p in P05P0a:
    if(j != -1):
        j = -1
    i = i + 1
    for a in AaA5:
        j = j + 1
        CFnja5_A5A3_P[i,j] = CFnja5_A5A3_p(Gamma_a,Gamma_5,Ma,a,p) 
        
np.savetxt('P05P0a.txt', (P05P0a))  
np.savetxt('CFnja5_A5A3_P.txt', (CFnja5_A5A3_P))
  
#calculation of CFnjA-5/A5_A3 as a funcion of S:
i = -1
j = -1 

for s in S:
    if(j != -1):
        j = -1
    i = i + 1
    for a in AaA5:
        j = j + 1
        CFnja5_A5A3_S[i,j] = CFnja5_A5A3_s(Gamma_a,Gamma_5,Ma,a,s)
        
np.savetxt('AaA5.txt', (AaA5))
np.savetxt('CFnja5_A5A3_S.txt', (CFnja5_A5A3_S))

        
#calculation of Lambda as afuncion of A6A5, for different eta_N:
i = -1
j = -1 
for eN in eta_N:
    if(j != -1):
        j = -1
    i = i + 1
    for a6a5 in A6A5:
        j = j + 1
        Lambda_1[i,j] = Lambda(a6a5,C2,C3,eN,Gamma_6,M6[:,j])
        
np.savetxt('Lambda_1.txt', (Lambda_1))
np.savetxt('A6A5.txt', (A6A5))
np.savetxt('eta_N.txt', (eta_N))
        
#calculation of Lambda as afuncion of Lambda*P05P0a, for different P05P0a:
i = -1
j = -1 
for p in P05P0a:
    if(j != -1):
        j = -1
    i = i + 1
    for Lp in Lambda_P05P0a:
        j = j + 1
        Lambda_2[i,j] = Lp / p
        
np.savetxt('Lambda_2.txt', (Lambda_2))



#calculation of DeltaCFnj5-6 as a funcion of Lambda*P05P0a, for different A6A5:
i = -1
j = -1 
for a6a5 in A6A5_2:
    if(j != -1):
        j = -1
    i = i + 1
    for Lp in Lambda_P05P0a:
        j = j + 1
        DeltaCFnj56_A5A3[i,j] = DeltaCFnj56_A5A3f(C1,Lp,a6a5)
        
np.savetxt('Lambda_P05P0a.txt', (Lambda_P05P0a))
np.savetxt('DeltaCFnj56_A5A3.txt', (DeltaCFnj56_A5A3))  
np.savetxt('A6A5_2.txt', (A6A5_2))      
 


