clc
clear 
close all

CFnja5_A5A3_1 = importdata('CFnja5_A5A3_P.txt');
CFnja5_A5A3_2 = importdata('CFnja5_A5A3_S.txt');
P05P0a = importdata('P05P0a.txt');
AaA5 = importdata('AaA5.txt');
S = importdata('S.txt');
eta_N = importdata('eta_N.txt');
A6A5 = importdata('A6A5.txt');
Lambda_1 = importdata('Lambda_1.txt');

Lambda_2 = importdata('Lambda_2.txt');
Lambda_P05P0a = importdata('Lambda_P05P0a.txt');

A6A5_2 = importdata('A6A5_2.txt');
DeltaCFnj56_A5A3 = importdata('DeltaCFnj56_A5A3.txt');



lgd1 = cell(1,(length(eta_N)));
lgd2 = cell(1,(length(P05P0a)));
lgd3 = cell(1,(length(A6A5_2)));


figure(1);
subplot(3,2,1) 
for i=1:1:length(eta_N)
  
    plot(A6A5(:),Lambda_1(i,:));
    lgd1{i} = sprintf(' \\eta =%.2f',eta_N(i));
    hold on;
end
xlabel('A_6/A_5');
ylabel('\lambda');
xlim([1.1,2]);
ylim([10,30]);
grid on;
legend(lgd1);
set(gca, 'xdir', 'reverse');
set(gca, 'YAxisLocation', 'Right');

hold off;

% figure(2); 
subplot(3,2,2) 
for i=1:1:length(P05P0a)
    lgd2{i} = sprintf('P05/P0a =%.1f',P05P0a(i));    
    plot(Lambda_P05P0a(:),Lambda_2(i,:));
    hold on;
end
xlabel('\lambda*P_0_5/P_0_a');
ylabel('\lambda');
ylim([0,7]);
grid on;
legend(lgd2,'Location', 'Best');

% figure(3);
subplot(3,2,3) 
for i=1:1:length(A6A5_2)
    lgd3{(length(A6A5_2)) - i + 1} = sprintf('A6/A5 =%.1f',A6A5_2(i));
    plot(Lambda_P05P0a(:),DeltaCFnj56_A5A3(i,:));
    hold on;
end
xlabel('\lambda*P_0_5/P_0_a');
ylabel('\DeltaC_F_n_j_5_6/(A_5/A_3)');
ylim([-0.2,0.4]);
grid on;
legend(lgd3,'Location', 'Best');
set(gca, 'ydir', 'reverse');
set(gca, 'XAxisLocation', 'top');


figure(2)
for i=1:1:length(P05P0a)

    plot(AaA5(:),CFnja5_A5A3_1(i,:),'b');
    hold on;
end


for i=1:1:length(S)

    plot(AaA5(:),CFnja5_A5A3_2(i,:),'r');
    hold on;
end
title('C_F_n_j/(A_5/A_3) function of A_a/A_5, Ma= 3.7');
xlabel('A_a/A_5');
ylabel('C_F_n_j_a_-_5/(A_5/A_3)');
ylim([0,5]);
grid on;



