
clear all
close all
clc

%ploting the results:
Ma = importdata('Ma.txt');
F_mdota = importdata('F_mdota.txt');
f = importdata('f.txt');
TSFC = importdata('TSFC.txt');
figure();
plot(Ma(:), f(:));
title('Fuel-Air ratio Vs. Mach');
grid on;
xlabel('Mach');
ylabel('f');

figure();
plot(Ma(:), F_mdota(:));
title('F/(mdot_a) (Specific Thrust) Vs. Mach');
grid on;
xlabel('Mach');
ylabel('F/(mdot_a) [kgf/(Kg*s)]');

figure();
plot(Ma(:), TSFC(:));
title('TSFC Vs. Mach');
grid on;
xlabel('Mach');
ylabel('TSFC [Kg/(hr*kgf)]');