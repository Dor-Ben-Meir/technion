import numpy as np
import astropy.units as u

Ta = 250 #K
Pa = 15
T04 = 2000
qR = 40 * (10**6)
R = 287
Cp = 1000
gamma = 1.4
M = 4

P0a = Pa*((1+((gamma-1)/2)*(M**2))**(gamma/(gamma-1)))
T0a = Ta*(1+((gamma-1)/2)*(M**2))
P02 = P0a
P03  = P0a
P04 = P0a
P05 = P0a
P06 = P0a
P6 = Pa
T02 = T0a
T03 = T0a
T05 = T04
T06 = T04 
T3 = T0a
T6 = T04 * ((P6 / P06) ** ((gamma - 1)/(gamma)))
P2 = P02
P3 = P03
T3 = T03
P4 = P04

M5 = 1
P5 = P05*((1+((gamma-1)/2)*(M5**2))**(-(gamma/(gamma-1))))
T5 = T05*((1+((gamma-1)/2)*(M5**2)) ** (-1))

U_a = np.sqrt(gamma * R * Ta) * M
U_e = np.sqrt(T04 / T0a) * U_a

f = round(((T04 / T0a) - 1) / ((qR / (Cp*T0a)) - (T04 / T0a)),3)
F_mdota = round((((1 + f) * U_e - U_a) / 9.8), 3) 
TSFC = round((f * 3600) / F_mdota, 3) 

ans1 = dict(P0a = P0a*(10**3)*u.Pa,P02 = P0a*(10**3)*u.Pa,P03  = P0a*(10**3)*u.Pa, P04 = P0a*(10**3)*u.Pa,P05 = P0a*(10**3)*u.Pa,P06 = P0a*(10**3)*u.Pa) 
ans2 = dict(Pa = 15 *(10**3)*u.Pa, P2 = P02*(10**3)*u.Pa,P3 = P02*(10**3)*u.Pa,P4 = P02*(10**3)*u.Pa,P5 = P5*(10**3)*u.Pa,P6 = Pa*(10**3)*u.Pa)
ans3 = dict(T0a = T0a * u.K,T02 = T0a * u.K,T03 = T0a * u.K,T04 = 2000* u.K,T05 = T04* u.K,T06 = T04* u.K)
ans4 = dict(Ta = 250 * u.K,T2 = T0a * u.K, T3 = T0a* u.K, T4 = 2000 * u.K, T5 = T5 * u.K, T6 = T6* u.K) 
ans5 = dict( U_a = U_a*(u.m / u.s), U_e = U_e*(u.m / u.s) )