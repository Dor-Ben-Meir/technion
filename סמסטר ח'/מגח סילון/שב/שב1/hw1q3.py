import numpy as np


gamma = 1.4
R = 287

rn = 0.96
rc = 0.95
rd = 0.9
T04 = 2220
Ta = 220

M_arry = []
Mr  = np.arange(0,10 + 0.01,0.01)

for M in Mr:

    m = ((1+((gamma-1)/2)*(M**2))) * ((rd*rc*rn)**((gamma-1)/gamma))
    Fmadot = np.sqrt((2*gamma * R * T04*(m-1))/((gamma-1)*m)) - M * np.sqrt(gamma * R * Ta)

    if(Fmadot > 0):
        M_arry.append(M)
    
M_min = np.min(M_arry)
M_max = np.max(M_arry)

