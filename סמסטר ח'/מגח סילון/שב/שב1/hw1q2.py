import numpy as np

#known parameters:
Ma=[1,2,3,4]
Ta = 220 #[k]
Pa = 12000 #[pa]
P6 = Pa
qR = 42*10**6
T04 = 2220; #[k]
gama_a2 = 1.4
gama_36 = 1.3
Cp_a = 1000 #[j/KgK]
Cp_e = 1200 #j/KgK]
R_a2 = Cp_a*((gama_a2-1)/gama_a2) #[j/KgK]
R_36 = Cp_e*((gama_36-1)/gama_36) #[j/KgK]
rc1 = 0.97
rc2 = 0.9
eta_b = 0.98
eta_n = 0.95

rd = np.zeros(4)
rn = np.zeros(4)
f = np.zeros(4)
T6s = np.zeros(4)
F_mdota = np.zeros(4)
TSFC = np.zeros(4)
T_zero = []
#energy and stagnation pressure losses:
for i in range(4):
    rd[i] = 1-0.1*((Ma[i]-1)**(1.5))
    P0a = Pa*((1+((gama_a2-1)/2)*(Ma[i]**2))**(gama_a2/(gama_a2-1)))
    P04 = rc2*rc1*rd[i]*P0a
    rn[i] = (P04/P6)*((1-eta_n*(1-((P6/P04)**((gama_36-1)/gama_36))))**(gama_36/(gama_36-1)))
    #print(P04)
#Calculating the Fuel-Air ratio for every mach:
    T0a = Ta*(1+((gama_a2-1)/2)*(Ma[i]**2))
    f[i] =  ((Cp_a*T0a) - (Cp_e*T04)) / ((Cp_e*T04) - (eta_b * qR))
##calculating the specific thrust:
    
    T6s = T04*(P6/P04)**((gama_36-1)/gama_36)
    T6 = T04 - eta_n *(T04 - T6s);
    U_a = np.sqrt(gama_a2 * R_a2 * Ta) * Ma[i]
    Me = np.sqrt(((T04/T6) - 1) * (2/(gama_36-1)))
    U_e = np.sqrt(gama_36 * R_36 * T6) * Me
    
    F_mdota[i] = ((1 + f[i]) * U_e - U_a) / 9.8 
    
    
#calculating the TSFC:

    TSFC[i] = (f[i]* 3600)/F_mdota[i]    
    T_zero.append(T0a)
    
np.savetxt('Ma.txt', (Ma))        
np.savetxt('f.txt', (f))
np.savetxt('F_mdota.txt', (F_mdota))
np.savetxt('TSFC.txt', (TSFC))    
    
    