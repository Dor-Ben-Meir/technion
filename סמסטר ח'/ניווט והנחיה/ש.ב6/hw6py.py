
import numpy as np
from numpy import linalg as LA

#C
q1 = True
q2 = True
q3 = True

pr = np.array([[22228206.42], [24096139.11], [21729070.63],[21259581.09]])
SVs = [(7766188.44, -21960535.34, 12522838.56), (-25922679.66, -6629461.28, 31864.37), (-5743774.02, -25828319.92, 1692757.72), (-2786005.69, -15900725.80,21302003.49)]
Xs = [np.zeros((4, 1))]
p = [-2430829.17, -4702341.01, 3546604.39]

def fi(Xs, SVs):
    f = np.zeros((4,1))
    for i in range(4):
        f1 = (Xs[-1][0][0] - SVs[i][0]) ** 2
        f2 = (Xs[-1][1][0] - SVs[i][1]) ** 2 
        f3 = (Xs[-1][2][0] - SVs[i][2]) ** 2
        f[i] = np.sqrt(f1 + f2 + f3)
       
    return f

def Hfi(Xs, SVs, f):
    H = np.zeros((4,4))
    for i in range(4):
        H1 = (Xs[-1][0][0] - SVs[i][0]) / f[i][0]
        H2 = (Xs[-1][1][0] - SVs[i][1]) / f[i][0]
        H3 = (Xs[-1][2][0] - SVs[i][2]) / f[i][0]
        H[i] = [H1, H2, H3, 1]
    return H
              
    
def loophool(Xs):
    L1 = abs(Xs[-1][0][0] - Xs[-2][0][0])
    L2 = abs(Xs[-1][1][0] - Xs[-2][1][0])
    L3 = abs(Xs[-1][2][0] - Xs[-2][2][0])
    L_dis = L1 + L2 + L3
    return L_dis

def error(Xs,p):
    Error =  np.zeros((6,4))
    for i, x in enumerate(Xs):
        x_e = p[0] - x[0][0]
        y_e = p[1] - x[1][0]
        z_e = p[2] - x[2][0]
        total_e = LA.norm([x_e,y_e,z_e])        
        #Error[i] = [round(x_e , 2), round(y_e, 2),round(z_e, 2), round(total_e, 2)]
        Error[i] = [x_e , y_e, z_e,  total_e ]
    return Error

if(q1):
    f = fi(Xs, SVs)
    H = Hfi(Xs, SVs, f)
    Xs.append(Xs[-1] + np.linalg.inv((H.T).dot(H)).dot(H.T).dot(pr - f))
   
    while loophool(Xs) > 0.01:
        f = fi(Xs, SVs)
        H = Hfi(Xs, SVs, f)
        Xs.append(Xs[-1] + np.linalg.inv((H.T).dot(H)).dot(H.T).dot(pr - f))
   
    
        
    p = [-2430829.17, -4702341.01, 3546604.39]    
    Error = error(Xs,p)
    x1 = Xs[-1][3][0]


if(q2):
    Xs = [np.zeros((4, 1))]
    pr = np.array([[22228209.42], [24096142.11], [21729073.63], [21259584.09]])
    f = fi(Xs, SVs)
    H = Hfi(Xs, SVs, f)
    Xs.append(Xs[-1] + np.linalg.inv((H.T).dot(H)).dot(H.T).dot(pr - f))
        
    while loophool(Xs) > 0.01:
        f = fi(Xs, SVs)
        H = Hfi(Xs, SVs, f)
        Xs.append(Xs[-1] + np.linalg.inv((H.T).dot(H)).dot(H.T).dot(pr - f))
        
    x2 = Xs[-1][3][0]   
    x_dis = abs(x1 - x2) / (3 * (10**8))


if(q3):
    Xs = [np.zeros((4, 1))]
    Xs[-1][2][0] = p[2]
    f = fi(Xs, SVs)
    H = Hfi(Xs, SVs, f)
    Xs.append(Xs[-1] + np.linalg.inv((H.T).dot(H)).dot(H.T).dot(pr - f))
   
    cont = 1
    while cont < 4:
        Xs[-1][2][0] = p[2]
        f = fi(Xs, SVs)
        H = Hfi(Xs, SVs, f)
        Xs.append(Xs[-1] + np.linalg.inv((H.T).dot(H)).dot(H.T).dot(pr - f))
        cont = cont + 1

    Xs[-1][2][0] = p[2]   
    Error3 = error(Xs,p)

       
        
    
    
