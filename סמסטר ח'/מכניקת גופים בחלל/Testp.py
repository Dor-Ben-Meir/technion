





import numpy as np


RE = 6378 # [Km]
mu = 3.986 * (10**5)  #[Km^3 / S^2]
deg_to_rad = 0.0174533
j2 = 0.0010826
We = (2 * np.pi) / 86164

def a_two(mu,T):
    a = (mu * ((T / (2 * np.pi))**2))**(1/3)
    return a

def T_n(a,i,T):
    Tn = T * (1 - (3/2)*j2 * ((RE/a)**2) * (3 - 4 * ((np.sin(i))**2)))
    return Tn
    
   
    
T = 12 * 60 * 60
i = np.deg2rad(55)
a = a_two(mu,T)
Tn = T_n(a,i,T)
Delta_n = np.pi - We*(T/2)
