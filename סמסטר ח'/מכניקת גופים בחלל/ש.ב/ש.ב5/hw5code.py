######################################################################################
#Python code for homework 5 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################


import numpy as np



RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533

def n_one(mu,a):
    n = np.sqrt(mu/ (a**3))
    return n

def T_one(a,mu):
    T = 2 * np.pi * np.sqrt((a**3) / mu)
    return T
    
a0 = 6878    
n = n_one(mu,a0)
T = T_one(a0,mu)

x0 = 0
y0 = 0
z0 = 0
xdot = 4.668
ydot = 5.535
zdot = 9.5655

t =  np.arange( 0, 5*T + 0.01, 0.01)
x = np.zeros(len(t))
y = np.zeros(len(t))
z = np.zeros(len(t))

x = -2*(ydot/n)*np.cos(n*t)+(xdot/n)*np.sin(n*t)+2*(ydot/n)
y = 2*(xdot/n)*np.cos(n*t)+4*(ydot/n)*np.sin(n*t)-2*(xdot/n)-3*ydot*t
z = (zdot/n)*np.sin(n*t)


np.savetxt('x.txt', (x))
np.savetxt('y.txt', (y))  
np.savetxt('z.txt', (z))
np.savetxt('t.txt', (t))     