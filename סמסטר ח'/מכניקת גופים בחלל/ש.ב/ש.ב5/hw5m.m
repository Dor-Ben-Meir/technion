clc
clear all

t = importdata('t.txt');
x = importdata('x.txt');
y = importdata('y.txt');
z = importdata('z.txt');

figure(1)
plot3(x(:), y(:), z(:) , 'Linewidth', 2);
grid on
xlabel('[m]','FontSize',10)
ylabel('[m]','FontSize',10)
zlabel('[m]','FontSize',10)
title('Relative path')

figure(2)
plot (t,x)
grid on
xlabel ('time [sec]')
ylabel ('x(t) [m]')
title('X vs. Time')

figure(3)
plot (t,y)
grid on
xlabel ('time [sec]')
ylabel ('y(t) [m]')
title('Y vs. Time')

figure(4)
plot (t,z)
grid on
xlabel ('time [sec]')
ylabel ('z(t) [m]')
title('Z vs. Time')