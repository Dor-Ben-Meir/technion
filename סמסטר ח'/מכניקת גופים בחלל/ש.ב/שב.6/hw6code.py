import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from scipy.optimize import fsolve
from scipy.misc import derivative

RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533
e = 0.2



def Mt_solve(theta):
        LHS = ((1 + e * np.cos(theta)) * np.cos(theta) + e * (np.sin(theta) **2)) / (1 + e * np.cos(theta))
        RHS =  (e * (np.sin(theta) **2)) / (1 + 2 * e * np.cos(theta) + e**2)
        solve = LHS + RHS
        return solve

Theta1 = np.rad2deg((fsolve(Mt_solve, 0.001)))
Theta2 =  360 - Theta1

ans1 = dict(Theta1 = Theta1 * u.deg, Theta2 = Theta2 * u.deg )