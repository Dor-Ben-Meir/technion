import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from astropy.table import QTable
import pandas as pd

RE = 6378 # [Km]
mu = 3.986 * (10**5) * ((u.km**3) / (u.s**2)) #[Km^3 / S^2]
deg_to_rad = 0.0174533

def epsilon_one(V,mu,r):
    epsi = ((V**2) / 2) - (mu / r)
    return epsi

def epsilon_two(mu,a):
    epsi = -mu / (2*a)
    return epsi   

def theta_one(h,r,v):
    theta = np.arccos( h / (r * v))
    return theta
    
def Phi(e,Ni):
    phi = np.arctan((e * np.sin(Ni)) / (1 + e * np.cos(Ni)))
    return phi
    
def h_one(V,r,theta):
    h = r * V * math.cos(theta)
    return h
    
def e_one(epsilon,h,mu):
    e = math.sqrt(1 + (2 * epsilon * (h**2) / (mu**2)))
    return e

def e_two(r_v,v_v,h_v,mu,r):
    e = (np.cross(v_v, h_v) / mu) - (r_v / r)
    return e
    
def a_one(mu, epsilon):
    a = -mu / (2 * epsilon)
    return a
    
def a_two(mu,T):
    a = (mu * ((T / (2 * np.pi))**2))**(1/3)
    return a
    
def Pr_one(a,e):
    Pr = a * (1 - e)
    return Pr

def ra_one(a,e):
    ra = a * (1 + e)
    return ra
    
def Ni_one(a,r,e):
    Ni = np.arccos((a * ( (1 - (e**2)) / r ) - 1) / e)
    return Ni

def E_one(a,e,r,Ni):
    E = np.arccos((a * e + r * np.cos(Ni)) / a)
    return E

def M_one(E,e):
    M = E - e * np.sin(E)
    return M

def n_one(mu,a):
    n = np.sqrt(mu/ (a**3))
    return n

def T_one(a,mu):
    T = 2 * np.pi * np.sqrt((a**3) / mu)
    return T

def V(mu,r,a):
    V = np.sqrt(2*mu * ((1 / r) - (1 / (2*a))))
    return V

def Delta_V(V1,V2,phi):
    Delta_V = np.sqrt( (V1**2) + (V2**2) - (2*V1*V2 * np.cos(phi)))
    return Delta_V
    
def i(h_v, h_size):
    i = np.arccos(h_v[2]/h_size)
    return i

def orbit(X,t):
    

    
	X1 = X[:3]
	X2 = X[3:6]

	r = LA.norm(X1)
    
	dX1 = X2
	dX2 = (-mu / (r**3)) * X1

	derivs = sci.concatenate((dX1,dX2))
	return derivs

  
# Q1 part a:    
T1 = 3 * 60 * 60 * u.s 
r_AN_x = 7650 * u.km
r_AN_y = 7650 * u.km
r_AN_z = 0 * u.km
r_AN = [r_AN_x,r_AN_y, r_AN_z] * u.km
r_AN_syze = LA.norm(r_AN) * u.km
V_AN_x = 2.4 * (u.km / u.s)
V_AN_y = -1.7 * (u.km / u.s)
a1 = a_two(mu,T1) 
V_AN_syze = V(mu,r_AN_syze,a1) 
V_AN_z = np.sqrt(V_AN_syze**2 - (((V_AN_x)**2) + ((V_AN_y)**2))) 
V_AN = [V_AN_x,V_AN_y,V_AN_z] * (u.km / u.s)

answer1 = dict(r_AN = r_AN ,V_AN = V_AN)


#part b:
h_v = np.cross(r_AN, V_AN) * (u.km * (u.km / u.s))
h = LA.norm(h_v) * (u.km * (u.km / u.s))
epsi = epsilon_two(mu,a1) 
dot_rv = np.dot(r_AN,V_AN) * (u.km * (u.km / u.s))
e_v = (1 / mu) * (((V_AN_syze**2) - (mu / r_AN_syze)) * r_AN) - (1 / mu) *(dot_rv * V_AN)
e_size = LA.norm(e_v)
e_vector = e_v / e_size
e1 = e_one(epsi,h,mu) 
i = i(h_v, h) 
n = np.cross([0,0,1],h_v)
n_size = LA.norm(n)
dot_ne = np.dot(n,e_v)
w =  np.arccos(dot_ne / (n_size * e1 )) 
if(e_v[2] < 0 ):
    w = (2 * np.pi - w) * u.rad
Omega = np.arccos(n[0]/n_size) * u.rad
rp1 = Pr_one(a1,e1)
rp1_v = rp1 * e_vector
ra1 = ra_one(a1,e1)
answer2 = dict(a = a1 ,e = e1, i = i, rp = rp1, w = w, Omega = Omega)




#part c:
t = np.arange( 0,100000,10)
X0  = sci.array([r_AN,V_AN])
X0 = X0.flatten()
sol = odeint(orbit,X0,t)

np.savetxt('sol.txt', (sol))
np.savetxt('t.txt', (t))  
np.savetxt('rp.txt', (rp1_v))
#######################################################################

# Q2 part a:
a3 = a1
r3 = a3
rp2 = rp1
ra2 = r3
a2 = (ra2 + rp2) / 2
Vp1 = V(mu,rp1,a1)
Vp2 = V(mu,rp2,a2)
Va2 = V(mu,ra2,a2)
V3 = V(mu,r3,a3)

Delta_V1 = np.abs(Vp2 - Vp1) + np.abs(V3 - Va2)
answer3 = dict(Vp1 = Vp1 ,Vp2 = Vp2, Va2 = Va2, V3 = V3, Delta_V1 = Delta_V1)

#part b:
ra2 = ra1
rp2 = r3
a2 = (ra2 + rp2) / 2
Va1 = V(mu,ra1,a1)
Va2 = V(mu,ra2,a2)
Vp2 = V(mu,rp2,a2)

Delta_V2 = np.abs(Va2 - Va1) + np.abs(V3 - Vp2)
answer4 = dict(Va1 = Va1 ,Va2 = Va2, Vp2 = Vp2, V3 = V3, Delta_V2 = Delta_V2)

#part c:
Ni1 = Ni_one(a1,r3,e1)
Phi1 = Phi(e1,Ni1)
V1 = V(mu,r3,a1)

Delta_V3 = Delta_V(V1,V3,Phi1)
answer5 = dict(Ni = Ni1 ,Phi = Phi1, V1 = V1, V3 = V3, Delta_V3 = Delta_V3)