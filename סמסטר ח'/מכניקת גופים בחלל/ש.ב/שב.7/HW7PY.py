######################################################################################
#Python code for homework 7 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import astropy.units as u

#Parameters:
miu=398600
g0=9.81 
Re=6378

T=5500
d=0.7
L=2 
m=400 
Isp=200 
mp=20 
Cd1=3 
Cd2=2.3 
i = np.deg2rad(60)

omegaE = 2 * np.pi/24/60/60

def F_fun(a,Vsat):
    F = (1-omegaE*a/Vsat*np.cos(i))**2
    return F
    
S1 = np.pi*d**2/4
S2=d*L
SCd=2/np.pi*(S1*Cd1+S2*Cd2)

a=(miu*(T/2/np.pi)**2)**(1/3)
Vsat=np.sqrt(miu/a)
h=a-Re

rho_b=9.5* (10**-12)
H=53.298
hb=350

F_Q1 = F_fun(a,Vsat)
Vrel=Vsat*np.sqrt(F_Q1)*10**3
rho=rho_b*np.exp(-(h-hb)/H)
ta=2*Isp*g0*mp/(rho*Vrel**2*SCd)

ans1 = dict(SCd = SCd * (u.m **2), Vsat = Vsat * (u.km / u.s), Vrel = Vrel * (u.m / u.s), t = ((ta / 86400)) * u.d)

#B:  
Kd=SCd/(m-mp) *10**-6
hb = np.arange( 350, 150, -50)                     #350:-50:200;
H = np.array([53.298, 53.628, 45.546, 37.105])
rho_b = np.array([9.5 * (10**-12), 2.41 * (10**-11), 7.24* (10**-11), 2.78 * (10**-10)]) * (10**9)
delta_t = np.zeros(len(hb))

for j in range(len(hb)):
    a_bar = Re+(h+hb[j])/2     
    Vsat = np.sqrt(miu/a_bar)     
    F_Q2 = F_fun(a_bar,Vsat)     
    delta_t[j]=H[j]/( rho_b[j]*Kd*F_Q2*np.sqrt(miu*a_bar) )*( np.exp((h-hb[j])/H[j]) -1 )    
    h=hb[j]
    
tb = np.sum(delta_t) /60/60/24

ans2 = dict(hb = hb * u.km, H = H * u.km, rho_b = rho_b * (u.kg / (u.m**3)), delta_t = delta_t * u.s, tb = tb * u.d)