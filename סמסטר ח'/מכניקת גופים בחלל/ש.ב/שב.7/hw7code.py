#######################################################################################
##Python code for homework 7 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
#######################################################################################
#
#
#import numpy as np
#from scipy.integrate import odeint
#import scipy as sci
#
#
#
#RE = 6378 # [Km]
#mu = 3.986 * (10**5) #[Km^3 / S^2]
#deg_to_rad = 0.0174533
#
#def n_one(mu,a):
#    n = np.sqrt(mu/ (a**3))
#    return n
#
#def T_one(a,mu):
#    T = 2 * np.pi * np.sqrt((a**3) / mu)
#    return T
#    
#a0 = 6878    
#n = n_one(mu,a0)
#T = T_one(a0,mu)
#
#x0 = 0
#y0 = 0
#z0 = 0
#xdot = 4.668 * (10**-3)
#ydot =  7.61 + 5.535 * (10**-3)
#zdot = 9.5655 * (10**-3)
#
#t =  np.arange( 0, 5*T + 0.01, 0.01)
#
#
#
#omega = n
#
#
#r = a0
#
## Clculate J2 equtions:
#def orbit_two(X,t):
#       
#	X1 = X[0:1]
#	X2 = X[1:2]
#	X3 = X[2:3]    
#	X4 = X[3:4]
#	X5 = X[4:5]
#	X6 = X[5:6]    
#    
#	#r = np.sqrt(X1**2 + X2**2 + X3**2)
#    
#	dX1 = X4
#	dX2 = X5
#	dX3 = X6
#	dX4 = 2 * omega * X5 + (omega**2) * X1 - mu * ( (r + X1) / ((((r + X1)**2) + X2**2 + X3**2)**(3/2))) + (mu / (r**2))
#	dX5 = -2 * omega * X4 + (omega**2) * X2 - mu * ( (X2) / ((((r + X1)**2) + X2**2 + X3**2)**(3/2))) 
#	dX6 = - mu * ( (X3) / ((((r + X1)**2) + X2**2 + X3**2)**(3/2))) 
#	derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6))
#	return derivs 
#    
#    
#t =  np.arange( 0, 5*T + 0.01, 0.01)
#X0  = sci.array([[x0,y0,z0],[xdot,ydot,zdot]])
#X0 = X0.flatten()
#sol = odeint(orbit_two,X0,t)   
#
#x = sol[:,0]
#y = sol[:,1]
#z = sol[:,2]
#
#np.savetxt('x.txt', (x))
#np.savetxt('y.txt', (y))  
#np.savetxt('z.txt', (z))
#np.savetxt('t.txt', (t)) 

######################################################################################
#Python code for homework 7 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################



import numpy as np
import astropy.units as u
from numpy import linalg as LA
from scipy.integrate import odeint


RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533

def n_one(mu,a):
    n = np.sqrt(mu/ (a**3))
    return n

def T_one(a,mu):
    T = 2 * np.pi * np.sqrt((a**3) / mu)
    return T
    
a0 = 6878    
n = n_one(mu,a0)
T = T_one(a0,mu)

x0 = 0
y0 = 0
z0 = 0
xdot = 4.668 * (10**-3)
ydot = 5.535 * (10**-3)
zdot = 9.5655 * (10**-3)

t =  np.arange( 0, 5*T + 0.01, 0.01)
x = np.zeros(len(t))
y = np.zeros(len(t))
z = np.zeros(len(t))

x = -2*(ydot/n)*np.cos(n*t)+(xdot/n)*np.sin(n*t)+2*(ydot/n)
y = 2*(xdot/n)*np.cos(n*t)+4*(ydot/n)*np.sin(n*t)-2*(xdot/n)-3*ydot*t
z = (zdot/n)*np.sin(n*t)

alpha = np.sqrt(z0**2 + ((zdot / n)**2)) / a0
V = np.sqrt(mu/a0)
xMAX = np.sqrt( (xdot/n)**2 + (3*x0+2*ydot/n)**2 )+4*x0+2*ydot/n
xMIN = -np.sqrt( (xdot/n)**2 + (3*x0+2*ydot/n)**2 )+4*x0+2*ydot/n
a1 = a0 + (xMAX+xMIN)/2
e1 = (xMAX-xMIN)/2/a1

r = np.array([a0, 0, 0])
Vi = np.array([0, V, 0])
h = np.cross(r,Vi)

answer1 = dict(a1 = a1 * u.km, e1 = e1, alpha  = alpha * u.rad)

theta_star = np.arctan(xdot/2/ydot)
theta = 0
B = np.array([[2*(a0**2)*V/mu, 0, 0], [2/V*np.cos(theta), 1/V*np.sin(theta), 0], [0, 0, a0/LA.norm(h)*np.cos(theta_star)]])
#delta_vec = B * (np.array([ydot, xdot,zdot]))
delta_vec = B.dot((np.array([ydot, xdot,zdot])))
a1_GVE = delta_vec[0] + a0
e1_GVE = delta_vec[1]
i1_GVE = delta_vec[2]

answer2 = dict(a_GVE = a1_GVE * u.km, e_GVE = e1_GVE, i_GVE  = i1_GVE * u.rad)


V0 = np.array([0, V, 0]) + np.array([ydot, xdot, zdot])
#V0 =  np.array([4.67 * (10**-3), 7.618, 9.57* (10**-3) ])
#a_Exact = -mu/2/(LA.norm(V0)**2/2-mu/LA.norm(r))
a_Exact = -mu * (((LA.norm(V0)**2) - (2*mu)/LA.norm(r))**-1)
h = np.cross(r,V0)
En = -mu/2/a_Exact
e_Exact = np.sqrt(1+2*En*LA.norm(h)**2/(mu**2))
i_Exact = np.arccos(h[2] / LA.norm(h))

answer3 = dict(a_Exact = a_Exact * u.km, e_Exact = e_Exact, i_Exact  = i_Exact * u.rad)
