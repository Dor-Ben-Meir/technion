######################################################################################
#Python code for homework 10 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import astropy.units as u
from numpy import linalg as LA

T = 43200
i = np.deg2rad(55)
Omega = np.deg2rad(45)
r = 26610.24 
RA = np.deg2rad(91.6636)
delta = np.deg2rad(23.45)



def DCM(Omega_sat,omega_sat,i_sat):
    ev1 = np.cos(omega_sat)*np.cos(Omega_sat) - np.sin(omega_sat)*np.cos(i_sat)*np.sin(Omega_sat)
    ev2 = np.cos(omega_sat)*np.sin(Omega_sat)+np.sin(omega_sat)*np.cos(i_sat)*np.cos(Omega_sat)
    ev3 = np.sin(omega_sat)*np.sin(i_sat)
    e_vec = np.array([ev1, ev2, ev3]) * r       
    return e_vec



rs = np.array([np.cos(delta)*np.cos(RA),np.cos(delta)*np.sin(RA),np.sin(delta)])

omega = np.linspace(0, 2 * np.pi, num = 100)
rSat = DCM(Omega,omega,i) 

cm = 8.63e-14
cs = 3.96e-14


fm = np.zeros([3,100])
fs =  np.zeros([3,100])
fsm = np.zeros([3,100])

fmm = np.zeros([1,100])
fss = np.zeros([1,100])
ftot = np.zeros([1,100])

for e in range(len(rSat[0,:])):
    rr = (rSat[:,e])
    fm[:,e] = np.transpose(cm * (-rr + 3 * np.dot(rs,rr) * rs))     
    fs[:,e]  = cs * (-rr + 3 * np.dot(rs,rr) * rs)  
    fsm[:,e]  = fs[:,e] + fm[:,e]

    fmm[:,e] = LA.norm(fm[:,e])
    fss[:,e] = LA.norm(fs[:,e])
    ftot[:,e] = LA.norm(fsm[:,e])

omega_max = []
fq = ftot
ind =  np.where(fq  == fq.max())
omega_max.append(omega[ind[1]] * u.rad)
omega_max.append((omega[ind[1]] - np.pi) * u.rad)

an1 = dict(omega_max1 = omega_max[0], omega_max2 = omega_max[1])

np.savetxt('omega.txt', (omega))   
np.savetxt('fmm.txt', (fmm))
np.savetxt('ftot.txt', (ftot)) 
np.savetxt('fss.txt', (fss))