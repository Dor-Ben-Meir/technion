######################################################################################
#Python code for homework 4 in SPACE MECHANICS 085915, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import astropy.units as u


Rv=6100;
miu=324850
rp=Rv+300
ra=Rv+800
Vinf=5


#a
e = (1-rp/ra)/(1+rp/ra)
a = rp/(1-e)

Vp = np.sqrt(2*miu*(1/rp-1/2/a))
Va = np.sqrt(2*miu*(1/ra-1/2/a))
Vp0 = np.sqrt(Vinf**2+2*miu/rp)
Va0 = np.sqrt(Vinf**2+2*miu/ra)

Delta_Vp = Vp0-Vp
Delta_Va = Va0-Va

Delta_rp = rp*Vp0/Vinf
Delta_ra = ra*Va0/Vinf

ans1 = dict(Delta_Vp = Delta_Vp * (u.km / u.s), Delta_Va = Delta_Va * (u.km / u.s), Delta_rp = Delta_rp * u.km, Delta_ra = Delta_ra * u.km) 

#b
a_Delta_Vmin = 2*miu/Vinf**2
Delta_Vmin = np.sqrt(Vinf**2+2*miu/a_Delta_Vmin)-np.sqrt(miu/a_Delta_Vmin)

ans2 = dict(a_Delta_Vmin = a_Delta_Vmin * u.km, Delta_Vmin = Delta_Vmin * (u.km / u.s))