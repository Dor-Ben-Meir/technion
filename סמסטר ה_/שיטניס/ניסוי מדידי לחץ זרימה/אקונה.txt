clc
clear
close all




Pa = 101325; %[pa]
g = 9.81; %[kg*m/s^2]
am = deg2rad(45); %[deg]
rhom = 800; %[kg/m^3]������ ����
rhoa = 1.225; %[kg/m^3] ������ ����� 
dP = @(L) g*L*(1e-3)*sin(am)*(rhom-rhoa);
V  = @(Ps,P0) sqrt(2*(P0-Ps)/rhoa);


%% prt 1
a1 = [0 10  20 25 30]; %[deg]
probe1 = {'cone' 'circ' 'invcone'};

for j = 1:length(probe1)
    switch probe1{j}
        case 'cone'
            dl(:,j) =92 -  [61.5 61 62 63.5 66]; %[m]
        case 'circ'
            dl(:,j) = 92 - [61 62 64 66 70 ] ; % [m]
        case 'invcone'
            dl(:,j) = 92 - [61 61 61 62 63] ; % [m]
    end
end

P1 = Pa + dP(dl); 
for j = 1:length(probe1)
    err1(:,j) = abs(P1(1,j) - P1(:,j))/P1(1,j);
end


 plt_ln= {'o-' '*--' 'x-.'};
 figure
 for j = 1:length(probe1)
     plot(a1,err1(:,j),plt_ln{j},'LineWidth',2,'DisplayName',probe1{j})
     hold on;
 end
 grid on;
 xlabel('\alpha')
 ylabel('Error')
 title('Error Vs. \alpha')
 legend show
saveas(gcf,'Error1.jpg')




%% prt 2

D = [1 2 4 8];

dl = abs(92 - [97 94 93.5 93]) ;% [cm]
P2 = Pa + dP(dl);
err2 = abs(P2(4)-P2)/P2(4);

 figure
 plot(D,err2,'o-','LineWidth',2)
 grid on
xlabel('x/D [mm]')
 ylabel('Error')
 title('Error Vs. Hole Distance Normalized by Probe Diameter')
 saveas(gcf,'Error2.jpg')


%% prt 3

degs = [-25 -20:10:20 25]; %enetr measurments from pipes
theta = ones(7,7);
phi = ones(7,7);

for j =1:7
    theta(:,j) = theta(:,j).*degs';
    phi(j,:) = phi(j,:).*degs;
end
dl_p{1} = (91 - transpose([109	105	99	87	78	70	70;
108	105	98	88	79	71	67;
108	105	99	88	79	71	68;
107	106	101	93	83.5	72.5	71;
108	107	102	95	88	80	76;
109	108	105	100	93	85	83;
108	108	107	102	65	89	86]));






dl_p{2} = (89 - transpose([63	65	64	68	74	82	81.5;
65	65	77	71.5	76	82	85;
71	73	74	81	82	88	92;
82	82	83	87	93	95	100;
91	91.5	93	96	97	104	105;
101	101	101	103	105	107	108;
105	105	106	105	105	108	109]));

dl_p{3} = (90 - transpose([96	95	102	106	109	110	110;
92	95	99	104	108	110	109;
84	84	90	98	105	108	108;
73.5	76	83	93	97	106	106;
68	71	79	89	96	104	106;
66	69	76	86	95	102	105;
65	69	75.5	86	94	102	106]));






dl_p{4} = (91 - transpose([108	109	110	109	111	111	112;
110	110	110	109	110	109	110;
111	109	107	104	105	103	101;
106	106	102	99	92	96	92;
103	100	94	90	90	81	83;
98	93	87	82	75	75	74;
94	89	83	77	75	70	70]));
           
dl_p{5} = (90  - transpose([84.5	79	77	75	76	79	85;
81	78	74	71	72	74	79;
76	72	66	64	66	68	70;
71	69	64	62	61	66	67;
73	69	64	62	61.5	66	67;
76	72	67	65	65	68	71;
79	74	71	67	65	71	74]));

     
for j = 1:5
    P_p{j} = (Pa + dP(dl_p{j}));
end

 for j =1:5
   figure         
     contourf(degs,degs,P_p{j})
     xlabel('\Phi [deg]')
     ylabel('\Theta [deg]')
     xlim([-25 25])
     ylim([-25 25])
     h = colorbar;
     h.Label.String = 'P [Pa]'; 
     title(['Pressure as function of Phi and Theta, P',num2str(j)])
     saveas(gcf,['Pressure as function of Phi and Theta, P',num2str(j),'.jpg'])
 end


    

P_ave = (P_p{1}+P_p{2}+P_p{3}+P_p{4})/4;
C_pt  = (P_p{3}-P_p{1})./(P_p{5}-P_ave);
C_pp  = (P_p{2}-P_p{4})./(P_p{5}-P_ave);
C_p5  = (P_p{5}-P2(4))./(P_p{5}(4,4)-P2(4));
C_pa  = (P_ave-P2(4))./(P_p{5}(4,4)-P2(4));

P1ex = 101406;
P2ex = 101320;
P3ex = 101280;
P4ex = 101338;
P5ex = 101458;

P_avex = (P1ex+P2ex+P3ex+P4ex)/4;
C_ptex = (P3ex-P1ex)/(P5ex-P_avex);
C_ppex = (P2ex-P4ex)/(P5ex-P_avex);

Ft = scatteredInterpolant(C_pt(:),C_pp(:),phi(:));
Fp = scatteredInterpolant(C_pt(:),C_pp(:),theta(:));

tex = Ft(C_ptex,C_ppex);
pex = Fp(C_ptex,C_ppex);

C_p5ex = interp2(degs,degs,C_p5,tex,pex);
C_pavex = interp2(degs,degs,C_pa,tex,pex);

Pex = (P5ex/C_p5ex - P_avex/C_pavex)/(1/C_p5ex - 1/C_pavex);
P0ex = (P5ex-Pex)/C_p5ex + Pex; 

Vex = V(Pex,P0ex);

 figure
 contourf(C_pt,C_pp,phi)
 xlabel('C_p_\Theta')
 ylabel('C_p_\Phi')
 title('\Phi')
 h = colorbar;
 h.Label.String = '\Phi [deg]';
 saveas(gcf,'Phi.jpg')
 figure
 contourf(C_pt,C_pp,theta)
 xlabel('C_p_\Theta')
 ylabel('C_p_\Phi')
 title('\Theta')
 h = colorbar;
 h.Label.String = '\Theta [deg]';
 saveas(gcf,'Theta.jpg')
 figure
 contourf(degs,degs,C_p5)
 xlabel('\Phi [deg]')
 ylabel('\Theta [deg]')
 title('C_P_5')
 h = colorbar;
 h.Label.String = 'C_P_5';
 saveas(gcf,'C_P_5.jpg')
figure
 contourf(degs,degs,C_pa)
 xlabel('\Phi [deg]')
 ylabel('\Theta [deg]')
 title('C_P_A_v_e')
 h = colorbar;
 h.Label.String = 'C_P_A_v_e';
 saveas(gcf,'C_P_A_v_e.jpg')