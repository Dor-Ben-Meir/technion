clc
clear
close all

rho = 997;  %[kg/m^3]
g   = 9.81; %[kg*m/s^2]
% Estimated Errors
rho_err     = 2.5;         %[kg/m^3]
tank_h_err  = 1*1e-2;      %[m]
tank_t_err  = 0.5;         %[s]
mano_err    = 1e-3;        %[m]
rot_q_err   = 0.8*1e-3/60; %[m^3/s]
rot_h_err   = 1e-3;        %[m]
f_err       = 0.5;         %[Hz]
mtank_h_err = 1e-3;        %[m]
pump_v_err  = 1;           %[V]
Pump_err    = 0.01*1e5;    %[Pa]
calib_v_err = 0.05;        %[V]
meas_v_err  = 0.1;         %[V]

%% prt1

h0     = 10.7*1e-2;                               %[m] ���� ������ �� ��������
h_calb = [61.4 107 127.2 149.8 172.5 185]*1e-2; %[m] ������ ������
V_i1   = [0.8 1.2 1.5 1.7 1.8 2];               %[V] ������ ������
dh     = h_calb-h0;                             %[m] ���� ��� ����� ����� ������ �������
dP_i1  = rho*g*dh;                              %[Pa] ���� �����

dP_calib_err = sqrt((g*dh*rho_err).^2+(rho*g*mano_err)^2);%����� ����� ���� ���� ������ ���� ����
V_i1_err     = ones(length(V_i1),1)*calib_v_err;%����� ���� ������ ���� ���� ������ ���� ���� ����������

% Creating linear fit
a_dP = polyfit(V_i1,dP_i1,1);%����� ���� ���� ���� ���� ���������� ������ ��� ���� ����� ����

dP = @(V) a_dP(1)*V+a_dP(2);

[~,~,~,~,stats] = regress(dP_i1',[dP(V_i1') ones(length(dP_i1),1)]);
Rs = stats(1);
errorbar(V_i1,dP_i1,dP_calib_err,dP_calib_err,V_i1_err,V_i1_err,'.','Linewidth',1,'DisplayName','Measured'); hold on; grid on;
fplot(dP,[V_i1(1) V_i1(end)],'Linewidth',1,'DisplayName','Calibration')
xlabel('V [V]')
ylabel('\DeltaP [Pa]')
title('Differential Pressure Transducer Calibration')
legend('show','Location','northwest')

%% prt 2
d_i     = 24*1e-3;           %[m]
d_o     = 11.5*1e-3;         %[m}
A1      = pi*(d_i)^2/4;      %[m^2]
A2      = pi*(d_o)^2/4;      %[m^2]
beta    = d_o/d_i;
tank_A  = pi*(190*1e-3)^2/4; %[m]
tank_h  = 30*1e-2;           %[m] 
V_tank  = tank_A*tank_h;     %[m^3]
M = sqrt(1-(A2/A1)^2);
rho_rot = 8000;              %[kg/m^3]
mu      = 8.91*1e-4;         %[kgm/s]
v       = @(dp) sqrt(2*dp/rho)/sqrt(1-(A2/A1)^2);
Q       = @(dp) A2*v(dp);

%% Rotameter, Vortex and Venturi
t_1          = [19.23*3 29.75 20.38 15.78 12.91]; %[s] 
Venturi.Q_m  = V_tank./t_1;                 %[m^3/s]
Rotameter.Q  = [8 16 24 32 40]*1e-3/60;    %[m^3/s] 
Rotameter.h  = [1.9 4.9 7.4 9.7 11.8]*1e-2;   %[m] 
Venturi.io.v = [0.6 0.9 1.4 1.8 2.4];       %[V] 
Venturi.if.v = [0.4 0.4 0.5 0.6 0.7];       %[V] 
Vortex.f     = [12.7 23.5 34 43.6 53.5];  %[Hz] 
Venturi.l    = 58*1e-3;                     %[m]
Venturi.L    = 263.5*1e-3;                  %[m]

Rotameter.aQ = polyfit(Rotameter.Q,Venturi.Q_m,1);
Rotameter.ah = polyfit(Rotameter.h,Venturi.Q_m,1);
Vortex.a     = polyfit(Vortex.f,Venturi.Q_m,1);

Rotameter.Q_ma = @(q) Rotameter.aQ(1)*q+Rotameter.aQ(2);
Rotameter.Q_h  = @(h) Rotameter.ah(1)*h+Rotameter.ah(2);
Vortex.Q       = @(f) Vortex.a(1)*f+Vortex.a(2);

[~,~,~,~,stats] = regress(Venturi.Q_m',[Rotameter.Q_ma(Rotameter.Q') ones(length(Venturi.Q_m),1)]);
Rotameter.Rsa   = stats(1);
[~,~,~,~,stats] = regress(Venturi.Q_m',[Rotameter.Q_h(Rotameter.h') ones(length(Venturi.Q_m),1)]);
Rotameter.Rsh   = stats(1);
[~,~,~,~,stats] = regress(Venturi.Q_m',[Vortex.Q(Vortex.f') ones(length(Venturi.Q_m),1)]);
Vortex.Rs       = stats(1);

Venturi.Q_m_err = sqrt((tank_A*tank_h_err./t_1).^2+(-tank_A*tank_h*tank_t_err./t_1.^2).^2);
Rotameter.Q_err = ones(length(Rotameter.Q),1)*rot_q_err;
Rotameter.h_err = ones(length(Rotameter.h),1)*rot_h_err;
Vortex.err      = ones(length(Vortex.f),1)*f_err;

figure
errorbar(Rotameter.Q,Venturi.Q_m,Venturi.Q_m_err,Venturi.Q_m_err,Rotameter.Q_err,Rotameter.Q_err,'o','LineWidth',2,'DisplayName','Measured'); hold on; grid on
fplot(Rotameter.Q_ma,[Rotameter.Q(1) Rotameter.Q(end)],'LineWidth',2,'DisplayName','Calibration')
xlabel('Q_r_o_t [m^3/s]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')
figure
errorbar(Rotameter.h,Venturi.Q_m,Venturi.Q_m_err,Venturi.Q_m_err,Rotameter.h_err,Rotameter.h_err,'o','LineWidth',2,'DisplayName','Measured'); hold on; grid on
fplot(Rotameter.Q_h,[Rotameter.h(1) Rotameter.h(end)],'LineWidth',2,'DisplayName','Calibration')
xlabel('h_r_o_t [m]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')
figure
errorbar(Vortex.f,Venturi.Q_m,Venturi.Q_m_err,Venturi.Q_m_err,Vortex.err,Vortex.err,'o','LineWidth',2,'DisplayName','Measured'); hold on; grid on
fplot(Vortex.Q,[Vortex.f(1) Vortex.f(end)],'LineWidth',2,'DisplayName','Calibration')
xlabel('f [Hz]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')

Venturi.io.dP = dP(Venturi.io.v);
Venturi.if.dP = dP(Venturi.if.v);
Venturi.Re    = 4*Venturi.Q_m*rho/(pi*d_o*mu);

Venturi.C = [0.96 0.96 0.96 0.97 0.97]; % From figure 7.10 % change to your discharge coefficient

Venturi.Q_i  = Q(Venturi.io.dP); % Ideal flow
Venturi.Q_C = Venturi.Q_i./Venturi.C; % discharge coefficient correction 

Venturi.dP_f     = Venturi.if.dP*(Venturi.l/Venturi.L); % Friction Pressure diff
Venturi.dP_fcorr = Venturi.io.dP-Venturi.dP_f;
Venturi.Q_f      = Q(Venturi.dP_fcorr);

Venturi.aQ_i = polyfit(Venturi.Q_m,Venturi.Q_i,1);
Venturi.aQ_C = polyfit(Venturi.Q_m,Venturi.Q_C,1);
Venturi.aQ_f = polyfit(Venturi.Q_m,Venturi.Q_f,1);

Venturi.Q_mi = @(q) Venturi.aQ_i(1)*q+Venturi.aQ_i(2);
Venturi.Q_mC = @(q) Venturi.aQ_C(1)*q+Venturi.aQ_C(2);
Venturi.Q_mf = @(q) Venturi.aQ_f(1)*q+Venturi.aQ_f(2);

[~,~,~,~,stats] = regress(Venturi.Q_i',[Venturi.Q_mi(Venturi.Q_m') ones(length(Venturi.Q_i),1)]);
Venturi.Rs_i    = stats(1);
[~,~,~,~,stats] = regress(Venturi.Q_C',[Venturi.Q_mC(Venturi.Q_m') ones(length(Venturi.Q_C),1)]);
Venturi.Rs_C    = stats(1); 
[~,~,~,~,stats] = regress(Venturi.Q_f',[Venturi.Q_mf(Venturi.Q_m') ones(length(Venturi.Q_f),1)]);
Venturi.Rs_f    = stats(1);

Venturi.asqrtdP = polyfit(sqrt(Venturi.io.dP),Venturi.Q_m,1);
Venturi.sqrtdP  = @(p) Venturi.asqrtdP(1)*p+Venturi.asqrtdP(2);
Venturi.asqrQ = polyfit(Venturi.Q_m.^2,Venturi.io.dP,1);
Venturi.sqrQ  = @(q) Venturi.asqrQ(1)*q+Venturi.asqrQ(2);

[~,~,~,~,stats]   = regress(Venturi.Q_m',[Venturi.sqrtdP(Venturi.io.dP') ones(length(Venturi.Q_m),1)]);
Venturi.Rs_sqrtdP = stats(1);
[~,~,~,~,stats]   = regress(Venturi.io.dP',[Venturi.sqrQ(Venturi.Q_m') ones(length(Venturi.io.dP),1)]);
Venturi.Rs_sqrQ   = stats(1);

Venturi.dP_err  = ones(length(Venturi.io.dP),1)*a_dP(1)*meas_v_err;
Venturi.Q_i_err = A2./(M*sqrt(2*rho*Venturi.io.dP)).*Venturi.dP_err';
Venturi.Q_C_err = Venturi.C.*Venturi.Q_i_err;
Venturi.Q_f_err = A2./(M*sqrt(2*rho*Venturi.dP_fcorr)).*Venturi.dP_err';

figure
plot(Venturi.Q_m,Venturi.Q_i,'o','Linewidth',2,'DisplayName','Ideal Flow'); hold on; grid on
fplot(Venturi.Q_mi,[Venturi.Q_m(1) Venturi.Q_m(end)],'Linewidth',2,'DisplayName','Ideal Flow, Calibration')
plot(Venturi.Q_m,Venturi.Q_C,'*','Linewidth',2,'DisplayName','Discharge Coef. Compensation')
fplot(Venturi.Q_mC,[Venturi.Q_m(1) Venturi.Q_m(end)],'--','Linewidth',2,'DisplayName','Discharge, Calibration')
plot(Venturi.Q_m,Venturi.Q_f,'+','Linewidth',2,'DisplayName','Pressure Compensation')
fplot(Venturi.Q_mf,[Venturi.Q_m(1) Venturi.Q_m(end)],':','Linewidth',2,'DisplayName','Pressure, Calibration')
xlabel('Q_r_e_a_l [m^3/s]')
ylabel('Q_c_a_l_c [m^3/s]')
legend('show','Location','northwest')

Venturi.sqrtdP_err = 1./(2*Venturi.io.dP).*Venturi.dP_err';

figure
errorbar(sqrt(Venturi.io.dP),Venturi.Q_m,Venturi.Q_m_err,Venturi.Q_m_err,Venturi.sqrtdP_err,Venturi.sqrtdP_err,'o','Linewidth',2,'DisplayName','Measured'); hold on; grid on;
fplot(Venturi.sqrtdP,[sqrt(Venturi.io.dP(1)) sqrt(Venturi.io.dP(end))],'Linewidth',2,'DisplayName','Calibration')
xlabel('\DeltaP^1^/^2 [Pa]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')


%% Nozzle
t_2  = [18.83*3 10.26*3 20.32 15.57 12.84]; %[s]
Nozzle.Q_m = V_tank./t_2;            %[m^3/s]
Nozzle.io.v = [0.5 0.8 1.2 1.7 2.3]; %[V]
Nozzle.if.v = [0.4 0.6 0.9 1.2 1.6]; %[V]
Nozzle.l    = 47*1e-3;               %[m]
Nozzle.L    = 228.5*1e-3;            %[m]

Nozzle.io.dP = dP(Nozzle.io.v);
Nozzle.if.dP = dP(Nozzle.if.v);
Nozzle.Re    = 4*Nozzle.Q_m*rho/(pi*d_o*mu);

Nozzle.C = [0.95 0.95 0.96 0.96 0.96]; % From figure 7.11

Nozzle.Q_i  = Q(Nozzle.io.dP); % Ideal flow
Nozzle.Q_C = Nozzle.Q_i./Nozzle.C; % discharge coefficient correction

Nozzle.dP_f     = Nozzle.if.dP*(Nozzle.l/Nozzle.L); % Friction Pressure diff
Nozzle.dP_fcorr = Nozzle.io.dP-Nozzle.dP_f;
Nozzle.Q_f      = Q(Nozzle.dP_fcorr);

Nozzle.aQ_i = polyfit(Nozzle.Q_m,Nozzle.Q_i,1);
Nozzle.aQ_C = polyfit(Nozzle.Q_m,Nozzle.Q_C,1);
Nozzle.aQ_f = polyfit(Nozzle.Q_m,Nozzle.Q_f,1);

Nozzle.Q_mi = @(q) Nozzle.aQ_i(1)*q+Nozzle.aQ_i(2);
Nozzle.Q_mC = @(q) Nozzle.aQ_C(1)*q+Nozzle.aQ_C(2);
Nozzle.Q_mf = @(q) Nozzle.aQ_f(1)*q+Nozzle.aQ_f(2);

[~,~,~,~,stats] = regress(Nozzle.Q_i',[Nozzle.Q_mi(Nozzle.Q_m') ones(length(Nozzle.Q_i),1)]);
Nozzle.Rs_i     = stats(1);
[~,~,~,~,stats] = regress(Nozzle.Q_C',[Nozzle.Q_mC(Nozzle.Q_m') ones(length(Nozzle.Q_C),1)]);
Nozzle.Rs_C     = stats(1);
[~,~,~,~,stats] = regress(Nozzle.Q_f',[Nozzle.Q_mf(Nozzle.Q_m') ones(length(Nozzle.Q_f),1)]);
Nozzle.Rs_f     = stats(1);

Nozzle.asqrtdP = polyfit(sqrt(Nozzle.io.dP),Nozzle.Q_m,1);
Nozzle.sqrtdP  = @(p) Nozzle.asqrtdP(1)*p+Nozzle.asqrtdP(2);
Nozzle.asqrQ = polyfit(Nozzle.Q_m.^2,Nozzle.io.dP,1);
Nozzle.sqrQ  = @(q) Nozzle.asqrQ(1)*q+Nozzle.asqrQ(2);

[~,~,~,~,stats]  = regress(Nozzle.Q_m',[Nozzle.sqrtdP(Nozzle.io.dP') ones(length(Nozzle.Q_m),1)]);
Nozzle.Rs_sqrtdP = stats(1);
[~,~,~,~,stats]  = regress(Nozzle.io.dP',[Nozzle.sqrQ(Nozzle.Q_m') ones(length(Nozzle.io.dP),1)]);
Nozzle.Rs_sqrQ   = stats(1);

Nozzle.Q_m_err = sqrt((tank_A*tank_h_err./t_2).^2+(-tank_A*tank_h*tank_t_err./t_2.^2).^2);
Nozzle.dP_err  = ones(length(Nozzle.io.dP),1)*a_dP(1)*meas_v_err;
Nozzle.Q_i_err = A2./(M*sqrt(2*rho*Nozzle.io.dP)).*Nozzle.dP_err';
Nozzle.Q_C_err = Nozzle.C.*Nozzle.Q_i_err;
Nozzle.Q_f_err = A2./(M*sqrt(2*rho*Nozzle.dP_fcorr)).*Nozzle.dP_err';

figure
plot(Nozzle.Q_m,Nozzle.Q_i,'o','Linewidth',2,'DisplayName','Ideal Flow'); hold on; grid on
fplot(Nozzle.Q_mi,[Nozzle.Q_m(1) Nozzle.Q_m(end)],'Linewidth',2,'DisplayName','Ideal Flow, Calibration')
plot(Nozzle.Q_m,Nozzle.Q_C,'*','Linewidth',2,'DisplayName','Discharge Coef. Compensation')
fplot(Nozzle.Q_mC,[Nozzle.Q_m(1) Nozzle.Q_m(end)],'--','Linewidth',2,'DisplayName','Discharge, Calibration')
plot(Nozzle.Q_m,Nozzle.Q_f,'+','Linewidth',2,'DisplayName','Pressure Compensation')
fplot(Nozzle.Q_mf,[Nozzle.Q_m(1) Nozzle.Q_m(end)],':','Linewidth',2,'DisplayName','Pressure, Calibration')
xlabel('Q_r_e_a_l [m^3/s]')
ylabel('Q_c_a_l_c [m^3/s]')
legend('show','Location','northwest')

Nozzle.sqrtdP_err = 1./(2*Nozzle.io.dP).*Nozzle.dP_err';

figure
errorbar(sqrt(Nozzle.io.dP),Nozzle.Q_m,Nozzle.Q_m_err,Nozzle.Q_m_err,Nozzle.sqrtdP_err,Nozzle.sqrtdP_err,'o','Linewidth',2,'DisplayName','Measured'); hold on; grid on;
fplot(Nozzle.sqrtdP,[sqrt(Nozzle.io.dP(1)) sqrt(Nozzle.io.dP(end))],'Linewidth',2,'DisplayName','Calibration')
xlabel('\DeltaP^1^/^2 [Pa]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')


%% Orrifice
t_3  = [19.1*3 9.38*3 20.59 15.68 13.96];   %[s]
Orifice.Q_m = V_tank./t_3;            %[m^3/s]
Orifice.io.v = [0.6 1.1 1.8 2.8 3.3]; %[V]
Orifice.if.v = [0.4 0.9 1.4 2.1 2.4]; %[V]
Orifice.l    = 36*1e-3;               %[m]
Orifice.L    = 236.5*1e-3;            %[m]


Orifice.io.dP = dP(Orifice.io.v);
Orifice.if.dP = dP(Orifice.if.v);
Orifice.Re    = 4*Orifice.Q_m*rho/(pi*d_o*mu);

CM = 0.61; % From figure 7.13
Orifice.C = CM/M; %CM = 0.62, M=1/sqrt(1-(A2/A1)^2)

Orifice.Q_i  = Q(Orifice.io.dP); % Ideal flow
Orifice.Q_C = Orifice.Q_i./Orifice.C; % discharge coefficient correction

Orifice.dP_f     = Orifice.if.dP*(Orifice.l/Orifice.L); % Friction Pressure diff
Orifice.dP_fcorr = Orifice.io.dP-Orifice.dP_f;
Orifice.Q_f      = Q(Orifice.dP_fcorr);

Orifice.aQ_i = polyfit(Orifice.Q_m,Orifice.Q_i,1);
Orifice.aQ_C = polyfit(Orifice.Q_m,Orifice.Q_C,1);
Orifice.aQ_f = polyfit(Orifice.Q_m,Orifice.Q_f,1);

Orifice.Q_mi = @(q) Orifice.aQ_i(1)*q+Orifice.aQ_i(2);
Orifice.Q_mC = @(q) Orifice.aQ_C(1)*q+Orifice.aQ_C(2);
Orifice.Q_mf = @(q) Orifice.aQ_f(1)*q+Orifice.aQ_f(2);

[~,~,~,~,stats] = regress(Orifice.Q_i',[Orifice.Q_mi(Orifice.Q_m') ones(length(Orifice.Q_i),1)]);
Orifice.Rs_i    = stats(1);
[~,~,~,~,stats] = regress(Orifice.Q_C',[Orifice.Q_mC(Orifice.Q_m') ones(length(Orifice.Q_C),1)]);
Orifice.Rs_C    = stats(1);
[~,~,~,~,stats] = regress(Orifice.Q_f',[Orifice.Q_mf(Orifice.Q_m') ones(length(Orifice.Q_f),1)]);
Orifice.Rs_f    = stats(1);

Orifice.asqrtdP = polyfit(sqrt(Orifice.io.dP),Orifice.Q_m,1);
Orifice.sqrtdP  = @(p) Orifice.asqrtdP(1)*p+Orifice.asqrtdP(2);
Orifice.asqrQ   = polyfit(Orifice.Q_m.^2,Orifice.io.dP,1);
Orifice.sqrQ    = @(q) Orifice.asqrQ(1)*q+Orifice.asqrQ(2);

[~,~,~,~,stats]   = regress(Orifice.Q_m',[Orifice.sqrtdP(Orifice.io.dP') ones(length(Orifice.Q_m),1)]);
Orifice.Rs_sqrtdP = stats(1);
[~,~,~,~,stats]   = regress(Orifice.io.dP',[Orifice.sqrQ(Orifice.Q_m') ones(length(Orifice.io.dP),1)]);
Orifice.Rs_sqrQ   = stats(1);

Orifice.Q_m_err = sqrt((tank_A*tank_h_err./t_2).^2+(-tank_A*tank_h*tank_t_err./t_2.^2).^2);
Orifice.dP_err  = ones(length(Orifice.io.dP),1)*a_dP(1)*meas_v_err;
Orifice.Q_i_err = A2./(M*sqrt(2*rho*Orifice.io.dP)).*Orifice.dP_err';
Orifice.Q_C_err = Orifice.C.*Orifice.Q_i_err;
Orifice.Q_f_err = A2./(M*sqrt(2*rho*Orifice.dP_fcorr)).*Orifice.dP_err';

figure
plot(Orifice.Q_m,Orifice.Q_i,'o','Linewidth',2,'DisplayName','Ideal Flow'); hold on; grid on
fplot(Orifice.Q_mi,[Orifice.Q_m(1) Orifice.Q_m(end)],'Linewidth',2,'DisplayName','Ideal Flow, Calibration')
plot(Orifice.Q_m,Orifice.Q_C,'*','Linewidth',2,'DisplayName','Discharge Coef. Compensation')
fplot(Orifice.Q_mC,[Orifice.Q_m(1) Orifice.Q_m(end)],'--','Linewidth',2,'DisplayName','Discharge, Calibration')
plot(Orifice.Q_m,Orifice.Q_f,'+','Linewidth',2,'DisplayName','Pressure Compensation')
fplot(Orifice.Q_mf,[Orifice.Q_m(1) Orifice.Q_m(end)],':','Linewidth',2,'DisplayName','Pressure, Calibration')
xlabel('Q_r_e_a_l [m^3/s]')
ylabel('Q_c_a_l_c [m^3/s]')
legend('show','Location','northwest')

Orifice.sqrtdP_err = 1./(2*Orifice.io.dP).*Orifice.dP_err';

figure
errorbar(sqrt(Orifice.io.dP),Orifice.Q_m,Orifice.Q_m_err,Orifice.Q_m_err,Orifice.sqrtdP_err,Orifice.sqrtdP_err,'o','Linewidth',2,'DisplayName','Measured'); hold on; grid on;
fplot(Orifice.sqrtdP,[sqrt(Orifice.io.dP(1)) sqrt(Orifice.io.dP(end))],'Linewidth',2,'DisplayName','Calibration')
xlabel('\DeltaP^1^/^2 [Pa]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')

%% Desired graphs for prt 2
figure
fplot(Venturi.sqrtdP,[sqrt(Venturi.io.dP(1)) sqrt(Venturi.io.dP(end))],'Linewidth',2,'DisplayName','Venturi'); hold on; grid on;
fplot(Nozzle.sqrtdP,[sqrt(Nozzle.io.dP(1)) sqrt(Nozzle.io.dP(end))],'--','Linewidth',2,'DisplayName','Nozzle')
fplot(Orifice.sqrtdP,[sqrt(Orifice.io.dP(1)) sqrt(Orifice.io.dP(end))],'-.','Linewidth',2,'DisplayName','Orifice')
xlabel('\DeltaP^1^/^2 [Pa]')
ylabel('Q_r_e_a_l [m^3/s]')
legend('show','Location','northwest')

Venturi.sqrQ_err = Venturi.Q_m.*Venturi.Q_m_err;
Nozzle.sqrQ_err = Nozzle.Q_m.*Nozzle.Q_m_err;
Orifice.sqrQ_err = Orifice.Q_m.*Orifice.Q_m_err;

figure
plot(Venturi.Q_m.^2,Venturi.io.dP,'o','Linewidth',2,'DisplayName','Venturi, Reults'); hold on; grid on;
fplot(Venturi.sqrQ,[Venturi.Q_m(1)^2 Venturi.Q_m(end)^2],'Linewidth',2,'DisplayName','Venturi, Calibration')
plot(Nozzle.Q_m.^2,Nozzle.io.dP,'*','Linewidth',2,'DisplayName','Nozzle, Results')
fplot(Nozzle.sqrQ,[Nozzle.Q_m(1)^2 Nozzle.Q_m(end)^2],'--','Linewidth',2,'DisplayName','Nozzle, Calibration')
plot(Orifice.Q_m.^2,Orifice.io.dP,'+','Linewidth',2,'DisplayName','Orrifice, Results')
fplot(Orifice.sqrQ,[Orifice.Q_m(1)^2 Orifice.Q_m(end)^2],'-.','Linewidth',2,'DisplayName','Orrifice, Calibration')
xlabel('Q_r_e_a_l^2 [m^3/s]')
ylabel('\DeltaP [Pa]')
legend('show','Location','northwest')

%% Prt 3

m = (2400*2*pi/60)/230; %[rad/(s*V)]
N_eq = @(v) m*v;        %[rad/s]
V = [160 180 200 220];  %[V]
D = 180*1e-3;           %[m]
h = 25*1e-2;          %[m]
N = N_eq(V);            %[rad/s]

P_i     = rho*g*h;
P_e_160 = 1e5*[0.35 0.4 0.45 0.5]; % Pressure from guage
P_e_180 = 1e5*[0.4 0.45 0.5 0.55 0.6 0.65];
P_e_200 = 1e5*[0.5 0.55 0.6 0.65 0.7 0.75];
P_e_220 = 1e5*[0.6 0.65 0.7 0.75 0.8 0.85];
f_160    = [19.3 16 12.6 10.4]; % vortex frequencies
f_180    = [34.1 30.2 27.9 21.6 18.3 11.6];
f_200    = [42.2 37.6 32.9 28.8 24.8 14.8];
f_220    = [56 48.8 41.8 34.3 28.2 17.2];

dP_160 = P_e_160-P_i;
dP_180 = P_e_180-P_i;
dP_200 = P_e_200-P_i;
dP_220 = P_e_220-P_i;
Q_160  = Vortex.Q(f_160);
Q_180  = Vortex.Q(f_180);
Q_200  = Vortex.Q(f_200);
Q_220  = Vortex.Q(f_220);

pdP_160 = dP_160/(rho*(N(1)*D)^2);
pdP_180 = dP_180/(rho*(N(2)*D)^2);
pdP_200 = dP_200/(rho*(N(3)*D)^2);
pdP_220 = dP_220/(rho*(N(4)*D)^2);
pQ_160  = Q_160/(N(1)*D^3);
pQ_180  = Q_180/(N(2)*D^3);
pQ_200  = Q_200/(N(3)*D^3);
pQ_220  = Q_220/(N(4)*D^3);

figure
plot(Q_160,dP_160,'o','LineWidth',2,'DisplayName','160 [V]'); hold on; grid on;
plot(Q_180,dP_180,'+','LineWidth',2,'DisplayName','180 [V]')
plot(Q_200,dP_200,'*','LineWidth',2,'DisplayName','200 [V]')
plot(Q_220,dP_220,'square','LineWidth',2,'DisplayName','220 [V]')
xlabel('Q [m^3/s]')
ylabel('\DeltaP [Pa]')
legend show

figure
plot(pQ_160,pdP_160,'o','LineWidth',2,'DisplayName','160 [V]'); hold on; grid on;
plot(pQ_180,pdP_180,'+','LineWidth',2,'DisplayName','180 [V]')
plot(pQ_200,pdP_200,'*','LineWidth',2,'DisplayName','200 [V]')
plot(pQ_220,pdP_220,'square','LineWidth',2,'DisplayName','220 [V]')
xlabel('\Pi_Q')
ylabel('\Pi_\Delta_P')
legend show

