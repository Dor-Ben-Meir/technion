transpose(




%���� ���� ����
Voltege = transpose(V_i1);
monometer_hight = transpose(dh);
delta_p = transpose(dP_i1);
Table_1 = table(monometer_hight,delta_p,Voltege );



%���� ������
Rotameter_hight = transpose(Rotameter.h);
Rotameter_Q     =  transpose(Rotameter.Q);
Real_flow_rate = transpose(Venturi.Q_m);
Table_2 = table(Real_flow_rate,Rotameter_Q,Rotameter_hight);


%������ ��������	
Vortex_frequncy = transpose(Vortex.f);
Real_flow_rate = transpose(Venturi.Q_m);
Table_3 = table(Real_flow_rate,Vortex_frequncy);


%���� ����� ���� �������
Venturi_Test_Number = transpose([1:5]);
Venturi_Real_flow_rate = transpose(Venturi.Q_m);
Venturi_one_to_two = transpose(Venturi.io.dP);
Venturi_one_to_three = transpose(Venturi.if.dP);
Venturi_Reynolds_Number = transpose(Venturi.Re);
Venturi_C_prika = transpose(Venturi.C);
Venturi_ideal_flow = transpose(Venturi.Q_i);
Venturi_flowrate_correction_with_C = transpose(Venturi.Q_C);
Venturi_flowrate_correction_with_pressure_diffrence = transpose(Venturi.Q_f);
Table_4= table(Venturi_Test_Number,Venturi_Real_flow_rate,Venturi_one_to_two,Venturi_one_to_three,Venturi_Reynolds_Number,Venturi_C_prika,Venturi_ideal_flow,Venturi_flowrate_correction_with_C,Venturi_flowrate_correction_with_pressure_diffrence);

%���� ����� ���� ����
Nozzle_Test_Number = transpose([1:5]);
Nozzle_Real_flow_rate = transpose(Nozzle.Q_m);
Nozzle_one_to_two =transpose(Nozzle.io.dP);
Nozzle_one_to_three = transpose(Nozzle.if.dP);
Nozzle_Reynolds_Number = transpose(Nozzle.Re);
Nozzle_C_prika = transpose(Nozzle.C);
Nozzle_ideal_flow = transpose(Nozzle.Q_i);
Nozzle_flowrate_correction_with_C = transpose(Nozzle.Q_C);
Nozzle_flowrate_correction_with_pressure_diffrence = transpose(Nozzle.Q_f);
Table_5= table(Nozzle_Test_Number,Nozzle_Real_flow_rate,Nozzle_one_to_two,Nozzle_one_to_three,Nozzle_Reynolds_Number,Nozzle_C_prika,Nozzle_ideal_flow,Nozzle_flowrate_correction_with_C,Nozzle_flowrate_correction_with_pressure_diffrence);

%���� ����� ���� ����
Orifice_Test_Number =transpose( [1:5]);
Orifice_Real_flow_rate = transpose(Orifice.Q_m);
Orifice_one_to_two = transpose(Orifice.io.dP);
Orifice_one_to_three =transpose(Orifice.if.dP);
Orifice_Reynolds_Number = transpose(Orifice.Re);
Orifice_C_prika = transpose([Orifice.C,Orifice.C,Orifice.C,Orifice.C,Orifice.C]);
Orifice_ideal_flow = transpose(Orifice.Q_i);
Orifice_flowrate_correction_with_C = transpose(Orifice.Q_C);
Orifice_flowrate_correction_with_pressure_diffrence = transpose(Orifice.Q_f);
Table_6 = table(Orifice_Test_Number,Orifice_Real_flow_rate,Orifice_one_to_two,Orifice_one_to_three,Orifice_Reynolds_Number,Orifice_C_prika,Orifice_ideal_flow,Orifice_flowrate_correction_with_C,Orifice_flowrate_correction_with_pressure_diffrence);

%������ ����� 160 ����
Test_Number_160 = transpose([1:4]);
Pressure_diffrence_160 = transpose(dP_160);
Vortex_frequencies_160 = transpose(f_160);
Flow_rate_160 = transpose(Q_160);
pi_dp_160 = transpose(pdP_160);
pi_flow_rate_160 = transpose(pQ_160);
Table_7 = table(Test_Number_160,Pressure_diffrence_160,Vortex_frequencies_160,Flow_rate_160,pi_dp_160,pi_flow_rate_160);


%������ ����� 180 ����
Test_Number_180 = transpose([1:6]);
Pressure_diffrence_180 = transpose(dP_180);
Vortex_frequencies_180 = transpose(f_180);
Flow_rate_180 = transpose(Q_180);
pi_dp_180 = transpose(pdP_180);
pi_flow_rate_180 = transpose(pQ_180);
Table_8 = table(Test_Number_180,Pressure_diffrence_180,Vortex_frequencies_180,Flow_rate_180,pi_dp_180,pi_flow_rate_180);

%������ ����� 200 ����
Test_Number_200 = transpose([1:6]);
Pressure_diffrence_200 = transpose(dP_200);
Vortex_frequencies_200 = transpose(f_200);
Flow_rate_200 = transpose(Q_200);
pi_dp_200 = transpose(pdP_200);
pi_flow_rate_200 = transpose(pQ_200);
Table_9 = table(Test_Number_200,Pressure_diffrence_200,Vortex_frequencies_200,Flow_rate_200,pi_dp_200,pi_flow_rate_200);



%������ ����� 220 ����
Test_Number_220 = transpose([1:6]);
Pressure_diffrence_220 = transpose(dP_220);
Vortex_frequencies_220 = transpose(f_220);
Flow_rate_220 = transpose(Q_220);
pi_dp_220 = transpose(pdP_220);
pi_flow_rate_220 = transpose(pQ_220);
Table_10 = table(Test_Number_220,Pressure_diffrence_220,Vortex_frequencies_220,Flow_rate_220,pi_dp_220,pi_flow_rate_220);


