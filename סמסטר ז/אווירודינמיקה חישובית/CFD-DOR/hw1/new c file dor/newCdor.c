
// C code (although python is better) for homework1 by Dor Ben Meir: 305702490
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846

// Globals:
int imax, jmax, iTEL, iLE, iTEU, m, n, i, j, l, max_ij,  control_function;
double t, Delta_y, Delta_x, XSF, YSF,  r, w, R0_x, R0_y;



// Inputs from the user:
void input()
{

    printf("Please enter 1 for no control functions,\n2 for phi only,\n3 for psi only,\n4 for phi and psi:\n");
    scanf("%d", &control_function);


    printf("Please enter thickness of the profile:\n");
    scanf("%lf", &t);


    printf("Please enter i-max:\n");
    scanf("%d", &imax);


    printf("Please enter j-max:\n");
    scanf("%d", &jmax);


    printf("Please enter i-TEL:\n");
    scanf("%d", &iTEL);


    printf("Please enter i-LE:\n");
    scanf("%d", &iLE);


    printf("Please enter i-TEU:\n");
    scanf("%d", &iTEU);


    printf("Please enter Delta y:\n");
    scanf("%lf", &Delta_y);


    printf("Please enter XSF:\n");
    scanf("%lf", &XSF);


    printf("Please enter YSF:\n");
    scanf("%lf", &YSF);


    printf("Please enter  The relaxation parameter r:\n");
	scanf("%lf", &r);


    printf("Please enter the relaxation parameter w:\n");
    scanf("%lf", &w);


    m = imax;
    n = jmax;
    max_ij = (imax > jmax ? imax : jmax);
    l = m * n;
}
//*********************************************************************************

//index offset for arrays:

int offset(int i, int j, int m)
{
    return (i + j*m);

}

//*********************************************************************************

// Return the absolute value:
double absolute(double v)
{
    return v < 0 ? -v : v;
}

//*********************************************************************************


// Profile coordinates:

double Y_X_profile(double x)
{

    return ( 5 * t * (0.2969 * sqrt(x) - 0.126 * (x) - 0.3516 * (x*x) + 0.2843 * (x*x*x) - 0.1015 * (x*x*x*x)));
}

//*********************************************************************************

// Boundary Conditions on the profile:

void profile_boundary(double *x, double *y)
{
	int itrex, index, index_m;
	double Delta_x;
    Delta_x = (1 / (double)( iLE - iTEL ));
    itrex = 0;

//for i in range((iTEL-1),iLE):
	 for (i=iTEL-1; i < iLE; i++)
	 {
        index = offset(i, 0, m);
        x[index] = 1 - cos( (PI)/2 * ( (iLE-1) - i) * Delta_x );
        y[index] = -Y_X_profile( x[index] );


// Mirroring:

        index_m = (iTEU - 1)  - itrex;
        x[index_m] = x[index];
        y[index_m] = -y[index];
        itrex  = itrex  + 1;
	 }

}

//*********************************************************************************

void wake_boundary(double *x, double *y)
{
	int index, index_m;
	//for i in range(iTEU,imax):
	for (i=iTEU; i < imax; i++)
	{
		index = offset(i, 0, m);
		x[index] = x[index-1] + ( x[index-1] - x[index-2] ) * XSF;
		y[index] = 0;

	}
	//for i in range(iTEL-1):
	for(i=0; i < iTEL-1; i++)
	{
		index = offset(i, 0, m);
		x[index] = x[ offset(imax-i-1, 0, m) ];
		y[index] = 0;
	}

}

//*********************************************************************************

// Boundary Conditions on exit boundary:
void exit_boundary(double *x, double *y)
{
	int j, index, index_m;

	index = offset(imax-1, 1, m);
	y[index] = Delta_y;
	x[index] = x[ offset(imax-1, 0, m)];

// Mirroring:

	index_m = offset(0, 1, m);
	y[index_m] = -y[index];
	x[index_m] = x[ offset(0, 0, m)];

	//for j in range(2,jmax):
	for (j=2; j < jmax; j++)
	{
		index = offset(imax-1, j, m);
		y[index] = y[offset(imax-1, j-1, m)] + ( y[offset(imax-1, j-1, m)] - y[offset(imax-1, j-2, m)] ) * YSF;
		x[index] = x[ offset(imax-1, 0, m) ];

// Mirroring:

		index_m = offset(0, j, m);
		y[index_m] = -y[index];
		x[index_m] = x[ offset(0, 0, m) ];
	}


}


//*********************************************************************************

// Boundary Conditions on outer boundary:
void outer_boundary(double *x, double *y)
{
	int N_Round_Sections, i, index, index_m, Straight_Sections, N_Straight_Sections;
	int index_u, index_il;
	double 	Y_max, X_max, Delta_s, Length, angele,Round_Sections;

		Y_max = y[ offset(imax-1, jmax-1, m) ];
		X_max = x[ offset(imax-1, 0, m) ];
		Length = 2 * X_max + PI * Y_max;
		Delta_s = (Length /  (double)( imax - 1));
		N_Round_Sections = 0;
		angele = 0;
		while (angele < (PI/2))
		{
			i = (iLE - 1 ) + N_Round_Sections;
			index = offset(i, jmax-1, m);
			angele =  N_Round_Sections * Delta_s / Y_max;
			x[index] = -Y_max * cos(angele);
			y[index] = Y_max * sin(angele);

// Mirroring:

			if (N_Round_Sections > 0)
			{
				i = (iLE - 1 )  - N_Round_Sections;
				index_m = offset(i, jmax-1, m);
				x[index_m] = x[index];
				y[index_m] = -y[index];
			}

			N_Round_Sections = 	N_Round_Sections + 1;
		}



// Outer boundary on the straight section:

		Round_Sections = angele * Y_max;
		Straight_Sections = floor(((Length/2) - Round_Sections)  / Delta_s);
		N_Straight_Sections = 1;
		index_u = imax - 2;
		index_il = 1;

		while (N_Straight_Sections <= Straight_Sections)
		{

			index = offset(index_u, jmax-1, m);
			x[index] = X_max - N_Straight_Sections * Delta_s;
			y[index] = Y_max;

// Mirroring:

			index_m = offset(index_il, jmax-1, m);
			x[index_m] = x[index];
			y[index_m] = -Y_max;

			N_Straight_Sections = N_Straight_Sections + 1;
			index_u  = index_u  - 1;
			index_il = index_il + 1;
		}

}
//*********************************************************************************

void boundry(double *x, double *y)
{
		profile_boundary(x, y);
		wake_boundary(x, y);
		exit_boundary(x, y);
		outer_boundary(x, y);
}

//*********************************************************************************

//linear interpolation along eta or xi coordinate

// linear interpolation along  eta direction:
void interp_eta(double *v)
{

    //for i in range(1,imax-1):
    for (i=1; i < imax - 1; i++)
	{
        //for j in range(1,jmax-1):
        for (j=1; j < jmax - 1; j++)
		{
            v[ offset( i, j, m ) ] = v[ offset(i,j-1,m)] +  ((v[ offset(i, (jmax - 1), m ) ] -  v[ offset(i,j-1,m) ]) / ((jmax-1) - (j-1)));
		}

    }

}

// linear interpolation along xi direction:
void interp_xi(double *v)
{
	int i, j;
    //for j in range(1,jmax-1):
    for (j=1; j < jmax - 1; j++)
	{
        //for i in range(1,imax-1):
        for (i=1; i < imax - 1; i++)
		{
            v[ offset( i, j, m ) ] = v[ offset(i-1,j,m)] +  ((v[ offset((imax-1), j, m ) ] -  v[ offset(i-1,j,m) ]) / ((imax-1) - (i-1)));

        }

    }


}

//*********************************************************************************

// Building the initial grid:
void initialize(double *x, double *y, double *x_initial, double *y_initial)
{
    int i, j , v;
	boundry(x,y);
	interp_eta(x);
	interp_eta(y);


// Copping X and Y to X_initial and Y_initial vectors:

    for (i = 0; i < imax; i++)
	{
        for (j = 0; j < jmax; j++)
        {
            v = offset(i, j, m);
            x_initial[v] = x[v];
            y_initial[v] = y[v];
        }
	}

}

//*********************************************************************************


//  Derivatives calculation: if dx_dxi  or dy_dxi is calculated than x_eflag is 1 and else
// dx_deta or dy_deta are calculated.

//Calculating first derivatives:
double calc_diff(double *v, int i, int j, int  x_eflag)
{
// xi:

    if(x_eflag)
    {
        return ( (v[offset(i+1, j, m)] - v[offset(i-1, j, m)]) / 2 );
    }

// eta:

    else
    {
        return ( (v[offset(i, j+1, m)] - v[offset(i, j-1, m)]) / 2 );
    }

}


// Calculating second derivatives:
double calc_2nd_diff(double *v, int  i, int  j, int  x_eflag)
{

// xi:

    if(x_eflag)
	{
        return (v[offset(i+1, j, m)] - 2*v[offset(i, j, m)] + v[offset(i-1, j, m)]);

	}


// eta:

    else
	{

        return (v[offset(i, j+1, m)] - 2*v[offset(i, j, m)] + v[offset(i, j-1, m)]);

	}

}

//*********************************************************************************

// Calculating dx/dxi, dx/deta, dy/dxi and  dy_deta:
void fill_diffs(double *x, double *y, double *dx_dxi, double *dx_deta, double *dy_dxi, double *dy_deta)
{
	int i, j, v;
    int xi = 1;
    int eta = 0;

     //for i in range(1, imax - 1):
    for (i=1; i < imax - 1; i++)
	{
        dx_dxi[offset( i, 0, m )] = calc_diff(x, i, 0, xi);
        dy_dxi[offset( i, 0, m )] = calc_diff(y, i, 0, xi);
        dx_dxi[offset( i, jmax - 1, m )] = calc_diff(x, i, jmax - 1, xi);
        dy_dxi[offset( i, jmax - 1, m )] = calc_diff(y, i, jmax - 1, xi);

        //for j  in range(1, jmax -1):
        for (j=1; j < jmax - 1; j++)
		{
            v = offset(i, j, m);
            dx_dxi[v] = calc_diff(x, i, j, xi);
            dx_deta[v] = calc_diff(x, i, j, eta);
            dy_dxi[v] = calc_diff(y, i, j, xi);
            dy_deta[v] = calc_diff(y, i, j, eta);

            if (i == 1)
			{
                dx_deta[offset( 0, j, m )] = calc_diff(x, 0, j, eta);
                dy_deta[offset( 0, j, m )] = calc_diff(y, 0, j, eta);
                dx_deta[offset( imax - 1, j, m )] = calc_diff(x, imax - 1, j, eta);
                dy_deta[offset( imax - 1, j, m )] = calc_diff(y, imax - 1, j, eta);
			}

		}

	}

}

//*********************************************************************************


// These functions calculate alpha, beta and  gamma.


// Calculate alpha:

double calc_alpha(double dx_deta, double dy_deta)
{

    return (dx_deta*dx_deta + dy_deta*dy_deta);

}

// Calculate beta:

double calc_beta(double dx_dxi, double dx_deta, double dy_dxi,  double dy_deta)
{

    return (dx_dxi*dx_deta + dy_dxi*dy_deta);

}

// Calculate gamma:

double calc_gamma( double dx_dxi, double dy_dxi)
{

    return (dx_dxi*dx_dxi + dy_dxi*dy_dxi);

}



void fill_alpha_beta_gamma(double *alpha, double *beta, double *gamma, double *dx_dxi, double *dx_deta, double *dy_dxi, double *dy_deta)
{
    int i, j, index;
    //for i in range(1, imax -1):
	for (i=1; i < imax - 1; i++)
	{
        //for j  in range(1, jmax -1):
        for (j=1; j < jmax - 1; j++)
		{
            index = offset(i, j, m);
            alpha[index] = calc_alpha(dx_deta[index], dy_deta[index]);
            beta[index] = calc_beta(dx_dxi[index], dx_deta[index], dy_dxi[index], dy_deta[index]);
            gamma[index] = calc_gamma(dx_dxi[index], dy_dxi[index]);

		}

	}

}
//*********************************************************************************

// Calculate the control functions ϕ(ξ, η) and ψ(ξ, η):
void fill_phi_psi(double *x, double *y, double *phi, double *psi, double *dy_deta, double *dx_deta, double *dx_dxi, double *dy_dxi)
{
    int i, j, v;
    int eta = 0;
    int xi = 1;

// ϕ = 0, ψ is calculated from the boundary conditions or ψ and ϕ are calculated from the boundary conditions:

    if ( (control_function  == 2 ) || ( control_function == 4 ) )
	{

// xi=const:

        //for j  in range(1, jmax -1):
        for (j=1; j < jmax - 1; j++)
		{
//xi = xi_min:

            v = offset(0, j, m);
            if ( absolute( dy_deta[v] ) > absolute( dx_deta[v] ) )
			{
                psi[v] = -calc_2nd_diff(y, 0, j, eta) / dy_deta[v];
            }

            else
			{
                psi[v] = -calc_2nd_diff(x, 0, j, eta) / dx_deta[v];

			}
// xi = xi_max:

            v = offset(imax-1, j, m);
            if ( absolute( dy_deta[v] ) > absolute( dx_deta[v] ) )
			{
                psi[v] = -calc_2nd_diff(y, imax-1, j, eta) / dy_deta[v];
			}
            else
			{
                psi[v] = -calc_2nd_diff(x, imax-1, j, eta) / dx_deta[v];
			}

        }
	}
// ψ = 0, ϕ is calculated from the boundary conditions or ψ and ϕ are calculated from the boundary conditions:

    if ((control_function == 3) || (control_function == 4))
	{

// eta=const:

        //for i in range(1, imax -1):
        for (i=1; i < imax - 1; i++)
		{
// eta = eta_min:

            v = offset(i, 0, m);
            if (absolute(dx_dxi[v]) > absolute(dy_dxi[v]))
			{
                phi[v] = -calc_2nd_diff(x, i, 0, xi) / dx_dxi[v];
			}

            else
			{
                phi[v] = -calc_2nd_diff(y, i, 0, xi) / dy_dxi[v];
			}
// eta = eta_max:

            v = offset(i, jmax-1, m);
            if (absolute(dx_dxi[v]) > absolute(dy_dxi[v]))
			{
                phi[v] = -calc_2nd_diff(x, i, jmax-1, xi) / dx_dxi[v];
			}

            else
			{
                phi[v] = -calc_2nd_diff(y, i, jmax-1, xi) / dy_dxi[v];

			}


		}
	}
// Interpolation:

    interp_xi(psi);
    interp_eta(phi);


}

//*********************************************************************************


// Recalculate alpha beta gamma after every iteration:
void fill_metrics(double *x, double *y, double *dx_dxi, double *dx_deta, double *dy_dxi, double *dy_deta, double *alpha, double *beta, double *gamma)
{
    fill_diffs(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta);
    fill_alpha_beta_gamma(alpha, beta, gamma, dx_dxi, dx_deta, dy_dxi, dy_deta);

}

//*********************************************************************************

// The Solution Scheme:

// Calculating operator L:

double L_i_j_n( int i, int  j, double *alpha, double *beta, double *gamma, double *x_or_y, double *phi, double *psi)
{
	double part_1 = alpha[offset(i, j, m)] * ((x_or_y[offset(i+1, j, m)] - 2*x_or_y[offset(i, j, m)] + x_or_y[offset(i-1, j, m)]) + 0.5*phi[offset(i, j, m)] * (x_or_y[offset(i+1, j, m)] - x_or_y[offset(i-1, j, m)]));
	double part_2 = - 0.5*beta[offset(i, j, m)] * (x_or_y[offset(i+1, j+1, m)] - x_or_y[offset(i+1, j-1, m)]- x_or_y[offset(i-1, j+1, m)] + x_or_y[offset(i-1, j-1, m)]);
	double part_3 = gamma[offset(i, j, m)] * (x_or_y[offset(i, j+1, m)] - 2*x_or_y[offset(i, j, m)] + x_or_y[offset(i, j-1, m)] + 0.5*psi[offset(i, j, m)] * (x_or_y[offset(i, j+1, m)] - x_or_y[offset(i, j-1, m)]));

	return (part_1 + part_2 + part_3);

}



// Calculating the right hand side of the matrix:
double RHS(int i, int j, double *alpha, double *beta, double *gamma, double *x_y, double *phi, double *psi)
{

    return r*w*L_i_j_n(i, j, alpha, beta, gamma, x_y, phi, psi);

}


// Calculating the left hand side of the matrix:
void LHS( int i, int  j, double *A, double *B, double *C, double *v, int add_to)
{
	int index;
	double a,b,c;

    index = offset(i, j, m);
    a = -v[index];
    b = r + 2 * v[index];
    c = -v[index];
    A[add_to] = a;
    B[add_to] = b;
    C[add_to] = c;


}

//*********************************************************************************
// Algorithm for 3-diag matrix solution:
int tridiag(double *a, double *b, double *c, double *d, double *u, int is, int ie)
{

#define ERROR 1
#define NOERROR 0

  int i;
  float beta;

  for (i = is + 1; i <= ie; i++)
    {
      if(b[i-1] == 0.) return(ERROR);
      beta = a[i] / b[i-1];
      b[i] = b[i] - c[i-1] * beta;
      d[i] = d[i] - d[i-1] * beta;
    }

  u[ie] = d[ie] / b[ie];
  for (i = ie - 1; i >= is; i--)
    {
      u[i] = (d[i] - c[i] * u[i+1]) / b[i];
    }
  return(NOERROR);
}

//*********************************************************************************

// Calculate the initial RHS for Sx and Sy:
void start_SX_SY(double *Sx, double *Sy, double *alpha, double *beta, double *gamma, double *x, double *y, double *phi, double *psi)
{
	int i, j, index;
    //for  i in range(1,imax-1):
    for (i=1; i < imax - 1; i++)
	{
        //for j in range(1,jmax-1):
		for (j=1; j < jmax - 1; j++)
		{

            index = offset(i, j, m);
            Sx[index] = RHS(i, j, alpha, beta, gamma, x, phi, psi);
            Sy[index] = RHS(i, j, alpha, beta, gamma, y, phi, psi);
		}

    }

}
//*********************************************************************************


// Zeros the arries A B C D and U:
void Zero_ABCDU(double *A, double *B, double *C, double *D, double *U)
{
	int i;
	for (i=0; i < max_ij; i++)
	{
		A[i] = 0;
		B[i] = 0;
		C[i] = 0;
		D[i] = 0;
		U[i] = 0;

	}

}
//*********************************************************************************

// Sweep 1 (fx,fy):
void fx_fy(double *alpha, double *U, double *Sx, double *Sy, double *A, double *B, double *C, double *D)
{
	int i, j, add_to, V;
	//j in range(1,jmax-1):
    for (j=1; j < jmax - 1; j++)
	{
// fx sweep:

			A[0] = 0;
			B[0] = 1;
			C[0] = 0;
			D[0] = 0;

			//i in range(1,imax-1):
			for (i=1; i < imax - 1; i++)
			{
				add_to = i;
				LHS(i, j, A, B, C, alpha,add_to);
				D[add_to] = Sx[offset(i, j, m)];
			}

			add_to += 1;
			A[add_to] = 0;
			B[add_to] = 1;
			C[add_to] = 0;
			D[add_to] = 0;


			tridiag(A, B, C, D, U, 0, imax-1);

			//i in range(imax):
			for (i=0; i < imax; i++)
			{
				V = offset(i, j, m);
				Sx[V] = U[i];
                //printf("%f\n",Sx[V]);
			}
// Zeros the arries A B C D and U:

			Zero_ABCDU(A, B, C, D, U);


// fy sweep:

			A[0] = 0;
			B[0] = 1;
			C[0] = 0;
			D[0] = 0;

			//for  i in range(1,imax-1):
			for (i=1; i < imax - 1; i++)
			{
				add_to = i;
				LHS(i, j, A, B, C, alpha,add_to);
				D[add_to] = Sy[offset(i, j, m)];
			}


			add_to += 1;
			A[add_to] = 0;
			B[add_to] = 1;
			C[add_to] = 0;
			D[add_to] = 0;



			tridiag(A, B, C, D, U, 0, imax-1);

			//for  i in range(imax):
			for (i=0; i < imax; i++)
			{
				V = offset(i, j, m);
				Sy[V] = U[i];

			}

			Zero_ABCDU(A, B, C, D, U);

	}
}

//*********************************************************************************

// Sweep 2 (Cx Cy):
void Cx_Cy(double *gamma, double *U, double *Sx, double *Sy, double *A, double *B, double *C, double *D)
{
	int i, j, add_to, V;

	 //for i in range(1,imax-1):
	for (i=1; i < imax - 1; i++)
	{

// Cx sweep:

			A[0] = 0;
			B[0] = 1;
			C[0] = 0;
			D[0] = 0;

			//for  j in range(1,jmax-1):
			for (j=1; j < jmax-1; j++)
			{
				add_to = j;
				LHS(i, j, A, B, C, gamma,add_to);
				D[add_to] = Sx[offset(i, j, m)];
			}



			add_to += 1;
			A[add_to] = 0;
			B[add_to] = 1;
			C[add_to] = 0;
			D[add_to] = 0;



			tridiag(A, B, C, D, U, 0, jmax-1);



			//for  j in range(jmax):
			for (j=0; j < jmax; j++)
			{
				V = offset(i, j, m);
				Sx[V] = U[j];
				//printf("%f\n",Sx[V]);
			}

			Zero_ABCDU(A, B, C, D, U);


// Cy sweep:

			A[0] = 0;
			B[0] = 1;
			C[0] = 0;
			D[0] = 0;

			//for  j in range(1,jmax-1):
			for (j=1; j < jmax-1; j++)
			{
				add_to = j;
				LHS(i, j, A, B, C, gamma,add_to);
				D[add_to] = Sy[offset(i, j, m)];
			}


			add_to += 1;
			A[add_to] = 0;
			B[add_to] = 1;
			C[add_to] = 0;
			D[add_to] = 0;

			tridiag(A, B, C, D, U, 0, jmax-1);

			//for  j in range(jmax):
			for (j=0; j < jmax; j++)
			{
				V = offset(i, j, m);
				Sy[V] = U[j];

			}

			Zero_ABCDU(A, B, C, D, U);


	}
}

//*********************************************************************************

// Advance the x and y solution:
void advancement_x_y(double *x, double *y, double *Sx, double *Sy)
{


	int V;
    //for  i  in range(1, imax -1):
    for (i=1; i < imax -1; i++)
	{
        //for j in range(1,jmax-1):
		for (j=1; j < jmax -1; j++)
		{
            V = offset(i, j, m);
            x[V] += Sx[V];
            y[V] += Sy[V];

		}
	}

}





//*********************************************************************************


// Calculate residual value:
int calc_residual(double *alpha, double *beta, double *gamma, double *x, double *y, double *phi, double *psi, double *Rx, double *Ry, int flag_R0_R0, int add)
{
    double comp,comp_x,comp_y;

	comp_x = absolute( L_i_j_n(0, 0, alpha, beta, gamma, x, phi, psi) );
	comp_y = absolute( L_i_j_n(0, 0, alpha, beta, gamma, y, phi, psi) );

    for (i=0; i< imax; i++)
    {
        for (j=1; j<jmax; j++)
        {
            comp = absolute( L_i_j_n(i, j, alpha, beta, gamma, x, phi, psi) );
            if (comp_x < comp)
			{
                comp_x = comp;
			}


            comp = absolute( L_i_j_n(i, j, alpha, beta, gamma, y, phi, psi) );
            if (comp_y < comp)
			{
                comp_y = comp;
			}

        }
    }

// Calculation for first residual value:
	if(flag_R0_R0 == 1)
	{
		R0_x = comp_x;
		R0_y = comp_y;
		return ;
	}

// Calculation of the residual value:
	else
	{
		Rx[add] = log10( comp_x / R0_x );
		Ry[add] = log10( comp_y / R0_y );
		return add;
	}


}


//*********************************************************************************

// Main solution of the program:
int step_func(double *Rx, double *Ry, double *alpha, double *beta, double *gamma, double *x, double *y, double *phi, double *psi, double *U, double *Sx, double *Sy, double *dx_dxi, double *dx_deta, double *dy_dxi, double *dy_deta, double *A, double *B, double *C, double *D)
{
	int num_iter, add;


	calc_residual(alpha, beta, gamma, x, y, phi, psi, Rx, Ry, 1, 0);


	num_iter = 0;
	do
	{

		start_SX_SY(Sx, Sy, alpha, beta, gamma, x, y, phi, psi);
		fx_fy(alpha, U, Sx, Sy, A, B, C, D);
		Cx_Cy(gamma, U, Sx, Sy, A, B, C, D);
		add = calc_residual(alpha, beta, gamma, x, y, phi, psi, Rx, Ry, 0, num_iter);
		advancement_x_y(x, y, Sx, Sy);
		fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma);
		num_iter =  num_iter + 1;

	}
	while(((Rx[add] > -6) || (Ry[add] > -6)) && (num_iter < 4000));


    return add;
}

//*********************************************************************************



int output(double *x, double *y, double *x_initial, double *y_initial, double *Rx, double *Ry, int itr)
{
    int i, j, index;
    FILE *X = fopen("X.txt", "w");
    FILE *Y = fopen("Y.txt", "w");
    FILE *Xi = fopen("Xi.txt", "w");
    FILE *Yi = fopen("Yi.txt", "w");
    FILE *R_x = fopen("Rx.txt", "w");
    FILE *R_y = fopen("Ry.txt", "w");

    for (i = 0; i < imax; i++)
    {
        for (j = 0; j < jmax; j++)
        {
            index = offset(i, j, m);
			fprintf(X, "%f\t\t", x[index]);
            fprintf(Y, "%f\t\t", y[index]);
			fprintf(Xi, "%f\t\t", x_initial[index]);
			fprintf(Yi, "%f\t\t", y_initial[index]);
        }

        fprintf(X, "\n");
        fprintf(Y, "\n");
        fprintf(Xi, "\n");
        fprintf(Yi, "\n");
    }
    fclose(X);
    fclose(Y);
    fclose(Xi);
    fclose(Yi);

    for (i = 0; i <= itr ; i++)
    {
        fprintf(R_x, "%f\n", Rx[i]);
        fprintf(R_y, "%f\n", Ry[i]);
    }
    fclose(R_x);
    fclose(R_y);
    return 0;
}




//*********************************************************************************




int main()
{
    int itr;

    input();

//memory allocation:

	double *A =	(double *) malloc( max_ij * sizeof(double) );
	double *B =	(double *) malloc( max_ij * sizeof(double) );
	double *C =	(double *) malloc( max_ij * sizeof(double) );
	double *D =	(double *) malloc( max_ij * sizeof(double) );
	double *U =	(double *) malloc( max_ij * sizeof(double) );
	double *Rx = (double *) calloc( 5000, sizeof(double) );
	double *Ry = (double *) calloc( 5000, sizeof(double) );


	double *x =	(double *) malloc( l * sizeof(double) );
	double *y =	(double *) malloc( l * sizeof(double) );
	double *x_initial = (double *) malloc( l * sizeof(double) );
	double *y_initial = (double *) malloc( l * sizeof(double) );
	double *dx_dxi = (double *) calloc( l, sizeof(double) );
	double *dx_deta = (double *) calloc( l, sizeof(double) );
	double *dy_dxi = (double *) calloc( l, sizeof(double) );
	double *dy_deta = (double *) calloc( l, sizeof(double) );
	double *alpha =	(double *) calloc( l, sizeof(double) );
	double *beta = (double *) calloc( l, sizeof(double) );
	double *gamma =	(double *) calloc( l, sizeof(double) );
	double *phi = (double *) calloc( l, sizeof(double) );
	double *psi = (double *) calloc( l, sizeof(double) );
	double *Sx =  (double *) calloc( l, sizeof(double) );
	double *Sy =  (double *) calloc( l, sizeof(double) );


	initialize(x, y, x_initial, y_initial);
	fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma);
	fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi);
	itr = step_func(Rx, Ry, alpha, beta, gamma, x, y, phi, psi, U, Sx, Sy, dx_dxi, dx_deta, dy_dxi, dy_deta, A, B, C, D);
	output(x, y, x_initial, y_initial, Rx, Ry, itr);


//Clearing The Memory:

    free(x);
    free(y);
    free(x_initial);
    free(y_initial);
    free(dx_dxi);
    free(dx_deta);
    free(dy_dxi);
    free(dy_deta);
    free(alpha);
    free(beta);
    free(gamma);
    free(phi);
    free(psi);
    free(Sx);
    free(Sy);
    free(A);
    free(B);
    free(C);
    free(D);
    free(U);
    free(Rx);
    free(Ry);



    return 0;
}
