
% x = importdata('GridX.txt');
% y = importdata('GridY.txt');
Rx = importdata('Rx.txt');
Ry = importdata('Ry.txt');



figure(1)
hold all
for i = 1 : length(x(:,1))
    hold all
    axis ([-0.25 1.3 -0.5 0.5]);
    plot(x(i,:),y(i,:),'b-')
     scatter(x(i,:),y(i,:),'.')
end

for j = 1 : length(x(1,:))
    hold all
    axis ([-0.25 1.3 -0.5 0.5]);
    plot(x(:,j),y(:,j),'b-')
     scatter(x(:,j),y(:,j),'.')
end
title('phi, psi, r=0.01, w=0.05')
xlabel('x axis')
ylabel('y axis')

figure(2)
hold all
for k = 1 : length(Rx(:,1))
    hold all
    plot(Rx(k,1),Rx(k,2),'b.')
%     scatter(Rx(k,1),Rx(k,2),'.')
end

title('Residual X phi, r=0.01, w=0.5')
xlabel('n')
ylabel('Max X')

figure(3)
hold all
for m = 1 : length(Ry(:,1))
    hold all
    plot(Ry(m,1),Ry(m,2),'b.')
%     scatter(Rx(m,1),Rx(m,2),'.')
end

title('Residual Y phi, psi, r=0.01, w=0.5')
xlabel('n')
ylabel('Max Y')