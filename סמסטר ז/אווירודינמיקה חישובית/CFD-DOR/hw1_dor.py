
import math
import matplotlib.pyplot as plt




def inputs():
    
    imax = int(input('Please enter i-max:\n')) 
    jmax = int(input('Please enter j-max:\n'))
    iTEL = int(input('Please enter i-TEL:\n')) 
    iLE = int(input('Please enter i-LE:\n'))
    iTEU = int(input('Please enter i-TEU:\n'))
    t = float(input('Please enter thickness of the profile:\n'))
    Delta_y = float(input('Please enter Delta y:\n'))
    XSF = float(input('Please enter XSF:\n')) 
    YSF = float(input('Please enter YSF:\n'))
    
    return (imax, jmax, iTEL, iLE, iTEU, t, Delta_y, XSF, YSF) 
            
            
  

#******************************************************************************************************

#index offset for arrays
def offset(i, j, m):
    return (i + j*m)


#******************************************************************************************************
    

# Profile coordinates:

def Y_X_profile(x):
  
     return ( 5 * t * (0.2969 * math.sqrt(x) - 0.126 * (x) - 0.3516 * (x*x) + 0.2843 * (x*x*x) - 0.1015 * (x*x*x*x)))
	 

#******************************************************************************************************

# Boundary Conditions on the profile:
def profile_boundary(x, y):

    Delta_x = float(1 / ( iLE - iTEL ))
    itrex = 0
	
    for i in range((iTEL-1),iLE): 
	
        index = offset(i, 0, m)
        x[index] = 1 - math.cos( (math.pi)/2 * ( (iLE-1) - i) * Delta_x )
        y[index] = -Y_X_profile( x[index] )
		

# Mirroring:
        
        index_m = (iTEU - 1)  - itrex 
        x[index_m] = x[index]
        y[index_m] = -y[index]
        itrex  = itrex  + 1
    return(x,y)




#Boundary Conditions on the  wake:
def wake_boundary(x, y):
	

	for i in range(iTEU,imax): 
		
		index = offset(i, 0, m)
		x[index] = x[index-1] + ( x[index-1] - x[index-2] ) * XSF
		y[index] = 0

        
	for i in range(iTEL-1): 
		
		index = offset(i, 0, m)
		x[index] = x[ offset(imax-i-1, 0, m) ]
		y[index] = 0
		
    
	return(x,y)
    
    

# Boundary Conditions on exit boundary:
def exit_boundary(x, y):
	
	index = offset(imax-1, 1, m)
	y[index] = Delta_y
	x[index] = x[ offset(imax-1, 0, m)]
		
# Mirroring:
    
	index_m = offset(0, 1, m)
	y[index_m] = -y[index]
	x[index_m] = x[ offset(0, 0, m)]
		
	for j in range(2,jmax):
		
		index = offset(imax-1, j, m)
		y[index] = y[offset(imax-1, j-1, m)] + ( y[offset(imax-1, j-1, m)] - y[offset(imax-1, j-2, m)] ) * YSF
		x[index] = x[ offset(imax-1, 0, m) ]
			
# Mirroring:
        
		index_m = offset(0, j, m)
		y[index_m] = -y[index]
		x[index_m] = x[ offset(0, 0, m) ]
		

	return(x,y)
	
	
	
# Boundary Conditions on outer boundary:
def outer_boundary(x, y):
	
		Y_max = y[ offset(imax-1, jmax-1, m) ]
		X_max = x[ offset(imax-1, 0, m) ]
		Length = 2 * X_max + math.pi*Y_max
		Delta_s = float(Length /  ( imax - 1))
		N_Round_Sections = 0
		angele = 0
		while angele < math.pi/2:	 
			i = (iLE - 1 ) + N_Round_Sections
			index = offset(i, jmax-1, m)
			angele =  N_Round_Sections * Delta_s / Y_max
			x[index] = -Y_max * math.cos(angele)
			y[index] = Y_max * math.sin(angele)
				
# Mirroring:
            
			if (N_Round_Sections > 0):
				i = (iLE - 1 )  - N_Round_Sections
				index_m = offset(i, jmax-1, m)
				x[index_m] = x[index]
				y[index_m] = -y[index]
			N_Round_Sections = 	N_Round_Sections + 1
		
# Outer boundary on the straight section:
            
		Round_Sections = angele * Y_max
		Straight_Sections = math.floor(((Length/2) - Round_Sections)  / Delta_s)    
		N_Straight_Sections = 1
		index_u = imax - 2
		index_il = 1		 		
						
		while N_Straight_Sections <= Straight_Sections:
		    
			index = offset(index_u, jmax-1, m)
			x[index] = X_max - N_Straight_Sections * Delta_s
			y[index] = Y_max
			
# Mirroring:
            
			index_m = offset(index_il, jmax-1, m)
			x[index_m] = x[index]
			y[index_m] = -Y_max
			
			N_Straight_Sections = N_Straight_Sections + 1
			index_u  = index_u  - 1
			index_il = index_il + 1
		return(x,y)
	
	

def boundry(x,y):
	
		(x,y) = profile_boundary(x, y)
		(x,y) = wake_boundary(x, y)
		(x,y) = exit_boundary(x, y)
		(x,y) = outer_boundary(x, y)
		
		return(x,y)


#******************************************************************************************************
    
#linear interpolation along eta or xi coordinate

# linear interpolation along  eta direction:
def interp_eta(v):


    for i in range(1,imax-1): 
    
        for j in range(1,jmax-1): 
            
            v[ offset( i, j, m ) ] = v[ offset(i,j-1,m)] +  ((v[ offset(i, (jmax - 1), m ) ] -  v[ offset(i,j-1,m) ]) / ((jmax-1) - (j-1)))

    
    return v 


# linear interpolation along xi direction:
def interp_xi(v):
    for j in range(1,jmax-1): 
    
        for i in range(1,imax-1): 
            
            v[ offset( i, j, m ) ] = v[ offset(i-1,j,m)] +  ((v[ offset((imax-1), j, m ) ] -  v[ offset(i-1,j,m) ]) / ((imax-1) - (i-1))) 
             
            
    
   
    return v

#******************************************************************************************************
# Bulding the initial grid:
def initialize(x, y):
        
		boundry(x,y)
		x = interp_eta(x)
		y = interp_eta(y)

 #******************************************************************************************************

# Plotting the initial grid:
def initial_plot(x, y):
		a = []
		b = []
		c = []
		d = []        

		for i in range(imax):
			x_initial_2 = []
			y_initial_2 = []

			for j in range(jmax):
				index = offset(i, j, m)


				x_initial[index] = x[index]
				y_initial[index] = y[index]                 
				x_initial_2.append(x_initial[index])
				y_initial_2.append(y_initial[index])
			a.append(x_initial_2)                
			b.append(y_initial_2)

		for j in range(jmax):
			qx = []
			qy = []
			for i in range(imax):
				index = offset(i, j, m)
				qx.append(x[index])
				qy.append(y[index])
			c.append(qx)                
			d.append(qy)
            
		plt.xlabel('x')
		plt.ylabel('y')                
		plt.title('initial grid')    
		plt.plot(a,b,'b',c,d,'b')
		#plt.grid(True)
		plt.show()
   




#******************************************************************************************************


#  Derivatives calculation: if dx_dxi  or dy_dxi is calculated than x_eflag is 1 and else
# dx_deta or dy_deta are calculated.

# Calculating first derivatives:
def calc_diff(v,  i,  j,  x_eflag):
    
# xi:
    
    if(x_eflag):	
    
        return ( (v[offset(i+1, j, m)] - v[offset(i-1, j, m)]) / 2 )
    
# eta:
        
    else:	
    
        return ( (v[offset(i, j+1, m)] - v[offset(i, j-1, m)]) / 2 )
    




# Calculating second derivatives:
def calc_2nd_diff(v,  i,  j,  x_eflag):
    
# xi:
	
    if(x_eflag):	
   
        return (v[offset(i+1, j, m)] - 2*v[offset(i, j, m)] + v[offset(i-1, j, m)])
# eta:
        
    else:	
    
        return (v[offset(i, j+1, m)] - 2*v[offset(i, j, m)] + v[offset(i, j-1, m)])



#******************************************************************************************************

# Calculating dx/dxi, dx/deta, dy/dxi and  dy_deta:
def fill_diffs(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta):

     xi = 1
     eta = 0
    
     for i in range(1, imax - 1):
         
        dx_dxi[offset( i, 0, m )] = calc_diff(x, i, 0, xi)
        dy_dxi[offset( i, 0, m )] = calc_diff(y, i, 0, xi)
        dx_dxi[offset( i, jmax - 1, m )] = calc_diff(x, i, jmax - 1, xi)
        dy_dxi[offset( i, jmax - 1, m )] = calc_diff(y, i, jmax - 1, xi)
         
        for j  in range(1, jmax -1):
            
            o = offset(i, j, m)
            dx_dxi[o] = calc_diff(x, i, j, xi)
            dx_deta[o] = calc_diff(x, i, j, eta)
            dy_dxi[o] = calc_diff(y, i, j, xi)
            dy_deta[o] = calc_diff(y, i, j, eta)
            
            if (i == 1):       
                dx_deta[offset( 0, j, m )] = calc_diff(x, 0, j, eta)
                dy_deta[offset( 0, j, m )] = calc_diff(y, 0, j, eta)
                dx_deta[offset( imax - 1, j, m )] = calc_diff(x, imax - 1, j, eta)
                dy_deta[offset( imax - 1, j, m )] = calc_diff(y, imax - 1, j, eta)



#******************************************************************************************************


	
# These functions calculate alpha, beta and  gamma.


#  Calculate alpha:

def calc_alpha( dx_deta, dy_deta):

    return dx_deta*dx_deta + dy_deta*dy_deta


# Calculate beta:

def calc_beta( dx_dxi,  dx_deta, dy_dxi,  dy_deta):

    return dx_dxi*dx_deta + dy_dxi*dy_deta


# Calculate gamma:

def calc_gamma( dx_dxi, dy_dxi):

    return dx_dxi*dx_dxi + dy_dxi*dy_dxi





def fill_alpha_beta_gamma(alpha, beta, gamma, dx_dxi, dx_deta, dy_dxi, dy_deta):

    
    for i in range(1, imax -1):
        for j  in range(1, jmax -1):
        
            index = offset(i, j, m)
            alpha[index] = calc_alpha(dx_deta[index], dy_deta[index])
            beta[index] = calc_beta(dx_dxi[index], dx_deta[index], dy_dxi[index], dy_deta[index])
            gamma[index] = calc_gamma(dx_dxi[index], dy_dxi[index])
        



#******************************************************************************************************	


# Calculate the control functions ϕ(ξ, η) and ψ(ξ, η): 
def fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi):

    
    eta = 0
    xi = 1
	
# ϕ = 0, ψ is calculated from the boundary conditions or ψ and ϕ are calculated from the boundary conditions:
	
    if ( (control_function  == 2 ) or ( control_function == 4 ) ):
	
# xi=const:
        
        for j  in range(1, jmax -1):	
        
#xi = xi_min:
            
            v = offset(0, j, m)
            if ( abs( dy_deta[v] ) > abs( dx_deta[v] ) ):
                psi[v] = -calc_2nd_diff(y, 0, j, eta) / dy_deta[v]
                
            else:
                psi[v] = -calc_2nd_diff(x, 0, j, eta) / dx_deta[v]
				
# xi = xi_max:
                
            v = offset(imax-1, j, m)
            if ( abs( dy_deta[v] ) > abs( dx_deta[v] ) ):
                psi[v] = -calc_2nd_diff(y, imax-1, j, eta) / dy_deta[v]
                
            else:
                psi[v] = -calc_2nd_diff(x, imax-1, j, eta) / dx_deta[v]
        
   
# ψ = 0, ϕ is calculated from the boundary conditions or ψ and ϕ are calculated from the boundary conditions:
	
    if ((control_function == 3) or (control_function == 4)):
	
# eta=const:
        
        for i in range(1, imax -1):	
        
# eta = eta_min:
            
            v = offset(i, 0, m)
            if (abs(dx_dxi[v]) > abs(dy_dxi[v])):
                phi[v] = -calc_2nd_diff(x, i, 0, xi) / dx_dxi[v]
                
            else:
                phi[v] = -calc_2nd_diff(y, i, 0, xi) / dy_dxi[v]
                
# eta = eta_max:
                
            v = offset(i, jmax-1, m)
            if (abs(dx_dxi[v]) > abs(dy_dxi[v])):
                phi[v] = -calc_2nd_diff(x, i, jmax-1, xi) / dx_dxi[v]
                
            else:
                phi[v] = -calc_2nd_diff(y, i, jmax-1, xi) / dy_dxi[v]
       
   
# Interpolation:
                
    interp_xi(psi)
    interp_eta(phi)



#******************************************************************************************************

# Recalculate alpha beta gamma after every iteration:
def fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi):

    fill_diffs(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta)
    fill_alpha_beta_gamma(alpha, beta, gamma, dx_dxi, dx_deta, dy_dxi, dy_deta)
    	


    
#******************************************************************************************************    


# The Solution Scheme:
    
# Calculating operator L:
def L_i_j_n( i,  j, alpha, beta, gamma, x_or_y, phi, psi):

	part_1 = alpha[offset(i, j, m)] * ((x_or_y[offset(i+1, j, m)] - 2*x_or_y[offset(i, j, m)] + x_or_y[offset(i-1, j, m)]) + 0.5*phi[offset(i, j, m)] * (x_or_y[offset(i+1, j, m)] - x_or_y[offset(i-1, j, m)]))
	part_2 = - 0.5*beta[offset(i, j, m)] * (x_or_y[offset(i+1, j+1, m)] - x_or_y[offset(i+1, j-1, m)]- x_or_y[offset(i-1, j+1, m)] + x_or_y[offset(i-1, j-1, m)])
	part_3 = gamma[offset(i, j, m)] * (x_or_y[offset(i, j+1, m)] - 2*x_or_y[offset(i, j, m)] + x_or_y[offset(i, j-1, m)] + 0.5*psi[offset(i, j, m)] * (x_or_y[offset(i, j+1, m)] - x_or_y[offset(i, j-1, m)]))
	
	return part_1 + part_2 + part_3 

 

 
# Calculating the right hand side of the matrix:
def RHS( i,  j, alpha, beta, gamma, x_y, phi, psi):

    return r*w*L_i_j_n(i, j, alpha, beta, gamma, x_y, phi, psi)  
    


    
# Calculating the left hand side of the matrix:
def LHS( i, j, A, B, C,v):

     index = offset(i, j, m)
     a = -v[index]
     b = r + 2*v[index]
     c = -v[index]
     A.append(a)
     B.append(b)
     C.append(c)

	 
#******************************************************************************************************

# Algorithm for 3-diag matrix solution:
def tridiag(a, b, c, d, u, i_s,  ie):

    for  i in range(i_s + 1,ie +1):
    
        if(b[i-1] == 0.):
            return(0)
			
        beta = a[i] / b[i-1]
        b[i] = b[i] - c[i-1] * beta
        d[i] = d[i] - d[i-1] * beta
    

    u[ie] = d[ie] / b[ie]
    i = ie - 1
    while(i >=  i_s):
    
        u[i] = (d[i] - c[i] * u[i+1]) / b[i]
        i = i - 1
        
#******************************************************************************************************        


# Calculate the initial RHS for Sx and Sy:
def start_SX_SY(Sx, Sy, alpha, beta, gamma, x, y, phi, psi):

    for  i in range(1,imax-1):
    
        for j in range(1,jmax-1):
        
            index = offset(i, j, m)
            Sx[index] = RHS(i, j, alpha, beta, gamma, x, phi, psi)
            Sy[index] = RHS(i, j, alpha, beta, gamma, y, phi, psi)
            
            
#******************************************************************************************************        

# Sweep 1 (fx,fy):
def fx_fy(alpha, U, Sx, Sy):

	 for j in range(1,jmax-1):
         
# fx sweep:
			
			A = [0]
			B = [1]
			C = [0]
			D = [0]
		
			for  i in range(1,imax-1):
        
				LHS(i, j, A, B, C, alpha)
				D.append(Sx[offset(i, j, m)])
				
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)
			
			
			tridiag(A, B, C, D, U, 0, imax-1)
            
			for  i in range(imax):
				V = offset(i, j, m)
				Sx[V] = U[i]
        
		
		
# fy sweep:
		
			A = [0]
			B = [1]
			C = [0]
			D = [0]
			
			for  i in range(1,imax-1):
        
				LHS(i, j, A, B, C, alpha)
				D.append(Sy[offset(i, j, m)])
        
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)

			tridiag(A, B, C, D, U, 0, imax-1)
			
			for  i in range(imax):
				V = offset(i, j, m)
				Sy[V] = U[i]
				
#******************************************************************************************************				
# Sweep 2 (Cx Cy):  
def Cx_Cy(gamma, U, Sx, Sy):

	
	 for i in range(1,imax-1):
         
# Cx sweep:
			
			A = [0]
			B = [1]
			C = [0]
			D = [0]
		
			for  j in range(1,jmax-1):
        
				LHS(i, j, A, B, C, gamma)
				D.append(Sx[offset(i, j, m)])
				
				
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)
			

			
			tridiag(A, B, C, D, U, 0, jmax-1)
			
	
			
            
			for  j in range(jmax):
				V = offset(i, j, m)
				Sx[V] = U[j]
        
		
		
# Cy sweep:
		
			A = [0]
			B = [1]
			C = [0]
			D = [0]
			
			for  j in range(1,jmax-1):
        
				LHS(i, j, A, B, C, gamma)
				D.append(Sy[offset(i, j, m)])
        
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)

			tridiag(A, B, C, D, U, 0, jmax-1)
			
			for  j in range(jmax):
				V = offset(i, j, m)
				Sy[V] = U[j]
                
                
                
 #******************************************************************************************************
# Calculate the first residual vector element:                            
def calc_R_0(alpha, beta, gamma, x, y, phi, psi):

	L_x = []
	L_y = []

	for i in range(1,imax -1  ):
	
		for j in range(1,jmax - 1):
		
			L_x.append(abs(L_i_j_n(i, j, alpha, beta, gamma, x, phi, psi)))
			L_y.append(abs(L_i_j_n(i, j, alpha, beta, gamma, y, phi, psi)))	



	L_x_max = max(L_x)
	L_y_max = max(L_y)
	Rx_0 = L_x_max
	Ry_0 = L_y_max
    
	return(Rx_0, Ry_0)
         
         
#******************************************************************************************************
# Calculate the  residual vector element for each step:
def calc_residual(alpha, beta, gamma, x, y, phi, psi, Rx_0,Ry_0):
	
	L_x = []
	L_y = []

	for i in range(imax - 1):
	
		for j in range(jmax - 1):
		
			L_x.append(abs(L_i_j_n(i, j, alpha, beta, gamma, x, phi, psi)))
			L_y.append(abs(L_i_j_n(i, j, alpha, beta, gamma, y, phi, psi)))	



	L_x_max = max(L_x)
	L_y_max = max(L_y)
	
        
	rx = (math.log10( L_x_max / Rx_0 ))
	ry = (math.log10( L_y_max / Ry_0 ))


	return(rx,ry)




#******************************************************************************************************

# Advance the x and y solution:
def advancement_x_y(x, y, Sx, Sy):
    for  i  in range(1, imax -1):
    
        for j in range(1,jmax-1): 
       
            V = offset(i, j, m)
            x[V] += Sx[V]
            y[V] += Sy[V]


#******************************************************************************************************

			
# Main solution of the program: 				
def step_func(alpha, beta, gamma, x, y, phi, psi, U, Sx, Sy, dx_dxi, dx_deta, dy_dxi, dy_deta):	
	Rx = []
	Ry = []
	(Rx_0, Ry_0) = calc_R_0(alpha, beta, gamma, x, y, phi, psi) 
	num_iter = 0
	while True:			
		start_SX_SY(Sx, Sy, alpha, beta, gamma, x, y, phi, psi)
		fx_fy(alpha, U, Sx, Sy)
		Cx_Cy(gamma, U, Sx, Sy)
		(rx,ry ) = calc_residual(alpha, beta, gamma, x, y, phi, psi, Rx_0,Ry_0)
		Rx.append(rx)
		Ry.append(ry)
		advancement_x_y(x, y, Sx, Sy)
		fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi)
		num_iter =  num_iter + 1
		if(((rx < -6) and (ry < -6)) or (num_iter > 4000)):
			break
        
	return(Rx,Ry,num_iter)	
    
#******************************************************************************************************

# Final plots for diffrent diffrents control functions and r:        
def finel_plot_r(X_Y,Rplot_x,Rplot_y,control_function):

		for x_y in X_Y:
			x = x_y[0]
			y = x_y[1]
			t = x_y[2]          			  
			a = []
			b = []
			c = []
			d = []        

			for i in range(imax):
				x_initial_2 = []
				y_initial_2 = []

				for j in range(jmax):
					index = offset(i, j, m)             
					x_initial_2.append(x[index])
					y_initial_2.append(y[index])
				a.append(x_initial_2)                
				b.append(y_initial_2)

			for j in range(jmax):
				qx = []
				qy = []
				for i in range(imax):
					index = offset(i, j, m)
					qx.append(x[index])
					qy.append(y[index])
				c.append(qx)                
				d.append(qy)
                

			if (control_function == 1):
				plt.xlabel('x')
				plt.ylabel('y')
				plt.title(' ϕ = ψ = 0 without control (r = %.3f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='w = 1.5')
				plt.show()	
 

					
					
			elif (control_function == 2):
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title(' ϕ = 0, ψ is calculated from the boundary conditions (r = %.3f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='w = 1.5')
				plt.show()	


					
							
					
			elif (control_function == 3):
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title(' ψ = 0, ϕ is calculated from the boundary conditions (r = %.3f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='w = 1.5')
				plt.show()	


									
			else:
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title('ψ and ϕ are calculated from the boundary conditions (r = %.3f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='w = 1.5')
				plt.show()			
					





		if (control_function == 1):
			plt.figure()    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ϕ = ψ = 0 without control)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='r:')
			#figg.append(plt.gcf())            
			plt.show()	
				
			plt.figure()        
			for j in Rplot_y:                
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ϕ = ψ = 0 without control)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='r:')  
			#figg.append(plt.gcf())            
			plt.show()
			plt.close()	
				
		elif (control_function == 2):
			plt.figure()    
			for i in Rplot_x:                
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ϕ = 0, ψ is calculated from the boundary conditions)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='r:')
			#figg.append(plt.gcf())            
			plt.show()	
				
			plt.figure()        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ϕ = 0, ψ is calculated from the boundary conditions)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='r:')
			#figg.append(plt.gcf())            
			plt.show()
				
						
				
		elif (control_function == 3):


			plt.figure()    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ψ = 0, ϕ is calculated from the boundary conditions)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='r:')
			#figg.append(plt.gcf())            
			plt.show()	
				
			plt.figure()        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ψ = 0, ϕ is calculated from the boundary conditions)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='r:') 
			#figg.append(plt.gcf())            
			plt.show()
								
		else:			
				
			plt.figure()    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ψ and ϕ are calculated from the boundary conditions)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='r:')
			#figg.append(plt.gcf())            
			plt.show()	
				
			plt.figure()        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ψ and ϕ are calculated from the boundary conditions)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='r:')
			#figg.append(plt.gcf())            
			plt.show()
   

                
#******************************************************************************************************
# Final plots for diffrent diffrents control functions and w: 
def finel_plot_w(X_Y,Rplot_x,Rplot_y,control_function):
	
		for x_y in X_Y:
			x = x_y[0]
			y = x_y[1]
			t = x_y[2]          			  
			a = []
			b = []
			c = []
			d = []        

			for i in range(imax):
				x_initial_2 = []
				y_initial_2 = []

				for j in range(jmax):
					index = offset(i, j, m)             
					x_initial_2.append(x[index])
					y_initial_2.append(y[index])
				a.append(x_initial_2)                
				b.append(y_initial_2)

			for j in range(jmax):
				qx = []
				qy = []
				for i in range(imax):
					index = offset(i, j, m)
					qx.append(x[index])
					qy.append(y[index])
				c.append(qx)                
				d.append(qy)
                
			
			if (control_function == 1):
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title(' ϕ = ψ = 0 without control (w = %.1f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='r = 0.01')
				plt.show()

					
					
			elif (control_function == 2):
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title(' ϕ = 0, ψ is calculated from the boundary conditions (w = %.1f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='r = 0.01')
				plt.show()	


						
								
						
			elif (control_function == 3):
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title(' ψ = 0, ϕ is calculated from the boundary conditions (w = %.1f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='r = 0.01')
				plt.show()	


										
			else:
				plt.xlabel('x')
				plt.ylabel('y')                
				plt.title('ψ and ϕ are calculated from the boundary conditions (w = %.1f)' % t)       
				plt.plot(a,b,'b',c,d,'b')
				plt.legend(title='r = 0.01')
				plt.show()
					




		if (control_function == 1):
			#plt.figure(1)    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ϕ = ψ = 0 without control)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='w:')    
			plt.show()	
				
			#plt.figure(2)        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ϕ = ψ = 0 without control)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='w:')            
			plt.show()
			plt.close()	
				
		elif (control_function == 2):


			#plt.figure(3)    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ϕ = 0, ψ is calculated from the boundary conditions)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='w:')    
			plt.show()	
				
			#plt.figure(4)        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ϕ = 0, ψ is calculated from the boundary conditions)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='w:')            
			plt.show()
				
						
				
		elif (control_function == 3):


			#plt.figure(5)    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ψ = 0, ϕ is calculated from the boundary conditions)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='w:')    
			plt.show()	
				
			#plt.figure(6)        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ψ = 0, ϕ is calculated from the boundary conditions)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='w:')            
			plt.show()
								
		else:			
				
			#plt.figure(7)    
			for i in Rplot_x:
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Rx')                 
				plt.title('Rx (ψ and ϕ are calculated from the boundary conditions)')
				t = i[1]
				plt.plot(i[0],label= t)
			plt.legend(title='w:')    
			plt.show()	
				
			#plt.figure(8)        
			for j in Rplot_y:      
				plt.grid(True)
				plt.xlabel('n')
				plt.ylabel('Ry')                
				plt.title('Ry (ψ and ϕ are calculated from the boundary conditions)')
				t = j[1]            
				plt.plot(j[0],label=t)
			plt.legend(title='w:')            
			plt.show()


#******************************************************************************************************

		
def bar_plot_r(N_iteraion,control_function):
				
				x_value = []
				y_value = []
				for i in N_iteraion:
					numi = i[0]
					ri = i[1]
					x_value.append(ri)
					y_value.append(numi)
                    
				import numpy as np	
				n_groups = len(x_value)
				index = np.arange(n_groups)
				plt.bar(index, y_value, tick_label=x_value, align='center')
					
				if (control_function == 1):
					plt.xlabel('r')
					plt.ylabel('number of iterations')
					plt.title(' ϕ = ψ = 0, w = 1.5')       
					plt.show()	
 

					
					
				elif (control_function == 2):
						plt.xlabel('r')
						plt.ylabel('number of iterations')                
						plt.title(' ϕ = 0, ψ = const, w = 1.5')       
						plt.show()	


						
								
						
				elif (control_function == 3):
						plt.xlabel('r')
						plt.ylabel('number of iterations')                
						plt.title(' ψ = 0, ϕ = const, w = 1.5')       
						plt.show()	


										
				else:
					plt.xlabel('r')
					plt.ylabel('number of iterations')              
					plt.title(' ψ and ϕ, w = 1.5')       
					plt.show()		



#******************************************************************************************************
					
def bar_plot_w(N_iteraion,control_function):
				
				x_value = []
				y_value = []
				for i in N_iteraion:
					numi = i[0]
					wi = i[1]
					x_value.append(wi)
					y_value.append(numi)
                    
				import numpy as np	
				n_groups = len(x_value)
				index = np.arange(n_groups)
				plt.bar(index, y_value, tick_label=x_value, align='center')
					
				if (control_function == 1):
					plt.xlabel('w')
					plt.ylabel('number of iterations')
					plt.title(' ϕ = ψ = 0, r = 0.01')       
					plt.show()	
 

					
					
				elif (control_function == 2):
						plt.xlabel('w')
						plt.ylabel('number of iterations')                
						plt.title(' ϕ = 0, ψ = const, r = 0.01')       
						plt.show()	


						
								
						
				elif (control_function == 3):
						plt.xlabel('w')
						plt.ylabel('number of iterations')                
						plt.title(' ψ = 0, ϕ = const, r = 0.01')       
						plt.show()	


										
				else:
					plt.xlabel('w')
					plt.ylabel('number of iterations')              
					plt.title('ψ and ϕ, r = 0.01')       
					plt.show()





#******************************************************************************************************




# Main porgram:
                    
# Creating the initiail grid plot:
(imax, jmax, iTEL, iLE, iTEU, t, Delta_y, XSF, YSF) = inputs()
m = imax
n = jmax
max_ij = max(imax,jmax)
control_function_arry = [1, 2, 3, 4]
ar = [0.002, 0.005, 0.01, 0.05]
aw = [1,1.5,2]
l = m * n
x = [None] * l
y = [None] * l
x_initial = [None] * l
y_initial = [None] * l
dx_dxi = [0] * l
dx_deta = [0] * l
dy_dxi = [0] * l
dy_deta = [0] * l
alpha =	[0] * l
beta = [0] * l
gamma = [0] * l
phi = [0] * l
psi = [0] * l
Sx =  [0] * l
Sy =  [0] * l
U =	[None] * max_ij

initialize(x, y)
initial_plot(x, y)




# Grid genration of diffrents control functions and r:								
for control in control_function_arry:
	Rplot_x = []
	Rplot_y = []
	X_Y = []
	N_iteraion = []
	w = 1.5
	control_function = control
				
	for qq  in ar:
		r = qq
		l = m * n
		x = [None] * l
		y = [None] * l
		x_initial = [None] * l
		y_initial = [None] * l
		dx_dxi = [0] * l
		dx_deta = [0] * l
		dy_dxi = [0] * l
		dy_deta = [0] * l
		alpha =	[0] * l
		beta = [0] * l
		gamma = [0] * l
		phi = [0] * l
		psi = [0] * l
		Sx =  [0] * l
		Sy =  [0] * l
		U =	[None] * max_ij  
		initialize(x, y)
		fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi)
		fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi)             
		(Rx_0, Ry_0) = calc_R_0(alpha, beta, gamma, x, y, phi, psi)
		(Rx, Ry, num_iter) = step_func(alpha, beta, gamma, x, y, phi, psi, U, Sx, Sy, dx_dxi, dx_deta, dy_dxi, dy_deta)
		Rplot_x.append((Rx,r))
		Rplot_y.append((Ry,r))
		X_Y.append((x,y,r))
		N_iteraion.append((num_iter, r))
        
	bar_plot_r(N_iteraion,control_function)	   
	finel_plot_r(X_Y,Rplot_x,Rplot_y,control_function)
    
    

# Grid genration of diffrents control functions and w:		
for control in control_function_arry:
	Rplot_x = []
	Rplot_y = []
	X_Y = []
	N_iteraion = []
	r = 0.01
	control_function = control
			
	for qw  in aw:
		w = qw
		l = m * n
		x = [None] * l
		y = [None] * l
		x_initial = [None] * l
		y_initial = [None] * l
		dx_dxi = [0] * l
		dx_deta = [0] * l
		dy_dxi = [0] * l
		dy_deta = [0] * l
		alpha =	[0] * l
		beta = [0] * l
		gamma = [0] * l
		phi = [0] * l
		psi = [0] * l
		Sx =  [0] * l
		Sy =  [0] * l
		U =	[None] * max_ij        
		initialize(x, y)
		fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi)
		fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi)             
		(Rx_0, Ry_0) = calc_R_0(alpha, beta, gamma, x, y, phi, psi)
		(Rx, Ry, num_iter) = step_func(alpha, beta, gamma, x, y, phi, psi, U, Sx, Sy, dx_dxi, dx_deta, dy_dxi, dy_deta)
		Rplot_x.append((Rx,w))
		Rplot_y.append((Ry,w))
		X_Y.append((x,y,w))
		N_iteraion.append((num_iter, w))
        
	bar_plot_w(N_iteraion,control_function)	   
	finel_plot_w(X_Y,Rplot_x,Rplot_y,control_function) 
    
    
#free the memory:
del(Rplot_x) 
del(Rplot_y) 
del(X_Y)
del(N_iteraion)
del(x) 
del(y )
del(x_initial)
del(y_initial)
del(dx_dxi)
del(dx_deta)
del(dy_dxi)
del(dy_deta)
del(alpha)
del(beta)
del(gamma)
del(phi )
del(psi)
del(Sx)
del(Sy)
del(U)  
del(Rx)        
del(Ry)   