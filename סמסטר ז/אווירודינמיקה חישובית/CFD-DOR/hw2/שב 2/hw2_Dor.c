#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.1415
#define GAMMA 1.4
#define R 287
#define T_sl 288.15
#define L 6.5E-3
#define p_sl 101325.0
#define g 9.81

// Globals:
int i_max, j_max, iTEL, iLE, iTEU, N, i, j;
float dt, FSMACH, AOA, h, epse, epsi, a_oo, u_oo, v_oo, p_oo, rho_oo, e_oo;


//***********************************************************************************************************************

// index offset for 2D arrays:
int offset_2D(int i,int j,int ni)
{
    return (j*ni + i);
}


// index offset for 3D arrays:
int offset_3D(int i, int j, int n, int imax, int jmax)
{

    return ( ( n*jmax + j )*imax + i );
}

//*********************************************************************************

// Return the absolute value:
float absolute(float v)
{
    return v < 0 ? -v : v;
}

//***********************************************************************************************************************


void input()
{
	i_max=51;
	j_max=26;
	iTEL=12;
	iLE=26;
	iTEU=40;
	FSMACH=1.5; //Free Stream Mach Number
	AOA=0.0; //angle of attack
	h=0.0; //[m]
	N = 2500; // number of iterations
	epse=0.06; // explicit smoothing coefficient
	epsi=0.06;
	dt=0.00005;
	p_oo = 101325;
    rho_oo = 12249;
	
	
	a_oo = sqrt(GAMMA*p_oo/rho_oo);
    u_oo = FSMACH * a_oo * cos(AOA*PI/180.0);
    v_oo = FSMACH * a_oo * sin(AOA*PI/180.0);
    e_oo = p_oo / (GAMMA - 1) + 0.5*rho_oo*pow(FSMACH*a_oo,2.0);

	

	
	
}

//***********************************************************************************************************************

void input_grid(float *x,float *y)
{
    FILE *file_x,*file_y;
    int index2D;
    file_x = fopen("GridX_for_HW2.txt","rt");
    file_y = fopen("GridY_for_HW2.txt","rt");
    for (i=0; i<i_max; i++)
        {
        for (j=0; j<j_max; j++)
            {
			index2D = offset_2D(i,j,i_max);
            fscanf(file_x,"%f",&x[index2D)])
          
            fscanf(file_y,"%f",&y[index2D])
             
            }
        }
	

    fclose(file_x);
    fclose(file_y);
}



//***********************************************************************************************************************


// Initializing the Q vector for free stream conditions:
void Initialize(float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q)
{
	
    int index;

    for (i=0; i<i_max; i++)
    {
        for (j=0; j<j_max; j++)
        {
			index =  j * i_max + i;
            rho_Q[index] = rho_oo;
            rho_u_Q[index] = rho_oo * u_oo;
			rho_v_Q[index] = rho_oo * v_oo;
            e_Q[index] = e_oo;
        }
    }
	
	
}


//***********************************************************************************************************************
void calc_diffs(float *V, int i_max, int j_max, float *X_Y_eta, float *X_Y_xi)

{
// eta direction: 
    for(i=0;i<i_max;i++)
    {
	// Forward difference:
        X_Y_eta[offset_2D(i,0,i_max)]=0.5*(4*V[offset_2D(i,1,i_max)]-3*V[offset_2D(i,0,i_max)]-V[offset_2D(i,2,i_max)]);
		
	// Central difference:	
        for(j=1;j<j_max-1;j++)
        {
            X_Y_eta[offset_2D(i,j,i_max)]=(V[offset_2D(i,j+1,i_max)]-V[offset_2D(i,j-1,i_max)])/2.0;
        }
	// Backward difference:
        X_Y_eta[offset_2D(i,j_max-1,i_max)]=0.5*(3*V[offset_2D(i,j_max-1,i_max)]-4*V[offset_2D(i,j_max-2,i_max)]+V[offset_2D(i,j_max-3,i_)]);
    }
	
	
// xi direction:
    for(j=0;j<j_max;j++)
    {
	// Forward difference:
        X_Y_xi[offset_2D(0,j,i_max)]=0.5*(4*V[offset_2D(1,j,i_max)]-3*V[offset_2D(0,j,i_max)]-V[offset_2D(2,j,i_max)]);
		
	// Central difference:
        for(i=1;i<i_max-1;i++)
        {
            X_Y_xi[offset_2D(i,j,i_max)]=(V[offset_2D(i+1,j,i_max)]-V[offset_2D(i-1,j,i_max)])/2.0;
        }
	// Backward difference:
        X_Y_xi[offset_2D(i_max-1,j,i_max)]=0.5*(3*V[offset_2D(i_max-1,j,i_max)]-4*V[offset_2D(i_max-2,j,i_max)]+V[offset_2D(i_max-3,j,i)]);
    }
}

//***********************************************************************************************************************

void Jscobian(float *J,float *x,float *y,float *xi_x,float *xi_y,float *eta_x,float *eta_y)
{
    int i,j,index;
    float *x_eta,*x_xi,*y_eta,*y_xi;
    y_eta=(float *)malloc(i_max*j_max* sizeof(float));
    x_eta=(float *)malloc(i_max*j_max* sizeof(float));
    y_xi=(float *)malloc(i_max*j_max* sizeof(float));
    x_xi=(float *)malloc(i_max*j_max* sizeof(float));
	
    calc_diffs(x,i_max,j_max,x_eta,x_xi);
    calc_diffs(y,i_max,j_max,y_eta,y_xi);
	
	
     for(i=0;i<i_max;i++)
    {
        for(j=0;j<j_max;j++)
        {
            index = offset_2D(i,j,i_max);
            J[index] = 1.0 / (x_xi[index]*y_eta[index] - x_eta[index]*y_xi[index]);
            xi_x[index] = J[index] * y_eta[index];
            xi_y[index] = -J[index] * x_eta[index];
            eta_x[index] = -J[index] * y_xi[index];
            eta_y[index] = J[index] * x_xi[index];
        }
    }
	
	free(y_eta);
	free(x_eta);
	free(y_xi);
	free(x_xi);
}
//***********************************************************************************************************************

// Adiabatic wall for the airfoil (j = jmin, i = itel . . . iteu):
void Boundry_on_wall(float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, float *xi_x, float *xi_y, float *eta_x, float *eta_y)
{
    int index3D,index2D,indexiTEU,indexiTEL;
    float u, v, U, V, det, u_0, v_0, rho_0, p_0, e_0, en_TE;
    float TEL[4], TEU[4], TE[4];
    for (i=iTEL-1;i<iTEU;i++)
    {
        /*u=Q[offset_3D(i,1,1,i_max,j_max)]/Q[offset_3D(i,1,0,i_max,j_max)];
        v=Q[offset_3D(i,1,2,i_max,j_max)]/Q[offset_3D(i,1,0,i_max,j_max)];*/
		
		j = 1;
		index3D =  j * i_max + i;
		u = rho_u_Q[index] / rho_Q[index];
		v = rho_v_Q[index] / rho_Q[index];
		
	// Contrav_ariant velocities:
		index2D = offset_2D(i,1,i_max);
        U = xi_x[index2D] * u + xi_y[index2D] * v;
        V = eta_x[index2D] * u + eta_y[index2D] * v;
		
	// Calculating u0 v0 rho0 p0 e0 with u1 v1 rho1 p1 e1: 	
		index2D = offset_2D(i,0,i_max);
        det = xi_x[index2D] * eta_y[index2D] - eta_x[index2D] * xi_y[index2D];
        u_0 = eta_y[index2D] * U / det;
        v_0 = -eta_x[index2D] * U / det;
        rho_0 = rho_Q[index3D];
        p_0 = (GAMMA-1) * e_Q[index3D] - 0.5 * rho_0 * (u*u + v*v);
        e_0 = (p_0 / ( GAMMA - 1 )) + 0.5 * rho_0 * (u_0*u_0 + v_0*v_0);
		
	// Entering the values to Q vector:	
		j = 0;
		index3D =  j * i_max + i;
		rho_Q[index3D] = rho_0;
        rho_u_Q[index3D] = rho_0 * u_0;
	    rho_v_Q[index3D] = rho_0 * v_0;
        e_Q[index3D] = e_0;
		
		if (i==(iTEL-1))
        {
            TEL[0] = rho_0;
			TEL[1] = u_0;
			TEL[2] = v_0;
			TEL[3] = (GAMMA-1) * (e_0 -0.5 * rho_0 * (u_0*u_0 + v_0*v_0));			
        }
		
		
        if (i==(iTEU-1))
        {			
             TEU[0] = rho_0;
             TEU[1] = u_0;
             TEU[2] = v_0;
             TEU[3] = (GAMMA-1) * (e_0 -0.5 * rho_0 * (u_0*u_0 + v_0*v_0));	 
        }
		
		
		
    }

// Average:
    for (i=0;i<4;i++)
    {
        TE[i] = 0.5 * (TEL[i] + TEU[i]);
    }
	
	en_TE = TE[3] / (GAMMA-1) + 0.5 * TE[0] * (TE[1]*TE[1] + TE[2]*TE[2]);
	
	j = 0;
	
	i = iTEU - 1;
	indexiTEU =  j * i_max + i;
    rho_Q[indexiTEU] = TE[0];
    rho_u_Q[indexiTEU] = TE[0] * TE[1];
    rho_v_Q[indexiTEU] = TE[0] * TE[2];
    e_Q[indexiTEU] = en_TE;
	
	i = iTEL - 1;
	indexiTEL =  j * i_max + i;
    rho_Q[indexiTEL] = TE[0];
    rho_u_Q[indexiTEL] = TE[0] * TE[1];
    rho_v_Q[indexiTEL] = TE[0] * TE[2];
    e_Q[indexiTEL] = en_TE;
	
}


	
	
//***********************************************************************************************************************	
// Cut type condition for j = jmin, i = 1 . . . itel, j = jmin and i = iteu . . . imax
void Cut_Type(float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q)
{
	int index3D_i, index3D_imax_minus_i;
    for (i=1; i<iTEL-1; i++)
    {
		j = 1;
		
		index3D_i = j * i_max + i;
		index3D_imax_minus_i = j * i_max + (i_max-1-i);
		
	// Left side for j = 0 and i, right side is average between i and i_max-1-i:	
		rho_Q[i] = 0.5 * (rho_Q[index3D_i] + rho_Q[index3D_imax_minus_i]);
		rho_u_Q[i] = 0.5 * (rho_u_Q[index3D_i] + rho_u_Q[index3D_imax_minus_i]);
		rho_v_Q[i] = 0.5 * (rho_v_Q[index3D_i] + rho_v_Q[index3D_imax_minus_i]);
		e_Q[i] = 0.5 * (e_Q[index3D_i] + e_Q[index3D_imax_minus_i]);

		
	
		j = 0;

		index3D_imax_minus_i = j * i_max + (i_max-1-i);
		
	// Left side for j = 0 and i_max-1-i, right side is for j = 0 and i:	
		rho_Q[index3D_imax_minus_i] = rho_Q[i];
		rho_u_Q[index3D_imax_minus_i] = rho_u_Q[i];
		rho_v_Q[index3D_imax_minus_i] = rho_v_Q[i];
		e_Q[index3D_imax_minus_i] = e_Q[i];
    }
	
}

//***********************************************************************************************************************

	
// Outlet conditions for i = imin and i = imax:
void Outlet_Boundry(float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q)
{
	int index3D_i, index3D_imax;
    for (j=1;j<j_max;j++)
    {
		i = 1;
		index3D_i = j * i_max + i;
		
	// Left side for i = 0, right side for i = 1:	
		rho_Q[j*i_max] = rho_Q[index3D_i];
		rho_u_Q[j*i_max] = rho_u_Q[index3D_i];
		rho_v_Q[j*i_max] = rho_v_Q[index3D_i];
		e_Q[j*i_max] = e_Q[index3D_i];
		
		
		i = i_max-1;
		index3D_imax = j * i_max + i;
		
	// Left side for i = i_max-1, right side for i = i_max-2:
		rho_Q[index3D_imax] = rho_Q[index3D_imax - 1];
		rho_u_Q[index3D_imax] = rho_u_Q[index3D_imax - 1];
		rho_v_Q[index3D_imax] = rho_v_Q[index3D_imax - 1];
		e_Q[index3D_imax] = e_Q[index3D_imax - 1];
    }

}

//***********************************************************************************************************************

// Fill the boundary conditions:
void fill_Boundry(float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, float *xi_x, float *xi_y, float *eta_x, float *eta_y)
{
	Boundry_on_wall(rho_Q, rho_u_Q, rho_v_Q, e_Q, xi_x, xi_y, eta_x, eta_y);
	Cut_Type(rho_Q, rho_u_Q, rho_v_Q, e_Q);
	Outlet_Boundry(rho_Q, rho_u_Q, rho_v_Q, e_Q);
}

//***********************************************************************************************************************

// Calculate E and F vectors:
void calc_E_F(float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, float *J, float *xi_x, float *xi_y, float *eta_x,float *eta_y,float *rho_E, float *rho_u_E, float *rho_v_E, float *e_E, float *rho_F, float *rho_u_F, float *rho_v_F, float *e_F)
{
    int index2D, index3D;
    float rho, u, v, en, J1, p, U, V;
	
    for (i=0;i<i_max;i++)
    {
        for (j=0;j<j_max;j++)
        {
			index3D = j * i_max + i;
            rho = rho_Q[index3D];
            u = rho_u_Q[index3D] / rho;
            v = rho_v_Q[index3D] / rho;
            en = e_Q[index3D];
			
			index2D = offset_2D(i,j,i_max);
            J1 = J[index2D];
            p = (GAMMA-1) * (en - 0.5 * rho * (u*u + v*v));
            U = xi_x[index2D] * u + xi_y[index2D] * v;
            V = eta_x[index2D] * u + eta_y[index2D] * v;

		// E calculation:	
		
            rho_E[index3D] = rho * U / J1;
            rho_u_E[index3D] = (rho * u * U + xi_x[index2D] * p) / J1;
            rho_v_E[index3D] = (rho * v * U + xi_y[index2D] * p) / J1;
            e_E[index3D] = (en+p) * U / J1;
			
			
		// F calculation:
		
            rho_F[index3D] = rho * V/J1;
            rho_u_F[index3D] = (rho * u * V + eta_x[index2D] * p) / J1;
            rho_v_F[index3D] = (rho * v * V+eta_y[index2D] * p) / J1;
            e_F[index3D] = (en + p) * V / J1;
        }
    }
}
		
//***********************************************************************************************************************

// Calculate RHS:
void RHS(float *rho_S, float *rho_u_S, float *rho_v_S, float *e_S, float *rho_E, float *rho_u_E, float *rho_v_E, float *e_E, float *rho_F, float *rho_u_F, float *rho_v_F, float *e_F, dt)
{

    int index3D, index3D_JUP, index3D_JDOWN;
    for (i=1;i<i_max-1;i++)
    {
        for (j=1;j<j_max-1;j++)
        {
			index3D = j * i_max + i;
			index3D_JUP = (j + 1) * i_max + i;
			index3D_JDOWN = (j - 1) * i_max + i;
			
			rho_S[index3D] = -0.5 * dt * (rho_E[index3D + 1] - rho_E[index3D - 1]);
            rho_u_S[index3D] = -0.5 * dt * (rho_u_E[index3D + 1] - rho_u_E[index3D - 1]);
            rho_v_S[index3D] = -0.5 * dt * (rho_v_E[index3D + 1] - rho_v_E[index3D - 1]);
            e_S[index3D] = -0.5 * dt * (e_E[index3D + 1] - e_E[index3D - 1]);
			
			rho_S[index3D] += -0.5 * dt * (rho_F[index3D_JUP] - rho_F[index3D_JDOWN]);
            rho_u_S[index3D] += -0.5 * dt * (rho_u_F[index3D_JUP] - rho_u_F[index3D_JDOWN]);
            rho_v_S[index3D] += -0.5 * dt * (rho_v_F[index3D_JUP] - rho_v_F[index3D_JDOWN]);
            e_S[index3D] += -0.5 * dt * (e_F[index3D_JUP] - e_F[index3D_JDOWN]);			
			
        }
    }
}

//***********************************************************************************************************************

int smooth(float *q, float *s, float *jac, float *xx, float *xy, 
           float *yx, float *yy, int id, int jd, float *s2, 
           float *rspec, float *qv, float *dd,
           float epse, float gamma, float fsmach, float dt)
{

    float *rho, *u_vel, *v_vel, *t_e;

    float eratio, smool, gm1, ggm1, cx, cy, eps, ra, u, v, qq, ss, st, 
          qav, qxx, ssfs, qyy;
    int ib, ie, jb, je, i, j, offset, offsetp1, offsetm1, ip, ir, n,
        jp, jr;
    eratio = 0.25 + 0.25 * pow(fsmach + 0.0001,gamma);
    smool = 1.0;
    gm1 = gamma - 1.0;
    ggm1 = gamma * gm1;
    ib = 1;
    ie = id - 1;
    jb = 1;
    je = jd - 1;

    cx = 2.;
    cy = 1.;

    rho = q;
    u_vel = &q[id*jd];
    v_vel = &q[2*id*jd];
    t_e = &q[3*id*jd];

/*     smoothing in xi direction */

    for (j = jb; j < je; j++) {
	for (i = 0; i < id; i++) {
	    offset = id * j + i;
	    eps = epse / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq);
            rspec[i] = eps * (fabs(xx[offset] * u + xy[offset] * v) + sqrt((xx[offset] * xx[offset] + xy[offset] * xy[offset]) * ss + 0.01));
	    qv[i] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (i = ib; i < ie; i++) {
	    ip = i + 1;
	    ir = i - 1;
	    qxx = qv[ip] - qv[i] * 2. + qv[ir];
	    qav = (qv[ip] + qv[i] * 2. + qv[ir]) * .25;
	    dd[i] = eratio * fabs(qxx / qav);
	}

	dd[0] = dd[1];
	dd[id - 1] = dd[id - 2];

	for (n = 0; n < 4; n++) {
	    for (i = ib; i < ie; i++) {
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j) * id + i + 1;
		offsetm1 = (jd * n + j) * id + i - 1;
		s2[i] = q[offsetp1] - 2.0 * q[offset] + q[offsetm1];
	    }

	    s2[0] = s2[1] * -1.;
	    s2[id - 1] = s2[id - 2] * -1.;

	    for (i = ib; i < ie; i++) {
		ip = i + 1;
		ir = i - 1;
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j) * id + i + 1;
		offsetm1 = (jd * n + j) * id + i - 1;
		st = ((dd[ip] + dd[i]) * .5 * (q[offsetp1] - q[offset]) - cx / (cx + dd[ip] + dd[i]) * (s2[ip] - s2[i])) * (rspec[ip] + rspec[i]) + ((dd[ir] + dd[i]) * .5 * (q[offsetm1] - q[offset]) - cx / (cx + dd[ir] + dd[i]) * (s2[ir] - s2[i])) * (rspec[ir] + rspec[i]);
		s[offset] += st * .5 * dt;
	    }
	}
    }

/*     smoothing in eta direction */

    ssfs = 1. / (0.001 + fsmach * fsmach);
    for (i = ib; i < ie; i++) {
	for (j = 0; j < jd; j++) {
	    offset = id * j + i;
	    eps = epse / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq) * (1.0 - smool) + smool * qq * ssfs;
            rspec[j] = eps * (fabs(yx[offset] * u + yy[offset] * v) + sqrt((yx[offset] * yx[offset] + yy[offset] * yy[offset]) * ss + 0.01));
	    qv[j] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (j = jb; j < je; j++) {
	    jp = j + 1;
	    jr = j - 1;
	    qyy = qv[jp] - qv[j] * 2. + qv[jr];
	    qav = (qv[jp] + qv[j] * 2. + qv[jr]) * .25;
	    dd[j] = eratio * fabs(qyy / qav);
	}

	dd[0] = dd[1];
	dd[jd - 1] = dd[jd - 2];

	for (n = 0; n < 4; n++) {
	    for (j = jb; j < je; j++) {
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j + 1) * id + i;
		offsetm1 = (jd * n + j - 1) * id + i;
		s2[j] = q[offsetp1] - 2.0 * q[offset] + q[offsetm1];
	    }

	    s2[0] = s2[1] * -1.;
	    s2[jd - 1] = s2[jd - 2] * -1.;

	    for (j = jb; j < je; j++) {
		jp = j + 1;
		jr = j - 1;
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j + 1) * id + i;
		offsetm1 = (jd * n + j - 1) * id + i;
		st = ((dd[jp] + dd[j]) * .5 * (q[offsetp1] - q[offset]) - cy / (cy + dd[jp] + dd[j]) * (s2[jp] - s2[j])) * (rspec[jp] + rspec[j]) + ((dd[jr] + dd[j]) * .5 * (q[offsetm1] - q[offset]) - cy / (cy + dd[jr] + dd[j]) * (s2[jr] - s2[j])) * (rspec[jr] + rspec[j]);
		s[offset] += st * .5 * dt;
	    }
	}
    }

    return 0;
} /* smooth */


//***********************************************************************************************************************


int smoothx(float *q, float *xx, float *xy, int id, int jd, float *a,
	   float *b, float *c, int j,float *jac, float *drr, float *drp, 
           float *rspec, float *qv, float *dd,
           float epsi, float gamma, float fsmach, float dt)
{

    float *rho, *u_vel, *v_vel, *t_e;

    float eratio, gm1, ggm1, eps, ra, u, v, qq, ss,
          qav, qxx, rr, rp;
    int ib, ie, i, offset, offsetp1, offsetm1, ip, ir, n;
    eratio = 0.25 + 0.25 * pow(fsmach + 0.0001,gamma);
    gm1 = gamma - 1.0;
    ggm1 = gamma * gm1;
    ib = 1;
    ie = id - 1;

    rho = q;
    u_vel = &q[id*jd];
    v_vel = &q[2*id*jd];
    t_e = &q[3*id*jd];

/*     smoothing in xi direction */

        for (i = 0; i < id; i++) {
	    offset = id * j + i;
	    eps = epsi / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq);
            rspec[i] = eps * (fabs(xx[offset] * u + xy[offset] * v) + sqrt((xx[offset] * xx[offset] + xy[offset] * xy[offset]) * ss + 0.01));
	    qv[i] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (i = ib; i < ie; i++) {
	    ip = i + 1;
	    ir = i - 1;
	    qxx = qv[ip] - qv[i] * 2. + qv[ir];
	    qav = (qv[ip] + qv[i] * 2. + qv[ir]) * .25;
	    dd[i] = eratio * fabs(qxx / qav);
	}

	dd[0] = dd[1];
	dd[id - 1] = dd[id - 2];

        for (i = ib; i < ie; i++) {
	    ip = i + 1;
	    ir = i - 1;
	    offset = j * id + i;
	    offsetp1 = j * id + i + 1;
	    offsetm1 = j * id + i - 1;
	    rp = (0.5 * (dd[ip] + dd[i]) + 2.5) * dt * 0.5 * (rspec[ip] + rspec[i]);
	    rr = (0.5 * (dd[ir] + dd[i]) + 2.5) * dt * 0.5 * (rspec[ir] + rspec[i]);
	    qv[i] = (rr + rp) * jac[offset];
	    drr[i] = rr * jac[offsetm1];
	    drp[i] = rp * jac[offsetp1];
	}

	for (n = 0; n < 4; n++) {
	    for (i = ib; i < ie; i++) {
	        offset = (n * 4 + n) * id + i;
		a[offset] -= drr[i];
		b[offset] += qv[i];
		c[offset] -= drp[i];
	    }
        }
    return 0;
} /* smoothx */



//***********************************************************************************************************************


int smoothy(float *q, float *yx, float *yy, int id, int jd, float *a,
	   float *b, float *c, int i,float *jac, float *drr, float *drp, 
           float *rspec, float *qv, float *dd,
           float epsi, float gamma, float fsmach, float dt)
{

    float *rho, *u_vel, *v_vel, *t_e;

    float eratio, smool, gm1, ggm1, eps, ra, u, v, qq, ss, 
          qav, ssfs, qyy, rp, rr;
    int jb, je, j, offset, offsetp1, offsetm1, n,
        jp, jr;
    eratio = 0.25 + 0.25 * pow(fsmach + 0.0001,gamma);
    smool = 1.0;
    gm1 = gamma - 1.0;
    ggm1 = gamma * gm1;
    jb = 1;
    je = jd - 1;

    rho = q;
    u_vel = &q[id*jd];
    v_vel = &q[2*id*jd];
    t_e = &q[3*id*jd];

/*     smoothing in eta direction */

        ssfs = 1. / (0.001 + fsmach * fsmach);
        for (j = 0; j < jd; j++) {
	    offset = id * j + i;
	    eps = epsi / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq) * (1.0 - smool) + smool * qq * ssfs;
            rspec[j] = eps * (fabs(yx[offset] * u + yy[offset] * v) + sqrt((yx[offset] * yx[offset] + yy[offset] * yy[offset]) * ss + 0.01));
	    qv[j] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (j = jb; j < je; j++) {
	    jp = j + 1;
	    jr = j - 1;
	    qyy = qv[jp] - qv[j] * 2. + qv[jr];
	    qav = (qv[jp] + qv[j] * 2. + qv[jr]) * .25;
	    dd[j] = eratio * fabs(qyy / qav);
	}

	dd[0] = dd[1];
	dd[jd - 1] = dd[jd - 2];

        for (j = jb; j < je; j++) {
	    jp = j + 1;
	    jr = j - 1;
	    offset = j * id + i;
	    offsetp1 = (j + 1) * id + i;
	    offsetm1 = (j - 1) * id + i;
	    rp = (0.5 * (dd[jp] + dd[j]) + 2.5) * dt * 0.5 * (rspec[jp] + rspec[j]);
	    rr = (0.5 * (dd[jr] + dd[j]) + 2.5) * dt * 0.5 * (rspec[jr] + rspec[j]);
	    qv[j] = (rr + rp) * jac[offset];
	    drr[j] = rr * jac[offsetm1];
	    drp[j] = rp * jac[offsetp1];
	}

	for (n = 0; n < 4; n++) {
	    for (j = jb; j < je; j++) {
	        offset = (n * 4 + n) * jd + j;
		a[offset] -= drr[j];
		b[offset] += qv[j];
		c[offset] -= drp[j];
	    }
        }
    return 0;
} /* smoothy */

//***********************************************************************************************************************

int btri4s(float *a, float *b, float *c, float *f, int kd, int ks, int ke)
{
  /* Local variables */
  int k, m, n, nd, md;

  float c1, d1, d2, d3, d4, c2, c3, c4, b11, b21, b22, b31, b32, b33, 
    b41, b42, b43, b44, u12, u13, u14, u23, u24, u34;
  
  
  /*   (A,B,C)F = F, F and B are overloaded, solution in F */

  md = 4;
  nd = 4;

  /*   Part 1. Forward block sweep */
  
  for (k = ks; k <= ke; k++)
    {
      
      /*      Step 1. Construct L in B */
      
      if (k != ks) 
	{
	  for (m = 0; m < md; m++) 
	    {
	      for (n = 0; n < nd; n++) 
		{
		  b[k + kd * (m + md * n)] = b[k + kd * (m + md * n)] 
		    - a[k + kd * (m + md * 0)] * b[k - 1 + kd * (0 + md * n)] 
		    - a[k + kd * (m + md * 1)] * b[k - 1 + kd * (1 + md * n)] 
		    - a[k + kd * (m + md * 2)] * b[k - 1 + kd * (2 + md * n)] 
		    - a[k + kd * (m + md * 3)] * b[k - 1 + kd * (3 + md * n)] ;
		}
	    }
	}
      
      /*      Step 2. Compute L inverse (block matrix) */
      
      /*          A. Decompose L into L and U */
      
      b11 = 1. / b[k + kd * (0 + md * 0)];
      u12 = b[k + kd * (0 + md * 1)] * b11;
      u13 = b[k + kd * (0 + md * 2)] * b11;
      u14 = b[k + kd * (0 + md * 3)] * b11;
      b21 = b[k + kd * (1 + md * 0)];
      b22 = 1. / (b[k + kd * (1 + md * 1)] - b21 * u12);
      u23 = (b[k + kd * (1 + md * 2)] - b21 * u13) * b22;
      u24 = (b[k + kd * (1 + md * 3)] - b21 * u14) * b22;
      b31 = b[k + kd * (2 + md * 0)];
      b32 = b[k + kd * (2 + md * 1)] - b31 * u12;
      b33 = 1. / (b[k + kd * (2 + md * 2)] - b31 * u13 - b32 * u23);
      u34 = (b[k + kd * (2 + md * 3)] - b31 * u14 - b32 * u24) * b33;
      b41 = b[k + kd * (3 + md * 0)];
      b42 = b[k + kd * (3 + md * 1)] - b41 * u12;
      b43 = b[k + kd * (3 + md * 2)] - b41 * u13 - b42 * u23;
      b44 = 1. / (b[k + kd * (3 + md * 3)] - b41 * u14 - b42 * u24 
		  - b43 * u34);
      
      /*      Step 3. Solve for intermediate vector */
      
      /*          A. Construct RHS */
      if (k != ks) 
	{
	  for (m = 0; m < md; m++) 
	    {
	      f[k + kd * m] = f[k + kd * m] 
		- a[k + kd * (m + md * 0)] * f[k - 1 + kd * 0] 
		- a[k + kd * (m + md * 1)] * f[k - 1 + kd * 1] 
		- a[k + kd * (m + md * 2)] * f[k - 1 + kd * 2] 
		- a[k + kd * (m + md * 3)] * f[k - 1 + kd * 3];
	    }
	}
      
      /*          B. Intermediate vector */
      
      /*          Forward substitution */
      
      d1 = f[k + kd * 0] * b11;
      d2 = (f[k + kd * 1] - b21 * d1) * b22;
      d3 = (f[k + kd * 2] - b31 * d1 - b32 * d2) * b33;
      d4 = (f[k + kd * 3] - b41 * d1 - b42 * d2 - b43 * d3) * b44;
      
      /*          Backward substitution */
      
      f[k + kd * 3] = d4;
      f[k + kd * 2] = d3 - u34 * d4;
      f[k + kd * 1] = d2 - u23 * f[k + kd * 2] - u24 * d4;
      f[k + kd * 0] = d1 - u12 * f[k + kd * 1] - u13 * f[k + kd * 2] - u14 * d4;
      
      /*      Step 4. Construct U = L ** (-1) * C */
      /*              by columns and store in B */
      
      if (k != ke) 
	{
	  for (n = 0; n < nd; n++) 
	    {

	      /*          Forward substitution */
	      
	      c1 = c[k + kd * (0 + md * n)] * b11;
	      c2 = (c[k + kd * (1 + md * n)] - b21 * c1) * b22;
	      c3 = (c[k + kd * (2 + md * n)] - b31 * c1 - b32 * c2) * 
		b33;
	      c4 = (c[k + kd * (3 + md * n)] - b41 * c1 - b42 * c2 - 
		    b43 * c3) * b44;
	      
	      /*          Backward substitution */
	      
	      b[k + kd * (3 + md * n)] = c4;
	      b[k + kd * (2 + md * n)] = c3 - u34 * c4;
	      b[k + kd * (1 + md * n)] = c2 - u23 * b[k + kd * (2 + md * n)] - u24 * c4;
	      b[k + kd * (0 + md * n)] = c1 - u12 * b[k + kd * (1 + md * n)] 
		- u13 * b[k + kd * (2 + md * n)] - u14 * c4;
	    }
	}
    }
  
  /*   Part 2. Backward block sweep */
  
  if (ke == ks) 
    {
      return 0;
    }

  for (k = ke - 1; k >= ks; --k) 
    {
      for (m = 0; m < md; m++) 
	{
	  f[k + kd * m] = f[k + kd * m] 
	    - b[k + kd * (m + md * 0)] * f[k + 1 + kd * 0] 
	    - b[k + kd * (m + md * 1)] * f[k + 1 + kd * 1] 
	    - b[k + kd * (m + md * 2)] * f[k + 1 + kd * 2] 
	    - b[k + kd * (m + md * 3)] * f[k + 1 + kd * 3];
	}
    }
  
  return 0;
  
} /* btri4s_ */

//***********************************************************************************************************************



void write_grid(int ni, int nj, float *x, float *y, FILE *grid)

{

  fwrite(&ni, sizeof(int), 1, grid);
  fwrite(&nj, sizeof(int), 1, grid);

  fwrite(x, sizeof(float), ni*nj, grid);
  fwrite(y, sizeof(float), ni*nj, grid);

}

//***********************************************************************************************************************

/* Writing the solution file */

void write_q(int ni, int nj, float *q, FILE *qsol, float fsmach, float alpha, float rey, float time)

{


  fwrite(&ni, sizeof(int), 1, qsol);
  fwrite(&nj, sizeof(int), 1, qsol);

  fwrite(&fsmach, sizeof(float), 1, qsol);
  fwrite(&alpha, sizeof(float), 1, qsol);
  fwrite(&rey, sizeof(float), 1, qsol);
  fwrite(&time, sizeof(float), 1, qsol); 

  fwrite(q, sizeof(float), 4*ni*nj, qsol); 

}


//***********************************************************************************************************************



void write_fun(int ni, int nj, float *f, FILE *grid)

{

  int i = 1;

  fwrite(&ni, sizeof(int), 1, grid);
  fwrite(&nj, sizeof(int), 1, grid);
  fwrite(&i, sizeof(int), 1, grid);

  fwrite(f, sizeof(float), ni*nj, grid);

}

//***********************************************************************************************************************

void SWEEP_Xi(float *A, float *B, float *C, float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, float *xi_x, float *xi_y, int j)
{
	
	int index2D, index3D; 
    float g1,g2,kx_a,ky_a,v_a,u_a,phi2_a,teta_a,beta_a,kx_c,ky_c,v_c,u_c,phi2_c,teta_c,beta_c;
	float *A_00, *A_01, *A_02, *A_03, *A_10, *A_11, *A_12, *A_13, *A_20, *A_21, *A_22, *A_23, *A_30, *A_31, *A_32, *A_33;
	float *B_00, *B_11, *B_22, *B_33;
	float *C_00, *C_01, *C_02, *C_03, *C_10, *C_11, *C_12, *C_13, *C_20, *C_21, *C_22, *C_23, *C_30, *C_31, *C_32, *C_33;

    g1 = GAMMA - 1.0;
    g2 = GAMMA - 2.0;
	
    for (i=1;i<i_max-1;i++)
    {
// For A vector:

		index2D = offset_2D(i-1,j,i_max);
		index3D = j * i_max + (i - 1);
		
        kx_a = xi_x[index2D];
        ky_a = xi_y[index2D];

		
        u_a = rho_u_Q[index3D] / rho_Q[index3D];
        v_a = rho_v_Q[index3D] / rho_Q[index3D];
        phi2_a = 0.5 * (GAMMA-1) * (u_a*u_a + v_a*v_a);
        teta_a = kx_a * u_a + ky_a * v_a;
        beta_a = GAMMA * e_Q[index3D] / rho_Q[index3D] - phi2_a;
		
		
		
		
// For C vector:

		index2D = offset_2D(i+1,j,i_max);
		index3D = j * i_max + (i + 1);
		
        kx_c = xi_x[index2D];
        ky_c = xi_y[index2D];
		
        u_c = rho_u_Q[index3D] / rho_Q[index3D];
        v_c = rho_v_Q[index3D] / rho_Q[index3D];
        phi2_c = 0.5 * (GAMMA-1) * (u_c*u_c + v_c*v_c);
        teta_c = kx_c * u_c + ky_c * v_c;
        beta_c = GAMMA * e_Q[index3D] / rho_Q[index3D] - phi2_c;
						
		
		
		
		
	// Define pointers:
	
	// For A:
		A_00 = &A[offset_3D(0,0,0,i_max,4)];  A_01 = &A[offset_3D(0,0,1,i_max,4)];       
		A_10 = &A[offset_3D(0,1,0,i_max,4)];  A_11 = &A[offset_3D(0,1,1,i_max,4)];
		A_20 = &A[offset_3D(0,2,0,i_max,4)];  A_21 = &A[offset_3D(0,2,1,i_max,4)];
		A_30 = &A[offset_3D(0,3,0,i_max,4)];  A_31 = &A[offset_3D(0,3,1,i_max,4)];

		A_02 = &A[offset_3D(0,0,2,i_max,4)];  A_03 = &A[offset_3D(0,0,3,i_max,4)];       
		A_12 = &A[offset_3D(0,1,2,i_max,4)];  A_13 = &A[offset_3D(0,1,3,i_max,4)];
		A_22 = &A[offset_3D(0,2,2,i_max,4)];  A_23 = &A[offset_3D(0,2,3,i_max,4)];
		A_32 = &A[offset_3D(0,3,2,i_max,4)];  A_33 = &A[offset_3D(0,3,3,i_max,4)];	
		
	// For B:	
		
		B_00 = &B[offset_3D(0,0,0,i_max,4)];
		B_11 = &B[offset_3D(0,1,1,i_max,4)];
		B_22 = &B[offset_3D(0,2,2,i_max,4)];
		B_33 = &B[offset_3D(0,3,3,i_max,4)];
		
	// For C:
	
		C_00 = &C[offset_3D(0,0,0,i_max,4)];  C_01 = &C[offset_3D(0,0,1,i_max,4)];       
		C_10 = &C[offset_3D(0,1,0,i_max,4)];  C_11 = &C[offset_3D(0,1,1,i_max,4)];
		C_20 = &C[offset_3D(0,2,0,i_max,4)];  C_21 = &C[offset_3D(0,2,1,i_max,4)];
		C_30 = &C[offset_3D(0,3,0,i_max,4)];  C_31 = &C[offset_3D(0,3,1,i_max,4)];

		C_02 = &C[offset_3D(0,0,2,i_max,4)];  C_03 = &C[offset_3D(0,0,3,i_max,4)];       
		C_12 = &C[offset_3D(0,1,2,i_max,4)];  C_13 = &C[offset_3D(0,1,3,i_max,4)];
		C_22 = &C[offset_3D(0,2,2,i_max,4)];  C_23 = &C[offset_3D(0,2,3,i_max,4)];
		C_32 = &C[offset_3D(0,3,2,i_max,4)];  C_33 = &C[offset_3D(0,3,3,i_max,4)];
		
		
		
		
		
	// Fill A:
	
		A_00[i] =  0;  											A_01[i] = -0.5 * dt * (kx_a);       
		A_10[i] = -0.5 * dt *(kx_a*phi2_a - u_a*teta_a);  		A_11[i] = -0.5 * dt * (teta_a - kx_a*g2*u_a);
		A_20[i] = -0.5 * dt * (ky_a*phi2_a - v_a*teta_a);  		A_21[i] = -0.5 * dt * (kx_a*v_a - ky_a*g1*u_a);
		A_30[i] = -0.5 * dt *(teta_a * (phi2_a - beta_a));  	A_31[i] = -0.5 * dt * (kx_a*beta_a - g1*u_a*teta_a);

		A_02[i] = -0.5 * dt * ky_a;  							A_03[i] = 0;       
		A_12[i] = -0.5 * dt * (ky_a*u_a - g1*kx_a*v_a);  		A_13[i] = -0.5 * dt * (kx_a*g1);
		A_22[i] = -0.5 * dt * (teta_a - ky_a * g2*v_a);  		A_23[i] = -0.5 * dt * (ky_a*g1);
		A_32[i] = -0.5 * dt * (ky_a*beta_a - g1*v_a*teta_a); 	A_33[i] = -0.5 * dt * (GAMMA*teta_a);
	
	
	// Fill B:
	
		B_00[i] = 1.0;
		B_11[i] = 1.0;
		B_22[i] = 1.0;
		B_33[i] = 1.0;
		
	// Fill C:
	
		C_00[i] =  0;  											C_01[i] = 0.5 * dt * (kx_c);       
		C_10[i] = 0.5 * dt * (kx_c*phi2_c - u_c*teta_c);  		C_11[i] = 0.5 * dt * (teta_c - kx_c*g2*u_c);
		C_20[i] = 0.5 * dt * (ky_c*phi2_c - v_c*teta_c);  		C_21[i] = 0.5 * dt * (kx_c*v_c - ky_c*g1*u_c);
		C_30[i] = 0.5 * dt * (teta_c*(phi2_c - beta_c));  		C_31[i] = 0.5 * dt * (kx_c*beta_c - g1*u_c*teta_c);

		C_02[i] = 0.5 * dt * ky_c;  							C_03[i] = 0;       
		C_12[i] = 0.5 * dt * (ky_c*u_c - g1*kx_c*v_c);  		C_13[i] = 0.5 * dt * (kx_c*g1);
		C_22[i] = 0.5 * dt * (teta_c - ky_c * g2*v_c);  		C_23[i] = 0.5 * dt * (ky_c*g1);
		C_32[i] = 0.5 * dt * (ky_c*beta_c - g1*v_c*teta_c); 	C_33[i] = 0.5 * dt * (GAMMA*teta_c);

    }
}


//***********************************************************************************************************************

void SWEEP_Eta(float *A, float *B, float *C, float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, float *xi_x, float *xi_y, int i)
{
    int index2D, index3D;
    float g1,g2,kx_a,ky_a,v_a,u_a,phi2_a,teta_a,beta_a,kx_c,ky_c,v_c,u_c,phi2_c,teta_c,beta_c;
	float *A_00, *A_01, *A_02, *A_03, *A_10, *A_11, *A_12, *A_13, *A_20, *A_21, *A_22, *A_23, *A_30, *A_31, *A_32, *A_33;
	float *B_00, *B_11, *B_22, *B_33;
	float *C_00, *C_01, *C_02, *C_03, *C_10, *C_11, *C_12, *C_13, *C_20, *C_21, *C_22, *C_23, *C_30, *C_31, *C_32, *C_33;
	
	
    g1 = GAMMA - 1.0;
    g2 = GAMMA - 2.0;
    for (j=1;j<j_max-1;j++)
    {
	// For A vector:

		index2D = offset_2D(i,j-1,i_max);
		index3D = (j - 1) * i_max + i ;
		
        kx_a = xi_x[index2D];
        ky_a = xi_y[index2D];

		
        u_a = rho_u_Q[index3D] / rho_Q[index3D];
        v_a = rho_v_Q[index3D] / rho_Q[index3D];
        phi2_a = 0.5 * (GAMMA-1) * (u_a*u_a + v_a*v_a);
        teta_a = kx_a * u_a + ky_a * v_a;
        beta_a = GAMMA * e_Q[index3D] / rho_Q[index3D] - phi2_a;
		
		
		
		
	// For C vector:

		index2D = offset_2D(i,j+1,i_max);
		index3D = (j + 1) * i_max + i ;
		
        kx_c = xi_x[index2D];
        ky_c = xi_y[index2D];
		
        u_c = rho_u_Q[index3D] / rho_Q[index3D];
        v_c = rho_v_Q[index3D] / rho_Q[index3D];
        phi2_c = 0.5 * (GAMMA-1) * (u_c*u_c + v_c*v_c);
        teta_c = kx_c * u_c + ky_c * v_c;
        beta_c = GAMMA * e_Q[index3D] / rho_Q[index3D] - phi2_c;
						
		
		
		
		
	// Define pointers:
				
	// For A:
	
		A_00 = &A[offset_3D(0,0,0,j_max,4)];  A_01 = &A[offset_3D(0,0,1,j_max,4)];       
		A_10 = &A[offset_3D(0,1,0,j_max,4)];  A_11 = &A[offset_3D(0,1,1,j_max,4)];
		A_20 = &A[offset_3D(0,2,0,j_max,4)];  A_21 = &A[offset_3D(0,2,1,j_max,4)];
		A_30 = &A[offset_3D(0,3,0,j_max,4)];  A_31 = &A[offset_3D(0,3,1,j_max,4)];

		A_02 = &A[offset_3D(0,0,2,j_max,4)];  A_03 = &A[offset_3D(0,0,3,j_max,4)];       
		A_12 = &A[offset_3D(0,1,2,j_max,4)];  A_13 = &A[offset_3D(0,1,3,j_max,4)];
		A_22 = &A[offset_3D(0,2,2,j_max,4)];  A_23 = &A[offset_3D(0,2,3,j_max,4)];
		A_32 = &A[offset_3D(0,3,2,j_max,4)];  A_33 = &A[offset_3D(0,3,3,j_max,4)];	
		
	// For B:	
		
		B_00 = &B[offset_3D(0,0,0,j_max,4)];
		B_11 = &B[offset_3D(0,1,1,j_max,4)];
		B_22 = &B[offset_3D(0,2,2,j_max,4)];
		B_33 = &B[offset_3D(0,3,3,j_max,4)];
		
	// For C:
	
		C_00 = &C[offset_3D(0,0,0,j_max,4)];  C_01 = &C[offset_3D(0,0,1,j_max,4)];       
		C_10 = &C[offset_3D(0,1,0,j_max,4)];  C_11 = &C[offset_3D(0,1,1,j_max,4)];
		C_20 = &C[offset_3D(0,2,0,j_max,4)];  C_21 = &C[offset_3D(0,2,1,j_max,4)];
		C_30 = &C[offset_3D(0,3,0,j_max,4)];  C_31 = &C[offset_3D(0,3,1,j_max,4)];

		C_02 = &C[offset_3D(0,0,2,j_max,4)];  C_03 = &C[offset_3D(0,0,3,j_max,4)];       
		C_12 = &C[offset_3D(0,1,2,j_max,4)];  C_13 = &C[offset_3D(0,1,3,j_max,4)];
		C_22 = &C[offset_3D(0,2,2,j_max,4)];  C_23 = &C[offset_3D(0,2,3,j_max,4)];
		C_32 = &C[offset_3D(0,3,2,j_max,4)];  C_33 = &C[offset_3D(0,3,3,j_max,4)];	
		
		
	// Fill A:
	
		A_00[j] =  0;  											A_01[j] = -0.5 * dt * (kx_a);       
		A_10[j] = -0.5 * dt * (kx_a*phi2_a-u_a*teta_a);  		A_11[j] = -0.5 * dt * (teta_a - kx_a*g2*u_a);
		A_20[j] = -0.5 * dt * (ky_a*phi2_a-v_a*teta_a);  		A_21[j] = -0.5 * dt * (kx_a*v_a - ky_a*g1*u_a);
		A_30[j] = -0.5 * dt * (teta_a*(phi2_a-beta_a));  		A_31[j] = -0.5 * dt * (kx_a*beta_a - g1*u_a*teta_a);

		A_02[j] = -0.5 * dt * ky_a;  							A_03[j] = 0;       
		A_12[j] = -0.5 * dt * (ky_a*u_a - g1*kx_a*v_a);  		A_13[j] = -0.5 * dt * (kx_a*g1);
		A_22[j] = -0.5 * dt * (teta_a - ky_a * g2*v_a);  		A_23[j] = -0.5 * dt * (ky_a*g1);
		A_32[j] = -0.5 * dt * (ky_a*beta_a - g1*v_a*teta_a); 	A_33[j] = -0.5 * dt * (GAMMA*teta_a);
	
	
	// Fill B:
	
		B_00[j] = 1.0;
		B_11[j] = 1.0;
		B_22[j] = 1.0;
		B_33[j] = 1.0;
		
	// Fill C:
	
		C_00[j] =  0;  											C_01[j] = 0.5 * dt * (kx_c);       
		C_10[j] = 0.5 * dt * (kx_c * phi2_c - u_c*teta_c);  	C_11[j] = 0.5 * dt * (teta_c - kx_c*g2*u_c);
		C_20[j] = 0.5 * dt * (ky_c * phi2_c - v_c*teta_c);  	C_21[j] = 0.5 * dt * (kx_c*v_c - ky_c*g1*u_c);
		C_30[j] = 0.5 * dt * (teta_c * (phi2_c - beta_c));  	C_31[j] = 0.5 * dt * (kx_c*beta_c - g1*u_c*teta_c);

		C_02[j] = 0.5 * dt * ky_c;  							C_03[j] = 0;       
		C_12[j] = 0.5 * dt * (ky_c * u_c - g1*kx_c*v_c);  		C_13[j] = 0.5 * dt * (kx_c*g1);
		C_22[j] = 0.5 * dt * (teta_c - ky_c * g2*v_c);  		C_23[j] = 0.5 * dt * (ky_c*g1);
		C_32[j] = 0.5 * dt * (ky_c*beta_c - g1*v_c*teta_c); 	C_33[j] = 0.5 * dt * (GAMMA*teta_c);

    }
}

//***********************************************************************************************************************

float L2Norm(float *rho_dQ, float *rho_u_dQ, float *rho_v_dQ, float *e_dQ)
{

	float sum = 0;
	
    for (i=1; i<i_max-1; i++)
    {
        for(j=1; j<j_max-1; j++)
        {
			index3D = j * i_max + i;
            rho_dQ[index3D] = rho_dQ[index3D] / rho_00;
            rho_u_dQ[index3D] = rho_u_dQ[index3D] / (rho_00 * a_00);
            rho_v_dQ[index3D] = rho_v_dQ[index3D] / (rho_00 * a_00);
            e_dQ[index3D] = e_dQ[index3D] / (rho_00*a_00*a_00);
			

            sum += pow(rho_dQ[index3D],2.0);
            sum += pow(rho_u_dQ[index3D],2.0);
            sum += pow(rho_v_dQ[index3D],2.0);
            sum += pow(e_dQ[index3D],2.0);
			
			
        }
    }
	return sqrt(sum);
}

//***********************************************************************************************************************

float step(float *Q, float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, float *J, float *xi_x, float *xi_y, float *eta_x, float *eta_y)
{
    float *F, *E, *S, *s2, *rspec, *qv, *dd, *A, *B, *C, *drr, *drp, *dQ, temp, *rho_wa, *rho_u_wa, *rho_v_wa, *e_wa, residual;
	float *rho_E, *rho_u_E, *rho_v_E, *e_E, *rho_F, *rho_u_F, *rho_v_F, *e_F, *rho_S, *rho_u_S, *rho_v_S, *e_S;
    int kd,ks,ke,index2D,index3D;
	
    E = (float *)malloc(4*i_max*j_max*sizeof(float));
    F = (float *)malloc(4*i_max*j_max*sizeof(float));
    S = (float *)malloc(4*i_max*j_max*sizeof(float));
    dQ = (float *)malloc(4*i_max*j_max*sizeof(float));
    wa = (float *)malloc(4*i_max*sizeof(float));
    A = (float *)malloc(4*4*i_max*sizeof(float));
    B = (float *)malloc(4*4*i_max*sizeof(float));
    C = (float *)malloc(4*4*i_max*sizeof(float));

    s2 = (float *)malloc(i_max*sizeof(float));
    rspec = (float *)malloc(i_max*sizeof(float));
    qv = (float *)malloc(i_max*sizeof(float));
    dd = (float *)malloc(i_max*sizeof(float));
    drr = (float *)malloc(i_max*sizeof(float));
    drp = (float *)malloc(i_max*sizeof(float));
	
// Pointers:

	rho_E = &E[offset_3D(0,0,0,i_max,j_max)];
    rho_u_E = &E[offset_3D(0,0,1,i_max,j_max)];
    rho_v_E = &E[offset_3D(0,0,2,i_max,j_max)];
    e_E = &E[offset_3D(0,0,3,i_max,j_max)];

    rho_F = &F[offset_3D(0,0,0,i_max,j_max)];
    rho_u_F = &F[offset_3D(0,0,1,i_max,j_max)];
    rho_v_F = &F[offset_3D(0,0,2,i_max,j_max)];
    e_F = &F[offset_3D(0,0,3,i_max,j_max)];
	
	rho_S = &S[offset_3D(0,0,0,i_max,j_max)];
    rho_u_S = &S[offset_3D(0,0,1,i_max,j_max)];
    rho_v_S = &S[offset_3D(0,0,2,i_max,j_max)];
    e_S = &S[offset_3D(0,0,3,i_max,j_max)];
	
	rho_dQ = &dQ[offset_3D(0,0,0,i_max,j_max)]
	rho_u_dQ = &dQ[offset_3D(0,0,1,i_max,j_max)]	
	rho_v_dQ  = &dQ[offset_3D(0,0,2,i_max,j_max)]	
	e_dQ = &dQ[offset_3D(0,0,3,i_max,j_max)]
	
//RHS calculation:
	
	calc_E_F(rho_Q, rho_u_Q, rho_v_Q, e_Q, J, xi_x, xi_y, eta_x, eta_y, rho_E, rho_u_E, rho_v_E, e_E, rho_F, rho_u_F, rho_v_F, e_F)
    RHS(rho_S, rho_u_S, rho_v_S, e_S, rho_E, rho_u_E, rho_v_E, e_E, rho_F, rho_u_F, rho_v_F, e_F, dt);
    smooth(Q,S,J,xi_x,xi_y,eta_x,eta_y,i_max,j_max,s2,rspec,qv,dd,epse,GAMMA,FSMACH,dt);
	
//sweep 1 (Xi direction): 

// wa is the work arry
	rho_wa = &wa[0];
	rho_u_wa = &wa[i_max];
	rho_v_wa = &wa[2*i_max];
	e_wa = &wa[3*i_max]

    ks = 1;
    kd = i_max;
    ke = i_max - 2;
	
    for (j=1;j<j_max-1;j++)
    {
        SWEEP_Xi(A, B, C, rho_Q, rho_u_Q, rho_v_Q, e_Q, xi_x, xi_y, j);
        smoothx(Q,xi_x,xi_y,i_max,j_max,A,B,C,j,J,drr,drp,rspec,qv,dd,epsi,GAMMA,FSMACH,dt);
		
		
	// Copy to a work array:
	
        for (i=0;i<i_max;i++)
        {			
			index3D = j * i_max + i;			
			rho_wa[i] = rho_S[index3D];
			rho_u_wa[i] = rho_u_S[index3D];
			rho_v_wa[i] = rho_v_S[index3D];
			e_wa[i] = e_S[index3D];			
        }
		
	//solve tri-diagonal block system:
	
        btri4s(A,B,C,wa,kd,ks,ke);
		
	// Save the  solution:
        for (i=0;i<i_max;i++)
        {
			index3D = j * i_max + i;			
			rho_S[index3D] = rho_wa[i];
			rho_u_S[index3D] = rho_u_wa[i];
			rho_v_S[index3D] = rho_v_wa[i];
			e_S[index3D] = e_wa[i];

        }
        
    }
	
	
	
//sweep 2 (Eta direction):

	rho_wa = &wa[0];
	rho_u_wa = &wa[j_max];
	rho_v_wa = &wa[2*j_max];
	e_wa = &wa[3*j_max]


    kd    = j_max;
    ke    = j_max - 2;
	
    for (i=1;i<i_max-1;i++)
    {
        SWEEP_Eta(A, B, C, rho_Q, rho_u_Q, rho_v_Q, e_Q, xi_x, xi_y, i);
        smoothy(Q,eta_x,eta_y,i_max,j_max,A,B,C,i,J,drr,drp,rspec,qv,dd,epsi,GAMMA,FSMACH,dt);
		
	// Copy to a work array:
	
        for (j=0;j<j_max;j++)
        {
			index3D = j * i_max + i;			
			rho_wa[j] = rho_S[index3D];
			rho_u_wa[j] = rho_u_S[index3D];
			rho_v_wa[j] = rho_v_S[index3D];
			e_wa[j] = e_S[index3D];

        }
	//solve tri-diagonal block system:
	
        btri4s(A,B,C,wa,kd,ks,ke);
		
	// Save the  solution:
	
        for (j=0;j<j_max;j++) 
        {
			index3D = j * i_max + i;			
			rho_S[index3D] = rho_wa[j];
			rho_u_S[index3D] = rho_u_wa[j];
			rho_v_S[index3D] = rho_v_wa[j];
			e_S[index3D] = e_wa[j];           
        }
    }
	
// Advance the solution:

    for (i=1;i<i_max-1;i++)
    {
        for (j=1;j<j_max-1;j++)
        {   
			index2D = offset_2D(i,j,i_max);
			index3D = j * i_max + i;
			
// Delta_Q calculation:
 			
			rho_dQ[index3D] = rho_S[index3D] * J[index2D];
			rho_u_dQ[index3D] = rho_u_S[index3D]* J[index2D];
			rho_v_dQ[index3D] = rho_v_S[index3D]* J[index2D];
			e_dQ[index3D] = e_S[index3D]* J[index2D];
			
// Advance Q:	
		
			rho_Q[index3D] += rho_dQ[index3D];
			rho_u_Q[index3D] += rho_u_dQ[index3D];
			rho_v_Q[index3D] += rho_v_dQ[index3D];
			e_Q[index3D] += e_dQ[index3D];
			

        }
    }

		
    residual = L2Norm(rho_dQ, rho_u_dQ, rho_v_dQ, e_dQ);
	
    free(S);
    free(dQ);
    free(wa);
    free(A);
    free(B);
    free(C);
    free(s2);
    free(rspec);
    free(qv);
    free(dd);
    free(drr);
    free(drp);
    return residual;
}

//***********************************************************************************************************************

void output(float *x, float *y, FILE *grid_file, float *Rn,FILE *Rn_file, int itr, float *Q, float *rho_Q, float *rho_u_Q, float *rho_v_Q, float *e_Q, FILE *q_file)
{

    int index3D;
    FILE *q1,*q2,*q3,*q4;
	q1=fopen("q1.txt","wt");
    q2=fopen("q2.txt","wt");
    q3=fopen("q3.txt","wt");
    q4=fopen("q4.txt","wt");
	
    for (i=0;i<i_max;i++)
    {
        for (j=0;j<j_max;j++)
        {
			index3D = j * i_max + i;
            fprintf(q1,"%f ",rho_Q[index3D]);
            fprintf(q2,"%f ",rho_u_Q[index3D]);
            fprintf(q3,"%f ",rho_v_Q[index3D]);
            fprintf(q4,"%f ",e_Q[index3D]);
        }
        fprintf(q1,"\n");
        fprintf(q2,"\n");
        fprintf(q3,"\n");
        fprintf(q4,"\n");
    }

    write_q(i_max,j_max,Q,q_file,FSMACH,AOA,1.0,1.0);
    write_grid(i_max,j_max,x,y,grid_file);

    for (i=0;i<itr;i++)
    {
       fprintf(Rn_file,"%f ",Rn[i]);
    }
    return;
}

//***********************************************************************************************************************
int main()
{
    int i_max,j_max,iTEL,iLE,iTEU,N,itr;
    float *x,*y,*Q,*J,*Rn,*xi_x,*xi_y,*eta_x,*eta_y;
    FILE *grid_file,*Rn_file,*q_file;

    input();

    y = (float *)malloc(i_max*j_max* sizeof(float));
    x = (float *)malloc(i_max*j_max* sizeof(float));
    J = (float *)malloc(i_max*j_max* sizeof(float));
    xi_x = (float *)malloc(i_max*j_max* sizeof(float));
    xi_y = (float *)malloc(i_max*j_max* sizeof(float));
    eta_x = (float *)malloc(i_max*j_max* sizeof(float));
    eta_y = (float *)malloc(i_max*j_max* sizeof(float));
    Rn = (float *)malloc(N* sizeof(float));
    Q = (float *)malloc(4*i_max*j_max* sizeof(float));
	
	input_grid(x,y);
	
	// Pointers:
	
	rho_Q = &Q[offset_3D(0,0,0,i_max,j_max)];
	rho_u_Q = &Q[offset_3D(0,0,1,i_max,j_max)];
	rho_v_Q = &Q[offset_3D(0,0,2,i_max,j_max)];
	e_Q = &Q[offset_3D(0,0,3,i_max,j_max)];
	
	
    Initialize(rho_Q, rho_u_Q, rho_v_Q, e_Q);
	Jscobian(J, x, y, xi_x, xi_y, eta_x, eta_y);
	fill_Boundry(rho_Q, rho_u_Q, rho_v_Q, e_Q, xi_x, xi_y, eta_x, eta_y);
	

	L0 = step(rho_Q, rho_u_Q, rho_v_Q, e_Q, J, xi_x, xi_y, eta_x, eta_y);
	fill_Boundry(rho_Q, rho_u_Q, rho_v_Q, e_Q, xi_x, xi_y, eta_x, eta_y);
	
	itr = 0;	
	do
	{
		R = step(rho_Q, rho_u_Q, rho_v_Q, e_Q, J, xi_x, xi_y, eta_x, eta_y);
		fill_Boundry(rho_Q, rho_u_Q, rho_v_Q, e_Q, xi_x, xi_y, eta_x, eta_y);
		Rn[itr] = log10(absolute(R / L0));
		itr += 1;
	}
	
	while( (Rn[itr - 1] > -6) && (itr  < N)); 
	

    grid_file = fopen("grid.txt","wb");
    Rn_file = fopen("R.txt","wt");
    q_file = fopen("Q.txt","wb");
    output(x,y,grid_file,i_max,j_max,Rn,Rn_file,itr,Q,rho_Q, rho_u_Q, rho_v_Q, e_Q, q_file);
	
    fclose(q_file);
    fclose(grid_file);
	fclose(Rn_file);
    free(x);
    free(y);
    free(J);
    free(xi_x);
    free(xi_y);
    free(eta_x);
    free(eta_y);
    free(Rn);
    free(Q);
	
    return 0;
}

