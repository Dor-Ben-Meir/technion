% Computational Fluid Dynamics - HW-2 - Shahar Wollmark - 201478591 
clc
close all
clear all

% Constants
gamma=1.4;
R=287;
T=288.15;
C=sqrt(gamma*R*T);

%Importing data
R=importdata('R.txt');
n=1:length(R);
q1=importdata('q1.txt'); %rho
q2=importdata('q2.txt'); %rho*u
q3=importdata('q3.txt'); %rho*v
q4=importdata('q4.txt'); %e
x=importdata('GridX_for_HW2.txt'); %x grid
y=importdata('GridY_for_HW2.txt'); %y grid
u=q2./q1; %u velocity
v=q3./q1; %v velocity
P=(gamma-1)*(q4-0.5*q1.*(u.^2+v.^2)); %pressure
Mach=sqrt(u.^2+v.^2)/C; %mach number

%Plotting graphs
figure()
plot(n,log10(R),'r','linewidth',2);
grid on;
axis tight;
xlabel('n');
ylabel('Log_1_0(|R|)');
title('Log_1_0(|R|) @ M=1.5, AOA=0');
axis tight;

figure()
hold all;
hcb=colorbar;
colorTitleHandle = get(hcb,'Title');
titleString = '[Mach]';
set(colorTitleHandle ,'String',titleString);
caxis ([0,1.65]);
title('Mach Distribution M=1.5, AOA=0');
xlabel('X coordinate');
ylabel('Y coordinate');
contourf(x,y,Mach,7000, 'edgecolor', 'none');
plot(x,y,'k',x',y','k');

figure()
hold all;
hcb=colorbar;
colorTitleHandle = get(hcb,'Title');
titleString = '[Pa]';
set(colorTitleHandle ,'String',titleString);
caxis ([0.7e5,3.6e5]);
title('Pressure Distribution M=1.5, AOA=0');
xlabel('X coordinate');
ylabel('Y coordinate');
contourf(x,y,P,7000, 'edgecolor', 'none');
plot(x,y,'k',x',y','k');

figure()
hold all;
quiver(x,y,u,v);
%plot(x,y,'k',x',y','k');
title('Velocity Distribution M=1.5, AOA=0');
xlabel('X coordinate');
ylabel('Y coordinate');