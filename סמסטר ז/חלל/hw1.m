clc
clear
close all


a = 10;
e = 0.8;
b = a * sqrt(1 - e^2);
p = a * (1 - e^2);
x0 = 0;
y0 = 0;
v = -pi:0.1:pi;
x = x0 + a*cos(v);
y = y0 + b*sin(v);
r = p ./(1 + e * cos(v)) ;

figure()
plot(x,y);
grid;
title('ellipse of a = 10[m], e = 0.8');
xlabel('x [m]'),ylabel('y [m]');

figure()
plot(v,r);
grid;
title('r(v)');
xlabel('v [rad]'),ylabel('v [m]');




