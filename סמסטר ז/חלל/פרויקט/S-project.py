
import numpy as np
from numpy import mean as mean
from scipy.integrate import odeint
import scipy as sci

RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533
J2 = 1.082 * (10 **-3)
day_to_sec = 60 * 60 * 24

   
# Define Parameters, caculate initial values:
a0 = 7200 #[Km]
e0 = 0.02
i0 = 15 * deg_to_rad #[rad]
Omega0 = 300 * deg_to_rad #[rad]
w0 = 20 * deg_to_rad # [rad]
Ni0 = 0 # [rad]



#Transformation between calsical elements to Position and Velocity Vectors in ECI system:
def Clasical_to_RVvectors(a,e,i,Omega,w,Ni):
    rv = []
    v = []
    r = a*(1 - e**2) / (1 + e * np.cos(Ni))
    h = np.sqrt(mu * a * ( 1 - e**2))
    p = a * (1 - e**2) 
    rv.append(r * (np.cos(Omega) * np.cos(w + Ni) - np.sin(Omega) * np.sin(w + Ni) * np.cos(i)))
    rv.append(r * (np.sin(Omega) * np.cos(w + Ni) + np.cos(Omega) * np.sin(w + Ni) * np.cos(i)))
    rv.append(r * (np.sin(i) * np.sin(w + Ni)))
    
    v.append(((rv[0]* h*e )/(r*p)) * np.sin(Ni) - (h/r) * (np.cos(Omega) * np.sin(w + Ni) + np.sin(Omega) * np.cos(w + Ni) * np.cos(i)))
    v.append(((rv[1]* h*e )/(r*p)) * np.sin(Ni) - (h/r) * (np.sin(Omega) * np.sin(w + Ni) - np.cos(Omega) * np.cos(w + Ni) * np.cos(i)))
    v.append(((rv[2]* h*e )/(r*p)) * np.sin(Ni) + (h/r) * (np.sin(i) * np.cos(w + Ni)))
    
    return(rv,v)
    
# Clculate J2 equtions:
def orbit_two(X,t):
       
	X1 = X[0:1]
	X2 = X[1:2]
	X3 = X[2:3]    
	X4 = X[3:4]
	X5 = X[4:5]
	X6 = X[5:6]    
    
	r = np.sqrt(X1**2 + X2**2 + X3**2)
    
	dX1 = X4
	dX2 = X5
	dX3 = X6
	#dX2 = (-mu / (r**3)) * X1
	dX4 = (-mu / (r**3)) * X1 * (1 - (1.5 * J2 * ((RE / r)**2)) * (5 * ((X3 / r)**2) - 1))
	dX5 = (-mu / (r**3)) * X2 * (1 - (1.5 * J2 * ((RE / r)**2)) * (5 * ((X3 / r)**2) - 1))
	dX6 = (-mu / (r**3)) * X3 * (1 - (1.5 * J2 * ((RE / r)**2)) * (5 * ((X3 / r)**2) - 3))
	derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6))
	return derivs     
  



def RVvectors_to_Clasical(sol,t):
 
    R = sol[:,0:3]
    R = R[0:len(t)]
    
    V = sol[:,3:6]
    V = V[0:len(t)]
    
    x = sol[:,0]
    y = sol[:,1]
    z = sol[:,2]

    v1 = sol[:,3]
    v2 = sol[:,4]
    v3 = sol[:,5]
    
    dot_rv = x*v1 + y*v2 + z*v3
    dot_rv = dot_rv[0:len(t)]
    
    R_syze = np.sqrt(x**2 + y**2 + z**2)
    R_syze = R_syze[0:len(t)]
    V_syze = np.sqrt(v1**2 + v2**2 + v3**2)
    V_syze = V_syze[0:len(t)]
    
    h = np.cross(R,V)
    h_size = np.sqrt(h[:,0]**2 + h[:,1]**2  + h[:,2]**2)
    h_size = h_size[0:len(t)]
    

    #e = (1 / mu) * ((V_syze**2) - (mu / R_syze))*R - (dot_rv * V)
    e = R
    e[:,0] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[:,0] - (dot_rv * V[:,0]))
    e[:,1] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[:,1] - (dot_rv * V[:,1]))
    e[:,2] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[:,2] - (dot_rv * V[:,2]))
    e_size = np.sqrt(e[:,0]**2 + e[:,1]**2  + e[:,2]**2)
    p = (h_size**2) / mu 
    a = p / (1 - e_size**2)
    n = np.cross([0,0,1],h)
    n_size = np.sqrt(n[:,0]**2 + n[:,1]**2  + n[:,2]**2)
    dot_ne = n[:,0]*e[:,0] + n[:,1]*e[:,1] + n[:,2]*e[:,2]
    dot_re = R[:,0]*e[:,0] + R[:,1]*e[:,1] + R[:,2]*e[:,2]
    i = np.arccos(h[:,2]/h_size)
    Omega = np.arccos(n[:,0]/n_size)
    w =  np.arccos(dot_ne / (n_size * e_size))
    Ni1 = np.arccos(dot_re / (R_syze * e_size))
   # Ni1 = np.arctan(np.sqrt(p/mu) * dot_rv)
    Ni = np.arctan2(np.sqrt(p/mu) * dot_rv, p-R_syze)

    
    ny = n[:,1]
    ek = e[:,2]
    
    for j in range(len(t)):
        if(ny[j] < 0 ):
            Omega[j] = 2 * np.pi - Omega[j]
            
        if(ek[j] < 0 ):
            w[j] = 2 * np.pi - w[j]
            
        if(dot_rv[j] < 0 ):
            #Ni[j] = 2 * np.pi - Ni[j]        
            Ni1[j] = 2 * np.pi - Ni1[j] 
            
        if(Ni[j] < 0 ):
           Ni[j] = 2 * np.pi - np.abs(Ni[j]) 
            

    return(a,e,i,Omega,w,Ni,Ni1,e_size,R,V)
    
def cal_average(num):
    sum_num = 0
    for t in num:
        sum_num = sum_num + t           

    avg = sum_num / len(num)
    return avg


def Mean_Clasical(t,a,e_size,i,Omega,w,Ni):
    a_mean = []
    e_mean = []
    i_mean = []
    Omega_mean = []
    w_mean = []
    Ni_mean = []
    
    Ma = []
    Me = []
    Mi = []
    MNi = []
    MOmega = []
    Mw = []
    
    for j in range(len(t)):
        a_mean.append(a[j])
        e_mean.append(e_size[j])
        i_mean.append(i[j])
        Omega_mean.append(Omega[j])
        w_mean.append(w[j])
        Ni_mean.append(Ni[j])
        
#        Ma.append(mean(a_mean))
#        Me.append(mean(e_mean))
#        Mi.append(mean(i_mean))
#        MNi.append(mean(Ni_mean))
#        MOmega.append(mean(Omega_mean))
#        Mw.append(mean(w_mean))
        
        
        Ma.append(cal_average(a_mean))
        Me.append(cal_average(e_mean))
        Mi.append(cal_average(i_mean))
        MNi.append(cal_average(Ni_mean))
        MOmega.append(cal_average(Omega_mean))
        Mw.append(cal_average(w_mean))
        
        #print(j)
    return(Ma,Me,Mi,MNi,MOmega,Mw)
    #return(Ma,Me,Mi,MOmega,Mw)
    
(r0_v,v0_v)  = Clasical_to_RVvectors(a0,e0,i0,Omega0,w0,Ni0)
t = np.arange( 0,(120 * day_to_sec + 10),100)
#X0  = sci.array([r0_v,v0_v])
#X0 = X0.flatten()
#sol = odeint(orbit_two,X0,t)
#t2 = np.arange( 0,((120 * day_to_sec + 10) / 10) ,10)
#(a,e,i,Omega,w,Ni,Ni1,e_size,R,V) = RVvectors_to_Clasical(sol,t)        
#(Ma,Me,Mi,MNi,MOmega,Mw) =  Mean_Clasical(t,a,e_size,i,Omega,w,Ni)        
##(Ma,Me,Mi,MOmega,Mw) =  Mean_Clasical(t,a,e_size,i,Omega,w,Ni) 

#np.savetxt('t.txt', (t))        
#np.savetxt('a.txt', (a))
#np.savetxt('e.txt', (e_size))
#np.savetxt('i.txt', (i))
#np.savetxt('Omega.txt', (Omega))
#np.savetxt('w.txt', (w))
#np.savetxt('Ni.txt', (Ni))  
#np.savetxt('Ni1.txt', (Ni1))       
#np.savetxt('r.txt', (R))
#np.savetxt('v.txt', (V))
#   
#np.savetxt('t2.txt', (t2)) 
#np.savetxt('a_mean.txt', (Ma))        
#np.savetxt('e_mean.txt', (Me))
#np.savetxt('i_mean.txt', (Mi))
#np.savetxt('Ni_mean.txt', (MNi))
#np.savetxt('Omega_mean.txt', (MOmega))
#np.savetxt('w_mean.txt', (Mw))
