clc
clear
close all

v = [ 0 -0.091 0.095];
u = [ - 1869.3 -2093 6654.9];
v_u = dot(v,u);
normy = norm(v) * norm(u);
teta = acos(v_u / normy);
teta = rad2deg(teta)

a = deg2rad(18);
b = deg2rad(20);
gamma = deg2rad(36);
c = acos(cos(b) * cos(a) + sin(b) * sin(a) * cos(gamma));
alpha = asin(sin(gamma) * sin(a) / sin(c));
beta = asin(sin(alpha) * sin(b) / sin(a));

c = rad2deg(c)
alpha = rad2deg(alpha)
beta = rad2deg(beta)