clc
clear
close all


e = 1.1;          %Eccentricity 
a = -560000;    %[m]
b = abs(a)*sqrt(e^2 -1);      %Equation for Semi-Minor Axis, b


teta_oo = 1/ cos(-1/e);
v2 = -teta_oo:0.01:teta_oo;
v = teta_oo:0.01: (2*pi -teta_oo);
r = (a * (e^2 -1)) ./ (1 + e * cos(v));
r2 = (a * (e^2 -1)) ./ (1 + e * cos(v2));
rp = a * ( e - 1);

x = -a -rp + r.*cos(v);
y = r.*sin(v);


x2 = -a -rp + r2.*cos(v2);
y2 = r2.*sin(v2);


figure()
plot(x,y,x2,y2);
grid;
title('Hyperbolic trajectorie of a = -560[km], e = 1.1');
xlabel('x [km]'),ylabel('y [km]');

figure()
plot(v,r);
grid;
title('r(v)');
xlabel('v [rad]'),ylabel('r [km]');


