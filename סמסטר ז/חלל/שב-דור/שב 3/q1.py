import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import scipy as sci

RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533

def epsilon_one(V,mu,r):
    e = ((V**2) / 2) - (mu / r)
    return e

def theta_one(h,r,v):
    theta = np.arccos( h / (r * v))
    return theta
    
def h_one(V,r,theta):
    h = r * V * math.cos(theta)
    return h
    
def e_one(epsilon,h,mu):
    e = math.sqrt(1 + (2 * epsilon * (h**2) / (mu**2)))
    return e

def e_two(r_v,v_v,h_v,mu,r):
    e = (np.cross(v_v, h_v) / mu) - (r_v / r)
    return e
    
def a_one(mu, epsilon):
    a = -mu / (2 * epsilon)
    return a
    
def Pr_one(a,e):
    Pr = a * (1 - e)
    return Pr

def ra_one(a,e):
    ra = a * (1 + e)
    return ra
    
def Ni_one(a,r,e):
    Ni = np.arccos((a * ( (1 - (e**2)) / r ) - 1) / e)
    return Ni

def E_one(a,e,r,Ni):
    E = np.arccos((a * e + r * np.cos(Ni)) / a)
    return E

def M_one(E,e):
    M = E - e * np.sin(E)
    return M

def n_one(mu,a):
    n = np.sqrt(mu/ (a**3))
    return n

def T_one(a,mu):
    T = 2 * np.pi * np.sqrt((a**3) / mu)
    return T
    
    
    
# Define Parameters, caculate initial values:
J2 = 1.082 * (10 **-3)
r0_v = 1000*np.array([-2.1689, 4.2406, 6.502]) #[km], Initial location
v0_v = np.array([-6.8283, -1.5083, -0.428]) #[km/sec], Initial velocity
r0 = LA.norm(r0_v)
V0 = LA.norm(v0_v)
epsi0 = epsilon_one(V0,mu,r0)
h0 = LA.norm(np.cross(r0_v, v0_v))
a = []
def orbit(X,t):
    

    
	X1 = X[:3]
	X2 = X[3:6]

	r = LA.norm(X1)
    
	dX1 = X2
	dX2 = (-mu / (r**3)) * X1

	derivs = sci.concatenate((dX1,dX2))
	return derivs

def orbit_two(X,t):
    

    
	X1 = X[0:1]
	X2 = X[1:2]
	X3 = X[2:3]    
	X4 = X[3:4]
	X5 = X[4:5]
	X6 = X[5:6]    


	r = np.sqrt(X1**2 + X2**2 + X3**2)
    
	dX1 = X4
	dX2 = X5
	dX3 = X6
	#dX2 = (-mu / (r**3)) * X1
	dX4 = (-mu / (r**3)) * X1 * (1 - (1.5 * J2 * ((RE / r)**2)) * (5 * ((X3 / r)**2) - 1))
	dX5 = (-mu / (r**3)) * X2 * (1 - (1.5 * J2 * ((RE / r)**2)) * (5 * ((X3 / r)**2) - 1))
	dX6 = (-mu / (r**3)) * X3 * (1 - (1.5 * J2 * ((RE / r)**2)) * (5 * ((X3 / r)**2) - 3))
	derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6))
	return derivs     
    
t = np.arange( 0,864010,10)
X0  = sci.array([r0_v,v0_v])
X0 = X0.flatten()
#sol = odeint(orbit,X0,t)
sol = odeint(orbit_two,X0,t)

t2 = np.arange( 0,(864010 / 10) ,10)
x = sol[:,0]
y = sol[:,1]
z = sol[:,2]

v1 = sol[:,3]
v2 = sol[:,4]
v3 = sol[:,5]

R_syze = np.sqrt(x**2 + y**2 + z**2)
R_syze = R_syze[0:len(t2)]
V_syze = np.sqrt(v1**2 + v2**2 + v3**2)
V_syze = V_syze[0:len(t2)]


R = sol[:,0:3]
V = sol[:,3:6]
h = np.cross(R,V)
h_size = np.sqrt(h[:,0]**2 + h[:,1]**2  + h[:,2]**2)
h_size = h_size[0:len(t2)]

epsi = epsilon_one(V_syze,mu,R_syze)


dot_rv = x*v1 + y*v2 + z*v3


theta = theta_one(h_size,R_syze,V_syze)
for i in range(len(t2)):
    
    if (dot_rv[i] < 0):
        theta[i] = -theta[i]
        
        
np.savetxt('sol.txt', (sol))
np.savetxt('t.txt', (t2))        
np.savetxt('Rsize.txt', (R_syze))
np.savetxt('Vsize.txt', (V_syze))
np.savetxt('phi.txt', (theta))
np.savetxt('epsilon.txt', (epsi))
np.savetxt('hsize.txt', (h_size))
       
