clear all
close all
clc

% sol = importdata('sol.txt');
% 
% t = importdata('t.txt');
% sol = sol(:,(1:length(t)));
 Kp = [1 2 3 4 5 6];
% x=sol(:,1);y=sol(:,2);z=sol(:,3);
% 
% 
% [rows, columns] = size(sol);
% lgd = cell(1,7);
% figure();
% plot(t,2000)
% lgd{1} = sprintf('reference line');
% hold on
% for i = 1:columns
%     sy = sol(:,i);
%     sy = sy(1:length(t));
%     Wy(:,i) = sy;
% 
%     plot(t,sy,'DisplayName',sprintf('Kp =%.0f',Kp(i)));
%     plot(t,sy);
%     lgd{i+1} = sprintf('Kp =%.0f',Kp(i));
%     
%     grid on;
%     title('\Theta under proportional control');
%     xlabel('time [sec]'),ylabel('\theta(t) [rad]');
%     
%     
%     
%     
% end
% legend(lgd);


Rel_p1 = -5;
Img_p1 = 7;
p1 = complex(Rel_p1,Img_p1);


Rel_p2 = -5;
Img_p2 = -7;
p2 = complex(Rel_p2,Img_p2);
Rel = [Rel_p1 Rel_p2];
img = [Img_p1 Img_p2];
lgd = cell(1,1);
x = -10:10;
y = -10:10;
x0 = x * 0;
y0 = y * 0; 
h = zeros(length(x(1,:)));
figure();
% hold
% plot(x,h(1,:))

hold
% plot(Rel,img,'*','DisplayName',sprintf('Kp =%.0f',Kp(1)))
plot(Rel,img,'*');
lgd{1} = sprintf('Kp =%.0f',Kp(1));
% plot(p2,'*','DisplayName',sprintf('Kp =%.0f',Kp(1))
% plot(p2,'*')
plot(x0 ,y,'b',x,y0,'b')
grid  on
legend(lgd);