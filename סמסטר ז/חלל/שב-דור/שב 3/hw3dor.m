clear all
close all
clc

%% Define Parameters, caculate initial values
r0=1000*[-2.1689 4.2406 6.502]; %[km], Initial location
v0= [-6.8283 -1.5083 -0.428]; %[km/sec], Initial velocity
mu=398603; %[km^3/sec^2], Earth gravitational paramter
R_E=6378; %[km], Earth radius
J2=1.082e-03; %2nd order approximation
eps0=(((norm(v0))^2)/2) - (mu/(norm(r0))); %[km^2/sec^2]' Initial specific energy
h0_size=norm(cross(r0,v0)); %[km^2/sec], Initial angular momentum


%% Import
sol = importdata('sol.txt');
t = importdata('t.txt');
x=sol(:,1);y=sol(:,2);z=sol(:,3);
vx=sol(:,4);vy=sol(:,5);vz=sol(:,6);
t=t/3600; %convert to hours
% r_size = importdata('Rsize.txt'); %[km], norm (r)
% v_size = importdata('Vsize.txt'); %[km/sec], norm (v)

%% Part 1.1 - Satellite's orbit
figure(1)
plot3(x,y,z,'k','LineWidth',1); %Orbit
hold on ; [X,Y,Z]=sphere; surf(R_E.*X , R_E.*Y , R_E.*Z); %Earth
hold on; plot3 (r0(1),r0(2),r0(3),'or','MarkerSize',4,'LineWidth',1); %Initial Location
axis equal ;grid on; xlabel ('x [km]'); ylabel ('y [km]'); zlabel ('z [km]');
legend ('Orbit','Earth','Initial Location'); title ('Satellites Orbit Around Earth')


%% Part 1.2 - Specific Energy
eps = importdata('epsilon.txt');%[km^2/sec^2], Specific energy 
figure(2)
plot (t,eps,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('\epsilon [km^2/sec^2]')
title ('\epsilon vs t'); xlim([0 24]); xticks ([0 6 12 18 24])
%% Part 1.3 - Specific Angular Momentum
h_size = importdata('hsize.txt'); %[km^2/sec]; Specific angular momentum
figure (3)
plot (t,h_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('h [km^2/sec]')
title ('|| h || vs t'); xlim([0 24]); xticks ([0 6 12 18 24])

%% Part 1.4 - Radius
r_size = importdata('Rsize.txt'); %[km], norm (r)
figure(4)
plot (t,r_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('r [km]')
r_p=min(r_size); r_a=max(r_size); %find perigee and apogee radiuses
title ('|| r || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); ylim ([r_p r_a])
%% Part 1.5 - Velocity
v_size = importdata('Vsize.txt'); %[km/sec], norm (v)
figure(5)
plot (t,v_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('v [km/sec]')
title ('|| v || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]);
%% Part 1.6 - Path Angle
phi = importdata('phi.txt'); %[rad] 
figure(6)
plot (t,phi,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('\phi [rad]')
title ('\phi vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); 
yticks([-0.5*pi -0.25*pi 0  0.25*pi 0.5*pi ])
yticklabels({'-0.5\pi','-0.25\pi','0', '0.25\pi','0.5\pi'}); ylim ([ -0.5*pi 0.5*pi]);
