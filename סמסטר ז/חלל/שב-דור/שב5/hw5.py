import math
import numpy as np
from numpy import linalg as LA
from numpy import pi as pi
import pandas as pd

RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533

h = 628 #[Km]
T = 2 * 60 * 60 #[sec]

r_minus = RE + h
a_minus = r_minus
a_plus = ((mu * (T**2)) / (4 * (np.pi**2)))**(1/3)
e_plus = 1 - (a_minus / a_plus)

V_minus = np.sqrt(mu / r_minus)
V_plus = np.sqrt((2 * mu / r_minus) - (mu / a_plus))

Phi = 5.62 * deg_to_rad
Delta_V = np.sqrt( (V_minus**2) + (V_plus**2) - (2 * V_minus * V_plus * np.cos(Phi))) 