import math
import numpy as np
from numpy import linalg as LA
from numpy import pi as pi
import pandas as pd

RE = 6378 # [Km]
mu = 3.986 * (10**5) #[Km^3 / S^2]
deg_to_rad = 0.0174533

def epsilon_one(V,mu,r):
    e = ((V**2) / 2) - (mu / r)
    return e

def theta_one(h,r,v):
    theta = np.arccos( h / (r * v))
    return theta
    
def h_one(V,r,theta):
    h = r * V * math.cos(theta)
    return h
    
def e_one(epsilon,h,mu):
    e = math.sqrt(1 + (2 * epsilon * (h**2) / (mu**2)))
    return e

def e_two(r_v,v_v,h_v,mu,r):
    e = (np.cross(v_v, h_v) / mu) - (r_v / r)
    return e
    
def a_one(mu, epsilon):
    a = -mu / (2 * epsilon)
    return a
    
def Pr_one(a,e):
    Pr = a * (1 - e)
    return Pr

def ra_one(a,e):
    ra = a * (1 + e)
    return ra


    
def Ni_one(a,r,e):
    Ni = np.arccos((a * ( (1 - (e**2)) / r ) - 1) / e)
    return Ni

def E_one(a,e,r,Ni):
    E = np.arccos((a * e + r * np.cos(Ni)) / a)
    return E

def M_one(E,e):
    M = E - e * np.sin(E)
    return M

def n_one(mu,a):
    n = np.sqrt(mu/ (a**3))
    return n

def T_one(a,mu):
    T = 2 * np.pi * np.sqrt((a**3) / mu)
    return T

def print_dic(answer):
    data = {}
    d= []
    dfr = []
    unit = []
    
    for k, v in answer.items():
        answer[k] = [np.round(v[0],3),v[1]]

    for key, value in answer.items():
        d.append(key)
        dfr.append(value[0])
        unit.append(value[1])
        
    data["variables"] = d
    data ['values'] = dfr
    data["units"] = unit
    df = pd.DataFrame(data)
    print(df)
    return df

#answer = {}
#data = {}  
#d= []
#dfr = []
#unit = []
 
def q_one():
    theta = 25 * deg_to_rad # [rad]
    r = 7500 #[Km]    
    V = 6 # [Km/sec]
    

    

    epsilon = epsilon_one(V,mu,r)
    h = h_one(V,r,theta)
    e = e_one(epsilon,h,mu)
    a = a_one(mu, epsilon)
    Pr = Pr_one(a,e)
    Ni1 = Ni_one(a,RE,e)
    Ni2 = 2*pi - Ni1
    Range = (Ni2 - Ni1) * RE
    
    answer = {}    
    answer["epsilon"] = [epsilon,"[Km^2/sec^2]"]
    answer["h"] = [h,"[Km^2/sec]"]
    answer["e"] = [e,"[nan]"]
    answer["a"] = [a,"[Km]"]
    answer["Pr"] = [Pr,"[Km]"]
    answer["Ni1"] = [Ni1,"[rad]"]
    answer["Ni2"] = [Ni2,"[rad]"]
    answer["Range"] = [Range,"[Km]"]
    
    
    df =  print_dic(answer)
    return df
#df = q_one()

def q_two():
    
    #part 1"    
    r_v = (1000 * np.array([-3.5, 6, 4]), "[Km]")
    v_v = (np.array([-5, -8, 1]), "[Km/sec]")
    r = (LA.norm(r_v[0]),"[Km]")
    V = (LA.norm(v_v[0]),"[Km/sec]")
    epsilon = (epsilon_one(V[0],mu,r[0]),"[Km^2/sec^2]")
    h_v = (np.cross(r_v[0], v_v[0]),"[Km^2/sec]")
    e_v = (np.around(e_two(r_v[0],v_v[0],h_v[0],mu,r[0]),3),"[nan]")
    e = (LA.norm(e_v[0]),"[nan]")
    a = (a_one(mu, epsilon[0]),"[Km]")
    
    
    answer = dict(r_v = (1000 * np.array([-3.5, 6, 4]), "[Km]"),
    v_v = (np.array([-5, -8, 1]), "[Km/sec]"),
    r = (LA.norm(r_v[0]),"[Km]"),
    V = (LA.norm(v_v[0]),"[Km/sec]"),
    epsilon = (epsilon_one(V[0],mu,r[0]),"[Km^2/sec^2]"),
    h_v = (np.cross(r_v[0], v_v[0]),"[Km^2/sec]"),
    e_v = (np.around(e_two(r_v[0],v_v[0],h_v[0],mu,r[0]),3),"[nan]"),
    e = (LA.norm(e_v[0]),"[nan]"),
    a = (a_one(mu, epsilon[0]),"[Km]"), 
    Ni = (2 * pi - Ni_one(a[0],r[0],e[0]), "[rad]"))
    
    df1 = print_dic(answer)
    

    
   
    
    r_v = np.array([-3.5, 6, 4]) # [Km]
    r_v = r_v * 1000
    v_v = np.array([-5, -8, 1]) # [Km/sec]
    r = LA.norm(r_v)
    V = LA.norm(v_v)
    epsilon = epsilon_one(V,mu,r)
    h_v = np.cross(r_v, v_v)
    e_v = np.around(e_two(r_v,v_v,h_v,mu,r),3)
    e = LA.norm(e_v)
    a = a_one(mu, epsilon)
    Ni = 2 * pi - Ni_one(a,r,e)
    
 
    
    
    answer = dict(T = (T_one(a,mu),"[sec]"))
    df2 = print_dic(answer)


    
    
    
    T = T_one(a,mu)
    h = LA.norm(h_v)
    
    h = (LA.norm(h_v),"[Km^2/sec]")
    r_dot_v = (np.dot(r_v,v_v),"[Km^2/sec]")
    theata = (theta_one(h[0],r,V), "[rad]")
    v_v_rad = (np.around(np.array([V * np.sin(2 * np.pi - theata[0]), V * np.cos(2 * np.pi - theata[0]), 0]),3), "[Km/sec]")
#    v_v_rad[0] = np.around(v_v_rad,3)
    
    answer = dict(h = (LA.norm(h_v),"[Km^2/sec]"),
    r_dot_v = (np.dot(r_v,v_v),"[Km^2/sec]"),
    theata = (2* pi - theta_one(h[0],r,V), "[rad]"),
    v_v_rad = (np.around(np.array([V * np.sin(2 * np.pi - theata[0]), V * np.cos(2 * np.pi - theata[0]), 0]),3), "[Km/sec]"))
    #print(answer)
    df3 =  print_dic(answer)
    
    


    
    h = (LA.norm(h_v),"[Km^2/sec]")
    h = LA.norm(h_v)
    r_dot_v = np.dot(r_v,v_v)
    theata = 2* pi - theta_one(h,r,V)
    v_v_rad = np.array([V * np.sin(2 * np.pi - theata), V * np.cos(2 * np.pi - theata), 0]) # [Km/sec]
    v_v_rad = np.around(v_v_rad,3)
    
    
    ra = (ra_one(a,e),"[km]")
    Ni2 = (Ni_one(a,ra[0],e),"[rad]")
    E1 = (np.pi,"[rad]")
    E0 =  (2 * np.pi - E_one(a,e,r,Ni), "[rad]")
    M1 = (M_one(E1[0],e), "[rad]")
    M0 = (M_one(E0[0],e), "[rad]") 
    n = (n_one(mu,a),"[1/sec]")
    t1 = ((((M1[0] + 2 * pi - M0[0]) / n[0]) / (60 * 60)), "[hours]")


    answer = dict(ra = (ra_one(a,e),"[km]"),
    Ni2 = (Ni_one(a,ra[0],e),"[rad]"),
    E1 = (np.pi,"[rad]"),
    E0 =  (2 * np.pi - E_one(a,e,r,Ni), "[rad]"),
    M1 = (M_one(E1[0],e), "[rad]"),
    M0 = (M_one(E0[0],e), "[rad]"),
    n = (n_one(mu,a),"[1/sec]"),
    t1 = ((((M1[0] + 2 * pi - M0[0]) / n[0]) / (60 * 60)), "[hours]"))
    
    df4 = print_dic(answer)
    return (df1,df2,df3,df4)
(df1,df2,df3,df4) = q_two()
    
#    ra = ra_one(a,e)
#    Ni2 = Ni_one(a,ra,e)
#    E1 = np.pi
#    E0 =  2 * np.pi - E_one(a,e,r,Ni)
#    M1 = M_one(E1,e)
#    M0 = M_one(E0,e) 
#    n = n_one(mu,a)
#    t1 = (M1 + 2 * pi - M0) / n 
#    t1 = t1 / (60 * 60) # [hours]
#
#delta_t = 2745
#
#N = 100
#E_z = E0
#F_E = E_z - e * np.sin(E_z) - (n * delta_t) + (2 * pi) - E0 + e * np.sin(E0)
#DF_E = 1 - e * np.cos(E_z)
#
#i = 0
#E = E_z - (F_E / DF_E)
#while(i<N):
#    E_z = E
#    E = E_z - (F_E / DF_E)
#    i = i + 1
#    print(np.absolute((F_E / DF_E)))
#E = (E % 2* pi)    
#    
#print(E_z)

