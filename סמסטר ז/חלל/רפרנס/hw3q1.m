%%% 084913 Space Engineering Fundamentals - HW3 %%%
%%%%%%%%% Daniel Benaroya 204393995 %%%%%%%%
clear all
close all
clc

%% Define Parameters, caculate initial values
r0=1000*[-2.1689 4.2406 6.502]; %[km], Initial location
v0= [-6.8283 -1.5083 -0.428]; %[km/sec], Initial velocity
mu=398603; %[km^3/sec^2], Earth gravitational paramter
R_E=6378; %[km], Earth radius
J2=1.082e-03; %2nd order approximation
eps0=(((norm(v0))^2)/2) - (mu/(norm(r0))); %[km^2/sec^2]' Initial specific energy
h0_size=norm(cross(r0,v0)); %[km^2/sec], Initial angular momentum

%% Question 1 - Simulate 
%(t=0:864e03 [sec], ode4 (Runge-Kutta). Fixed step size - 10 [sec])
sim ('hw3q1sim.slx')
%% Import,redefine and analyze simulation data
r=r.signals.values;
x=r(:,1);y=r(:,2);z=r(:,3);
v=v.signals.values;
vx=v(:,1);vy=v(:,2);vz=v(:,3);
 t=tout(1:(((end-1)/10)+1)); %cut time vector to 864e02 [sec] as specified
 t=t/3600; %convert to hours
r_size=sqrt(x.^2+y.^2+z.^2); %[km], norm (r)
v_size=sqrt(vx.^2+vy.^2+vz.^2); %[km/sec], norm (v)
%% Part 1.1 - Satellite's orbit
figure(1)
plot3(x,y,z,'k','LineWidth',1); %Orbit
hold on ; [X,Y,Z]=sphere; surf(R_E.*X , R_E.*Y , R_E.*Z); %Earth
hold on; plot3 (r0(1),r0(2),r0(3),'or','MarkerSize',4,'LineWidth',1); %Initial Location
axis equal ;grid on; xlabel ('x [km]'); ylabel ('y [km]'); zlabel ('z [km]');
legend ('Orbit','Earth','Initial Location'); title ('Satellites Orbit Around Earth')
% axis equal ;grid on; xlabel ('x [km]'); ylabel ('y [km]'); zlabel ('z [km]');
%% Part 1.2 - Specific Energy
eps=(((v_size).^2)/2)-(mu./(r_size));%[km^2/sec^2], Specific energy 
eps=eps(1:length(t));% fit to t
figure(2)
plot (t,eps,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('\epsilon [km^2/sec^2]')
title ('\epsilon vs t'); xlim([0 24]); xticks ([0 6 12 18 24])
%% Part 1.3 - Specific Angular Momentum
h=cross(r,v); %[km^2/sec]; Specific angular momentum
h_size=sqrt(h(:,1).^2+h(:,2).^2+h(:,3).^2);
h_size=h_size(1:length(t));
figure (3)
plot (t,h_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('h [km^2/sec]')
title ('|| h || vs t'); xlim([0 24]); xticks ([0 6 12 18 24])
%% Part 1.4 - Radius
r_size=r_size(1:length(t)); %fit to t
figure(4)
plot (t,r_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('r [km]')
r_p=min(r_size); r_a=max(r_size); %find perigee and apogee radiuses
title ('|| r || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); ylim ([r_p r_a])
%% Part 1.5 - Velocity
v_size=v_size(1:length(t)); %fit to t
figure(5)
plot (t,v_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('v [km/sec]')
title ('|| v || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]);
%% Part 1.6 - Path Angle
dot_rv=dot (r,v,2);
phi=acos(h_size./(r_size.*v_size));
phi=phi(1:length(t)); %fit to t 
for i=1:length(t)
    if dot_rv(i)<0
        phi(i)=-phi(i);
    end
end
figure(6)
plot (t,phi,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('\phi [rad]')
title ('\phi vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); 
yticks([-0.5*pi -0.25*pi 0  0.25*pi 0.5*pi ])
yticklabels({'-0.5\pi','-0.25\pi','0', '0.25\pi','0.5\pi'}); ylim ([ -0.5*pi 0.5*pi]);
%% Find a & e
a=(r_a+r_p)/2; %[km], Semi major axis
e=(r_a-r_p)/(2*a); %Eccentricity

%% Question 2 - Simulate 
%(t=0:864e03 [sec], ode4 (Runge-Kutta). Fixed step size - 10 [sec])
sim ('hw3q2sim.slx')
%% Import,redefine and analyze simulation data
x2=x2.signals.values;
y2=y2.signals.values;
z2=z2.signals.values;
vx2=vx2.signals.values;
vy2=vy2.signals.values;
vz2=vz2.signals.values;
r2= [x2 y2 z2];
r2_size=sqrt(x2.^2+y2.^2+z2.^2); %[km], norm (r2)
v2= [vx2 vy2 vz2];
v2_size=sqrt(vx2.^2+vy2.^2+vz2.^2); %[km/sec], norm (v2)
%% Part 2.1 - Satellite's orbit
figure(7)
plot3(x2,y2,z2,'k'); %Orbit
hold on ; surf(R_E.*X , R_E.*Y , R_E.*Z); %Earth
axis equal ;grid on; xlabel ('x [km]'); ylabel ('y [km]'); zlabel ('z [km]');
legend ('Orbit','Earth'); title ('Satellites Orbit Around Earth')
%% Part 2.2 - Specific Energy
eps2=(((v2_size).^2)/2)-(mu./(r2_size));%[km^2/sec^2], Specific energy 
eps2=eps2(1:length(t));% fit to t
figure(8)
plot (t,eps2,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('\epsilon [km^2/sec^2]')
title ('\epsilon vs t'); xlim([0 24]); xticks ([0 6 12 18 24])
%% Part 2.3 - Specific Angular Momentum
h2=cross(r2,v2); %[km^2/sec]; Specific angular momentum
h2_size=sqrt(h2(:,1).^2+h2(:,2).^2+h2(:,3).^2);
h2_size=h2_size(1:length(t));
figure (9)
plot (t,h2_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('h [km^2/sec]')
title ('|| h || vs t'); xlim([0 24]); xticks ([0 6 12 18 24])
%% Part 2.4 - Radius
r2_size=r2_size(1:length(t)); %fit to t
figure(10)
plot (t,r2_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('r [km]')
r_p2=min(r2_size); r_a2=max(r2_size); %find perigee and apogee radiuses
title ('|| r || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); ylim ([r_p2 r_a2])
%% Part 2.5 - Velocity
v2_size=v2_size(1:length(t)); %fit to t
figure(11)
plot (t,v2_size,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('v [km/sec]')
title ('|| v || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]);
%% Part 2.6 - Path Angle
dot_rv2=dot (r2,v2,2);
phi2=acos(h2_size./(r2_size.*v2_size));
phi2=phi2(1:length(t)); %fit to t 
for i=1:length(t)
    if dot_rv2(i)<0
        phi2(i)=-phi2(i);
    end
end
figure(12)
plot (t,phi2,'LineWidth',1); grid on; xlabel ('Time [hours]'); ylabel('\phi [rad]')
title ('\phi vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); 
yticks([-0.5*pi -0.25*pi 0  0.25*pi 0.5*pi ])
yticklabels({'-0.5\pi','-0.25\pi','0', '0.25\pi','0.5\pi'}); ylim ([ -0.5*pi 0.5*pi]);

%% Graphic Comparison
figure (13)%Orbit
surf(R_E.*X , R_E.*Y , R_E.*Z);hold on; plot3(x2,y2,z2);hold on;
plot3 (x,y,z,'LineWidth',2); grid on; axis equal; xlabel ('x [km]'); ylabel ('y [km]'); zlabel ('z [km]');
legend ('Earth','J2','Perfect Sphere'); title ('Satellites Orbit Around Earth')

figure (14)%Specific Energy
plot(t,eps,'LineWidth',1);hold on;plot (t,eps2,'LineWidth',1);xlabel ('Time [hours]');ylabel('\epsilon [km^2/sec^2]')
 grid on;title ('\epsilon vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); legend ('Perfect Sphere','J2')

 figure(15)%Specific Angular Momentum
plot(t,h_size,'LineWidth',1);hold on;plot (t,h2_size,'LineWidth',1);xlabel ('Time [hours]');ylabel('h [km^2/sec]')
 grid on;title ('|| h || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); legend ('Perfect Sphere','J2')

 figure(16)%Radius
plot (t,r_size,'LineWidth',1);hold on; plot (t,r2_size,'--','LineWidth',1); xlabel ('Time [hours]'); ylabel('r [km]')
grid on;title ('|| r || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); legend ('Perfect Sphere','J2')

figure (17)%Velocity
plot (t,v_size,'LineWidth',1);hold on; plot (t,v2_size,'--','LineWidth',1); xlabel ('Time [hours]'); ylabel('v [km/sec]')
grid on;title ('|| v || vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); legend ('Perfect Sphere','J2')

figure(18)%Path Angle
plot (t,phi,'LineWidth',1);hold on;plot (t,phi2,'--','LineWidth',1); xlabel ('Time [hours]'); ylabel('\Phi [rad]')
title ('\Phi vs t'); xlim([0 24]); xticks ([0 6 12 18 24]); yticks([0  0.5*pi  pi  1.5*pi 2*pi]); grid on;
yticklabels({'0', '0.5\pi','\pi','1.5\pi','2\pi'}); ylim ([ 0 2*pi]);legend ('Perfect Sphere','J2')
