clc
clear
close all

s = tf('s');
Wn = 4;
zeta = 0.3;


G_ol = Wn^2 / (s * (s + 2*Wn));
figure()
step(G_ol)
title('Step response-open loop');
xlabel('Time [sec]'),ylabel('Amplitude');
grid on;

G_cl = Wn^2 / (s^2 + 2*Wn*s*zeta + Wn^2) ;
figure()
step(G_cl)
title('Step response-close loop');
xlabel('Time [sec]'),ylabel('Amplitude');
grid on;