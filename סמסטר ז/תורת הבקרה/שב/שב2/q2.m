clc
clear
close all

% u = @(zeta) sqrt((1 - 2*zeta^2) + sqrt(4*(zeta^4) - 4*(zeta^2) + 2));

zeta = 0.1:0.1:0.9;
u = sqrt((1 - 2*zeta.^2) + sqrt(4*(zeta.^4) - 4*(zeta.^2) + 2));

figure()
plot(zeta,u);
grid;
title('u(zeta)');
xlabel('zeta'),ylabel('u');



