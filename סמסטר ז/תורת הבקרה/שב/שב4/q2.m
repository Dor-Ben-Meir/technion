clc
close all
clear 

s = tf('s');
Td = 0.06;
Kp = 190;
K = 190 * (1 + 0.06 * s);
G = 1 / ((s + 2) * (s + 5));
sys1 = K * G / (1 + K * G);

hold
step(sys1)

for gamma = [ 1 0.1 0.01]
	K = Kp * (1 + (Td*s / (gamma* Td*s + 1)));
	sys2 = G * K / (1 + G * K);
	step(sys2)
	
end
grid on

legend('original','gamma = 1', 'gamma = 0.1', 'gamma = 0.01')
