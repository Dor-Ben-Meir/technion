clc
close all
clear 
% Kp = 80;
% zetta = sqrt(2) / 2;
% Wn = sqrt(80);
% z = zetta * Wn / 10;
% 
% s = tf('s');
%  K = Kp * ( (s + z) / s);
% % K = Kp * ( 1 + 0.108 * s);
% G = 1 / (s*(s + 4));
% sys1 = K * G / (1 + K * G);
% P = pole(sys1)

s = tf('s');
K = 13.73 * ( (s + 0.35 ) / s);
% K = Kp * ( 1 + 0.108 * s);
G = 1 / ((s + 3)*(s + 4));
sys1 = K * G;
P = pole(sys1)