clc
close all
clear 

s = tf('s');


p = -8.147 + 15.9i;

Siv = angle(p + 8) - angle( p + 3) - angle( p + 6) - angle( p + 10);

a = -pi - Siv;
Td = tan(a)/ (15.9 + tan(a)*8.147);
KpD = 1 / ((abs(1 + Td*p) * abs(p + 8)) / (abs( p + 3) * abs(p + 6) * abs(p + 10)));
Zetta = 8.147 / abs(p);
Wn = 15.9 / sqrt(1 - Zetta^2);
zero_i = Zetta * Wn / 10;
Kpi = 1 / ((KpD * abs(1 + Td*p) * abs(p + 8) * abs(p + zero_i)) / (abs(p) * abs( p + 3) * abs(p + 6) * abs(p + 10)));

D_pd = KpD * (1 + Td*s);
D_pi = Kpi * ((s + zero_i) / s);
D_pid = D_pd * D_pi;

G = (s + 8) / ((s+3) * (s + 6) * (s + 10));
G_cl = D_pid*G / (1 + D_pid*G);
step(G_cl)
grid on

% atan2(15.9, -8.147 + 8)


% CtoR= (10.4* ( s^2 + 4.5192 * s + 15.385)) / ((s^3) + 14 * (s^2) + 56*s +160);
% step(CtoR);
% grid on;
% data = stepinfo(CtoR);
% 
% data.SettlingTime
% data.Overshoot