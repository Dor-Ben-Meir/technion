clc
close all
clear 

s = tf('s');

Rel_p =  -2 * sqrt(10);
Img_p = 2 * sqrt(10);
p = complex(Rel_p,Img_p);

Zetta = sqrt(2) / 2;
Wn = sqrt(80);
zero_i = Zetta * Wn / 10;
Kp = (1 / 80) / ((abs(1 + 0.108*p) * abs(p + zero_i)) / (abs(p) * abs(p) * abs(p + 4)));

D_pd = 80 * (1 + 0.108*s);
D_pi = Kp * (1 + (zero_i / s));
D_pid = D_pd * D_pi;

figure();
G = 1 / (s*(s + 4));
G_cl = D_pid*G / (1 + D_pid*G);
step(G_cl)
grid on

figure();
G_cl2 = D_pd*G / (1 + D_pd*G);
step(G_cl2)
grid on