clc
clear
close all

s = tf('s');
G_OL = (s + 1.25) * (s + 0.25) / ((s^2) * ((s + 1)^2));
G_cl = G_OL / (1 + G_OL);
E = 1 - G_cl;

run = 1 / s;
acel = 1 / s^2;

figure()
step(G_cl)
title('Step response');
xlabel('Time [sec]'),ylabel('Response');
grid on;

figure()
step(E)
title('Step response');
xlabel('Time [sec]'),ylabel('Error');
grid on;


figure()
step(G_cl * run)
title('Step response');
xlabel('Time [sec]'),ylabel('Response');
grid on;

figure()
step(E * run)
title('Step response');
xlabel('Time [sec]'),ylabel('Error');
grid on;

figure()
step(G_cl * acel)
title('Step response');
xlabel('Time [sec]'),ylabel('Response');
xlim([0 25])
ylim([0 300])
grid on;

figure()
step(E * acel)
title('Step response');
xlabel('Time [sec]'),ylabel('Error');
grid on;