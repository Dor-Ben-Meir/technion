clc
clear
close all

s = tf('s');

G = (s - 10) / ((s^2 + 2*s + 4) * ((s + 3)^2));
Kp = -1.5;
KI = -2;
D = Kp + (KI/s);

G_system = D * G / (1 + D*G);
BW1 = bandwidth(G_system);
stepinfo(G_system)

figure()
step(G_system)
grid on;


