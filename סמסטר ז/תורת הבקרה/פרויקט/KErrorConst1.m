function [kv, evss, Type] = KErrorConst1(sys)
% Calculating static error constant for type 1
% linear transfer functions only
% return k1

% finding the denominator and the numerator of transfer function
s = tf('s');
[num,den] = tfdata(minreal(s*sys)); 
% working with arrays
den = cell2mat(den);
num = cell2mat(num);
% Lim(s) = 0
kv = num(end)/den(end);
evss = 1/kv;
p = pole(sys);
Type = sum(p==0); 
end
