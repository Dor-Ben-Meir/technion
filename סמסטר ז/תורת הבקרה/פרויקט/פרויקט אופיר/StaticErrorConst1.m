function kv = StaticErrorConst1(sys)
% Calculating static error constant for type 1
% linear transfer functions only
% return k1

% tf vairable s
s = tf('s');
% finding the denominator and the numerator of transfer function
[num,den] = tfdata(minreal(s*sys)); 
% working with arrays
den = cell2mat(den);
num = cell2mat(num);
% s = 0
kv = num(end)/den(end);
end

