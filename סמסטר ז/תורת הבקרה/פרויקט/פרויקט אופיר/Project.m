%% Fresh Start
clear
close all
clc
%% Parameters
B = 300; % viscous damping [N*m*s]
J = 100; % Antena Moment of Inertia [kg*m^2]
JTolerance = 0.2; % +- change in antena moment of inertia [kg*m^2]
BTolerance = 0.1; % +- change in viscous damping [N*m*s]
tau = 1/10; % time constant of the engine [sec]
Tw = 0; % wind perturbation moment
mu = 0; % Mean of perturbation - wind
sigma= 10; % Varience of perturbation - wind
muomega = 0; % Mean of perturbation - sensor
sigmaomega = 5*pi/180; % Varience of perturbation - sensor
Runclosed = 1; % Simulink Operator, 1 for step, 0 for Ramp (run)
satclosed = 1; % Simulink Operator, 1 for No saturation to DC Motor, 0 for saturation
NoMeasurePertur = 1;% Simulink Operator, 1 for No Perturbation in measurement, 0 for Perturbation
%% Controller Requirements
ts = 1; % settling time to 5 percent (eqoual or less) [sec]  
tr = 0.4; % rising time (equal or less) [sec]
evss = 0; % error steady-state for ramp response converge to zero [-] 
Mp = 10; % overshoot (equal or less) [percent] 
minTwrej = 40; %  able to reject at least 40 dB of Tw [dB]
Tmax = 100; % Part B DC Motor limit
%% Part A
    %% Section 1
    fprintf('Section 1\nTransfer Function Theta/T(s)\n')
    s = tf('s');
    ThetaT = 1/s/(J*s+B)
    %% Section 2
    fprintf('Section 2\nTransfer Function T/Tcom(s)\n')
    TTcom = 30/(tau*s+1)
    fprintf('\nTransfer Function of open loop without controller G(s)\n')
    G = TTcom*ThetaT
        %% Root Locus for system with only propotional Controller
        figure('Name','Part A Section 3 - RL');
        hold on
        rlocus(G)
        grid 
        %% Finiding steady state error for ramp 
        kv = StaticErrorConst1(G);
        evss = 1/kv;
        %% Finding the type of the system
        p = pole(G);
        Type = sum(p==0); 
    %% Section 3
    if evss ~= 0
        fprintf('Section 3\nFor zero steady-state ramp error the type of system need to be 2\nThe type of the system with propotional controller = %.0f\nError to Ramp Response = %.3f',Type,evss)
    end
    %% Section 4
        %% Checking Controller
        % using rltool......
        Gc = 2.4975e07*(s+0.001)*(s^2+20*s+125)/s/(s+1000)^2; % The Controller that i found
        % confirm that the controller makes all the uncertainty in J & B
        for i = B*(1-BTolerance):2*B*BTolerance/9:B*(1+BTolerance)
            for k = J*(1-JTolerance):2*J*JTolerance/9:J*(1+JTolerance)
            thetaTw = feedback(1/s/(k*s+i),30/(tau*s+1)*Gc);
            sys =  1/s/(k*s+i)*30/(tau*s+1);
            work = SysReq(ts,tr,Mp,minTwrej,sys,Gc,thetaTw);
            end
        end
        ControlclosedLoop = feedback(Gc*G,1); % Closed loop transfer function
        PolesClosed = pole(ControlclosedLoop); % Poles of the closed loop transfer function  
        ZerosClosed = zero(ControlclosedLoop); % zeroes of the closed loop transfer function
        fprintf('Section 4\nAfter working with rltool the Controller that met all the requirements in the uncertainty of the parameters\n')
        Gc = Gc
    %% Section 5
    figure('Name','RL With Controller')
    hold on
    rlocus(Gc*G)
    grid
    plot(PolesClosed,'x','color','g')
    legend('RL','poles of closed loop','Location','best')
    %% Section 6
    figure('Name','Step Response Control System')
    hold on
    grid on
    grid minor
    step(ControlclosedLoop)
    stepinfo(ControlclosedLoop)
    figure('Name','Step Error Control System')
    hold on
    grid on
    grid minor
    step(1-ControlclosedLoop)
    title('Step Error')
    figure('Name','Ramp Response Control System')
    hold on
    grid on
    grid minor
    step(ControlclosedLoop/s)
    title('Ramp Response')
    figure('Name','Ramp Error Control System')
    hold on
    grid on
    grid minor
    step((1-ControlclosedLoop)/s)
    title('Ramp Error')
    figure('Name','Acceleration Response Control System')
    hold on
    grid on
    grid minor
    step(ControlclosedLoop/s^2)
    title('Acceleration Response')
    figure('Name','Acceleration Error Control System')
    hold on
    grid on
    grid minor
    step((1-ControlclosedLoop)/s^2)
    title('Acceleration Error')
        %% Requirements Approved
        info = stepinfo(ControlclosedLoop);
        fprintf('Section 6\nOvershoot = %.3f\nSettling-Time = %.3f\nRise-Time = %.3f\n',info.Overshoot,info.SettlingTime,info.RiseTime)
    %% Section 7
    fprintf('Section 7\n Transfer Function of closed loop with controller for Theta/Tw(s)\n')
    thetaTw = feedback(ThetaT,TTcom*Gc)
    [mag,~,~] = bode(thetaTw);
    mag = squeeze(mag);
    fprintf('Max Bode Magnitude - Closed Loop Theta/Tw(s) = %.3f [dB]\n',max(20*log10(mag)))
    %% Section 8
    AcceptedError = 0.01; % Accpeted Error after perturbation (0.1%)
    stop = 0; 
    simtime = 10; % Simulation time [sec]
    while  stop == 0
        Runclosed = 1;
        sim('ProjectModel.slx')
        InfoWithTw = stepinfo(theta(:,2),theta(:,1));
        if InfoWithTw.Overshoot>Mp||InfoWithTw.RiseTime>tr||InfoWithTw.SettlingTime>ts|| max(abs(Error))>AcceptedError
            stop = 1;
            sigma = sigma-10;
        end
        Runclosed = 0;
        sim('ProjectModel.slx')
        if max(abs(Error))>AcceptedError
            stop = 1;
            sigma = sigma-10;
        end
        sigma = sigma+10;
    end
    fprintf('Section 8\nThe max varience to meet requirements = %.f\n',sigma^2)
%% Part B
    %% Section 1+2
    j = 1;
    l = 1;
    Bvec = B*(1-BTolerance):2*B*BTolerance/49:B*(1+BTolerance);
    Jvec = J*(1-JTolerance):2*J*JTolerance/49:J*(1+JTolerance);
    for i = B*(1-BTolerance):2*B*BTolerance/49:B*(1+BTolerance)
        for k = J*(1-JTolerance):2*J*JTolerance/49:J*(1+JTolerance)
            thetaTw = feedback(1/s/(k*s+i),30/(tau*s+1)*Gc);
            sys =  1/s/(k*s+i)*30/(tau*s+1);
            infoTolrance = stepinfo(feedback(Gc*sys,1));
            OverShootMat(j,l) = infoTolrance.Overshoot;
            RiseTimeMat(j,l) = infoTolrance.RiseTime;
            SettlingTimeMat(j,l) = infoTolrance.SettlingTime;
            [magTwTolerance,~,~] = bode(thetaTw);
            magTwTolerance = squeeze(magTwTolerance);
            BodeMaxMat(j,l) = max(20*log10(magTwTolerance(magTwTolerance~=0)));
            l = l+1;
        end
        l = 1;
        j = j+1;
    end
    fprintf('Part B\n')
        %% Plotting Tolerance Influence
        figure('Name','OverShoot Comparing')
        hold on
        surf(Bvec,Jvec,OverShootMat)
        colorbar
        xlabel('B [N\cdotm\cdotsec]')
        ylabel('J [kg\cdotm^{2}]')
        zlabel('OverShoot [%]')
        title(['Meeting the requirement for Overshoot < ' num2str(Mp) ' %'])
        view(3)
        grid on
        grid minor
        figure('Name','RiseTime Comparing')
        hold on
        surf(Bvec,Jvec,RiseTimeMat)
        colorbar
        xlabel('B [N\cdotm\cdotsec]')
        ylabel('J [kg\cdotm^{2}]')
        zlabel('Rise-Time [sec]')
        title(['Meeting the requirement for Rise-Time < ' num2str(tr) ' [sec]'])
        view(3)
        grid on
        grid minor
        figure('Name','SettlingTime Comparing')
        hold on
        surf(Bvec,Jvec,SettlingTimeMat)
        colorbar
        xlabel('B [N\cdotm\cdotsec]')
        ylabel('J [kg\cdotm^{2}]')
        zlabel('Settling-Time [sec]')
        title(['Meeting the requirement for Settling-Time < ' num2str(ts) ' [sec]'])
        view(3)
        grid on
        grid minor
        figure('Name','MaxBodeMagnitude Comparing')
        hold on
        surf(Bvec,Jvec,BodeMaxMat)
        colorbar
        xlabel('B [N\cdotm\cdotsec]')
        ylabel('J [kg\cdotm^{2}]')
        zlabel('Max Magnitude [dB]')
        title(['Meeting the requirement for Bode Maximum Magnitude < ' num2str(-minTwrej) ' [dB]'])
        view(3)
        grid on
        grid minor
    %% Section 3
    satclosed = 0;
    Runclosed = 1;
    mu = 0;
    sigma = 0;
    stop = 0;
    Tmax = 10;
    while  stop == 0
        sim('ProjectModel.slx')
        InfoWithTw = stepinfo(theta(:,2),theta(:,1));
        if InfoWithTw.Overshoot<Mp && InfoWithTw.RiseTime<tr && InfoWithTw.SettlingTime<ts 
            stop = 1;
            Tmax = Tmax-10;
        end
        Tmax = Tmax+10;
    end
    fprintf('Section 3\nThe minimum T to accomplish requirements = %.f [Nm]\n',Tmax)
    %% Section 4
    Tmax = 100;
    Runclosed = 1;
    sim('ProjectModel.slx')
    stepinfo(theta(:,2),theta(:,1))
    figure('Name','T blocked - Step')
    hold on
    grid on
    grid minor
    plot(theta(:,1),theta(:,2))
    xlabel('Time [sec]')
    ylabel('\theta [rad]')
    title('Step Response when |T|<T_{max}')
    Runclosed = 0;
    sim('ProjectModel.slx')
    figure('Name','T blocked - Ramp')
    hold on
    grid on
    grid minor
    plot(theta(:,1),theta(:,2))
    xlabel('Time [sec]')
    ylabel('\theta [rad]')
    title('Ramp Response when |T|<T_{max}')
    %% Section 5
    satclosed = 1;
    NoMeasurePertur = 0;
    Runclosed = 1;
    sim('ProjectModel.slx')
    stepinfo(theta(:,2),theta(:,1))
    figure('Name','Deterministic Perturbation - Step')
    hold on
    grid on
    grid minor
    plot(theta(:,1),theta(:,2))
    xlabel('Time [sec]')
    ylabel('\theta [rad]')
    title('Step Response with Deterministic Perturbation')
    Runclosed = 0;
    sim('ProjectModel.slx')
    figure('Name','Deterministic Perturbation - Ramp')
    hold on
    grid on
    grid minor
    plot(theta(:,1),theta(:,2))
    xlabel('Time [sec]')
    ylabel('\theta [rad]')
    title('Ramp Response with Deterministic Perturbation')
    figure('Name','Deterministic Perturbation - Ramp Error')
    hold on
    grid on
    grid minor
    plot(theta(:,1),Error)
    xlabel('Time [sec]')
    ylabel('Error [rad]')
    title('Ramp Error with Deterministic Perturbation')
%% Used Function
type SysReq.m
type StaticErrorConst1.m
%% BackUp
save('BackUp.mat')
print('-sProjectModel','-dpdf','ModelProject.pdf')