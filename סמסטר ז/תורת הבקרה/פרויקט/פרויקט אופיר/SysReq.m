function work = SysReq(ts,tr,Mp,minmag,Plantsys,con,Twsys)
% this function check if the demand of the controller to the system applyed
% ts - maximum settling time [sec]
% tr - maximum rising time [sec]
% Mp - maximum overshoot [percent]
% minmag - Bode min range [dB]
% sys - open loop without controller transfer function
% con - controller transfer function

if length(zero(con))>length(pole(con))
    error 'Find Another Controller!'
end
info = stepinfo(feedback(con*Plantsys,1));
[mag,~,~] = bode(Twsys);
mag = squeeze(mag);
polesplant = pole(con*Plantsys);
Type = sum(polesplant==0);
if info.SettlingTime<=ts && info.RiseTime<=tr && info.Overshoot<Mp && max(20*log10(mag))<= -minmag && Type ==2
    work = 1;
else
    error 'Find Another Controller!'
end
end

