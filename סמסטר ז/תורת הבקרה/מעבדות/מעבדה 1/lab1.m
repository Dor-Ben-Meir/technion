% clc
% close all
% clear 

%%
% % model = 'prefilter2';
% % load_system(model);
% % sim(model);
% time1 = prefilter.time;
% Theta1 = (prefilter.signals.values);
% time1 = prefilter.time;
% %writematrix(Theta1,'Theta3.txt','Delimiter','tab')
% ref = (pi/4)* ones(1,length(Theta1));
% figure()
% plot(time1,ref,'--','LineWidth',2);
% hold on;
% grid on;
% plot(time1,Theta1,'LineWidth',1);
% title('Pre-Filter step response');
% xlabel('time [sec]'),ylabel('Response');
% legend('Refernce Input','Filter-Output');

%%
time1 = cell(1,6);
time1{1} = Theta1.time;
time1{2} = Theta2.time;
time1{3} = Theta3.time;
time1{4} = Theta4.time;
time1{5} = Theta5.time;
time1{6} = Theta6.time;

sol = cell(1,6);
sol{1} = (Theta1.signals.values);
sol{2} = (Theta2.signals.values);
sol{3} = (Theta3.signals.values);
sol{4} = (Theta4.signals.values);
sol{5} = (Theta5.signals.values);
sol{6} = (Theta6.signals.values);


Kp = [0.5 1 3 5 7 11];
[rows, columns] = size(sol);

ref = (pi/4)* ones(1,length(sol{1}));
lgd = cell(1,(length(Kp)+1));

figure();
plot(time1{1},ref,'--','LineWidth',2);
lgd{1} = sprintf('Reference Signal');
hold on;

for i = 1:columns
    plot(time1{i},sol{i});
    lgd{i+1} = sprintf('Kp =%.1f',Kp(i));    
    grid on;
    title('Motor response Proportional feedback');
    xlabel('time [sec]'),ylabel('Respond [rad]');
    
end
legend(lgd);

%%
x = -10:10;
y = -10:10;
x0 = x * 0;
y0 = y * 0; 

lgd2 = cell(1,length(Kp));

figure();
hold on;
for i = 1:columns
    s = stepinfo(sol{i},time1{i},'SettlingTimeThreshold',0.05);
    os = s.Overshoot / 100;
    ts = s.SettlingTime;
    tr = s.RiseTime;
    
    Zetta = -log(os) / (sqrt((pi^2) + ((log(os))^2)));
    Wn = 3 / (Zetta * ts);

    Rel_p1 = -Zetta*Wn;
    Img_p1 = Wn * sqrt(1 - (Zetta^2));
    p1 = complex(Rel_p1,Img_p1);

    Rel_p2 = -Zetta*Wn;
    Img_p2 = -Wn * sqrt(1 - (Zetta^2));
    p2 = complex(Rel_p2,Img_p2);
    
    Rel = [Rel_p1 Rel_p2];
    img = [Img_p1 Img_p2];
    
    plot(Rel,img,'*');
    lgd2{i} = sprintf('Kp =%.1f',Kp(i));    
    grid on;
    title('Closed-loop poles');
    xlabel('Re [S]'),ylabel('Im [S]');
     
end
plot(x0 ,y,'b',x,y0,'b');
legend(lgd2);

%%
time2 = cell(1,6);
time2{1} = Theta1.time;
time2{2} = Theta2.time;
time2{3} = Theta3.time;
time2{4} = Theta4.time;
time2{5} = Theta5.time;
time2{6} = Theta6.time;

sol2 = cell(1,6);
sol2{1} = (Theta1.signals.values);
sol2{2} = (Theta2.signals.values);
sol2{3} = (Theta3.signals.values);
sol2{4} = (Theta4.signals.values);
sol2{5} = (Theta5.signals.values);
sol2{6} = (Theta6.signals.values);

KI = [0 1 3 5 7 9];
[rows, columns] = size(sol2);

ref = (pi/4)* ones(1,length(sol2{1}))

lgd3 = cell(1,(length(KI)+1));

figure();
plot(time2{1},ref,'--','LineWidth',2);
lgd{1} = sprintf('Reference Signal');
hold on;

for i = 1:columns
    plot(time1{i},sol{i});
    lgd3{i+1} = sprintf('Kp =%.1f',KI(i));    
    grid on;
    title('Motor response PI feedback');
    xlabel('time [sec]'),ylabel('Respond [rad]');
    
end
legend(lgd3);

