clear all
close all
clc

K_u = 1.2;
R_a = 2.2;
K_m = 24 * (10^-3);
J = 5 * (10^-3);
K_b = 24 * (10^-3);
L_a = 0.26 * (10^-3);
b = 4.3 * (10^-3);
N_r = 4.9;
5.1:

s = tf('s');
L_a = 0;
Num2 = 1 + (K_b* N_r * (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U2 = minreal((K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b)))) / (s*Num2))
os = 0.2;
ts = 1;
Zetta = -log(os) / sqrt((pi^2) + ((log(os))^2));
Wn = 3 / (Zetta * ts);

Zetta = 0.55;
Wn = 5.5;

Rel_p =  -Zetta * Wn;
Img_p1 = Wn*sqrt(1 - (Zetta^2));
Img_p2 = -Wn*sqrt(1 - (Zetta^2));
p = complex(Rel_p,Img_p1);

Root Locus with proportional controller:
C_g = -0.2117 / 2;

Rel = [Rel_p Rel_p];
img = [Img_p1 Img_p2];

figure();
plot(Rel,img,'r*');
lgd = sprintf('Sp');
hold on;
rlocus(Theta_to_U2);
title('Root Locus with proportional controller');

legend(lgd);
hold off;

Siv = -angle(p) - angle( p + 2.117);
a = -pi - Siv;
Td = tan(a) / (Img_p1 - tan(a)*Rel_p);
KpD = 1 / ((abs(1 + Td*p) * 12.829) / (abs( p) * abs(p + 2.117)));
D_pd = KpD * (1 + Td*s);
K_D = KpD * Td;
Table3 = table(Td,KpD,K_D)
Root Locus with PD controller:
figure();
plot(Rel,img,'r*');
lgd = sprintf('Sp');
hold on;
rlocus((KpD + K_D*s) * Theta_to_U2)
title('Root Locus with PD controller');

legend(lgd);
hold off;

figure();
plot(Rel,img,'r*');
lgd = sprintf('Sp');
hold on;
rlocus((KpD + K_D*s + ((KpD*0.3)/s)) * Theta_to_U2)
title('Root Locus with PID controller');

legend(lgd);
hold off;

7.3.1:
Motor input:
time1 = MinputPre.time;
time2 = Minputlab.time;

sol1 = MinputPre.signals.values;
sol2 = Minputlab.signals.values;

figure();
plot(time1,sol1,time2,sol2);   
grid on;
title('Motor Input');
xlabel('Time [sec]'),ylabel('Input [Va]');
legend('Prelab-Input','Lab-Input');

Motor output:
time3 = MoutputPre.time;
time4 = Moutputlab.time;
time5 = ref.time;

sol3 = MoutputPre.signals.values;
sol4 = Moutputlab.signals.values;
sol5 = ref.signals.values;

figure();
plot(time5,sol5,'--','LineWidth',2);
hold on;
plot(time3,sol3,time4,sol4);   
grid on;
title('Motor Output');
xlabel('Time [sec]'),ylabel('Output [rad]');
legend('reference signal','Prelab-Output','Lab-Output');

Error:
time6 = errorPre.time;
time7 = errorlab.time;

sol6 = errorPre.signals.values;
sol7 = errorlab.signals.values;

figure();
plot(time6,sol6,time7,sol7);   
grid on;
title('Error');
xlabel('Time [sec]'),ylabel('Error [rad]');
legend('Prelab-Error','Lab-Error');

7.3.2:
Pre-lab systeminfo:
Ts = stepinfo(sol1,time1,'SettlingTimeThreshold',0.05);
os = Ts.Overshoot/100; 
ts = Ts.SettlingTime;
tr = Ts.RiseTime;

RiseTime = tr;
BandWidth = BW1;
SettlingTime = ts;
Overshoot = os;
Table1 = table(BandWidth,SettlingTime,Overshoot,RiseTime)
Lab systeminfo:
Ts = stepinfo(sol2,time2,'SettlingTimeThreshold',0.05);
os = Ts.Overshoot/100; 
ts = Ts.SettlingTime;
tr = Ts.RiseTime;

RiseTime = tr;
BandWidth = BW1;
SettlingTime = ts;
Overshoot = os;
Table2 = table(BandWidth,SettlingTime,Overshoot,RiseTime)
7.3.3:

Hz = [1 3 5 7];

time = cell(1,4);
time{1} = Theata1.time;
time{2} = Theata2.time;
time{3} = Theata3.time;
time{4} = Theata4.time;

% The name of the Reference in simulnik is Tref1....Tref4
ref = cell(1,4);
ref{1} = (Tref1.signals.values);
ref{2} = (Tref2.signals.values);
ref{3} = (Tref3.signals.values);
ref{4} = (Tref4.signals.values);

% The name of the Theta_prelab in simulnik is Theta1....Theta4
Theta_prelab = cell(1,4);
Theta_prelab{1} = (Theata1.signals.values);
Theta_prelab{2} = (Theata2.signals.values);
Theta_prelab{3} = (Theata3.signals.values);
Theta_prelab{4} = (Theata4.signals.values);

% The name of the Theta_lab in simulnik is Tlab1....Tlab4
Theta_lab = cell(1,4);
Theta_lab{1} = (Tlab1.signals.values);
Theta_lab{2} = (Tlab2.signals.values);
Theta_lab{3} = (Tlab3.signals.values);
Theta_lab{4} = (Tlab4.signals.values);

Gain_prelab = zeros(4,1);
Gain_prelab_DB = zeros(4,1);
Phase_prelab = zeros(4,1);

Gain_lab = zeros(4,1);
Gain_lab_DB = zeros(4,1);
Phase_lab = zeros(4,1);
 
Diffrence_Gain = zeros(4,1);
Diffrence_Phase = zeros(4,1);
 
Freqency = cell(4,1);
for i = 1:4
    t = time{i};
    x = ref{i};
    y1 = Theta_prelab{i};
    y2 = Theta_lab{i};
    
    figure();
    plot(t,x,t,y1,t,y2);
    title (sprintf('Hz =%d[Hz]',Hz(i)));
    legend('Input', 'Prelab',  'Lab');
    grid on;
    
    [Amp_prelab, Ph_prelab] = freqrespmeasure(x, y1);
    Gain_prelab(i) = Amp_prelab;
    Gain_prelab_DB(i) = db(Amp_prelab);
    Phase_prelab(i) = Ph_prelab;
    
    [Amp_lab, Ph_lab] = freqrespmeasure(x, y2);
    Gain_lab(i) = Amp_lab;
    Gain_lab_DB(i) = db(Amp_lab);
    Phase_lab(i) = Ph_lab;
    
    Diffrence_Gain(i) = abs(Gain_prelab(i) - Gain_lab(i));
    Diffrence_Phase(i) = abs(Phase_prelab(i) - Phase_lab(i));
    
    Freqency{i} = sprintf('%d[Hz]',Hz(i)); 
end

table3 = table(Freqency,Gain_prelab,Gain_prelab_DB,Gain_lab,Gain_lab_DB,Diffrence_Gain)

table4 = table(Freqency,Phase_prelab,Phase_lab,Diffrence_Phase)
