clc
close all
clear 

Theta1 = zeros;
Theta2 = zeros;
Theta3 = zeros;
Theta4 = zeros;
omega = zeros;
 p = zeros;
K_u = 1.2;
R_a = 2.2;
K_m = 24;
J = 0.005;
K_b = 0.024;
L_a = 0.26;
b = 0.0043;
N_r = 4.9;


Kp = 0.25;
Td = 1;



    model = 'prelab_six_sim';
    load_system(model);
    sim(model);
% 
%     Theta2 = theta1.signals.values;
%     time2 = theta1.time;
% 
%    figure()
%     plot(time2,Theta2) 
%     grid on;
%     title('\Theta under proportional control');
%     xlabel('time [sec]'),ylabel('\theta(t) [rad * \pi]');