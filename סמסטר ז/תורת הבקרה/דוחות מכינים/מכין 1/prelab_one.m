clc
close all
clear 

Theta1 = zeros;
Theta2 = zeros;
Theta3 = zeros;
Theta4 = zeros;
omega = zeros;
p = zeros;
K_u = 1.2;
R_a = 2.2;
K_m = 24;
J = 0.005;
K_b = 0.024;
L_a = 0.26;
b = 0.0043;
N_r = 4.9;

%%
model = 'prelab_one_sim';
load_system(model);
sim(model);


Theta1 = theta.signals.values;
omega = w1.signals.values;
% Theta1 = theta.signals.values;
% omega = w.signals.values;
time1 = theta.time;
s= stepinfo(Theta1,time1);
os= s.Overshoot;
ts= s.SettlingTime;
tr= s.RiseTime;


figure()
plot(time1,Theta1);
grid;
title('\Theta under step response');
xlabel('time [sec]'),ylabel('\theta(t)  [rad]');

figure()
plot(tout,omega);
grid;
title('\Omega under step response');
xlabel('time [sec]'),ylabel('\omega(t) [deg] ');
% % 
% % 
% % %%
% %model = 'prelab_two_sim';
% Kp = 0.25;
% figure()
% hold
% 
% for Td = [ 1 ]
%     model2 = 'prelab_two_sim';
%     load_system(model2);
%     sim(model2);
% 
%     Theta2 = theta1.signals.values;
%     time2 = theta1.time;
% 
%    
%     plot(time2,Theta2,'DisplayName',sprintf('Td =%f',Td)); 
%     %axis([0 3 0 0.17])
%     grid on;
%     title('\Theta under proportional control');
%     xlabel('time [sec]'),ylabel('\theta(t) [rad * \pi]');
%     
%     
%     legend;
%     
% end
% 
% 
%  %%
% % Kp = 0.25;
% % Td = 1;
% % model2 = 'prelab_fouru_sim';
% % load_system(model2);
% % sim(model2);
% % 
% % Theta3 = rad2deg(bob.signals.values);
% % time3 = bob.time;
% % 
% % figure()
% % plot(time3,Theta3);
% % %axis([0 5 0 0.23])
% % grid;
% % title('\Theta under PI controller');
% % xlabel('time [sec]'),ylabel('\theta(t) [rad * \pi]');
% % 
% % 
% % 
% % %%
% model = 'prelab_five_sim';
% load_system(model);
% sim(model);
% 
% Theta4 = theta5.signals.values / pi;
% time4 = theta5.time;
% 
% figure()
% plot(time4,Theta4);
% axis([0 5 0 0.23])
% grid;
% title('\Theta under step response');
% xlabel('time [sec]'),ylabel('\theta(t) [rad * \pi]');
