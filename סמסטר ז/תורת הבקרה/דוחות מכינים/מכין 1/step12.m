s = tf('s');
Wn = 4;
zeta = 0.3;


G_ol = Wn^2 / (s * (s + 2*Wn));
figure()
step(G_ol)
grid on;

G_cl = Wn^2 / (s^2 + 2*Wn*s*zeta + Wn^2) ;
figure()
step(G_cl)
grid on;