clc
close all
clear 


BW = zeros;
ts = zeros;
Mp = zeros;


s = tf('s');
i = 1;
for Kp = [0.025 0.05 1 10];
	G = 10 / (s * (2*s + 1));
	YtoR = Kp*G / (1 + Kp*G);
	EtoR = 1 / (1 + Kp*G);
	UtoR = Kp / (1 + Kp*G);
  	r = pole(YtoR)
	data = stepinfo(YtoR);
	BW(i) = bandwidth(YtoR);
	ts(i) = data.SettlingTime;
	Mp(i) = data.Overshoot;
	figure()
	step(YtoR)
	bode(YtoR)
	grid on;
	i = i + 1;
	
end
Kp = [0.025 0.05 1 10];
Kp = transpose(Kp);
BW = transpose(BW);
SettlingTime = transpose(ts);
Overshoot = transpose(Mp);


Table1 = table(Kp,BW,SettlingTime,Overshoot);


%%
G = 10 / (s * ( 2 * s + 1) * ( 0.01 * s + 1));
Kp = 0.9;
YtoR = Kp*G / (1 + Kp*G);
EtoR = 1 / (1 + Kp*G);
r = pole(YtoR)
step(YtoR)
grid on;
data = stepinfo(YtoR);
BW = bandwidth(YtoR);
ts = data.SettlingTime;
Mp = data.Overshoot;

BW = transpose(BW);
SettlingTime = transpose(ts);
Overshoot = transpose(Mp);
Table2 = table(BW,SettlingTime,Overshoot);