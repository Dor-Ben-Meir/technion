clc
close all
clear 

% Theta1 = zeros;
% Theta2 = zeros;
% Theta3 = zeros;
% Theta4 = zeros;
% omega = zeros;
p = zeros;
K_u = 1.2;
R_a = 2.2;
K_m = 24;
J = 0.005;
K_b = 0.024;
L_a = 0.26;
b = 0.0043;
N_r = 4.9;

model = 'prelab_five_sim';
load_system(model);
sim(model);

Theta4 = theta5.signals.values;
time4 = theta5.time;

s= stepinfo(Theta4,time4,'SettlingTimeThreshold',0.05);
os= s.Overshoot;
ts= s.SettlingTime;
tr= s.RiseTime;

figure()
plot(time4,Theta4);
grid;
title('\Theta under step response');
xlabel('time [sec]'),ylabel('\theta(t) [deg]');