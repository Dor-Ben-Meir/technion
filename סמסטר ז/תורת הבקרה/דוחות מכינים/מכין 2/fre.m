

function [mag_x,mag_y,amplitude_ratio, phase_lag] = fre(x, y, t)

% remove bias
% x = x - mean(x);
% y = y - mean(y);

% take the FFT
% X=fft(x);
 Y=fft(y);

 X=x;



% Determine the max value and max point.
% This is where the sinusoidal
% is located. See Figure 2.
[mag_x idx_x] = max(abs(X)/2);
[mag_y idx_y] = max(abs(Y)/2);
% determine the phase difference
% at the maximum point.
px = angle(X(idx_x));
py = angle(Y(idx_y));
phase_lag = py - px
% determine the amplitude scaling
amplitude_ratio = mag_y/mag_x

% plot the signal
figure(1)
plot(t,x,t,y);
xlabel('Time (s)');
ylabel('Amplitude');



