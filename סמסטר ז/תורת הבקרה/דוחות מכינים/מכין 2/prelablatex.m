clear all
close all
clc


syms s K_b N_r L_a R_a K_m J b K_u ;
Num = 1 + (K_b* N_r * (1 / (L_a*s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U1 = K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b))) / (s*Num);
[n, d] = numden(Theta_to_U1);
latex(n/d)


syms s K_b N_r L_a R_a K_m J b K_u ;
L_a = 0;
Num = 1 + (K_b* N_r * (1 / (L_a*s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U1 = K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b))) / (s*Num);
[n, d] = numden(Theta_to_U1);
latex(n/d)


K_u = 1.2;
R_a = 2.2;
K_m = 24 * (10^-3);
J = 5 * (10^-3);
K_b = 24 * (10^-3);
L_a = 0.26 * (10^-3);
b = 4.3 * (10^-3);
N_r = 4.9;

syms s;
Num = 1 + (K_b* N_r * (1 / (L_a*s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U1 = K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b))) / (s*Num);
[n, d] = numden(Theta_to_U1);
latex(simplify(n/d))


syms s;
L_a = 0;
Num = 1 + (K_b* N_r * (1 / (L_a*s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U1 = K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b))) / (s*Num);
[n, d] = numden(Theta_to_U1);
latex(simplify(n/d))

syms p ;
Siv = -angle(p) - angle( p + 2.117);
a = -pi - Siv;
Td = tan(a) / (Img_p1 - tan(a)*Rel_p);
KpD = 1 / ((abs(1 + Td*p) * 12.829) / (abs( p) * abs(p + 2.117)));
D_pd = KpD * (1 + Td*s);
K_D = KpD * Td;
latex(Siv)

syms os ts Zetta Wn ;
os = 0.2;
ts = 1;
Zetta = -log(os) / sqrt((pi^2) + ((log(os))^2));
Wn = 3 / (Zetta * ts);
syms Img_p1 Rel_p;
Td = tan(a) / (Img_p1 - tan(a)*Rel_p);
latex(Td)
