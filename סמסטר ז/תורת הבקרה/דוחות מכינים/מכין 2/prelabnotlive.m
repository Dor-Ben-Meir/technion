clear all
close all
clc

s = tf('s');
G_phi_u = (-7.3 * s) / ((s - 5.2) * (s+ 7.4) * (s + 2.7));
G_theata_u = (10 * (s - 4.6) * (s + 4.6)) / ((s - 5.2) * (s+ 7.4) * (s + 2.7));


rlocus(G_phi_u);
title('Root Locus K > 0');
nyquist(G_phi_u);
title('Nyquist plot K > 0');

rlocus(-G_phi_u);
title('Root Locus K < 0');
nyquist(-G_phi_u);
title('Nyquist plot K < 0');

for a = [ -3 -1.5  ]
    
    rlocus(-G_phi_u * ((s + a)/s));
    hold on;
end
legend('a = -1.5', 'a = -3');
title('Root Locus K < 0 ; a < 0');
hold off;

for a = [  1.5  3 ]
    
    rlocus(-G_phi_u * ((s + a)/s));
    hold on;
end
title('Root Locus K > 0 ; a > 0');
legend('a = 1.5', 'a = 3')
hold off;

a = 3;
nyquist(-G_phi_u * ((s + a)/s));
title('Nyquist plot K < 0 ; a = 3');


a = 2.7;
os = 0.1;
Zetta_min = -log(os) / (sqrt((pi^2) + ((log(os))^2)));

rlocus(minreal(-G_phi_u * ((s + a)/s)));
hold on;
sgrid;
