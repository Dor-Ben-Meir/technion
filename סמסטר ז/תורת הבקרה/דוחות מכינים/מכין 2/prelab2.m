clear all
close all
clc
%%
K_u = 1.2;
R_a = 2.2;
K_m = 24 * (10^-3);
J = 5 * (10^-3);
K_b = 24 * (10^-3);
L_a = 0.26 * (10^-3);
b = 4.3 * (10^-3);
N_r = 4.9;

s = tf('s');


Num = 1 + (K_b* N_r * (1 / (L_a*s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U1 = K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b))) / (s*Num); 

step(Theta_to_U1)
xlim([0 2]);
title ('\Theta 3rd-order model step response');
ylim([0 10]);
grid on

L_a = 0;
Num2 = 1 + (K_b* N_r * (1 / (L_a*s + R_a)) * K_m * N_r * (1 / (J*s + b)));
Theta_to_U2 = K_u * ( (1 / (L_a * s + R_a)) * K_m * N_r * (1 / (J*s + b)) / (s*Num2));

figure();
step(Theta_to_U2)
xlim([0 2]);
ylim([0 10]);
title ('\Theta 2rd-order model step response');
grid on

%%
os = 0.2;
ts = 1;
Zetta = -log(os) / sqrt((pi^2) + ((log(os))^2));
Wn = 3 / (Zetta * ts);

Zetta = 0.55;
Wn = 5.5;

Rel_p =  -Zetta * Wn;
Img_p1 = Wn*sqrt(1 - (Zetta^2));
Img_p2 = -Wn*sqrt(1 - (Zetta^2));
p = complex(Rel_p,Img_p1);
C_g = -0.2117 / 2;

Rel = [Rel_p Rel_p];
img = [Img_p1 Img_p2];

figure();
plot(Rel,img,'r*');
lgd = sprintf('Sp');
hold
rlocus(Theta_to_U2)

legend(lgd)

Siv = -angle(p) - angle( p + 2.117);
a = -pi - Siv;
Td = tan(a) / (Img_p1 - tan(a)*Rel_p);
KpD = 1 / ((abs(1 + Td*p) * 12.829) / (abs( p) * abs(p + 2.117)));
D_pd = KpD * (1 + Td*s);
K_D = KpD * Td;
Table3 = table(Td,KpD,K_D );
Hpd = ((KpD + K_D*s) * Theta_to_U2) / (1 + ((KpD + K_D*s) * Theta_to_U2)); 

model = 'prelab2sim1';
load_system(model);
sim(model);

Theta1 = (ans.Theata.signals.values);
time1 = ans.Theata.time;

Tref1= (ans.Tref.signals.values);
Error1 = (ans.error.signals.values);
Va1 = (ans.Va.signals.values);


figure()
hold
plot(time1,Theta1);
xlim([0 2.5]);
title('\Theta under step response');
xlabel('time [sec]'),ylabel('\theta(t)  [rad]');

plot(time1,Tref1,'--');
plot(time1,Error1,'r-.');
plot(time1,Va1,'g:','LineWidth',1);
legend('\Theta under step response' , '\Theta Reference', 'Error', 'Va[V]')
grid on


Ts = stepinfo(Theta1,time1,'SettlingTimeThreshold',0.05);
os = Ts.Overshoot/100; 
ts = Ts.SettlingTime;
tr = Ts.RiseTime;
BW1 = bandwidth(Hpd);

RiseTime = tr;
BandWidth = BW1;
SettlingTime = ts;
Overshoot = os;
Table1 = table(BandWidth,SettlingTime,Overshoot,RiseTime);

%%
epsi = 0.02;
model = 'prelab2sim2';
load_system(model);
sim(model);

Theta1 = (ans.Theata.signals.values);
time1 = ans.Theata.time;
Tref1= (ans.Tref.signals.values);
Error1 = (ans.error.signals.values);
Va1 = (ans.Va.signals.values);

figure()
hold
plot(time1,Theta1);
xlim([0 2.5]);
title('\Theta under step response');
xlabel('time [sec]'),ylabel('\theta(t)  [rad]');

plot(time1,Tref1,'--');
plot(time1,Error1,'r-.');
plot(time1,Va1,'g:');
legend('\Theta under step response' , '\Theta Reference', 'Error', 'Va[V]')
grid on

Ts = stepinfo(Theta1,time1,'SettlingTimeThreshold',0.05);
os = Ts.Overshoot/100; 
ts = Ts.SettlingTime;
tr = Ts.RiseTime;
BW1 = bandwidth(Hpd);

RiseTime = tr;
SettlingTime = ts;
Overshoot = os;
Table2 = table(SettlingTime,Overshoot,RiseTime);




