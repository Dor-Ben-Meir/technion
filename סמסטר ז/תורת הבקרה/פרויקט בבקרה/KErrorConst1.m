function [kv, evss, Type] = KErrorConst1(sys)

s = tf('s');
[num,den] = tfdata(minreal(s*sys)); 

den = cell2mat(den);
num = cell2mat(num);
% Lim(s) = 0
kv = num(end)/den(end);
evss = 1/kv;
p = pole(sys);
Type = sum(p==0); 
end
