import math


L = 6.5 * (10**-3)
Tsl_std = 288 #[k]
Ttr = 216.7 #[K]
Psl_std = 101325 #[Pa]
Rhosl_std = 1.225 #[kg / m^3]
g = 9.81 #[m/s^2]
R = 287.1 # [ J / kg * K]

ft2meter = 0.3048

def P1(rho,R,T):
    P = rho * R * T
    return P
    
def P_two(Psl,L,h,Tsl,g,R):
    P = Psl * ((1 - (L*Delta_h / Tsl)) ** (g/(L*R)))
    return P
    
def rho_one(rhosl,L,h,Tsl,g,R):
    rho = rhosl * ((1 - (L*h / Tsl)) ** ((g/(L*R) - 1)))
    return rho
    
def h_one(P,Psl,L,R,g,Tsl):
    h = (1 - ((P/ Psl) ** (L * R / g))) * (Tsl / L)
    return h

def a_one(gamma,R,T):
    a = math.sqrt(gamma*R*T)
    return a

def VT(gamma,P,rho,delata_p):
    VT = math.sqrt((2 * gamma / (gamma - 1)) * (P / rho) * (( ((delata_p / P) + 1) ** (gamma - 1 / gamma)) - 1))
    return VT
    
def Ve_one(gamma,P,rhosl,delata_p):
    Ve = math.sqrt((2 * gamma / (gamma - 1)) * (P / rhosl) * (( ((delata_p / P) + 1) ** (gamma - 1 / gamma)) - 1))
    return Ve
    
def Ve_two(rho,rhosl,VT):
    Ve = math.sqrt(rho / rhosl) * VT
    return Ve
    
def Vc(gamma,Psl,rhosl,delata_p):
    Vc = math.sqrt((2 * gamma / (gamma - 1)) * (Psl / rhosl) * (( ((delata_p / Psl) + 1) ** (gamma - 1 / gamma)) - 1))
    return Vc

h = 700
R = rho_one(Rhosl_std,L,h,Tsl_std ,g,R)
    
 
## q3: 
#Psl = Psl_std
#Tsl = Tsl_std 
#P1 = 96000 # [Pa]
#P2 = 95000 # [Pa]
#
#h1 = h_one(P1,Psl,L,R,g,Tsl)
#h2 = h_one(P2,Psl,L,R,g,Tsl)
#Delta_h = h2 - h1
#print(Delta_h)
#
## delta_h = 87.26 [m]

#It can be seen that in a high pressure zone a smaller pressure height is obtained than that obtained in a low pressure zon.
#In order to maintain a constant pressure altitude the pilot will compensate for the increase in the pressure altitude by a decrease in the geometric altitude, 
#and thus he may place obstacles in his route that he would avoid in the previous flight altitude.

#########################################################################################################


# q4:

#hp = 13000 * ft2meter
#hs = 8200 * ft2meter
#Psl = 74670 # [Pa]
#Tsl = Tsl_std 
#Delta_h = hp - hs
#
## Calculte P
#P = Ptwo(Psl,L,Delta_h,Tsl,g,R)
#
## Calculte h
#h = h_one(P,Psl_std,L,R,g,Tsl)
#
#print(h)
#
##h = 3880.104 [m]
#
#
#
#Tsl2 = 272
## Calculte P
#P = Ptwo(Psl,L,Delta_h,Tsl2,g,R)
#
## Calculte h
#h2 = h_one(P,Psl_std,L,R,g,Tsl_std)
#

#print(h2)

#h2 = 3961.31[m]

# When it can be assumed that there is an error due to value rounding. Hence the second day is indeed standard.

