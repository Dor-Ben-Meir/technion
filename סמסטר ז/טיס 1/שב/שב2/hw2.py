import math


L = 6.5 * (10**-3)
Tsl_std = 288 #[k]
Ttr = 216.7 #[K]
Psl_std = 101325 #[Pa]
Rhosl_std = 1.225 #[kg / m^3]
g = 9.81 #[m/s^2]
R = 287.1 # [ J / kg * K]
gamma = 1.4

ft2meter = 0.3048

def T(Tsl,L,h):
    T = Tsl - L*h
    return T

def P1(rho,R,T):
    P = rho * R * T
    return P
    
def P_two(Psl,L,h,Tsl,g,R):
    P = Psl * ((1 - (L*h / Tsl)) ** (g/(L*R)))
    return P
    
def Delta_P_one(gamma,P,rho,V):
    Delta_P = P * ((((rho/(2*P)) * (V**2) * ((gamma - 1) / gamma) + 1) ** (gamma / (gamma - 1))) - 1)
    return Delta_P
    
def Delta_P_two(gamma,P,M):
    Delta_P = P * (((1 + ((gamma - 1) / 2) * M**2) ** (gamma / (gamma - 1))) - 1)
    return Delta_P
    
def rho_one(rhosl,L,h,Tsl,g,R):
    rho = rhosl * ((1 - (L*h / Tsl)) ** ((g/(L*R) - 1)))
    return rho
    
def h_one(P,Psl,L,R,g,Tsl):
    h = (1 - ((P/ Psl) ** (L * R / g))) * (Tsl / L)
    return h

def a_one(gamma,R,T):
    a = math.sqrt(gamma *R * T)
    return a

def VT(gamma,P,rho,delata_p):
    VT = math.sqrt((2 * gamma / (gamma - 1)) * (P / rho) * (( ((delata_p / P) + 1) ** ((gamma - 1) / gamma)) - 1))
    return VT
    
def Ve_one(gamma,P,rhosl,delata_p):
    Ve = math.sqrt((2 * gamma / (gamma - 1)) * (P / rhosl) * (( ((delata_p / P) + 1) ** ((gamma - 1) / gamma)) - 1))
    return Ve
    
def Ve_two(rho,rhosl,VT):
    Ve = math.sqrt(rho / rhosl) * VT
    return Ve
    
def Vc(gamma,Psl,rhosl,delata_p):
    Vc = math.sqrt((2 * gamma / (gamma - 1)) * (Psl / rhosl) * (( ((delata_p / Psl) + 1) ** ((gamma - 1) / gamma)) - 1))
    return Vc
    
def M_one(gamma,P,Delta_P):
    M = math.sqrt(((((Delta_P / P) + 1) ** ((gamma - 1) / gamma)) - 1) * (2 / (gamma - 1)))
    return M
    
    
# for 5000[ft]:
h = 5000 * ft2meter
P = 0.8321 * Psl_std
rho = 0.8617 * Rhosl_std
V_GPS = 150 * (10**3) / (60 ** 2) # [m/sec] 
VT1 = V_GPS
Delta_P = Delta_P_one(gamma,P,rho,VT1)
VE = Ve_one(gamma,P,Rhosl_std,Delta_P)
VC = Vc(gamma,Psl_std,Rhosl_std,Delta_P)
print("VC1 =",VC)
print("VE1 =",VE)
print("********************************************")

# for T = 308 [K]:
Tsl = 308 #[K]
Vw = 2.57 # [m/sec]
Psl = Psl_std
rhosl = Psl / (R * Tsl)
P = P_two(Psl,L,h,Tsl,g,R) # [Pa]
rho = rho_one(rhosl,L,h,Tsl,g,R) # [Kg / m^3]
VT2 = V_GPS + Vw
Delta_P = Delta_P_one(gamma,P,rho,VT2)
VE = Ve_one(gamma,P,Rhosl_std,Delta_P)
VC = Vc(gamma,Psl_std,Rhosl_std,Delta_P)
print("VT2 =",VT2)
print("Delta_P =",Delta_P)
print("VC2 =",VC)
print("VE2 =",VE)
print("********************************************")
##
# Mach calculation:
a1 = 334.32 # [m/sec]
M1 = VT1 / a1
T2 = T(Tsl,L,h)
a2 = a_one(gamma,R,T2)
M2 = VT2 / a2
print("M1 =",M1)
print("M2 =",M2)
print("********************************************")

# Q2.1:
vc = 180.1 # [m/sec]
M = 0.91
Delta_P = Delta_P_one(gamma,Psl_std,Rhosl_std,vc)
P = Delta_P / Delta_P_two(gamma,1,M)
h = h_one(P,Psl_std,L,R,g,Tsl_std)
print("Delta_P =",Delta_P)
print("P =",P)
print("h =",h)
print("********************************************")

Tsl = 278 # [K]
h2 = h_one(P,Psl_std,L,R,g,Tsl)
print("P =",P)
print("h2 =",h2)
print("********************************************")

# Q2.2:
VC = 118.3 # [m/sec]
h_p = 20000 * ft2meter #[m]
P = P_two(Psl_std,L,h_p,Tsl_std,g,R)
Delta_P = Delta_P_one(gamma,Psl_std,Rhosl_std,VC)
M = M_one(gamma,P,Delta_P)
print("P =",P)
print("Delta_P =",Delta_P)
print("h_p =",h_p)
print("M  =", M )
 