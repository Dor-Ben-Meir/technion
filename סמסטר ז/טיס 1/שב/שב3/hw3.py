import math

import numpy as np
from numpy import linalg as LA
from numpy import pi as pi
import pandas as pd

L = 6.5 * (10**-3)
Tsl_std = 288 #[k]
Ttr = 216.7 #[K]
Psl_std = 101325 #[Pa]
Rhosl_std = 1.225 #[kg / m^3]
g = 9.81 #[m/s^2]
R = 287.1 # [ J / kg * K]
gamma = 1.4

ft2meter = 0.3048
red2dag = 0.0174533


def T(Tsl,L,h):
    T = Tsl - L*h
    return T

def P1(rho,R,T):
    P = rho * R * T
    return P
    
def P_two(Psl,L,h,Tsl,g,R):
    P = Psl * ((1 - (L*h / Tsl)) ** (g/(L*R)))
    return P
    
def Delta_P_one(gamma,P,rho,V):
    Delta_P = P * ((((rho/(2*P)) * (V**2) * ((gamma - 1) / gamma) + 1) ** (gamma / (gamma - 1))) - 1)
    return Delta_P
    
def Delta_P_two(gamma,P,M):
    Delta_P = P * (((1 + ((gamma - 1) / 2) * M**2) ** (gamma / (gamma - 1))) - 1)
    return Delta_P
    
def rho_one(rhosl,L,h,Tsl,g,R):
    rho = rhosl * ((1 - (L*h / Tsl)) ** ((g/(L*R) - 1)))
    return rho
    
def rho_two(Psl,R,Tsl):
    rho = Psl / (R * Tsl)
    return rho
    
    
def h_one(P,Psl,L,R,g,Tsl):
    h = (1 - ((P/ Psl) ** (L * R / g))) * (Tsl / L)
    return h

def a_one(gamma,R,T):
    a = math.sqrt(gamma *R * T)
    return a

def VT(gamma,P,rho,delata_p):
    VT = math.sqrt((2 * gamma / (gamma - 1)) * (P / rho) * (( ((delata_p / P) + 1) ** ((gamma - 1) / gamma)) - 1))
    return VT
    
def Ve_one(gamma,P,rhosl,delata_p):
    Ve = math.sqrt((2 * gamma / (gamma - 1)) * (P / rhosl) * (( ((delata_p / P) + 1) ** ((gamma - 1) / gamma)) - 1))
    return Ve
    
def Ve_two(rho,rhosl,VT):
    Ve = math.sqrt(rho / rhosl) * VT
    return Ve
    
def Vc(gamma,Psl,rhosl,delata_p):
    Vc = math.sqrt((2 * gamma / (gamma - 1)) * (Psl / rhosl) * (( ((delata_p / Psl) + 1) ** ((gamma - 1) / gamma)) - 1))
    return Vc
    
def M_one(gamma,P,Delta_P):
    M = math.sqrt(((((Delta_P / P) + 1) ** ((gamma - 1) / gamma)) - 1) * (2 / (gamma - 1)))
    return M
    
def e_one(AR):
    e = 1.78 * ( 1 - 0.044 * (AR ** 0.68)) - 0.64
    return e
   
def K_one(AR,e):
    K = 1 / (pi * AR * e)
    return K

#Lambda on radians    
def Swing_wet(Sw,Lambda,t_to_c):
    Swet = (1.977 + 0.52 * t_to_c) * (Sw / np.cos(Lambda))
    return Swet
    
def Sbody_wet(K,S_top,S_side):
    Sb_wet = (K/2) * (S_top + S_side)
    return Sb_wet

def Stotal_wet(Swing_wet,Sbody_wet):
    Stotal = Swing_wet + Sbody_wet
    return Stotal
    
def Cd_zero(Cfe, Stotal_wet, S_ref):
    Cd = Cfe * Stotal_wet / S_ref
    return Cd
 
 
def Vstoll( W_to_S, rhosl, CL_max):
    Vs = math.sqrt((2 * W_to_S) / (rhosl * CL_max))
    return Vs

def D_two(rho,V,S,Cd0,K,W):
    D = 0.5 *(rho* (V**2) * S * Cd0) + (2 * K * (W**2)) / (rho* (V**2) * S)
    return D


    
def V_star(rho,W_to_S,K, Cd0):
    V = np.sqrt((2/rho) * W_to_S * np.sqrt(K/Cd0))
    return V
    
def Cl_star_one(W_to_S,rho,Vstar):
    Cl = 2 * W_to_S / (rho * (Vstar**2))
    return Cl
    
def Cl_star_two(Cd0, K):
    Cl = np.sqrt(Cd0 / K)
    return Cl
    
def Cd_star(Cd0):
    Cd = 2 * Cd0
    return Cd
    
def D_one(W,K,Cd0):
    D = 2 * W * np.sqrt(K * Cd0)
    return D
    
def gamma_min_one(Cd0,K):
    gamma = 2 * np.sqrt(Cd0 * K)
    return gamma



def print_dic(answer):
    for key in answer.keys():           
        answer[key] = np.round(answer[key],3)
        print(key,"=",answer[key])
        

   
    
def q_one():    
    
    Lambda = 0.122173
    t_to_c = 0.15
    Sw = 14.12
    S_top = 8.2
    S_side = 8.1
    S_ref = 1.1 * Sw
    Cfe = 0.0055
    K = 3.4

    Sw_wet = Swing_wet(Sw,Lambda,t_to_c)
    Sb_wet = Sbody_wet(K,S_top,S_side)
    St_wet = Stotal_wet(Sw_wet,Sb_wet)
    Cd0 = Cd_zero(Cfe, St_wet, S_ref)
    
    answer = dict(Sw_wet = Swing_wet(Sw,Lambda,t_to_c),
    Sb_wet = Sbody_wet(K,S_top,S_side),
    St_wet = Stotal_wet(Sw_wet,Sb_wet),
    Cd0 = Cd_zero(Cfe, St_wet, S_ref))
    print_dic(answer)
    
q_one()

def q_two():
    W_to_S = 5000
    CL_max = 1.4
    Ve_max = 257.2
    Cd0 = 0.015
    K = 0.075
    S = 30
    W = W_to_S * S
    #2.1:
    
    Vs1 = Vstoll( W_to_S, Rhosl_std, CL_max)
    VE1 = Ve_two(Rhosl_std,Rhosl_std,Vs1)
    
    
    Tsl = 308 
    rhosl = rho_two(Psl_std,R,Tsl)
    Vs2 =  Vstoll( W_to_S, rhosl, CL_max)
    VE2 = Ve_two(rhosl,Rhosl_std,Vs2)
    
    #2.2:
    
    h = 5000 * ft2meter
    rho_st = rho_one(Rhosl_std,L,h,Tsl_std,g,R)
    Vs3 = Vstoll( W_to_S, rho_st, CL_max)
    VE3 = Ve_two(rho_st,Rhosl_std,Vs3)
    
    rho_t =  rho_one(rhosl,L,h,Tsl,g,R)
    Vs4 = Vstoll( W_to_S, rho_t, CL_max)
    VE4 = Ve_two(rho_t,Rhosl_std,Vs4)
    
    answer = dict(VE1 = Ve_two(Rhosl_std,Rhosl_std,Vs1),
    VE2 = Ve_two(rhosl,Rhosl_std,Vs2),
    VE3 = Ve_two(rho_st,Rhosl_std,Vs3),    
    VE4 = Ve_two(rho_t,Rhosl_std,Vs4),
    Vs1 = Vstoll( W_to_S, Rhosl_std, CL_max),
    Vs2 =  Vstoll( W_to_S, rhosl, CL_max),
    Vs3 = Vstoll( W_to_S, rho_st, CL_max),   
    Vs4 = Vstoll( W_to_S, rho_t, CL_max),
    rho_t =  rho_one(rhosl,L,h,Tsl,g,R),
    rho_st = rho_one(Rhosl_std,L,h,Tsl_std,g,R))
    print_dic(answer)
    
    
    
    V = np.arange( 50, 310 ,10)
    D_Hot = D_two(rhosl,V,S,Cd0,K,W)
    D_st = D_two(Rhosl_std,V,S,Cd0,K,W)
    
    VE_hot = Ve_two(rhosl,Rhosl_std,V)
    D_Hot2 = D_two(rhosl,VE_hot,S,Cd0,K,W)
    
    
    
    
    np.savetxt('VE_hot.txt', (VE_hot))
    np.savetxt('Vt.txt', (V))
    np.savetxt('D_Hot.txt', (D_Hot))
    np.savetxt('D_Hot2.txt', (D_Hot2))        
    np.savetxt('D_st.txt', (D_st))
    
q_two()