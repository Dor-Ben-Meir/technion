clear all
close all
clc



V = importdata('Vt.txt');
VE = importdata('VE_hot.txt');
D_HOT = importdata('D_Hot.txt');
D_st = importdata('D_st.txt');
D_HOT2 = importdata('D_Hot2.txt');
figure();
plot (V,D_HOT,V,D_st,'LineWidth',1); grid on; xlabel ('V [m/sec]'); ylabel('D [N]')
legend ('D_HOT','D_st');
title ('Drad force vs V'); xlim([80 300]);


figure();
plot (VE,D_HOT2,V,D_st,'LineWidth',1); grid on; xlabel ('V [m/sec]'); ylabel('D [N]')
legend ('D_HOT','D_st');
title ('Drad force vs V'); xlim([80 300]);