
######################################################################################
#Python code for homework 9 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################
#%%
import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from scipy.optimize import fsolve
import pandas as pd
import matplotlib.pyplot as plt

day2sec = 24 * 60 * 60

Req = 6378.1363 #[km] 
a = 42164.16 #[km]
K = 1.68 * (10**-3) #[deg/day^2]
lambda_n = 10 #[deg]
delta_lambda = -0.05 #[deg]
lambda_s = min([abs(75.1), abs(lambda_n - 255.1)])
lambda_2dot_n = -K * np.sin(np.deg2rad(2*(lambda_n - lambda_s)))
lambda_0 = lambda_n - delta_lambda
lamda_dot_0 = -2 * np.sign(lambda_2dot_n) * np.sqrt(-(lambda_2dot_n)  * delta_lambda)
lamda_dot_01 = np.deg2rad(lamda_dot_0) / day2sec
tm = 2 * abs(lamda_dot_0 / lambda_2dot_n)
delta_v = (2/3) * a * abs(lamda_dot_01)
delta_1 = 0.132 * np.sin(np.deg2rad(2 * (lambda_n - lambda_s) * tm))
ans1 = dict(tm = tm * u.d, delta_v = delta_v * (10**3) * (u.m / u.s), delta_a = delta_1 * u.km)
ans2 = dict(delta_lambda = delta_lambda, lambda_s = lambda_s, lambda_2dot_n  = lambda_2dot_n, lamda_dot_01 = lamda_dot_01, lamda_dot_0  = lamda_dot_0 )




#%%
def orbit(X,t):
#X1- a
#X2- lambda
#X3- lambda_dot
 

    
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]

    dX1 = 0.132 * np.sin(np.deg2rad(2*(X2-lambda_s)))
    dX2 = X3
    dX3 = -K * np.sin(np.deg2rad(2*(X2-lambda_s)))

    derivs = np.column_stack((dX1,dX2,dX3))
    derivs = np.reshape(derivs, (3, ))
    return derivs











#%%

t = np.arange( 0, 365*2 + 1   ,1)  
X0 = np.array([a, lambda_0,lamda_dot_0 ])  
sol = odeint(orbit,X0,t)   


Llmbdadf= pd.DataFrame(data={'t[days]':t})
Llmbdadf['Lambda[deg]'] = sol[:,1]
Llmbdadf['delta_a[km]'] = sol[:,0]


ax = plt.gca()
ax.grid()
Llmbdadf.plot(kind='line',x='t[days]',y='Lambda[deg]' ,ax=ax)
ax.set_ylabel('Lambda[deg]')
plt.show()

ax = plt.gca()
ax.grid()
Llmbdadf.plot(kind='line',x='t[days]',y='delta_a[km]' ,ax=ax)
ax.set_ylabel('delta_a[km]')
plt.show()






# a_dot = 0.132 * np.sin(np.deg2rad(2 * (lambda_n - lambda_s)))
# delta_a0 = a_dot * (lamda_dot_0 / lambda_2dot_n)

# Lambda_dot = lamda_dot_0 + lambda_2dot_n * t
# Lambda =   lambda_0 + lamda_dot_0 * t + 0.5 * lambda_2dot_n * (t**2)
# delta_a = delta_a0 + a_dot*t

   

# T = 86164.09 / day2sec #[1/day]
# #n = 360 / T


# omegae = (7.272 * (10**-5)) / day2sec# [rad/sec]
# omegae = omegae * (180 / np.pi)
# lambda_22 = -14.9 #[deg]
# J22 = 1.81543019 * (10**-6)





# def orbit(X,t):
# #X1- a
# #X2- da
# #X3- lambda
# #X4- lambda_dot
 
#     mu  = 398601.2 / (day2sec**2)
    
#     X1 = X[0:1]
#     X2 = X[1:2]

#     n = np.sqrt(mu / (X1**3))
#     dX1 = -12*(n/X1)* Req * J22 * np.sin(np.deg2rad(2 * X2 - lambda_22))
#     dX2 = n - omegae + 3 * (n/(X1**2)) * (Req**2) * J22 + 18 * (n/(X1**2)) * (Req**2) * J22 * np.cos(np.deg2rad(2 * X2 - lambda_22))

#     derivs = np.column_stack((dX1,dX2))
#     derivs = np.reshape(derivs, (2, ))
#     return derivs  
    

    
# X0 = np.array([a, lambda_0])
# t = np.arange( 0, 365*2 + 1   ,1)   
# sol = odeint(orbit,X0,t)      

 
# Llmbdadf['Lambda2[deg]'] = sol[:,1]

# ax = plt.gca()
# ax.grid()
# Llmbdadf.plot(kind='line',x='t[days]',y='Lambda2[deg]' ,ax=ax)
# ax.set_ylabel('Lambda2[deg]')
# plt.show()
# %%
