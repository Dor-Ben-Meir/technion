
######################################################################################
#Python code for homework 7 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
from scipy import integrate
from scipy.integrate import quad_vec
from functools import reduce

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
T = 100*60
n = 2*np.pi/T
fMax = 4 * (10**-6) # [km/s^2]
x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0
tf = 2.5*T

xDot0 = n*y0/2
yDot0 = -2*n*x0
xMax = x0
yMax = 2*x0
xDotMax = n*yMax/2
yDotMax = yDot0

F = np.array([ [0 , 1 , 0, 0],[3*(n**2), 0 , 0, 2*n],[0 , 0 , 0, 1],[0 , -2*n, 0, 0]])
#G = np.array([0,0,0,1]).T
G = np.array([[0, 0],[0, 0],[0, 0],[0, 1]])

def phi11Func(t):
    tau = 80*60 - t
    phi = np.array( [[(4 - 3*np.cos(n*t)) , (1/n)*np.sin(n*t)] , [3*n*np.sin(n*t) , np.cos(n*t)]])
    return phi

def phi12Func(t):
    tau = 80*60 - t
    phi = np.array([[0, (2/n)*(1 - np.cos(n*t))], [0, 2*np.sin(n*t)]])
    return phi
    
def phi21Func(t):
    tau = 80*60 - t
    phi = np.array([ [6*(np.sin(n*t) - n*t), 2/n*(np.cos(n*t) - 1)] , [6*n*(np.cos(n*t) - 1), -2*np.sin(n*t)]])
    return phi
    
def phi22Func(t):
    tau = 80*60 - t
    phi = np.array([[1, 1/n*(4*np.sin(n*t) - 3*(n*t))],[0, 4*np.cos(n*t) - 3]])
    return phi
    
def ef(t):
    #t = -t + 80*60
    qq = np.concatenate((phi11Func(t), phi12Func(t)),axis=1)
    qq2 = np.concatenate((phi21Func(t), phi22Func(t)),axis=1)
    qq3 = np.concatenate((qq, qq2), axis=0)
    return qq3
    
def contorl_M(t):
    M = np.zeros((4,4))
    for m in range(4):
        Ms = lambda tao: reduce(np.dot, [ef(t - tao), G, G.T, ef(-tao).T])[:,m]
        M[:,m] = quad_vec(Ms, 0, t)[0]
    M = M * -1
    return M
    
def orbitB(X,t):

    x = X[0:4]
    deltaV = X[4:5]
    U = X[5:6]
    
    
    # if(LA.norm(x) < 1e-5):
    #     u = 0
        
    # else:
    #     tgo = tf - t
    #     K = reduce(np.dot, [G.T, contorl_M(tgo).T, ef(tgo)])
    #     u = np.matmul(K,x)
    tgo = tf - t
    K = reduce(np.dot, [G.T, inv(contorl_M(tgo)), ef(tgo)])
    u = np.dot(K,x)
    
    dX1 = np.array(np.matmul(F,x)) + np.array(np.matmul(G,u)).T
    dX2 = LA.norm(u)
    #dX2 = np.reshape(dX2, (1, ))
    dX1 = np.reshape(dX1, (1,4))
    dX3 = np.dot(K,x)[1]
    
    derivs = np.column_stack((dX1,dX2,dX3))
    derivs = np.reshape(derivs, (6, ))
    return derivs  


X0 = np.array([x0, xDot0, y0, yDot0,0,6.652276136766206e-08])
t = np.linspace(0,tf, num=7625)  
sol_B  = odeint(orbitB,X0,t)    

u = sol_B[:,5]

# x = sol_B[:,0:4]
# deltaV = sol_B[:,4]
# u = np.zeros((len(t),1))
# K = np.zeros((len(t),4))
# G = np.array([0,0,0,1])
# for i in range(len(t)):
#     if(LA.norm(x[i,:]) < 1e-5):
#         u[i] = 0
        
#     else:
#         tgo = tf - t[i]
#         K[i,:] = reduce(np.dot, [G.T, inv(contorl_M(tgo)), ef(tgo)])
#         u[i] = np.dot(K[i,:],x[i])
    
np.savetxt('sol_B.txt', (sol_B))
np.savetxt('u.txt', (u))
np.savetxt('tpy7.txt', (t))