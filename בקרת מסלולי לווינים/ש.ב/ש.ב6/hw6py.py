######################################################################################
#Python code for homework 6 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit
from scipy import signal
from control.matlab import *
from control.statefbk import ctrb, obsv, place, place_varga, lqr, gram, acker

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
T = 100*60
n = 2*np.pi/T
fMax = 4 * (10**-6) # [km/s^2]
x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0
tf = 80*60

xDot0 = n*y0/2
yDot0 = -2*n*x0
xMax = x0
yMax = 2*x0
xDotMax = n*yMax/2
yDotMax = yDot0

F = np.array([ [0 , 1 , 0, 0],[3*(n**2), 0 , 0, 2*n],[0 , 0 , 0, 1],[0 , -2*n, 0, 0]])
#G = np.array([0,0,0,1]).T
G = np.array([[0, 0],[0, 0],[0, 0],[0, 1]])


R = (1/fMax**2) * np.diag([1, 1]) 
Q = (1/15) * np.diag(np.array([1/xMax**2, 1/xDotMax**2, 1/yMax**2, 1/yDotMax**2]))
#Q = (1/100) * np.diag(np.array([1, 1, 1/n**2, 1/n**2]))
[control_K,S,e] = lqr(F, G, Q, R)


def orbitA(X,t):

    x = X[0:4]
    deltaV = X[4:5]
    
    u = -np.matmul(control_K,x).T
    
    dX1 = np.array(np.matmul(F,x)) + np.array(np.matmul(G,u)).T
    dX2 = LA.norm(u)
    derivs = np.column_stack((dX1,dX2))
    derivs = np.reshape(derivs, (5, ))
    return derivs  

X0 = np.array([x0, xDot0, y0, yDot0,0])
t = np.linspace(0.0, 2.5*T, num=5441)  
sol = odeint(orbitA,X0,t)





np.savetxt('output6.txt', (sol))
np.savetxt('contorlk6.txt', (control_K))
np.savetxt('tpy6.txt', (t))