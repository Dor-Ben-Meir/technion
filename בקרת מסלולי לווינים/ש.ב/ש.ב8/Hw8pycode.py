
######################################################################################
#Python code for homework 8 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from scipy.optimize import fsolve

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
Re = 6371 #[Km]
OMds = 2*np.pi/365.25/86400
OMe = 7.272 * (10**-5) # [rad/sec]
d2r = 1 / (180/np.pi) # deeg to rad



k = 2
A_drag = 4 #[m^2]
Cd = 2.2
m = 500 #[Kg]
Isp = 210 # [sec]
Kd = Cd * A_drag/m
years = 3
Delta_Omega0 = 0.25 * d2r #[Km]
Aa = -7/2
Tday = 86164 #[sec]
Nrev1=Tday/2/np.pi*(mu/(Re+400)**3)**0.5
Nrev2=Tday/2/np.pi*(mu/(Re+500)**3)**0.5
j = k*Nrev2
j = 31


def den(h_ellp):
	if h_ellp >= 0  and h_ellp < 25 :
		h_0 = 0
		rho_0 = 1.225
		H = 7.249		
	if h_ellp >= 25 and h_ellp < 30 :
		h_0 = 25
		rho_0 = 3.899*10**(-2)
		H = 6.349		
	if h_ellp >= 30 and h_ellp < 40 :
		h_0 = 30
		rho_0 = 1.774*10**(-2)
		H = 6.682
	if h_ellp >= 40 and h_ellp < 50 :
		h_0 = 40
		rho_0 = 3.972*10**(-3)
		H = 7.554
	if h_ellp >= 50 and h_ellp < 60 :
		h_0 = 50
		rho_0 = 1.057*10**(-3)
		H = 8.382
	if h_ellp >= 60 and h_ellp < 70 :
		h_0 = 60
		rho_0 = 3.206*10**(-4)
		H = 7.714
	if h_ellp >= 70 and h_ellp < 80 :
		h_0 = 70
		rho_0 = 8.77*10**(-5)
		H = 6.549
	if h_ellp >= 80 and h_ellp < 90 :
		h_0 = 80
		rho_0 = 1.905*10**(-5)
		H = 5.799
	if h_ellp >= 90 and h_ellp < 100 :
		h_0 = 90
		rho_0 = 3.396*10**(-6)
		H = 5.382
	if h_ellp >= 100 and h_ellp < 110 :
		h_0 = 100
		rho_0 = 5.297*10**(-7)
		H = 5.877
	if h_ellp >= 110 and h_ellp < 120 :
		h_0 = 110
		rho_0 = 9.661*10**(-8)
		H = 7.263
	if h_ellp >= 120 and h_ellp < 130 :
		h_0 = 120
		rho_0 = 2.438*10**(-8)
		H = 9.473												
	if h_ellp >= 130 and h_ellp < 140 :
		h_0 = 130
		rho_0 = 8.484*10**(-9)
		H = 12.636
	if h_ellp >= 140 and h_ellp < 150 :
		h_0 = 140
		rho_0 = 3.845*10**(-9)
		H = 16.149
	if h_ellp >= 150 and h_ellp < 180 :
		h_0 = 150
		rho_0 = 2.070*10**(-9)
		H = 22.523
	if h_ellp >= 180 and h_ellp < 200 :
		h_0 = 180
		rho_0 = 5.464*10**(-10)
		H = 29.74
	if h_ellp >= 200 and h_ellp < 250 :
		h_0 = 200
		rho_0 = 2.789*10**(-10)
		H = 37.105
	if h_ellp >= 250 and h_ellp < 300 :
		h_0 = 250
		rho_0 =7.248*10**(-11)
		H = 45.546
	if h_ellp >= 300 and h_ellp < 350 :
		h_0 = 300
		rho_0 = 2.418*10**(-11)
		H = 53.628
	if h_ellp >= 350 and h_ellp < 400 :
		h_0 = 350
		rho_0 = 9.518*10**(-12)
		H = 53.298
	if h_ellp >= 400 and h_ellp < 450 :
		h_0 = 400
		rho_0 = 3.725*10**(-12)
		H = 58.515
	if h_ellp >= 450 and h_ellp < 500 :
		h_0 = 450
		rho_0 = 1.585*10**(-12)
		H = 60.828
	if h_ellp >= 500 and h_ellp < 600 :
		h_0 = 500
		rho_0 = 6.967*10**(-13)
		H = 63.822
	if h_ellp >= 600 and h_ellp < 700 :
		h_0 = 600
		rho_0 = 1.454*10**(-13)
		H = 71.835
	if h_ellp >= 700 and h_ellp < 800:
		h_0 = 700
		rho_0 = 3.614*10**(-14)
		H = 88.667
	if h_ellp >= 800 and h_ellp < 900 :
		h_0 = 800
		rho_0 = 1.17*10**(-14)
		H = 124.64
	if h_ellp >= 900 and h_ellp < 1000 :
		h_0 = 900
		rho_0 = 5.245*10**(-15)
		H = 181.05
	if h_ellp >= 1000:
		h_0 = 1000
		rho_0 = 3.019*10**(-15)
		H = 268									
	return rho_0*math.exp(-(h_ellp-h_0)/H)

Delta_L0 = 100 #[Km]
Delta_H_L = lambda a_dot, n: np.sqrt((-8 * Delta_L0 * a_dot) / (3 * n))
t_H_L= lambda a_dot, n: (-2 * Delta_H_L(a_dot, n) ) / a_dot
Delta_V_L= lambda a_dot, n: (n / 2) * Delta_H_L(a_dot, n)



Delta_Omega0 = 0.25 * d2r #[Km]
Delta_H_Omega = lambda a_dot, n, a: np.sqrt((-8 * Delta_Omega0 * a * a_dot) / (3 * OMds))
#Delta_H_Omega = lambda a_dot, n, a: np.sqrt((4 * Delta_Omega0 * a) /(Aa*OMds))
t_H_Omega= lambda a_dot, n, a: (-2 * Delta_H_Omega(a_dot, n, a) ) / a_dot
Delta_V_Omega= lambda a_dot, n, a: ( Delta_H_Omega(a_dot, n, a) ) / a



Delta_GT0 = 20 / Re #[1/Km]
Delta_H_G = lambda a_dot, a: np.sqrt((-8 * Delta_GT0 * a * a_dot) / (3 * OMe))
t_H_G= lambda a_dot, a: (-2 * Delta_H_G(a_dot, a) ) / a_dot
Delta_V_G= lambda a_dot, n, a: ( Delta_H_G(a_dot, a) ) * n


h0 = 42164.173 * ((k/j)**(2/3)) - Re
a = Re + h0
n0 = np.sqrt(mu / (a**3))
p = a
e = 0 
def ncalc(n):
    cosi = OMds/((-3/2)*J2*n*((Re/p)**2))
    omegads = -(3/2) * J2 * np.sqrt(mu) * (Re**2) * a**(-7/2) * (1-e**2)**-2 * cosi
    w_dot = (3/4) * J2 * np.sqrt(mu) * Re**2 * a**(-7/2) * ((1-e**2)**-2) * (5*(cosi**2)-1)
    M_dot = (3/4) * J2 * np.sqrt(mu) * Re**2 * a**(-7/2) * (1-e**2)**(-3/2) * (3*(cosi**2)-1)
    return(n - ((j/k)*(OMe - omegads) - (M_dot + w_dot)))

n = fsolve(ncalc,(n0))[0]
i = np.arccos(OMds/((-3/2)*J2*n*((Re/p)**2)))
#h = ((mu / (n**2))**(1/3)) - Re
h = 410.75
a = Re + h



rho_bb = den(h)
rho_b=1.58e-12* np.exp(-(h-450)/60.828)
a_dot = -rho_bb * Kd * n * 1000 * (a**2)
adot=-1000*Kd*rho_b*((mu*a)**0.5)



Delta_H = np.array([Delta_H_L(a_dot, n), Delta_H_Omega(a_dot, n, a), Delta_H_G(a_dot, a)])
t_H = np.array([t_H_L(a_dot, n), t_H_Omega(a_dot, n, a), t_H_G(a_dot, a)])
Delta_V = np.array([Delta_V_L(a_dot, n), Delta_V_Omega(a_dot, n, a), Delta_V_G(a_dot, n, a)])
itemindex = np.where(Delta_H==min(Delta_H))[0]
itemindex = itemindex[0]
tc = t_H[itemindex] / 86400
delV = Delta_V[itemindex]

V = np.sqrt(mu / a)
delVtot = V *  (min(Delta_H) / a)
Np = 163
mf = m * 1000 * delVtot/(Isp*9.8)
mf = mf * Np

ans1 = dict(h = h * u.km, i = np.rad2deg(i) * u.deg, Delta_h0 = min(Delta_H) * u.km, time_between_pulse = tc*u.d, Velocity_change = (delVtot * 10**3) * (u.m/u.s), Fual_amount = mf *u.kg)