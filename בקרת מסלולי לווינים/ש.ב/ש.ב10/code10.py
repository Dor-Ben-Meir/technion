######################################################################################
#Python code for homework 10 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from numpy.linalg import inv
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit
from scipy import signal
from control.matlab import *
#from control import *
from control.statefbk import ctrb, obsv, place, place_varga, lqr, gram, acker
from pyproj import Proj
import pyproj
from astropy.coordinates import EarthLocation
from functools import reduce
import pandas as pd
import matplotlib.pyplot as plt

#
#Parameters:
T = 86164.09 #[sec]
n = 2*np.pi/T
fMax = (2 * (10**-5)) *(10**-3) # [km/s^2]
Req = 6378.1363 #[km] 
a = 42164.16 #[km]

dlambda0 = np.deg2rad(0.05)      # [rad] 
lambda0 = 10                  # [deg] 
lambdadot0 = -3.2497e-09      # [rad/sec]  
da0 = 1.2605                  # [km] 
da0dot = -0.1011 / T        # [km/sec] 
tols = 1e-6

#
#Initial Conditions:
x0 = da0                # [km]
y0 = a * np.deg2rad(dlambda0) # [km]
xdot0 = da0dot          # [km/sec]
ydot0 = a * lambdadot0    # [km/sec]
X0 = np.array([x0,y0,xdot0,ydot0,0])
Xmin = np.array([x0,y0,xdot0,ydot0])
F = np.array([ [0 , 0 , 1, 0],[0 , 0 , 0, 1], [3*(n**2), 0 , 0, 2*n],[0 , 0,-2*n, 0]])
#G = np.array([[0, 0],[0, 0],[0, 0],[0, 1]])
G = np.array([0, 0 , 0 , 1])
C = np.array([0.000201262609068935,-2.62516446611654e-05,	1.080,	1])
#K = LA.norm(reduce(np.matmul, [C, F, Xmin.T]))
K = fMax

def orbitA(X,t):
     
    # X1 = X[0:4]
    # X2 = X[4:5]
    
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    
    
 
    # X = np.array([0.0, 0.0, y0, y0_dot])
    #s = np.matmul(C, X)
    # K = LA.norm(reduce(np.matmul, [C, F, X])) 
    
#X0 = np.array([x0,y0,xdot0,ydot0,0])
    X = np.array([X1, X2, X3, X4])
    c = np.reshape(C, (1,4))
    s = np.matmul(c, X)
    if(abs(s) < tols):
        fY = 0.0
        A = np.matmul(F,X)
    else:
        fY = -K * np.sign(s)
        A = np.matmul(F,X) + (G*fY).T

    dX1 = A[0]
    dX2 = A[1]
    dX3 = A[2]
    dX4 = A[3]
    dX5 = abs(fY)
    dX5 = np.reshape(dX5, (1, ))
    derivs = np.column_stack((dX1,dX2,dX3,dX4,dX5))
    derivs = np.reshape(derivs, (5, ))
    return derivs   

#t = np.arange( 0, T*2 + 1 ,1)  
t = np.linspace(0.0, T*2, num=17233)
sol = odeint(orbitA,X0,t) 

t = t / 86400


x = sol[:,0]
y = sol[:,1]
Vx = sol[:,2][-1]
Vy = sol[:,3][-1]
fy = sol[:,4]
Vc = 0 * x 
r_miss = np.sqrt((x[-1])**2 + (y[-1])**2)

X = np.array([x, y, sol[:,2], sol[:,3]])
c = np.reshape(C, (1,4))
S = np.matmul(c, X).T
    
    
    

for i, s in enumerate(S):
    Vc[i] = LA.norm([sol[i,2],sol[i,3]])
    if(abs(s) < tols):
        s =  0.0       
    
    Vc[i] =  -K * np.sign(s)

Lambtda = pd.DataFrame(data={'time[days]':t})
Lambtda['X[Km]'] = sol[:,0]
Lambtda['Y[Km]'] = sol[:,1]

Lambtda['lon[deg]'] = lambda0 + np.rad2deg(y / a)
Lambtda['alt[km]'] = x + a
Lambtda['deltaV[km/sec]'] = fy
Lambtda['fy[km/sec^2]'] = Vc


#Plot of the relative trajectory in the altitude-longitude plane:
ax = plt.gca()
ax.grid()
ax.set_title('Relative Trajectory', fontsize=20)
Lambtda.plot(kind='line',x='Y[Km]',y='X[Km]' ,ax=ax)
ax.set_ylabel('X[Km]')
plt.show()

#Plot of the relative trajectory in the altitude-longitude plane:
ax = plt.gca()
ax.grid()
ax.set_title('Relative Trajectory', fontsize=20)
Lambtda.plot(kind='line',x='alt[km]',y='lon[deg]' ,ax=ax)
ax.set_ylabel('lon[deg]')
plt.show()


#Altitude and longitude vs. time:
ax = plt.gca()
ax.grid()
ax.set_title('Longitude vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[days]',y='lon[deg]' ,ax=ax)
ax.set_ylabel('lon[deg]')
plt.show()


ax = plt.gca()
ax.grid()
ax.set_title('Altitude vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[days]',y='alt[km]' ,ax=ax)
ax.set_ylabel('alt[km]')
plt.show()


#Thrust acceleration vs. time:
ax = plt.gca()
ax.grid()
ax.set_title('Accumulated ΔV vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[days]',y='deltaV[km/sec]' ,ax=ax)
ax.set_ylabel('ΔV [km/sec]')
plt.show()

#Thrust acceleration vs. time
ax = plt.gca()
ax.grid()
ax.set_title('Thrust acceleration vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[days]',y='fy[km/sec^2]' ,ax=ax)
ax.set_ylabel('fy[km/sec^2]')
plt.show()
