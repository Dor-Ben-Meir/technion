

######################################################################################
#Python code for homework 1 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
Re=6371 #[Km]


Month =  'October'
Day = 1   
ran1 = -4975
rrdn=7050   
LTAN = 14    
T = 95*60
n = 2*np.pi/T
OMds = 2*np.pi/365.25/86400
rtd = 180/np.pi
A_drag = 0.8
A_sun = 5
Cd = 2.4
m = 400
beta = 1
th = -1.2530416121104706


def den(h_ellp):
	if h_ellp >= 0  and h_ellp < 25 :
		h_0 = 0
		rho_0 = 1.225
		H = 7.249		
	if h_ellp >= 25 and h_ellp < 30 :
		h_0 = 25
		rho_0 = 3.899*10**(-2)
		H = 6.349		
	if h_ellp >= 30 and h_ellp < 40 :
		h_0 = 30
		rho_0 = 1.774*10**(-2)
		H = 6.682
	if h_ellp >= 40 and h_ellp < 50 :
		h_0 = 40
		rho_0 = 3.972*10**(-3)
		H = 7.554
	if h_ellp >= 50 and h_ellp < 60 :
		h_0 = 50
		rho_0 = 1.057*10**(-3)
		H = 8.382
	if h_ellp >= 60 and h_ellp < 70 :
		h_0 = 60
		rho_0 = 3.206*10**(-4)
		H = 7.714
	if h_ellp >= 70 and h_ellp < 80 :
		h_0 = 70
		rho_0 = 8.77*10**(-5)
		H = 6.549
	if h_ellp >= 80 and h_ellp < 90 :
		h_0 = 80
		rho_0 = 1.905*10**(-5)
		H = 5.799
	if h_ellp >= 90 and h_ellp < 100 :
		h_0 = 90
		rho_0 = 3.396*10**(-6)
		H = 5.382
	if h_ellp >= 100 and h_ellp < 110 :
		h_0 = 100
		rho_0 = 5.297*10**(-7)
		H = 5.877
	if h_ellp >= 110 and h_ellp < 120 :
		h_0 = 110
		rho_0 = 9.661*10**(-8)
		H = 7.263
	if h_ellp >= 120 and h_ellp < 130 :
		h_0 = 120
		rho_0 = 2.438*10**(-8)
		H = 9.473												
	if h_ellp >= 130 and h_ellp < 140 :
		h_0 = 130
		rho_0 = 8.484*10**(-9)
		H = 12.636
	if h_ellp >= 140 and h_ellp < 150 :
		h_0 = 140
		rho_0 = 3.845*10**(-9)
		H = 16.149
	if h_ellp >= 150 and h_ellp < 180 :
		h_0 = 150
		rho_0 = 2.070*10**(-9)
		H = 22.523
	if h_ellp >= 180 and h_ellp < 200 :
		h_0 = 180
		rho_0 = 5.464*10**(-10)
		H = 29.74
	if h_ellp >= 200 and h_ellp < 250 :
		h_0 = 200
		rho_0 = 2.789*10**(-10)
		H = 37.105
	if h_ellp >= 250 and h_ellp < 300 :
		h_0 = 250
		rho_0 =7.248*10**(-11)
		H = 45.546
	if h_ellp >= 300 and h_ellp < 350 :
		h_0 = 300
		rho_0 = 2.418*10**(-11)
		H = 53.628
	if h_ellp >= 350 and h_ellp < 400 :
		h_0 = 350
		rho_0 = 9.518*10**(-12)
		H = 53.298
	if h_ellp >= 400 and h_ellp < 450 :
		h_0 = 400
		rho_0 = 3.725*10**(-12)
		H = 58.515
	if h_ellp >= 450 and h_ellp < 500 :
		h_0 = 450
		rho_0 = 1.585*10**(-12)
		H = 60.828
	if h_ellp >= 500 and h_ellp < 600 :
		h_0 = 500
		rho_0 = 6.967*10**(-13)
		H = 63.822
	if h_ellp >= 600 and h_ellp < 700 :
		h_0 = 600
		rho_0 = 1.454*10**(-13)
		H = 71.835
	if h_ellp >= 700 and h_ellp < 800:
		h_0 = 700
		rho_0 = 3.614*10**(-14)
		H = 88.667
	if h_ellp >= 800 and h_ellp < 900 :
		h_0 = 800
		rho_0 = 1.17*10**(-14)
		H = 124.64
	if h_ellp >= 900 and h_ellp < 1000 :
		h_0 = 900
		rho_0 = 5.245*10**(-15)
		H = 181.05
	if h_ellp >= 1000:
		h_0 = 1000
		rho_0 = 3.019*10**(-15)
		H = 268									
	return rho_0*math.exp(-(h_ellp-h_0)/H)


def a_two(mu,T):
    a = (mu * ((T / (2 * np.pi))**2))**(1/3)
    return a


#Calculate the J2 induced forces in radial FOR
def J2radForce(r,i,th,o,mu):
    Fr=-1.5*J2*(mu/(r**2))*((Re/r)**2)*(1-3*(np.sin(i)**2)*(np.sin(th+o))**2)
    Fth=-3*J2*(mu/(r**2))*((Re/r)**2)*((np.sin(i)**2)*np.sin(th+o)*np.cos(th+o))
    Fh=-3*J2*(mu/(r**2))*((Re/r)**2)*np.sin(i)*np.cos(i)*np.sin(th+o)
    Veec = [Fr,Fth,Fh]
    return Veec

#def J2radForce(r,i,th,o,mu):
#    Fr=-1.5*J2*(mu/(r**2))*((Re/r)**2)*(1-3*(np.sin(i)**2)*(np.sin(th+o))**2)
#    Fth=-3*J2*(mu/(r**2))*((Re/r)**2)*((np.sin(i)**2)*np.sin(th+o)*np.cos(th+o))
#    Fh=-3*J2*(mu/(r**2))*((Re/r)**2)*np.sin(i)*np.cos(i)*np.sin(th+o)
#    
#    return(Fr,Fth,Fh)

#Calculate the sun- direction vector
def SunDirection(Month,Day):

	if(Month == 'Jan'):
		D0=0
	elif(Month == 'Feb'):
		D0=31
	elif(Month == 'March'):
		D0=60
	elif(Month == 'Apr'):
		D0=91
	elif(Month == 'May'):
		D0=121
	elif(Month == 'Jun'):
		D0=151
	elif(Month == 'July'):
		D0=181
	elif(Month == 'August'):
		D0=212
	elif(Month == 'september'):
		D0=243		
	elif(Month == 'October'):
		D0=274
	elif(Month == 'November'):
		D0=304
	else:
	#(Month == December)
		D0=334

	N = D0 + Day 
	RAAN = 0.98563 * (N-80) * np.pi/180
	delta = np.arcsin(0.39795 * np.cos(np.deg2rad(0.98563*(N-173))))	
	r = np.transpose(np.array([np.cos(delta)*np.cos(RAAN),np.cos(delta)*np.sin(RAAN),np.sin(delta)]))
	return (r,RAAN)


#Transformation between calsical elements to Position and Velocity Vectors in ECI system
def Clasical_to_RVvectors(a,e,i,Omega,w,Ni):
    rv = []
    v = []
    r = a*(1 - e**2) / (1 + e * np.cos(Ni))
    h = np.sqrt(mu * a * ( 1 - e**2))
    p = a * (1 - e**2) 
    rv.append(r * (np.cos(Omega) * np.cos(w + Ni) - np.sin(Omega) * np.sin(w + Ni) * np.cos(i)))
    rv.append(r * (np.sin(Omega) * np.cos(w + Ni) + np.cos(Omega) * np.sin(w + Ni) * np.cos(i)))
    rv.append(r * (np.sin(i) * np.sin(w + Ni)))
    
    v.append(((rv[0]* h*e )/(r*p)) * np.sin(Ni) - (h/r) * (np.cos(Omega) * np.sin(w + Ni) + np.sin(Omega) * np.cos(w + Ni) * np.cos(i)))
    v.append(((rv[1]* h*e )/(r*p)) * np.sin(Ni) - (h/r) * (np.sin(Omega) * np.sin(w + Ni) - np.cos(Omega) * np.cos(w + Ni) * np.cos(i)))
    v.append(((rv[2]* h*e )/(r*p)) * np.sin(Ni) + (h/r) * (np.sin(i) * np.cos(w + Ni)))
    
    return(rv,v)  




def SunForce(beta,S,m):
    Gs=4.65e-6 #N/m^2
    fs=-(1+beta)*Gs*(S/m)
    return fs

def DragForce(r,v,Kd,a,e):
    rho = den(r-Re)
    V = (mu/a*(1+e)/(1-e))**0.5
    fd = 0.5*rho*V**2*Kd*1000
    return fd

#DCM matrix
def DCM(O,o,i):
    e_vec = np.zeros((3, 3))
    e_vec[0,0] = np.cos(O)*np.cos(o)-np.sin(O)*np.sin(o)*np.cos(i)
    e_vec[1,0] = np.sin(O)*np.cos(o)+np.cos(O)*np.sin(o)*np.cos(i)
    e_vec[2,0] = np.sin(o)*np.sin(i)
    e_vec[0,1] = -np.cos(O)*np.sin(o)-np.sin(O)*np.cos(o)*np.cos(i)
    e_vec[1,1] = -np.sin(O)*np.sin(o)+np.cos(O)*np.cos(o)*np.cos(i)
    e_vec[2,1] = np.cos(o)*np.sin(i)
    e_vec[0,2] = np.sin(O)*np.sin(i)
    e_vec[1,2] = -np.cos(O)*np.sin(i)
    e_vec[2,2] = np.cos(i)		
    return e_vec



#Transformation between Position and Velocity Vectors in ECI system to calsical elements:
def RVvectors_to_Clasical(r,v):
 
    
    e = np.zeros(3)
    R = r    
    V = v
    
    
    
    x = r[0]
    y = r[1]
    z = r[2]

    v1 = v[0]
    v2 = v[1]
    v3 = v[2]
    
    dot_rv = x*v1 + y*v2 + z*v3
    
    R_syze = np.sqrt(x**2 + y**2 + z**2)
    V_syze = np.sqrt(v1**2 + v2**2 + v3**2)
    
    h = np.transpose(np.cross(np.transpose(R),np.transpose(V)))
    #h_size = np.sqrt(h[:,0]**2 + h[:,1]**2  + h[:,2]**2)
    h_size = LA.norm(h)
    e = R
    e[0] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[0] - (dot_rv * V[0]))
    e[1] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[1] - (dot_rv * V[1]))
    e[2] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[2] - (dot_rv * V[2]))
    e_size = LA.norm(e)
    p = (h_size**2) / mu 
    a = p / (1 - e_size**2)
    n = np.cross([0,0,1],np.transpose(h))
    n_size = LA.norm(n)
    dot_ne = np.dot(n,e)
    #dot_re = np.dot(r,e)
    i = np.arccos(h[2]/h_size)
    Omega = np.arccos(n.T[0]/n_size)
    w =  np.arccos(dot_ne / (n_size * e_size))
    th = np.arccos(((a*(1-e_size**2)-R_syze)/(R_syze*e_size)))

    n = np.transpose(n)
    ny = n[1]
    ek = e[2]
    

    if(ny < 0 ):
        Omega = 2 * np.pi - Omega
            
    if(ek < 0 ):
        w = 2 * np.pi - w
            
    if(dot_rv < 0 ):       
        th = 2 * np.pi - th 
            
            

    return(a,e,i,Omega,w,th,e_size,R,V) 


#Transformation between Position and Velocity Vectors in ECI system to calsical elements:
def RVvectors_to_Clasical2(sol,t):
 
    R = sol[:,0:3]
    R = R[0:len(t)]
    
    V = sol[:,3:6]
    V = V[0:len(t)]
    
    x = sol[:,0]
    y = sol[:,1]
    z = sol[:,2]

    v1 = sol[:,3]
    v2 = sol[:,4]
    v3 = sol[:,5]
    
    dot_rv = x*v1 + y*v2 + z*v3
    dot_rv = dot_rv[0:len(t)]
    
    R_syze = np.sqrt(x**2 + y**2 + z**2)
    R_syze = R_syze[0:len(t)]
    V_syze = np.sqrt(v1**2 + v2**2 + v3**2)
    V_syze = V_syze[0:len(t)]
    
    h = np.cross(R,V)
    h_size = np.sqrt(h[:,0]**2 + h[:,1]**2  + h[:,2]**2)
    h_size = h_size[0:len(t)]
    
    e = R
    e[:,0] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[:,0] - (dot_rv * V[:,0]))
    e[:,1] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[:,1] - (dot_rv * V[:,1]))
    e[:,2] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[:,2] - (dot_rv * V[:,2]))
    e_size = np.sqrt(e[:,0]**2 + e[:,1]**2  + e[:,2]**2)
    p = (h_size**2) / mu 
    a = p / (1 - e_size**2)
    n = np.cross([0,0,1],h)
    n_size = np.sqrt(n[:,0]**2 + n[:,1]**2  + n[:,2]**2)
    dot_ne = n[:,0]*e[:,0] + n[:,1]*e[:,1] + n[:,2]*e[:,2]
    dot_re = R[:,0]*e[:,0] + R[:,1]*e[:,1] + R[:,2]*e[:,2]
    i = np.arccos(h[:,2]/h_size)
    Omega = np.arccos(n[:,0]/n_size)
    w =  np.arccos(dot_ne / (n_size * e_size))
    th = np.arccos(((a*(1-e_size**2)-R_syze)/(R_syze*e_size)))

    
    ny = n[:,1]
    ek = e[:,2]
    
    for j in range(len(t)):
        if(ny[j] < 0 ):
            Omega[j] = 2 * np.pi - Omega[j]
            
        if(ek[j] < 0 ):
            w[j] = 2 * np.pi - w[j]
            
        if(dot_rv[j] < 0 ):       
            th[j] = 2 * np.pi - th[j] 
            
            

    return(a,e,i,Omega,w,th,e_size,R,V)


def GaussMat_polar(h,a,e,th,o,i,mu,r):

    n=np.sqrt(mu/(a**3))
    th_star=th+o
    p=a*(1-e**2)
    Mat=np.zeros((6,3))
    Mat[0,0]=((2*a**2)/h)*e*np.sin(th)
    Mat[0,1]=((2*a**2)/h)*(1+e*np.cos(th))
    Mat[1,0]=np.sin(th)*p/h
    Mat[1,1]=(r/h)*(e+2*np.cos(th)+e*(np.cos(th))**2)
    Mat[2,2]=r*np.cos(th_star)/h
    Mat[3,2]=r*np.sin(th_star)/(h*np.sin(i))
    Mat[4,0]=-(p/(h*e))*np.cos(th)
    Mat[4,1]=(r/(h*e))*(2+e*np.cos(th))*np.sin(th)
    Mat[4,2]=-(r*np.sin(th_star)*np.cos(i))/(h*np.sin(i))
    Mat[5,0]=(p*np.cos(th)-2*r*e)/(n*(a**2)*e)
    Mat[5,1]=-(p+r)*np.sin(th)/(n*(a**2)*e)
    return Mat
    

def orbit(X,t):
#X7- inclination
#X8- RAAN
#X9- Argument of perigee
#X10- a
#X11- e   

    
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]    
    X7 = X[6:7]
    X8 = X[7:8]
    X9 = X[8:9]
    X10 = X[9:10]
    X11 = X[10:11]
	
    
    r_v = np.array([X1,X2,X3])
    v_v = np.array([X4,X5,X6])
    
    r_v = np.transpose(np.array([X1,X2,X3]))
    v_v = np.transpose(np.array([X4,X5,X6]))
    h = LA.norm(np.cross(r_v,v_v))
    
    r_v = np.array([X1,X2,X3])
    v_v = np.array([X4,X5,X6])
    
    r = LA.norm(r_v)
    v = LA.norm(v_v)
    #h = LA.norm(np.cross(r_v,v_v))
    #h = LA.norm(np.cross([X1,X2,X3],[X4,X5,X6]))
    (X10,e,X7,X8,X9,th,X11,R,V) = RVvectors_to_Clasical(r_v,v_v)

  
    
    #(Fr,Fth,Fh) = J2radForce(r,X7,th,X9,mu)
    F = np.array(J2radForce(r,X7,th,X9,mu))
    DCM2 = DCM(X8,X9 + th,X7)
    J2_Force = np.dot(DCM2  ,F[:,:,0])
##    
###    
    rs = SunDirection(Month,1)[0]
    Sun_Force = SunForce(beta,A_sun,m)*rs/LA.norm(rs)
###    
###    
    Kd = Cd * A_drag/m
    fdrag = DragForce(r,v,Kd,X10,X11)
    Fdrag = -np.array([X4,X5,X6]/v) * fdrag

    f1 = Sun_Force[0] + J2_Force[0] + Fdrag[0] 
    f2 = Sun_Force[1] + J2_Force[1] + Fdrag[1] 
    f3 = Sun_Force[2] + J2_Force[2] + Fdrag[2]
#
    FF = np.array([f1,f2,f3])
##
    DCM3 = np.transpose(DCM(X8,X9+th,X7))
    F_rth = np.dot(DCM3,FF)
    Gauss = GaussMat_polar(h,X10,X11,th,X9,X7,mu,r)
    dx = np.dot(Gauss,F_rth) #dx=[da,de,di,dO,do,dM]
##    
#    
    dX1 = X4
    dX2 = X5
    dX3 = X6	
    dX4 = (-mu / (r**3)) * X1 + f1
    dX5 = (-mu / (r**3)) * X2 + f2
    dX6 = (-mu / (r**3)) * X3 + f3
    
#X7- inclination
#X8- RAAN
#X9- Argument of perigee
#X10- a
#X11- e  
    derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6,dx[2],dx[3],dx[4],dx[0],dx[1]))
    #derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6))
    #derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6,X7,X8,X9,X10,X11))
    return derivs  
		
		


n = 2*np.pi/T
a = a_two(mu,T)
OMds = 2*np.pi/365.25/86400
rtd = 180/np.pi
(r,OMs) = SunDirection(Month,Day)
OM = ((15*(LTAN-12))/rtd + OMs)
ran = np.array([ran1,ran1*np.tan(OM),0])
rran = LA.norm(ran)
rdn = -rrdn*ran/rran
k = rran/rrdn
x = (1-k)/(1+k)
p = rran*(1+x)
e = (1-p/a)**0.5
om = np.arccos(x/e)
cosi = -2/3*OMds/J2/n*(p/Re)**2
inc = np.arccos(cosi)
th = -om

(r0,V0)  = Clasical_to_RVvectors(a,e,inc,OM,om,-om)
t = np.arange( 0,T + 0.1 ,0.1) 
#X0  = sci.array([rp1_V,V_pr1,inc,OM,om,a,e])
#X0  = sci.array([rp1_V,V_pr1])
#X0 = X0.flatten()
#(X10,e,X7,X8,X9,th,X11,R,V) = RVvectors_to_Clasical(rp1_V,V_pr1)
##(X10,e,X7,X8,X9,th,X11,R,V) = RVvectors_to_Clasical(np.transpose(rp1_V),np.transpose(V_pr1))
#F = np.transpose(np.array(J2radForce(LA.norm(r),X7,th,X9,mu)))
#DCM2 = np.array(DCM(X8,X9 + th,X7)).astype('float64')
#J2_Force = np.dot(np.array(DCM2), F)
X0  = sci.array([r0,V0])
ww = sci.array([inc,OM,om,a,e])
ar = np.append(X0, ww)
X0 = ar.flatten()

#
#sol2 = np.zeros((60001, 11))
sol = odeint(orbit,X0,t) 

#for q in range(6):
#    sol2[:,q] = sol[:,q]

#rsol = np.transpose([sol[:,0], sol[:,1], sol[:,2]])
#vsol = np.transpose([sol[:,3], sol[:,4], sol[:,5]])


answer1 = dict(a = a * u.km,e = e ,i = inc*u.rad, Omega = OM*u.rad,omega = om*u.rad ,Theata = -om* u.rad)
answer12 = dict(a = a * u.km,e = e ,i = np.rad2deg(inc)*u.deg, Omega = np.rad2deg(OM)*u.deg,omega = np.rad2deg(om)*u.deg ,Theata = np.rad2deg(-om)* u.deg)
np.savetxt('t.txt', (t))        
np.savetxt('sol.txt', (sol))
np.savetxt('rp1_V.txt', (r0))
np.savetxt('V_p.txt', (V0))

#X7- inclination
#X8- RAAN
#X9- Argument of perigee
#X10- a
#X11- e 
(sol[:,9],e,sol[:,6],sol[:,7],sol[:,8],th,sol[:,10],R,V) = RVvectors_to_Clasical2(sol,t)
res = np.where(th == min(th))
(rp1_V,V_pr1)  = Clasical_to_RVvectors(sol[:,9][res],sol[:,10][res],sol[:,6][res],sol[:,7][res],sol[:,8][res],th[res])
V_pr1_size = LA.norm(V_pr1)
V_vector = V_pr1 / V_pr1_size
V_pr2 = V_pr1 + 12 * (10**-3) * V_vector
(a2,e2,i2,Omega2,omega2,th2,ee2,R,V) = RVvectors_to_Clasical(rp1_V,V_pr2)


answer2 = dict(rp1 = rp1_V * u.km, V_pr = V_pr2 * (u.km/u.s))
answer3 = dict(a = a2 * u.km,e = ee2 ,i = i2*u.rad, Omega = Omega2*u.rad,omega = omega2*u.rad ,Theata = th2* u.rad)
answer32 = dict(a = a2 * u.km,e = ee2 ,i = np.rad2deg(i2)*u.deg, Omega = np.rad2deg(Omega2)*u.deg,omega = np.rad2deg(omega2)*u.deg ,Theata = np.rad2deg(th2)* u.deg)