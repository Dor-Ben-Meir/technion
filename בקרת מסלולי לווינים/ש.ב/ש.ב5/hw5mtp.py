######################################################################################
#Matlab to Python hw5 
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit
from scipy import signal
from control.matlab import *
#from control import *
from control.statefbk import ctrb, obsv, place, place_varga, lqr, gram, acker

# numerical solver
tolerance = 3e-14; # solver error tolerance
# general parameters
mu = 398603; # [km^3/s^2] - Earth gravitational constant
Re = 6378; # [km] - Earth equatorial radius
# givens
a = 6778; # [km]
fMax = 5 * (10**-6); # [km/s^2]
vr = 3 * (10**-5) # [km/s]
x0 = 1; # [km]
y0 = 0;
z0 = 0;
zDot0 = 0;
# chosen values
r0 = 2; # [km]
xr0 = 0;
yr0 = -r0/np.sqrt(2); # [km]
zr0 = -r0/np.sqrt(2); # [km]
xrDot0 = 0;
yrDot0 = vr/np.sqrt(2); # [km/s]
zrDot0 = vr/np.sqrt(2); # [km/s]

# calculated parameters
n = np.sqrt(mu/a**3);
T = 2*np.pi/n;
xDot0 = n*y0/2;
yDot0 = -2*n*x0;

F = np.array([ [0 , 1 , 0, 0 , 0 , 0],[3*(n**2), 0 , 0, 2*n, 0 , 0],[0 , 0 , 0, 1 , 0 , 0],[0 , -2*n, 0, 0 , 0 , 0],[0, 0 , 0, 0 , 0 , 1],[0, 0 , 0, 0 , -n**2, 0]])
G = np.array([[0, 0, 0],[1, 0, 0],[0, 0, 0],[0, 1, 0],[0, 0, 0],[0, 0, 1]])
Fr = np.array([[0, 1, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 0],
[0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0]])

tauXY = 0.3*T;
tauZ = 0.2*T;
dominantReal = 1/tauXY;
dominantRealZ = 1/tauZ;


poles = np.zeros((4),dtype=complex)
polesZ = np.zeros((2),dtype=complex)
poles[0] = -dominantReal + 1j*dominantReal
poles[1] = np.conjugate(poles[0])
poles[2] = -dominantReal*4 + 1j*dominantReal
poles[3] = np.conjugate(poles[2])

polesZ[0] = -dominantRealZ + 1j*dominantRealZ
polesZ[1] = np.conjugate(polesZ[0])

KXY = place(F[0:4,0:4], G[0:4,0:2], poles)
#KZ = place(F[4:6,4:6], G[4:6,2], polesZ)
KZ = place(F[4:6,4:6], np.array([[0],[1]]), polesZ)

ga = np.column_stack((KXY , np.zeros((2, 2))))
gc = np.column_stack((np.zeros((1, 4)), KZ))
control_K = np.vstack((ga,gc))
control_K = np.array(control_K)
#control_K = np.array([[4.61597571647994*(10**-6),	0.00323541494496110,	-1.12225947186756*(10**-6),	0.00116825912698973,	0,	0],[4.99902983446862*(10**-6),	-0.000241106123706940,	1.46861816211771*(10**-6),	0.00396733035473714,	0,	0],[0,	0,0,	0,	3.41159283252823*(10**-7),	0.00180068632492465]])


def init_orbit(X,t):
     
    x = X[0:6]
    xr = X[6:12]
    deltaV  = X[12:13]    
    distanceTravelled = X[13:14]
    deltax = x - xr
    
    Gf = np.zeros((6,1))
    f = np.zeros((3,1))
        
    
    dX1 = np.matmul(F,x) + np.reshape(Gf, (1,6 ))
    dX2 = np.matmul(Fr,xr)
    dX2 = np.reshape(dX2, (1,6 ))
    dX3 = LA.norm(f)
    dX4 = LA.norm(x[3:6])
    derivs = np.column_stack((dX1,dX2,dX3,dX4))
    #derivs = np.array(derivs.T)
    derivs = np.reshape(derivs, (14, ))
    #derivs = np.concatenate((dX1,dX2,dX3,dX4))
    return derivs

def orbitA(X,t):
    # x = states(1:6);
    # xr = states(7:12);
    # deltaV = states(13);
    # distanceTravelled = states(14);
    # deltax = x - xr
    
    x = X[0:6]
    xr = X[6:12]
    deltaV  = X[12:13]    
    distanceTravelled = X[13:14]
    deltax = x - xr
    
    Gf = -np.matmul((F - Fr),x) +  np.matmul( (np.matmul(-G,control_K)) , deltax)
    f = np.choose([1, 3, 5], Gf.T)
        
    dX1 = np.matmul(F,x) + np.reshape(Gf, (1,6 ))
    dX2 = np.matmul(Fr,xr)
    dX2 = np.reshape(dX2, (1,6 ))
    dX3 = LA.norm(f)
    dX4 = LA.norm(x[3:6])
    derivs = np.column_stack((dX1,dX2,dX3,dX4))
    #derivs = np.array(derivs.T)
    derivs = np.reshape(derivs, (14, ))
    # #derivs = np.concatenate((dX1,dX2,dX3,dX4))
    # dX1 = np.matmul(F,x) + Gf
    # dX2 = np.matmul(Fr,xr)
    # dX2 = np.reshape(dX2, (6, ))
    # dX3 = LA.norm(f)
    # dX4 = LA.norm(x[3:6])
    # derivs = np.column_stack((dX1,dX2,dX3,dX4))
    # derivs = np.array(derivs.T)
    # derivs = np.reshape(derivs, (14, ))
    # #derivs = np.concatenate((dX1,dX2,dX3,dX4))
    return derivs    

t = np.arange( 0,T + 0.1 ,0.1)   
X0 = np.array([x0, xDot0, y0, yDot0, z0, zDot0, 0, 0, 0, 0, 0, 0, 0, 0])
#X0 = X0.flatten()
solint = odeint(init_orbit,X0,t)

#t2 = np.arange( 0,2*T + 5 ,5) 
t2 = np.linspace(0.0, 6.659997717765140 * (10**4), num=8925)  
X02 = np.array([x0, xDot0, y0, yDot0, z0, zDot0, xr0, xrDot0, yr0, yrDot0, zr0, zrDot0, 0, 0])
#X02 = X0.flatten()
sol = odeint(orbitA,X02,t2)

np.savetxt('output1.txt', (sol))
np.savetxt('output.txt', (solint))
np.savetxt('contorlk.txt', (control_K))
np.savetxt('tpy.txt', (t2))