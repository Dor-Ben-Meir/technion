######################################################################################
#Python code for homework 5 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit
from scipy import signal
from control.matlab import *
#from control import *
from control.statefbk import ctrb, obsv, place, place_varga, lqr, gram, acker

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
T = 100*60
n = 2*np.pi/T
fMax = 4 * (10**-6) # [km/s^2]
x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0
tf = 80*60
xDot0 = n*y0/2
yDot0 = -2*n*x0
zDot0 = 0
vXMax = x0 / tf
vX = vXMax


xr0 = 0.0003
zr0 = 2 * xr0 # [km]
yr0 = np.sqrt(1 - (zr0**2)) # [km]
xrDot0 = n*yr0/2
yrDot0 = -2*n*xr0
zrDot0 = 2 * xrDot0



F = np.array([ [0 , 1 , 0, 0 , 0 , 0],[3*(n**2), 0 , 0, 2*n, 0 , 0],[0 , 0 , 0, 1 , 0 , 0],[0 , -2*n, 0, 0 , 0 , 0],[0, 0 , 0, 0 , 0 , 1],[0, 0 , 0, 0 , -n**2, 0]])
G = np.array([[0, 0, 0],[1, 0, 0],[0, 0, 0],[0, 1, 0],[0, 0, 0],[0, 0, 1]])
Fr = F

# dominantReal = 0.6*n
# dominantRealZ = 0.4*n

# dominantReal = n
# dominantRealZ = n

tauXY = 0.3*T
tauZ = 0.2*T

dominantReal = 1/tauXY;
dominantRealZ = 1/tauZ;

poles = np.zeros((4),dtype=complex)
polesZ = np.zeros((2),dtype=complex)
poles[0] = -dominantReal + 1j*dominantReal
poles[1] = np.conjugate(poles[0])
poles[2] = -dominantReal*4 + 1j*dominantReal
poles[3] = np.conjugate(poles[2])

polesZ[0] = -dominantRealZ + 1j*dominantRealZ
polesZ[1] = np.conjugate(polesZ[0])

KXY = place_varga(F[0:4,0:4], G[0:4,0:2], 0.89*poles)
#KZ = place(F[4:6,4:6], G[4:6,2], polesZ)
KZ = place_varga(F[4:6,4:6], np.array([[0],[1]]), polesZ)

ga = np.column_stack((KXY , np.zeros((2, 2))))
gc = np.column_stack((np.zeros((1, 4)), KZ))
control_K = np.vstack((ga,gc))
control_K = np.vstack((ga,gc))
control_K = np.array(control_K)

def init_orbit(X,t):
     
    x = X[0:6]
    xr = X[6:12]
    deltaV  = X[12:13]    
    distanceTravelled = X[13:14]
    deltax = x - xr
    
    Gf = np.zeros((6,1))
    f = np.zeros((3,1))
        
    
    dX1 = np.matmul(F,x) + np.reshape(Gf, (1,6 ))
    dX2 = np.matmul(Fr,xr)
    dX2 = np.reshape(dX2, (1,6 ))
    dX3 = LA.norm(f)
    dX4 = LA.norm(x[3:6])
    derivs = np.column_stack((dX1,dX2,dX3,dX4))
    derivs = np.reshape(derivs, (14, ))
    return derivs

def orbitA(X,t):
    
    x = X[0:6]
    xr = X[6:12]
    deltaV  = X[12:13]    
    distanceTravelled = X[13:14]
    deltax = x - xr
    
    Gf = -np.matmul((F - Fr),x) +  np.matmul( (np.matmul(-G,control_K)) , deltax)
    f = np.choose([1, 3, 5], Gf.T)
        
    dX1 = np.matmul(F,x) + np.reshape(Gf, (1,6 ))
    dX2 = np.matmul(Fr,xr)
    dX2 = np.reshape(dX2, (1,6 ))
    dX3 = LA.norm(f)
    dX4 = LA.norm(x[3:6])
    derivs = np.column_stack((dX1,dX2,dX3,dX4))
    derivs = np.reshape(derivs, (14, ))
    return derivs    

t = np.arange( 0,T + 0.1 ,0.1)   
X0 = np.array([x0, xDot0, y0, yDot0, z0, zDot0, 0, 0, 0, 0, 0, 0, 0, 0])
#X0 = X0.flatten()
solint = odeint(init_orbit,X0,t)

#t2 = np.arange( 0,2*T + 5 ,5) 
t2 = np.linspace(0.0, 2*T, num=8925)  
X02 = np.array([x0, xDot0, y0, yDot0, z0, zDot0, xr0, xrDot0, yr0, yrDot0, zr0, zrDot0, 0, 0])
#X02 = X0.flatten()
sol = odeint(orbitA,X02,t2)

np.savetxt('output1.txt', (sol))
np.savetxt('output.txt', (solint))
np.savetxt('contorlk.txt', (control_K))
np.savetxt('tpy.txt', (t2))

