/* Shahar Wollmark 201478591 - Computational Fluid Dynamics - HW-2 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.1415
#define GAMMA 1.4
#define R 287
#define T_sl 288.15
#define L 6.5E-3
#define p_sl 101325.0
#define g 9.81

//***********************************************************************
// This programm will solve the Euler equations
// for a two dimensional flow over a symmetric airfoil
// using  curvilinear coordinates using "Beam & Warming" algorithm
// (using the Euler implicit, first order temporal accuracy, scheme).
//***********************************************************************

/***** Importing programs from the Moodle *****/
int smooth(float *q, float *s, float *jac, float *xx, float *xy,float *yx, float *yy, int id, int jd, float *s2,float *rspec, float *qv, float *dd,float epse, float gamma, float fsmach, float dt)
{

    float *rho, *u_vel, *v_vel, *t_e;

    float eratio, smool, gm1, ggm1, cx, cy, eps, ra, u, v, qq, ss, st,
          qav, qxx, ssfs, qyy;
    int ib, ie, jb, je, i, j, offset, offsetp1, offsetm1, ip, ir, n,
        jp, jr;
    eratio = 0.25 + 0.25 * pow(fsmach + 0.0001,gamma);
    smool = 1.0;
    gm1 = gamma - 1.0;
    ggm1 = gamma * gm1;
    ib = 1;
    ie = id - 1;
    jb = 1;
    je = jd - 1;

    cx = 2.;
    cy = 1.;

    rho = q;
    u_vel = &q[id*jd];
    v_vel = &q[2*id*jd];
    t_e = &q[3*id*jd];

/*     smoothing in xi direction */

    for (j = jb; j < je; j++) {
	for (i = 0; i < id; i++) {
	    offset = id * j + i;
	    eps = epse / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq);
            rspec[i] = eps * (fabs(xx[offset] * u + xy[offset] * v) + sqrt((xx[offset] * xx[offset] + xy[offset] * xy[offset]) * ss + 0.01));
	    qv[i] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (i = ib; i < ie; i++) {
	    ip = i + 1;
	    ir = i - 1;
	    qxx = qv[ip] - qv[i] * 2. + qv[ir];
	    qav = (qv[ip] + qv[i] * 2. + qv[ir]) * .25;
	    dd[i] = eratio * fabs(qxx / qav);
	}

	dd[0] = dd[1];
	dd[id - 1] = dd[id - 2];

	for (n = 0; n < 4; n++) {
	    for (i = ib; i < ie; i++) {
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j) * id + i + 1;
		offsetm1 = (jd * n + j) * id + i - 1;
		s2[i] = q[offsetp1] - 2.0 * q[offset] + q[offsetm1];
	    }

	    s2[0] = s2[1] * -1.;
	    s2[id - 1] = s2[id - 2] * -1.;

	    for (i = ib; i < ie; i++) {
		ip = i + 1;
		ir = i - 1;
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j) * id + i + 1;
		offsetm1 = (jd * n + j) * id + i - 1;
		st = ((dd[ip] + dd[i]) * .5 * (q[offsetp1] - q[offset]) - cx / (cx + dd[ip] + dd[i]) * (s2[ip] - s2[i])) * (rspec[ip] + rspec[i]) + ((dd[ir] + dd[i]) * .5 * (q[offsetm1] - q[offset]) - cx / (cx + dd[ir] + dd[i]) * (s2[ir] - s2[i])) * (rspec[ir] + rspec[i]);
		s[offset] += st * .5 * dt;
	    }
	}
    }

/*     smoothing in eta direction */

    ssfs = 1. / (0.001 + fsmach * fsmach);
    for (i = ib; i < ie; i++) {
	for (j = 0; j < jd; j++) {
	    offset = id * j + i;
	    eps = epse / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq) * (1.0 - smool) + smool * qq * ssfs;
            rspec[j] = eps * (fabs(yx[offset] * u + yy[offset] * v) + sqrt((yx[offset] * yx[offset] + yy[offset] * yy[offset]) * ss + 0.01));
	    qv[j] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (j = jb; j < je; j++) {
	    jp = j + 1;
	    jr = j - 1;
	    qyy = qv[jp] - qv[j] * 2. + qv[jr];
	    qav = (qv[jp] + qv[j] * 2. + qv[jr]) * .25;
	    dd[j] = eratio * fabs(qyy / qav);
	}

	dd[0] = dd[1];
	dd[jd - 1] = dd[jd - 2];

	for (n = 0; n < 4; n++) {
	    for (j = jb; j < je; j++) {
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j + 1) * id + i;
		offsetm1 = (jd * n + j - 1) * id + i;
		s2[j] = q[offsetp1] - 2.0 * q[offset] + q[offsetm1];
	    }

	    s2[0] = s2[1] * -1.;
	    s2[jd - 1] = s2[jd - 2] * -1.;

	    for (j = jb; j < je; j++) {
		jp = j + 1;
		jr = j - 1;
		offset = (jd * n + j) * id + i;
		offsetp1 = (jd * n + j + 1) * id + i;
		offsetm1 = (jd * n + j - 1) * id + i;
		st = ((dd[jp] + dd[j]) * .5 * (q[offsetp1] - q[offset]) - cy / (cy + dd[jp] + dd[j]) * (s2[jp] - s2[j])) * (rspec[jp] + rspec[j]) + ((dd[jr] + dd[j]) * .5 * (q[offsetm1] - q[offset]) - cy / (cy + dd[jr] + dd[j]) * (s2[jr] - s2[j])) * (rspec[jr] + rspec[j]);
		s[offset] += st * .5 * dt;
	    }
	}
    }

    return 0;
} /* smooth */
int smoothx(float *q, float *xx, float *xy, int id, int jd, float *a,
	   float *b, float *c, int j,float *jac, float *drr, float *drp,
           float *rspec, float *qv, float *dd,
           float epsi, float gamma, float fsmach, float dt)
{

    float *rho, *u_vel, *v_vel, *t_e;

    float eratio, gm1, ggm1, eps, ra, u, v, qq, ss,
          qav, qxx, rr, rp;
    int ib, ie, i, offset, offsetp1, offsetm1, ip, ir, n;
    eratio = 0.25 + 0.25 * pow(fsmach + 0.0001,gamma);
    gm1 = gamma - 1.0;
    ggm1 = gamma * gm1;
    ib = 1;
    ie = id - 1;

    rho = q;
    u_vel = &q[id*jd];
    v_vel = &q[2*id*jd];
    t_e = &q[3*id*jd];

/*     smoothing in xi direction */

        for (i = 0; i < id; i++) {
	    offset = id * j + i;
	    eps = epsi / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq);
            rspec[i] = eps * (fabs(xx[offset] * u + xy[offset] * v) + sqrt((xx[offset] * xx[offset] + xy[offset] * xy[offset]) * ss + 0.01));
	    qv[i] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (i = ib; i < ie; i++) {
	    ip = i + 1;
	    ir = i - 1;
	    qxx = qv[ip] - qv[i] * 2. + qv[ir];
	    qav = (qv[ip] + qv[i] * 2. + qv[ir]) * .25;
	    dd[i] = eratio * fabs(qxx / qav);
	}

	dd[0] = dd[1];
	dd[id - 1] = dd[id - 2];

        for (i = ib; i < ie; i++) {
	    ip = i + 1;
	    ir = i - 1;
	    offset = j * id + i;
	    offsetp1 = j * id + i + 1;
	    offsetm1 = j * id + i - 1;
	    rp = (0.5 * (dd[ip] + dd[i]) + 2.5) * dt * 0.5 * (rspec[ip] + rspec[i]);
	    rr = (0.5 * (dd[ir] + dd[i]) + 2.5) * dt * 0.5 * (rspec[ir] + rspec[i]);
	    qv[i] = (rr + rp) * jac[offset];
	    drr[i] = rr * jac[offsetm1];
	    drp[i] = rp * jac[offsetp1];
	}

	for (n = 0; n < 4; n++) {
	    for (i = ib; i < ie; i++) {
	        offset = (n * 4 + n) * id + i;
		a[offset] -= drr[i];
		b[offset] += qv[i];
		c[offset] -= drp[i];
	    }
        }
    return 0;
} /* smoothx */
int smoothy(float *q, float *yx, float *yy, int id, int jd, float *a, float *b, float *c, int i,float *jac, float *drr, float *drp,float *rspec, float *qv, float *dd,float epsi, float gamma, float fsmach, float dt)
{

    float *rho, *u_vel, *v_vel, *t_e;

    float eratio, smool, gm1, ggm1, eps, ra, u, v, qq, ss,
          qav, ssfs, qyy, rp, rr;
    int jb, je, j, offset, offsetp1, offsetm1, n,
        jp, jr;
    eratio = 0.25 + 0.25 * pow(fsmach + 0.0001,gamma);
    smool = 1.0;
    gm1 = gamma - 1.0;
    ggm1 = gamma * gm1;
    jb = 1;
    je = jd - 1;

    rho = q;
    u_vel = &q[id*jd];
    v_vel = &q[2*id*jd];
    t_e = &q[3*id*jd];

/*     smoothing in eta direction */

        ssfs = 1. / (0.001 + fsmach * fsmach);
        for (j = 0; j < jd; j++) {
	    offset = id * j + i;
	    eps = epsi / jac[offset];
	    ra = 1. / rho[offset];
	    u = u_vel[offset] * ra;
	    v = v_vel[offset] * ra;
	    qq = u * u + v * v;
	    ss = ggm1 * (t_e[offset] * ra - 0.5 * qq) * (1.0 - smool) + smool * qq * ssfs;
            rspec[j] = eps * (fabs(yx[offset] * u + yy[offset] * v) + sqrt((yx[offset] * yx[offset] + yy[offset] * yy[offset]) * ss + 0.01));
	    qv[j] = gm1 * (t_e[offset] - 0.5 * qq * rho[offset]);
	}

	for (j = jb; j < je; j++) {
	    jp = j + 1;
	    jr = j - 1;
	    qyy = qv[jp] - qv[j] * 2. + qv[jr];
	    qav = (qv[jp] + qv[j] * 2. + qv[jr]) * .25;
	    dd[j] = eratio * fabs(qyy / qav);
	}

	dd[0] = dd[1];
	dd[jd - 1] = dd[jd - 2];

        for (j = jb; j < je; j++) {
	    jp = j + 1;
	    jr = j - 1;
	    offset = j * id + i;
	    offsetp1 = (j + 1) * id + i;
	    offsetm1 = (j - 1) * id + i;
	    rp = (0.5 * (dd[jp] + dd[j]) + 2.5) * dt * 0.5 * (rspec[jp] + rspec[j]);
	    rr = (0.5 * (dd[jr] + dd[j]) + 2.5) * dt * 0.5 * (rspec[jr] + rspec[j]);
	    qv[j] = (rr + rp) * jac[offset];
	    drr[j] = rr * jac[offsetm1];
	    drp[j] = rp * jac[offsetp1];
	}

	for (n = 0; n < 4; n++) {
	    for (j = jb; j < je; j++) {
	        offset = (n * 4 + n) * jd + j;
		a[offset] -= drr[j];
		b[offset] += qv[j];
		c[offset] -= drp[j];
	    }
        }
    return 0;
} /* smoothy */
void write_q(int ni, int nj, float *q, FILE *qsol, float fsmach, float alpha, float rey, float time)

{


  fwrite(&ni, sizeof(int), 1, qsol);
  fwrite(&nj, sizeof(int), 1, qsol);

  fwrite(&fsmach, sizeof(float), 1, qsol);
  fwrite(&alpha, sizeof(float), 1, qsol);
  fwrite(&rey, sizeof(float), 1, qsol);
  fwrite(&time, sizeof(float), 1, qsol);

  fwrite(q, sizeof(float), 4*ni*nj, qsol);

}

void write_grid(int ni, int nj, float x[], float y[], FILE *grid)

{

  fwrite(&ni, sizeof(int), 1, grid);
  fwrite(&nj, sizeof(int), 1, grid);

  fwrite(x, sizeof(float), ni*nj, grid);
  fwrite(y, sizeof(float), ni*nj, grid);

}

int btri4s(float *a, float *b, float *c, float *f, int kd, int ks, int ke)
{
  /* Local variables */
  int k, m, n, nd, md;

  float c1, d1, d2, d3, d4, c2, c3, c4, b11, b21, b22, b31, b32, b33,
    b41, b42, b43, b44, u12, u13, u14, u23, u24, u34;


  /*   (A,B,C)F = F, F and B are overloaded, solution in F */

  md = 4;
  nd = 4;

  /*   Part 1. Forward block sweep */

  for (k = ks; k <= ke; k++)
    {

      /*      Step 1. Construct L in B */

      if (k != ks)
	{
	  for (m = 0; m < md; m++)
	    {
	      for (n = 0; n < nd; n++)
		{
		  b[k + kd * (m + md * n)] = b[k + kd * (m + md * n)]
		    - a[k + kd * (m + md * 0)] * b[k - 1 + kd * (0 + md * n)]
		    - a[k + kd * (m + md * 1)] * b[k - 1 + kd * (1 + md * n)]
		    - a[k + kd * (m + md * 2)] * b[k - 1 + kd * (2 + md * n)]
		    - a[k + kd * (m + md * 3)] * b[k - 1 + kd * (3 + md * n)] ;
		}
	    }
	}

      /*      Step 2. Compute L inverse (block matrix) */

      /*          A. Decompose L into L and U */

      b11 = 1. / b[k + kd * (0 + md * 0)];
      u12 = b[k + kd * (0 + md * 1)] * b11;
      u13 = b[k + kd * (0 + md * 2)] * b11;
      u14 = b[k + kd * (0 + md * 3)] * b11;
      b21 = b[k + kd * (1 + md * 0)];
      b22 = 1. / (b[k + kd * (1 + md * 1)] - b21 * u12);
      u23 = (b[k + kd * (1 + md * 2)] - b21 * u13) * b22;
      u24 = (b[k + kd * (1 + md * 3)] - b21 * u14) * b22;
      b31 = b[k + kd * (2 + md * 0)];
      b32 = b[k + kd * (2 + md * 1)] - b31 * u12;
      b33 = 1. / (b[k + kd * (2 + md * 2)] - b31 * u13 - b32 * u23);
      u34 = (b[k + kd * (2 + md * 3)] - b31 * u14 - b32 * u24) * b33;
      b41 = b[k + kd * (3 + md * 0)];
      b42 = b[k + kd * (3 + md * 1)] - b41 * u12;
      b43 = b[k + kd * (3 + md * 2)] - b41 * u13 - b42 * u23;
      b44 = 1. / (b[k + kd * (3 + md * 3)] - b41 * u14 - b42 * u24
		  - b43 * u34);

      /*      Step 3. Solve for intermediate vector */

      /*          A. Construct RHS */
      if (k != ks)
	{
	  for (m = 0; m < md; m++)
	    {
	      f[k + kd * m] = f[k + kd * m]
		- a[k + kd * (m + md * 0)] * f[k - 1 + kd * 0]
		- a[k + kd * (m + md * 1)] * f[k - 1 + kd * 1]
		- a[k + kd * (m + md * 2)] * f[k - 1 + kd * 2]
		- a[k + kd * (m + md * 3)] * f[k - 1 + kd * 3];
	    }
	}

      /*          B. Intermediate vector */

      /*          Forward substitution */

      d1 = f[k + kd * 0] * b11;
      d2 = (f[k + kd * 1] - b21 * d1) * b22;
      d3 = (f[k + kd * 2] - b31 * d1 - b32 * d2) * b33;
      d4 = (f[k + kd * 3] - b41 * d1 - b42 * d2 - b43 * d3) * b44;

      /*          Backward substitution */

      f[k + kd * 3] = d4;
      f[k + kd * 2] = d3 - u34 * d4;
      f[k + kd * 1] = d2 - u23 * f[k + kd * 2] - u24 * d4;
      f[k + kd * 0] = d1 - u12 * f[k + kd * 1] - u13 * f[k + kd * 2] - u14 * d4;

      /*      Step 4. Construct U = L ** (-1) * C */
      /*              by columns and store in B */

      if (k != ke)
	{
	  for (n = 0; n < nd; n++)
	    {

	      /*          Forward substitution */

	      c1 = c[k + kd * (0 + md * n)] * b11;
	      c2 = (c[k + kd * (1 + md * n)] - b21 * c1) * b22;
	      c3 = (c[k + kd * (2 + md * n)] - b31 * c1 - b32 * c2) *
		b33;
	      c4 = (c[k + kd * (3 + md * n)] - b41 * c1 - b42 * c2 -
		    b43 * c3) * b44;

	      /*          Backward substitution */

	      b[k + kd * (3 + md * n)] = c4;
	      b[k + kd * (2 + md * n)] = c3 - u34 * c4;
	      b[k + kd * (1 + md * n)] = c2 - u23 * b[k + kd * (2 + md * n)] - u24 * c4;
	      b[k + kd * (0 + md * n)] = c1 - u12 * b[k + kd * (1 + md * n)]
		- u13 * b[k + kd * (2 + md * n)] - u14 * c4;
	    }
	}
    }

  /*   Part 2. Backward block sweep */

  if (ke == ks)
    {
      return 0;
    }

  for (k = ke - 1; k >= ks; --k)
    {
      for (m = 0; m < md; m++)
	{
	  f[k + kd * m] = f[k + kd * m]
	    - b[k + kd * (m + md * 0)] * f[k + 1 + kd * 0]
	    - b[k + kd * (m + md * 1)] * f[k + 1 + kd * 1]
	    - b[k + kd * (m + md * 2)] * f[k + 1 + kd * 2]
	    - b[k + kd * (m + md * 3)] * f[k + 1 + kd * 3];
	}
    }

  return 0;

} /* btri4s_ */
/***** Programs by Me *****/

int offset_2D(int i,int j,int ni)
{
    return (j*ni + i);
}

int offset_3D(int i, int j, int n, int imax, int jmax)
{
    // index offset for 3D arrays:
    return ( ( n*jmax + j )*imax + i );
}

void input(int *i_max,int *j_max,int *iTEL,int *iLE,int *iTEU,int *N,float *dt,float *FSMACH,float *AOA,float *h,float *epse,float *epsi)
{
    *i_max=51;
    *j_max=26;
    *iTEL=12;
    *iLE=26;
    *iTEU=40;
    *FSMACH=1.5; //Free Stream Mach Number
    *AOA=0.0; //angle of attack
    *h=0.0; //[m]
    *N=2500; //number of iterations
    *epse=0.06; // explicit smoothing coefficient
    *epsi=0.06; // implicit smoothing coefficient
    *dt=0.00005;

    return;
}

void input_grid(float x[],float y[],int i_max,int j_max)
{
    FILE *file_x,*file_y;
    int i,j;
    file_x=fopen("GridX_for_HW2.txt","rt");
    file_y=fopen("GridY_for_HW2.txt","rt");
    for (i=0; i<i_max; i++)
        {
        for (j=0; j<j_max; j++)
            {
            if (!fscanf(file_x,"%f",&x[offset_2D(i,j,i_max)]))
                {
                    printf("error reading x file in [%d,%d]",i,j);
                    return;
                }
            if (!fscanf(file_y,"%f",&y[offset_2D(i,j,i_max)]))
                {
                    printf("error reading y file in [%d,%d]",i,j);
                    return;
                }
            }
        }
    fclose(file_x);
    fclose(file_y);
    return;
}

void calc_diffs(float A[],int i_max,int j_max,float A_eta[],float A_csi[])
/*from projec 1*/
{
    int i,j;
    /*A_eta*/
    for(i=0;i<i_max;i++)
    {
        A_eta[offset_2D(i,0,i_max)]=0.5*(4*A[offset_2D(i,1,i_max)]-3*A[offset_2D(i,0,i_max)]-A[offset_2D(i,2,i_max)]);/*j=0*/
        for(j=1;j<j_max-1;j++)/*j=1....j_max-2*/
        {
            A_eta[offset_2D(i,j,i_max)]=(A[offset_2D(i,j+1,i_max)]-A[offset_2D(i,j-1,i_max)])/2.0;
        }
        A_eta[offset_2D(i,j_max-1,i_max)]=0.5*(3*A[offset_2D(i,j_max-1,i_max)]-4*A[offset_2D(i,j_max-2,i_max)]+A[offset_2D(i,j_max-3,i_max)]);/*j=j_max-1*/
    }
    /*A_csi*/
    for(j=0;j<j_max;j++)
    {
        A_csi[offset_2D(0,j,i_max)]=0.5*(4*A[offset_2D(1,j,i_max)]-3*A[offset_2D(0,j,i_max)]-A[offset_2D(2,j,i_max)]);/*i=0*/
        for(i=1;i<i_max-1;i++)/*i=1....i_max-2*/
        {
            A_csi[offset_2D(i,j,i_max)]=(A[offset_2D(i+1,j,i_max)]-A[offset_2D(i-1,j,i_max)])/2.0;
        }
        A_csi[offset_2D(i_max-1,j,i_max)]=0.5*(3*A[offset_2D(i_max-1,j,i_max)]-4*A[offset_2D(i_max-2,j,i_max)]+A[offset_2D(i_max-3,j,i_max)]);/*i=i_max-1*/
    }
}

void metrics(float J[],float x[],float y[],float csi_x[],float csi_y[],float eta_x[],float eta_y[],int i_max,int j_max)
{
    int i,j,i_j;
    float *x_eta,*x_csi,*y_eta,*y_csi;
    y_eta=(float *)malloc(i_max*j_max* sizeof(float));
    x_eta=(float *)malloc(i_max*j_max* sizeof(float));
    y_csi=(float *)malloc(i_max*j_max* sizeof(float));
    x_csi=(float *)malloc(i_max*j_max* sizeof(float));
    calc_diffs(x,i_max,j_max,x_eta,x_csi);
    calc_diffs(y,i_max,j_max,y_eta,y_csi);
     for(i=0;i<i_max;i++)
    {
        for(j=0;j<j_max;j++)
        {
            i_j=offset_2D(i,j,i_max);
            J[i_j]=1.0/(x_csi[i_j]*y_eta[i_j]-x_eta[i_j]*y_csi[i_j]);
            csi_x[i_j]=J[i_j]*y_eta[i_j];
            csi_y[i_j]=-J[i_j]*x_eta[i_j];
            eta_x[i_j]=-J[i_j]*y_csi[i_j];
            eta_y[i_j]=J[i_j]*x_csi[i_j];
        }
    }
}

void Init(float  Q[],int i_max,int j_max,float AOA,float FSMACH,float h)
{
    /* Initializing the Q vector to free stream (FS) conditions*/
    float T,a_fs,u_fs,v_fs,p_fs,rho_fs,e_fs;
    int i,j;
    /* fs = free stream*/
    T=T_sl-L*h;/*temp [k]*/
    a_fs=sqrt(GAMMA*R*T);/* speed of sound of free stream*/
    u_fs=FSMACH*a_fs*cos(AOA*PI/180.0);
    v_fs=FSMACH*a_fs*sin(AOA*PI/180.0);
    p_fs=p_sl*pow ((1.0-L*h/T_sl),g/(L*R));
    rho_fs=p_fs/(R*T);
    e_fs=p_fs/(GAMMA - 1)+0.5*rho_fs*pow(FSMACH*a_fs,2.0);

    for (i=0; i<i_max; i++)
    {
        for (j=0; j<j_max; j++)
        {
            Q[offset_3D(i,j,0,i_max,j_max)] = rho_fs;
            Q[offset_3D(i,j,1,i_max,j_max)] = rho_fs*u_fs;
            Q[offset_3D(i,j,2,i_max,j_max)] = rho_fs*v_fs;
            Q[offset_3D(i,j,3,i_max,j_max)] = e_fs;
        }
    }
}

void BC(float  Q[],float csi_x[],float csi_y[],float eta_x[],float eta_y[],int i_max,int j_max,int iTEL,int iTEU)
{
    int i,j,n;
    float u,v,U,V,det,u_0,v_0,rho,p_0,en_0,en_TE;
    float TEL[4],TEU[4],TE[4];
    for (i=iTEL-1;i<iTEU;i++)/*adiabatic wall for the airfoil*/
    {
        u=Q[offset_3D(i,1,1,i_max,j_max)]/Q[offset_3D(i,1,0,i_max,j_max)];
        v=Q[offset_3D(i,1,2,i_max,j_max)]/Q[offset_3D(i,1,0,i_max,j_max)];

        U=csi_x[offset_2D(i,1,i_max)]*u+csi_y[offset_2D(i,1,i_max)]*v;/*contravariant velocities at j=1*/
        V=eta_x[offset_2D(i,1,i_max)]*u+eta_y[offset_2D(i,1,i_max)]*v;/*contravariant velocities at j=1*/

        det=csi_x[offset_2D(i,0,i_max)]*eta_y[offset_2D(i,0,i_max)]-eta_x[offset_2D(i,0,i_max)]*csi_y[offset_2D(i,0,i_max)];
        u_0=eta_y[offset_2D(i,0,i_max)]*U/det;
        v_0=-eta_x[offset_2D(i,0,i_max)]*U/det;
        rho=Q[offset_3D(i,1,0,i_max,j_max)];
        p_0=(GAMMA-1)*(Q[offset_3D(i,1,3,i_max,j_max)]-0.5*rho*(u*u+v*v));
        en_0=p_0/( GAMMA - 1 )+0.5*rho*(u_0*u_0+v_0*v_0);
        for (n=0;n<4;n++)
        {
            Q[offset_3D(i,0,n,i_max,j_max)]=(n==0)*rho+(n==1)*rho*u_0+(n==2)*rho*v_0+(n==3)*en_0;
            /*seve solution of TE fo TE BC*/
            if (i==(iTEL-1))
            {
               TEL[n]=(n==0)*rho+(n==1)*u_0+(n==2)*v_0+(n==3)*(GAMMA-1)*(en_0-0.5*rho*(u_0*u_0+v_0*v_0));
            }
            if (i==(iTEU-1))
            {
               TEU[n]=(n==0)*rho+(n==1)*u_0+(n==2)*v_0+(n==3)*(GAMMA-1)*(en_0-0.5*rho*(u_0*u_0+v_0*v_0));
            }
        }
    }
    /* TE BC */
    for (n=0;n<4;n++)
    {
        TE[n]=0.5*(TEL[n]+TEU[n]);
    }
    en_TE=TE[3]/(GAMMA-1)+0.5*TE[0]*(TE[1]*TE[1]+TE[2]*TE[2]);
    Q[offset_3D(iTEU-1,0,0,i_max,j_max)]=TE[0];
    Q[offset_3D(iTEU-1,0,1,i_max,j_max)]=TE[0]*TE[1];
    Q[offset_3D(iTEU-1,0,2,i_max,j_max)]=TE[0]*TE[2];
    Q[offset_3D(iTEU-1,0,3,i_max,j_max)]=en_TE;
    for (n=0;n<4;n++)
    {
        Q[offset_3D(iTEL-1,0,n,i_max,j_max)]=Q[offset_3D(iTEU-1,0,n,i_max,j_max)];
    }
    /*cut type BC*/
    for (i=1; i<iTEL-1; i++)
    {
       for (n=0;n<4;n++)
       {
           Q[offset_3D(i,0,n,i_max,j_max)]=0.5*(Q[offset_3D(i,1,n,i_max,j_max)]+Q[offset_3D(i_max-1-i,1,n,i_max,j_max)]);
           /*mirror*/
           Q[offset_3D(i_max-1-i,0,n,i_max,j_max)]=Q[offset_3D(i,0,n,i_max,j_max)];
       }
    }
    /*outlet BC*/
    for (j=1;j<j_max;j++)
    {
        for (n=0;n<4;n++)
        {
            Q[offset_3D(0,j,n,i_max,j_max)]=Q[offset_3D(1,j,n,i_max,j_max)];
            Q[offset_3D(i_max-1,j,n,i_max,j_max)]=Q[offset_3D(i_max-2,j,n,i_max,j_max)];
        }
    }

}

void RHS(float S[],float  E_hat[],float  F_hat[],float dt,int i_max,int j_max)
{
    /*clac RHS without smoothing*/
    int i,j,n;
    for (i=1;i<i_max-1;i++)
    {
        for (j=1;j<j_max-1;j++)
        {
            for (n=0;n<4;n++)
            {
                S[offset_3D(i,j,n,i_max,j_max)]=-0.5*dt*(E_hat[offset_3D(i+1,j,n,i_max,j_max)]-E_hat[offset_3D(i-1,j,n,i_max,j_max)]);
                S[offset_3D(i,j,n,i_max,j_max)] += -0.5*dt*(F_hat[offset_3D(i,j+1,n,i_max,j_max)]-F_hat[offset_3D(i,j-1,n,i_max,j_max)]);
            }
        }
    }
}

void LHS_SWEEP1(float A[],float B[],float C[],float Q[],float csi_x[],float csi_y[],float dt,int i_max,int j_max,int j)
{
    int i,k,n,ij;
    float g1,g2,kxa,kya,va,ua,phi2a,teta_a,beta_a,kxc,kyc,vc,uc,phi2c,teta_c,beta_c;

    g1=GAMMA-1.0;/*g1=gamma1*/
    g2=GAMMA-2.0;/*g2=gamma2*/
    for (i=1;i<i_max-1;i++)
    {
        /* _a= _(i-1)*/
        kxa=csi_x[offset_2D(i-1,j,i_max)];
        kya=csi_y[offset_2D(i-1,j,i_max)];
        ua=Q[offset_3D(i-1,j,1,i_max,j_max)]/Q[offset_3D(i-1,j,0,i_max,j_max)];
        va=Q[offset_3D(i-1,j,2,i_max,j_max)]/Q[offset_3D(i-1,j,0,i_max,j_max)];
        phi2a=0.5*(GAMMA-1)*(ua*ua+va*va);
        teta_a= kxa*ua + kya*va;
        beta_a=GAMMA*Q[offset_3D(i-1,j,3,i_max,j_max)]/Q[offset_3D(i-1,j,0,i_max,j_max)]-phi2a;
        /* _c= _(i+1)*/
        kxc=csi_x[offset_2D(i+1,j,i_max)];
        kyc=csi_y[offset_2D(i+1,j,i_max)];
        uc=Q[offset_3D(i+1,j,1,i_max,j_max)]/Q[offset_3D(i+1,j,0,i_max,j_max)];
        vc=Q[offset_3D(i+1,j,2,i_max,j_max)]/Q[offset_3D(i+1,j,0,i_max,j_max)];
        phi2c=0.5*(GAMMA-1)*(uc*uc+vc*vc);
        teta_c= kxc*uc + kyc*vc;
        beta_c=GAMMA*Q[offset_3D(i+1,j,3,i_max,j_max)]/Q[offset_3D(i+1,j,0,i_max,j_max)]-phi2c;
        for (k=0;k<4;k++)
        {
            for (n=0;n<4;n++)
            {
                ij=i+i_max*(k+4*n);
                A[ij]  = -0.5*dt*((n==1 && k==0)*(kxa)+(n==2 && k==0)*(kya));
                A[ij] += -0.5*dt*((n==0 && k==1)*(kxa*phi2a-ua*teta_a)+(n==1 && k==1)*(teta_a-kxa*g2*ua)+(n==2 && k==1)*(kya*ua-g1*kxa*va)+(n==3 && k==1)*(kxa*g1));
                A[ij] += -0.5*dt*((n==0 && k==2)*(kya*phi2a-va*teta_a)+(n==1 && k==2)*(kxa*va-kya*g1*ua)+(n==2 && k==2)*(teta_a-kya*g2*va)+(n==3 && k==2)*(kya*g1));
                A[ij] += -0.5*dt*((n==0 && k==3)*(teta_a*(phi2a-beta_a))+(n==1 && k==3)*(kxa*beta_a-g1*ua*teta_a)+(n==2 && k==3)*(kya*beta_a-g1*va*teta_a)+(n==3 && k==3)*(GAMMA*teta_a));
                B[ij]  =(n==k);
                C[ij]  = 0.5*dt*((n==1 && k==0)*(kxc)+(n==2 && k==0)*(kyc));
                C[ij] += 0.5*dt*((n==0 && k==1)*(kxc*phi2c-uc*teta_c)+(n==1 && k==1)*(teta_c-kxc*g2*uc)+(n==2 && k==1)*(kyc*uc-g1*kxc*vc)+(n==3 && k==1)*(kxc*g1));
                C[ij] += 0.5*dt*((n==0 && k==2)*(kyc*phi2c-vc*teta_c)+(n==1 && k==2)*(kxc*vc-kyc*g1*uc)+(n==2 && k==2)*(teta_c-kyc*g2*vc)+(n==3 && k==2)*(kyc*g1));
                C[ij] += 0.5*dt*((n==0 && k==3)*(teta_c*(phi2c-beta_c))+(n==1 && k==3)*(kxc*beta_c-g1*uc*teta_c)+(n==2 && k==3)*(kyc*beta_c-g1*vc*teta_c)+(n==3 && k==3)*(GAMMA*teta_c));

            }
        }
    }
}

void LHS_SWEEP2(float A[],float B[],float C[],float Q[],float eta_x[],float eta_y[],float dt,int i_max,int j_max,int i)
{
    int j,k,n,ij;
    float g1,g2,kxa,kya,va,ua,phi2a,teta_a,beta_a,kxc,kyc,vc,uc,phi2c,teta_c,beta_c;

    g1=GAMMA-1.0;/*g1=gamma1*/
    g2=GAMMA-2.0;/*g2=gamma2*/
    for (j=1;j<j_max-1;j++)
    {
        /* _a= _(j-1)*/
        kxa=eta_x[offset_2D(i,j-1,i_max)];
        kya=eta_y[offset_2D(i,j-1,i_max)];
        ua=Q[offset_3D(i,j-1,1,i_max,j_max)]/Q[offset_3D(i,j-1,0,i_max,j_max)];
        va=Q[offset_3D(i,j-1,2,i_max,j_max)]/Q[offset_3D(i,j-1,0,i_max,j_max)];
        phi2a=0.5*(GAMMA-1)*(ua*ua+va*va);
        teta_a= kxa*ua + kya*va;
        beta_a=GAMMA*Q[offset_3D(i,j-1,3,i_max,j_max)]/Q[offset_3D(i,j-1,0,i_max,j_max)]-phi2a;
        /* _c= _(j+1)*/
        kxc=eta_x[offset_2D(i,j+1,i_max)];
        kyc=eta_y[offset_2D(i,j+1,i_max)];
        uc=Q[offset_3D(i,j+1,1,i_max,j_max)]/Q[offset_3D(i,j+1,0,i_max,j_max)];
        vc=Q[offset_3D(i,j+1,2,i_max,j_max)]/Q[offset_3D(i,j+1,0,i_max,j_max)];
        phi2c=0.5*(GAMMA-1)*(uc*uc+vc*vc);
        teta_c= kxc*uc + kyc*vc;
        beta_c=GAMMA*Q[offset_3D(i,j+1,3,i_max,j_max)]/Q[offset_3D(i,j+1,0,i_max,j_max)]-phi2c;
        for (k=0;k<4;k++)
        {
            for (n=0;n<4;n++)
            {
                ij=j+j_max*(k+4*n);
                A[ij]  = -0.5*dt*((n==1 && k==0)*(kxa)+(n==2 && k==0)*(kya));
                A[ij] += -0.5*dt*((n==0 && k==1)*(kxa*phi2a-ua*teta_a)+(n==1 && k==1)*(teta_a-kxa*g2*ua)+(n==2 && k==1)*(kya*ua-g1*kxa*va)+(n==3 && k==1)*(kxa*g1));
                A[ij] += -0.5*dt*((n==0 && k==2)*(kya*phi2a-va*teta_a)+(n==1 && k==2)*(kxa*va-kya*g1*ua)+(n==2 && k==2)*(teta_a-kya*g2*va)+(n==3 && k==2)*(kya*g1));
                A[ij] += -0.5*dt*((n==0 && k==3)*(teta_a*(phi2a-beta_a))+(n==1 && k==3)*(kxa*beta_a-g1*ua*teta_a)+(n==2 && k==3)*(kya*beta_a-g1*va*teta_a)+(n==3 && k==3)*(GAMMA*teta_a));
                B[ij]  =(n==k);
                C[ij]  = 0.5*dt*((n==1 && k==0)*(kxc)+(n==2 && k==0)*(kyc));
                C[ij] += 0.5*dt*((n==0 && k==1)*(kxc*phi2c-uc*teta_c)+(n==1 && k==1)*(teta_c-kxc*g2*uc)+(n==2 && k==1)*(kyc*uc-g1*kxc*vc)+(n==3 && k==1)*(kxc*g1));
                C[ij] += 0.5*dt*((n==0 && k==2)*(kyc*phi2c-vc*teta_c)+(n==1 && k==2)*(kxc*vc-kyc*g1*uc)+(n==2 && k==2)*(teta_c-kyc*g2*vc)+(n==3 && k==2)*(kyc*g1));
                C[ij] += 0.5*dt*((n==0 && k==3)*(teta_c*(phi2c-beta_c))+(n==1 && k==3)*(kxc*beta_c-g1*uc*teta_c)+(n==2 && k==3)*(kyc*beta_c-g1*vc*teta_c)+(n==3 && k==3)*(GAMMA*teta_c));

            }
        }
    }
}

void calc_E_F_hat(float Q[],float J[],float csi_x[],float csi_y[],float eta_x[],float eta_y[],float E_hat[],float F_hat[],int i_max,int j_max)
{
    int i,j;
    float rho,u,v,en,J1,p,U,V;
    for (i=0;i<i_max;i++)
    {
        for (j=0;j<j_max;j++)
        {
            rho=Q[offset_3D(i,j,0,i_max,j_max)];
            u=Q[offset_3D(i,j,1,i_max,j_max)]/rho;
            v=Q[offset_3D(i,j,2,i_max,j_max)]/rho;
            en=Q[offset_3D(i,j,3,i_max,j_max)];
            J1=J[offset_2D(i,j,i_max)];
            p=(GAMMA-1)*(en-0.5*rho*(u*u+v*v));
            U=csi_x[offset_2D(i,j,i_max)]*u+csi_y[offset_2D(i,j,i_max)]*v;/*contravariant velocities*/
            V=eta_x[offset_2D(i,j,i_max)]*u+eta_y[offset_2D(i,j,i_max)]*v;/*contravariant velocities*/
            /*E_hat clac*/
            E_hat[offset_3D(i,j,0,i_max,j_max)]=rho*U/J1;
            E_hat[offset_3D(i,j,1,i_max,j_max)]=(rho*u*U+csi_x[offset_2D(i,j,i_max)]*p)/J1;
            E_hat[offset_3D(i,j,2,i_max,j_max)]=(rho*v*U+csi_y[offset_2D(i,j,i_max)]*p)/J1;
            E_hat[offset_3D(i,j,3,i_max,j_max)]=(en+p)*U/J1;
            /*F_hat clac*/
            F_hat[offset_3D(i,j,0,i_max,j_max)]=rho*V/J1;
            F_hat[offset_3D(i,j,1,i_max,j_max)]=(rho*u*V+eta_x[offset_2D(i,j,i_max)]*p)/J1;
            F_hat[offset_3D(i,j,2,i_max,j_max)]=(rho*v*V+eta_y[offset_2D(i,j,i_max)]*p)/J1;
            F_hat[offset_3D(i,j,3,i_max,j_max)]=(en+p)*V/J1;
        }
    }
}

float L2norm(float q[],int i_max,int j_max,float h)
{
    float T,a_fs,rho_fs,s,p_fs;
    int i,j,n;
    /*normalizing dQ vector*/
    T=T_sl-L*h;/*temp [k]*/
    a_fs=sqrt(GAMMA*R*T);/* speed of sound of free stream*/
    p_fs=p_sl*pow ((1.0-L*h/T_sl),g/(L*R));
    rho_fs=p_fs/(R*T);
    for (i=0;i<i_max;i++)
    {
        for(j=0;j<j_max;j++)
        {
            q[offset_3D(i,j,0,i_max,j_max)]=q[offset_3D(i,j,0,i_max,j_max)]/rho_fs;
            q[offset_3D(i,j,1,i_max,j_max)]=q[offset_3D(i,j,1,i_max,j_max)]/(rho_fs*a_fs);
            q[offset_3D(i,j,2,i_max,j_max)]=q[offset_3D(i,j,2,i_max,j_max)]/(rho_fs*a_fs);
            q[offset_3D(i,j,3,i_max,j_max)]=q[offset_3D(i,j,3,i_max,j_max)]/(rho_fs*a_fs*a_fs);
        }
    }
    s=0;
    for (n=0;n<4;n++)
    {
        for (i=0;i<i_max;i++)
        {
            for(j=0;j<j_max;j++)
            {
                s += pow(q[offset_3D(i,j,n,i_max,j_max)],2.0);
            }
        }
    }
    return sqrt(s);
}

float step(float Q[],float J[],float csi_x[],float csi_y[],float eta_x[],float eta_y[],float dt,int i_max,int j_max,float epse,float epsi, float FSMACH,float h)
{
    float *F_hat,*E_hat,*S,*s2,*rspec,*qv,*dd,*A,*B,*C,*drr,*drp,*s,*dQ,temp;
    int j,i,n,kd,ks,ke;
    E_hat=(float *)malloc(4*i_max*j_max*sizeof(float));
    F_hat=(float *)malloc(4*i_max*j_max*sizeof(float));
    S=(float *)malloc(4*i_max*j_max*sizeof(float));
    dQ=(float *)malloc(4*i_max*j_max*sizeof(float));
    s=(float *)malloc(4*i_max*sizeof(float));
    A=(float *)malloc(4*4*i_max*sizeof(float));
    B=(float *)malloc(4*4*i_max*sizeof(float));
    C=(float *)malloc(4*4*i_max*sizeof(float));
    /******************************/
    s2=(float *)malloc(i_max*sizeof(float));
    rspec=(float *)malloc(i_max*sizeof(float));
    qv=(float *)malloc(i_max*sizeof(float));
    dd=(float *)malloc(i_max*sizeof(float));
    drr=(float *)malloc(i_max*sizeof(float));
    drp=(float *)malloc(i_max*sizeof(float));
    /*****************************/
    calc_E_F_hat(Q,J,csi_x,csi_y,eta_x,eta_y,E_hat,F_hat,i_max,j_max);
    RHS(S,E_hat,F_hat,dt,i_max,j_max);
    smooth(Q,S,J,csi_x,csi_y,eta_x,eta_y,i_max,j_max,s2,rspec,qv,dd,epse,GAMMA,FSMACH,dt);
    /*sweep 1 (j=const)*/
    ks    = 1;
    kd    = i_max;
    ke    = i_max - 2;
    for (j=1;j<j_max-1;j++)
    {
        LHS_SWEEP1(A,B,C,Q,csi_x,csi_y,dt,i_max,j_max,j);
        smoothx(Q,csi_x,csi_y,i_max,j_max,A,B,C,j,J,drr,drp,rspec,qv,dd,epsi,GAMMA,FSMACH,dt);

        for (i=0;i<i_max;i++)/*copy to a work array*/
        {
            for (n=0;n<4;n++)
            {
                s[n*i_max+i]=S[offset_3D(i,j,n,i_max,j_max)];
            }

        }
        btri4s(A,B,C,s,kd,ks,ke);/*solve tri-diagonal block system*/
        for (n=0;n<4;n++) /*save solution back into RHS matrix*/
        {
            for (i=0;i<i_max;i++)
            {
                S[offset_3D(i,j,n,i_max,j_max)]=s[n*i_max+i];
            }
        }
    }
    /*sweep 2 (i=const)*/
    kd    = j_max;
    ke    = j_max - 2;
    for (i=1;i<i_max-1;i++)
    {
        LHS_SWEEP2(A,B,C,Q,eta_x,eta_y,dt,i_max,j_max,i);
        smoothy(Q,eta_x,eta_y,i_max,j_max,A,B,C,i,J,drr,drp,rspec,qv,dd,epsi,GAMMA,FSMACH,dt);
        for (j=0;j<j_max;j++)/*copy to a work array*/
        {
            for (n=0;n<4;n++)
            {
                s[n*j_max+j]=S[offset_3D(i,j,n,i_max,j_max)];
            }

        }
        btri4s(A,B,C,s,kd,ks,ke);
        for (n=0;n<4;n++) /*save solution back into RHS matrix*/
        {
            for (j=0;j<j_max;j++)
            {
                S[offset_3D(i,j,n,i_max,j_max)]=s[n*j_max+j];
            }
        }
    }
    /*update the solution*/
    for (n=0;n<4;n++)
    {
        for (i=1;i<i_max-1;i++)
        {
            for (j=1;j<j_max-1;j++)
            {
                dQ[offset_3D(i,j,n,i_max,j_max)]=S[offset_3D(i,j,n,i_max,j_max)]*J[offset_2D(i,j,i_max)];
                Q[offset_3D(i,j,n,i_max,j_max)] += dQ[offset_3D(i,j,n,i_max,j_max)];
            }
        }
    }
    temp=L2norm(dQ,i_max,j_max,h);
    free(S);
    free(dQ);
    free(s);
    free(A);
    free(B);
    free(C);
    free(s2);
    free(rspec);
    free(qv);
    free(dd);
    free(drr);
    free(drp);
    return temp;
}

void solve(float Q[],float J[],float csi_x[],float csi_y[],float eta_x[], float eta_y[],float dt,int i_max,int j_max,float epse,float epsi,float FSMACH,float h,int N,float Rn[])
{
    int i;
    float L0;
    for (i=0;i<N;i++)
    {
       Rn[i]= step(Q,J,csi_x,csi_y,eta_x,eta_y,dt,i_max,j_max,epse,epsi,FSMACH,h);
       //printf("%f ",Rn[i]);
       BC(Q,csi_x,csi_y,eta_x,eta_y,i_max,j_max,12,40);
    }
    L0=Rn[0];
    for (i=0;i<N;i++)
    {
        Rn[i]=Rn[i]/L0;
    }
}


/*temp*/

void output(float x[],float y[],FILE *grid_file,int i_max,int j_max,float Rn[],FILE *Rn_file,int N,float Q[],FILE *q_file,float FSMACH,float AOA)
{

    int i,j,n;
    FILE *q1,*q2,*q3,*q4;
  q1=fopen("q1.txt","wt");
    q2=fopen("q2.txt","wt");
    q3=fopen("q3.txt","wt");
    q4=fopen("q4.txt","wt");
    for (i=0;i<i_max;i++)
    {
        for (j=0;j<j_max;j++)
        {
            fprintf(q1,"%f ",Q[offset_3D(i,j,0,i_max,j_max)]);
            fprintf(q2,"%f ",Q[offset_3D(i,j,1,i_max,j_max)]);
            fprintf(q3,"%f ",Q[offset_3D(i,j,2,i_max,j_max)]);
            fprintf(q4,"%f ",Q[offset_3D(i,j,3,i_max,j_max)]);
        }
        fprintf(q1,"\n");
        fprintf(q2,"\n");
        fprintf(q3,"\n");
        fprintf(q4,"\n");
    }

    write_q(i_max,j_max,Q,q_file,FSMACH,AOA,1.0,1.0);
    write_grid(i_max,j_max,x,y,grid_file);

    for (n=0;n<N;n++)
    {
       fprintf(Rn_file,"%f ",Rn[n]);
    }
    return;
}

void EXIT(float y[],float x[],float J[],float csi_x[],float csi_y[],float eta_x[], float eta_y[],float Rn[],float Q[],FILE *grid_file ,FILE *q_file)
{
    fclose(q_file);
    fclose(grid_file);
    free(x);
    free(y);
    free(J);
    free(csi_x);
    free(csi_y);
    free(eta_x);
    free(eta_y);
    free(Rn);
    free(Q);
}

int main()
{
    int i_max,j_max,iTEL,iLE,iTEU,N;
    float *x,*y,*Q,*J,*Rn,*csi_x,*csi_y,*eta_x,*eta_y;
    float AOA,h,FSMACH,epse,epsi,dt;
    FILE *grid_file,*Rn_file,*q_file;/*temp*/

    input(&i_max,&j_max,&iTEL,&iLE,&iTEU,&N,&dt,&FSMACH,&AOA,&h,&epse,&epsi);

    y=(float *)malloc(i_max*j_max* sizeof(float));
    x=(float *)malloc(i_max*j_max* sizeof(float));
    J=(float *)malloc(i_max*j_max* sizeof(float));
    csi_x=(float *)malloc(i_max*j_max* sizeof(float));
    csi_y=(float *)malloc(i_max*j_max* sizeof(float));
    eta_x=(float *)malloc(i_max*j_max* sizeof(float));
    eta_y=(float *)malloc(i_max*j_max* sizeof(float));
    Rn=(float *)malloc(N* sizeof(float));
    Q=(float *)malloc(4*i_max*j_max* sizeof(float));
    input_grid(x,y,i_max,j_max);
    Init(Q,i_max,j_max,AOA,FSMACH,h);
    metrics(J,x,y,csi_x,csi_y,eta_x,eta_y,i_max,j_max);
    BC(Q,csi_x,csi_y,eta_x,eta_y,i_max,j_max,iTEL,iTEU);
    solve(Q,J,csi_x,csi_y,eta_x,eta_y,dt,i_max,j_max,epse,epsi,FSMACH,h,N,Rn);
    /*temp*/
    grid_file=fopen("grid.txt","wb");
    Rn_file=fopen("R.txt","wt");
    q_file=fopen("Q.txt","wb");
    output(x,y,grid_file,i_max,j_max,Rn,Rn_file,N,Q,q_file,FSMACH,AOA);
    EXIT(y,x,J,csi_x,csi_y,eta_x,eta_y,Rn,Q,grid_file,q_file);
    return 0;
}
