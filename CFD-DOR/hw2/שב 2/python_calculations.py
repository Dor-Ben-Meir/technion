


i_max=51
j_max=26
iTEL=12
iLE=26
iTEU=40


# index offset for 3D arrays:
def offset_3D(i,  j,  n,  imax,  jmax):


    return ( ( n*jmax + j )*imax + i )
	
	
ij = []
A = []
#for i in range(1,i_max - 1):

#i = 4	
#A_00 = offset_3D(0,0,0,i_max,4)  + i
#A_01 = offset_3D(0,0,1,i_max,4)  + i       
#A_10 = offset_3D(0,1,0,i_max,4)  + i  
#A_11 = offset_3D(0,1,1,i_max,4)  + i
#A_20 = offset_3D(0,2,0,i_max,4)  + i  
#A_21 = offset_3D(0,2,1,i_max,4)  + i
#A_30 = offset_3D(0,3,0,i_max,4)  + i 
#A_31 = offset_3D(0,3,1,i_max,4)  + i
#
#A_02 = offset_3D(0,0,2,i_max,4)  + i  
#A_03 = offset_3D(0,0,3,i_max,4)  + i       
#A_12 = offset_3D(0,1,2,i_max,4)  + i  
#A_13 = offset_3D(0,1,3,i_max,4)  + i
#A_22 = offset_3D(0,2,2,i_max,4)  + i  
#A_23 = offset_3D(0,2,3,i_max,4)  + i
#A_32 = offset_3D(0,3,2,i_max,4)  + i  
#A_33 = offset_3D(0,3,3,i_max,4)  + i
#
#
#A.append(A_00)
#A.append(A_01)
#A.append(A_02)
#A.append(A_03)
#A.append(A_10)
#A.append(A_11)
#A.append(A_12)
#A.append(A_13)
#A.append(A_20)
#A.append(A_21)
#A.append(A_22)
#A.append(A_23)
#A.append(A_30)
#A.append(A_31)
#A.append(A_32)
#A.append(A_33)

j = 1

#A_00 = offset_3D(0,0,0,j_max,4) + j
#A_01 = offset_3D(0,0,1,j_max,4) + j       
#A_10 = offset_3D(1,0,0,j_max,4) + j  
#A_11 = offset_3D(1,0,1,j_max,4) + j
#A_20 = offset_3D(2,0,0,j_max,4) + j  
#A_21 = offset_3D(2,0,1,j_max,4) + j
#A_30 = offset_3D(3,0,0,j_max,4) + j 
#A_31 = offset_3D(3,0,1,j_max,4) + j
#
#A_02 = offset_3D(0,0,2,j_max,4) + j  
#A_03 = offset_3D(0,0,3,j_max,4) + j       
#A_12 = offset_3D(1,0,2,j_max,4) + j  
#A_13 = offset_3D(1,0,3,j_max,4) + j
#A_22 = offset_3D(2,0,2,j_max,4) + j  
#A_23 = offset_3D(2,0,3,j_max,4) + j
#A_32 = offset_3D(3,0,2,j_max,4) + j  
#A_33 = offset_3D(3,0,3,j_max,4) + j


A_00 = offset_3D(0,0,0,j_max,4)  + j
A_01 = offset_3D(0,0,1,j_max,4)  + j      
A_10 = offset_3D(0,1,0,j_max,4)  + j  
A_11 = offset_3D(0,1,1,j_max,4)  + j
A_20 = offset_3D(0,2,0,j_max,4)  + j  
A_21 = offset_3D(0,2,1,j_max,4)  + j
A_30 = offset_3D(0,3,0,j_max,4)  + j 
A_31 = offset_3D(0,3,1,j_max,4)  + j

A_02 = offset_3D(0,0,2,j_max,4)  + j  
A_03 = offset_3D(0,0,3,j_max,4)  + j       
A_12 = offset_3D(0,1,2,j_max,4)  + j  
A_13 = offset_3D(0,1,3,j_max,4)  + j
A_22 = offset_3D(0,2,2,j_max,4)  + j  
A_23 = offset_3D(0,2,3,j_max,4)  + j
A_32 = offset_3D(0,3,2,j_max,4)  + j  
A_33 = offset_3D(0,3,3,j_max,4)  + j

A.append(A_00)
A.append(A_01)
A.append(A_02)
A.append(A_03)
A.append(A_10)
A.append(A_11)
A.append(A_12)
A.append(A_13)
A.append(A_20)
A.append(A_21)
A.append(A_22)
A.append(A_23)
A.append(A_30)
A.append(A_31)
A.append(A_32)
A.append(A_33)





for k in range(4):
	for n in range(4):
			   
		ij.append(j+j_max*(k+4*n))