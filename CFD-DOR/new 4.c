#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846


int imax, jmax, iTEL, iLE, iTEU, m, n, i, j, it, max_ij, num_iter, cntrl_scheme;
double t, Delta_y, Delta_x, XSF, YSF,  r, w, conv;

// calculating max(i, j)
int max_size(int i, int j)
{
    return i > j ? i : j ; // If Condition is true ? then value i : otherwise value j
}

//******************************************************************************************************




// This function enter the next values: t, i-max j-max, iTEL, iLE,  iTEU, Delta_y, XSF, YSF, r, w, convergence criteria, number of iterations,  control functions status.

int input(void)
{
    imax = 51, jmax = 26, iTEL = 12, iLE = 26, iTEU = 40;
	t = 0.12, Delta_y = 0.02, Delta_x, XSF = 1.15, YSF = 1.15;
    m = imax, n = jmax;
	max_ij = max_size(imax, jmax);
    //printf("%d\n",imax);
    return 0;
}



//******************************************************************************************************

// index offset for arrays
int offset(int i, int j, int m)
{
    return (i + j*m);
}

//******************************************************************************************************




// linear interpolation along eta or xi coordinate


// linear interpolation along  eta direction:
void interp_eta(double *vec)
{
    int i,j;
    for (i = 1; i < imax-1; i++)
    {
        for (j = 0; j < jmax; j++)
        {
            vec[ offset(i, j, m) ] = j*vec[ offset(i,jmax-1,m) ] + (jmax-1-j)*vec[ offset(i, 0, m) ];
			vec[ offset(i, j, m) ] /= (jmax-1);
        }
    }
    return;
}




// linear interpolation along xi direction:
void interp_xi(double *vec)
{

    int i,j;
    for (j = 1; j < jmax-1; j++)
    {
        for (i = 0; i < imax; i++)
        {
            vec[ offset( i, j, m ) ] = i * vec[ offset( imax-1, j, m ) ] + (imax-1-i) * vec[ offset(0,j,m) ];
            vec[ offset( i, j, m ) ] /= (imax-1);
        }
    }
    return;
}


//******************************************************************************************************



// absolute value
double absolute(double a)
{
    return a < 0 ? -a : a; // If Condition is true ? then value -a : otherwise value a
}






//******************************************************************************************************
//******************************************************************************************************
//******************************************************************************************************






// Profile coordinates

double YpX(double x)
{
    double arg1 = 1 * x;
    double arg2 = arg1 * arg1;
    double arg3 = arg2 * arg1;
    double arg4 = arg2 * arg2;

    return ( 5 * t * (0.2969 * sqrt(arg1) - 0.126 * (arg1) - 0.3516 * (arg2) + 0.2843 * (arg3) - 0.1015 * (arg4)));
}


//******************************************************************************************************
//******************************************************************************************************
//******************************************************************************************************






// Calculating Boundary Conditions for grid

void boundary(double *x, double *y)
{
    Delta_x = 1 / (double) ( iLE - iTEL );
    int ind, indm, indPrevj, indPrev2j;
    double ang;

    //printf("%d\n",iTEL);
    //printf("%f\n",imax);
    // Boundary Conditions on the NACA profile:
    for (i=iTEL-1; i < iLE; i++)
    {
        ind = offset(i, 0, m);
        //printf("%d\n",ind);
        x[ind] = 1 - cos( PI/2 * ( (iLE-1) - i) * Delta_x );
		y[ind] = -YpX( x[ind] );
		//printf("%f\n",y[ind] );

        // mirroring:
        indm = ind + 2*( ( (iLE-1) - (iTEL-1) - 1 ) - ( ind - (iTEL-1) ) ) + 2;
        //printf("%d\n",ind);
        printf("%d\n",indm);
        x[indm] = x[ind];
        y[indm] = -y[ind];
        printf("%f\n",y[indm] );
    }





    //******************************************************************************************************


	// Boundary Conditions on the  wake:

	// for the boundary j = j_min:
    for (i=iTEU; i < imax; i++)
    {
        ind = offset(i, 0, m);
        x[ind] = x[ind-1] + XSF*( x[ind-1] - x[ind-2] );
        y[ind] = 0;
    }

    for(i=0; i < iTEL-1; i++)
    {
        ind = offset(i, 0, m);
        x[ind] = x[ offset(imax-i-1, 0, m) ];
        y[ind] = 0;
    }


    //******************************************************************************************************


    //Boundary Conditions on exit boundary:

    ind = offset(imax-1, 1, m);
    y[ind] = Delta_y;
    x[ind] = x[ offset(imax-1, 0, m)];
    indm = offset(0, 1, m);
    y[indm] = -y[ind];
    x[indm] = x[ offset(0, 0, m)];
    for (j=2; j < jmax; j++)
    {
        ind = offset(imax-1, j, m);
        indPrevj = offset(imax-1, j-1, m);
        indPrev2j = offset(imax-1, j-2, m);
        y[ind] = y[indPrevj] + ( y[indPrevj] - y[indPrev2j] ) * YSF;
        x[ind] = x[ offset(imax-1, 0, m) ];
        indm = offset(0, j, m);
        y[indm] = -y[ind];
        x[indm] = x[ offset(0, 0, m) ];
    }


    //******************************************************************************************************

    //Boundary Conditions on outer boundary:

    double Ymax = y[ offset(imax-1, jmax-1, m) ];
    double Xmax = x[ offset(imax-1, 0, m) ];
    double Lout = 2 * Xmax + PI*Ymax;
    double Delta_s = Lout / (double) ( imax - 1);
    int total_Straight_Sections = floor(Xmax / Delta_s);
    int total_Round_Sections = floor((PI*Ymax/2)/Delta_s);
    int num_Straight_Sections, num_Round_Sections, iu, il;
    //printf("%f\n",Xmax );
    //printf("%f\n",Delta_s );
    //printf("%d\n",total_Straight_Sections );
    //printf("%d\n",total_Round_Sections );


    // outer boundary on the straight section
    for (num_Straight_Sections = 1, iu = imax - 2, il = 1; num_Straight_Sections <= total_Straight_Sections; num_Straight_Sections++, iu--, il++)
    {
        ind = offset(iu, jmax-1, m);
        //printf("%d\n",ind );
        x[ind] = Xmax - num_Straight_Sections*Delta_s;
        //printf("%f\n",x[ind] );
        y[ind] = Ymax;
        indm = offset(il, jmax-1, m);
        //printf("%d\n",indm );
        x[indm] = x[ind];
        y[indm] = -Ymax;
    }


    // outer boundary on the half round section:
    for (num_Round_Sections = 0; num_Round_Sections <= total_Round_Sections; num_Round_Sections++)
    {
        i = (iLE - 1) + num_Round_Sections;
        ind = offset(i, jmax-1, m);
        //printf("%d\n",ind );
        ang = num_Round_Sections * Delta_s / Ymax;
        x[ind] = -Ymax * cos(ang);
        //printf("%f\n",x[ind] );
        y[ind] = Ymax * sin(ang);

        if (num_Round_Sections > 0)
        {
            i = (iLE - 1) - num_Round_Sections;
            indm = offset(i, jmax-1, m);
            //printf("%d\n",indm );
            x[indm] = x[ind];
            y[indm] = -y[ind];
        }

    }


}




//******************************************************************************************************

void grid_initial_conditions(double *x, double *y)
{
    interp_eta(x);
    interp_eta(y);
}



//******************************************************************************************************

void initialize(double *x, double *y, double *x_initial, double *y_initial)
{
    int o;
    boundary(x,y);
    grid_initial_conditions(x, y);
    for (i = 0; i < imax; i++)
        for (j = 0; j < jmax; j++)
        {
            o = offset(i, j, m);
            x_initial[o] = x[o];
            y_initial[o] = y[o];
        }
}


//******************************************************************************************************
//******************************************************************************************************
//******************************************************************************************************

/*
 //  Derivatives calculation: if dx_dxi  or dy_dxi is calculated than dir is 1 and else
// 	 dx_deta or dy_deta are calculated.

 // calculating first derivatives.
double calc_diff(double *vec, int i, int j, int dir)
	{
    if(dir)	//xi
    {
        return ( (vec[offset(i+1, j, m)] - vec[offset(i-1, j, m)]) / 2 );
    }
    else	//eta
    {
        return ( (vec[offset(i, j+1, m)] - vec[offset(i, j-1, m)]) / 2 );
    }
}



// calculating second derivatives
double calc_2nd_diff(double *vec, int i, int j, int dir)
	{
    if(dir)	// (xi)
    {
        return (vec[offset(i+1, j, m)] - 2*vec[offset(i, j, m)] + vec[offset(i-1, j, m)]);
    }
    else	// (eta)
    {
        return (vec[offset(i, j+1, m)] - 2*vec[offset(i, j, m)] + vec[offset(i, j-1, m)]);
    }
}

//******************************************************************************************************



// These functions calculate alpha, beta and  gamma.


//  Calculate alpha.

double calc_alpha(double dx_deta, double dy_deta)
{
    return dx_deta*dx_deta + dy_deta*dy_deta;
}

//  Calculate beta.

double calc_beta(double dx_dxi, double dx_deta,double dy_dxi, double dy_deta)
{
    return dx_dxi*dx_deta + dy_dxi*dy_deta;
}

//  Calculate gamma.

double calc_gamma(double dx_dxi,double dy_dxi)
{
    return dx_dxi*dx_dxi + dy_dxi*dy_dxi;
}

//******************************************************************************************************



void fill_alpha_beta_gamma(double *alpha, double *beta, double *gamma, double *dx_dxi, double *dx_deta, double *dy_dxi, double *dy_deta)
{
    int ind;
    for (i=1; i < imax-1; i++)
        for (j=1; j < jmax-1; j++)
        {
        ind = offset(i, j, m);
        alpha[ind] = calc_alpha(dx_deta[ind], dy_deta[ind]);
        beta[ind] = calc_beta(dx_dxi[ind], dx_deta[ind], dy_dxi[ind], dy_deta[ind]);
        gamma[ind] = calc_gamma(dx_dxi[ind], dy_dxi[ind]);
        }
}


//******************************************************************************************************



void fill_phi_psi(double *x, double *y, double *phi, double *psi, double *dy_deta, double *dx_deta, double *dx_dxi, double *dy_dxi)
{
    int o, xi, eta; xi = 1;
    eta = 0;

    if ( ( cntrl_scheme == 2 ) ||( cntrl_scheme == 3 ) )
    {
        for (j=1; j < jmax-1; j++)	// xi=const
        {
        //xi = xi_min:
        o = offset(0, j, m);
        if ( absolute( dy_deta[o] ) > absolute( dx_deta[o] ) )
            psi[o] = -calc_2nd_diff(y, 0, j, eta) / dy_deta[o];
        else
            psi[o] = -calc_2nd_diff(x, 0, j, eta) / dx_deta[o];
        //xi = xi_max:
        o = offset(imax-1, j, m);
        if ( absolute( dy_deta[o] ) > absolute( dx_deta[o] ) )
            psi[o] = -calc_2nd_diff(y, imax-1, j, eta) / dy_deta[o];
        else
        psi[o] = -calc_2nd_diff(x, imax-1, j, eta) / dx_deta[o];
        }
    }

    if ((cntrl_scheme == 1) ||(cntrl_scheme == 3))
    {
        for (i=1; i < imax-1; i++)	// eta=const
        {
        //eta = eta_min:
        o = offset(i, 0, m);
        if (absolute(dx_dxi[o]) > absolute(dy_dxi[o]))
            phi[o] = -calc_2nd_diff(x, i, 0, xi) / dx_dxi[o];
        else
            phi[o] = -calc_2nd_diff(y, i, 0, xi) / dy_dxi[o];
            //eta = eta_max:
            o = offset(i, jmax-1, m);
        if (absolute(dx_dxi[o]) > absolute(dy_dxi[o]))
            phi[o] = -calc_2nd_diff(x, i, jmax-1, xi) / dx_dxi[o];
        else
            phi[o] = -calc_2nd_diff(y, i, jmax-1, xi) / dy_dxi[o];
        }
    }
    //interpolation inside domain:
    interp_xi(psi);
    interp_eta(phi);
}


//******************************************************************************************************


void metrics(double *x, double *y, double *dx_dxi, double *dx_deta, double *dy_dxi, double *dy_deta, double *alpha, double *beta, double *gamma, double *phi, double *psi)
{
    fill_diffs(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta);
    fill_alpha_beta_gamma(alpha, beta, gamma, dx_dxi, dx_deta, dy_dxi, dy_deta);
    fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi);
}


//******************************************************************************************************


// This part will form the solution of the program:

void LHS(int i, int j, int dir, double *A, double *B, double *C, double *vec)
{
    int o = offset(i, j, m);
    int ind = (dir) ? i : j;
    A[ind] = -vec[o];
    B[ind] = r + 2*vec[o];
    C[ind] = -vec[o];
}



 double L_i_j_n(int i, int j, double *alpha, double *beta, double *gamma, double *x, double *phi, double *psi)
{
    double out = alpha[offset(i, j, m)] * ((x[offset(i+1, j, m)] - 2*x[offset(i, j, m)] + x[offset(i-1, j, m)]) + 0.5*phi[offset(i, j, m)] * (x[offset(i+1, j, m)] - x[offset(i-1, j, m)]))- 0.5*beta[offset(i, j, m)] * (x[offset(i+1, j+1, m)] - x[offset(i+1, j-1, m)]- x[offset(i-1, j+1, m)]+ x[offset(i-1, j-1, m)])+ gamma[offset(i, j, m)] * (x[offset(i, j+1, m)] - 2*x[offset(i, j, m)] + x[offset(i, j-1, m)] + 0.5*psi[offset(i, j, m)] * (x[offset(i, j+1, m)] - x[offset(i, j-1, m)]));
    return out;
}




 double RHS(int i, int j, double *alpha, double *beta, double *gamma, double *x, double *phi, double *psi)
{
    return r*w*L_i_j_n(i, j, alpha, beta, gamma, x, phi, psi);
}





*/




int output(double *x, double *y, double *x_initial, double *y_initial)
{
    int ind;
    FILE *fpX = fopen("GridX.txt", "w");
    FILE *fpY = fopen("GridY.txt", "w");
    FILE *fpXi = fopen("GridXi.txt", "w");
    FILE *fpYi = fopen("GridYi.txt", "w");

    for (i = 0; i < imax; i++)
    {
        for (j = 0; j < jmax; j++)
        {
            ind = offset(i, j, m); fprintf(fpX, "%f\t\t", x[ind]);
            fprintf(fpY, "%f\t\t", y[ind]); fprintf(fpXi, "%f\t\t", x_initial[ind]); fprintf(fpYi, "%f\t\t", y_initial[ind]);
        }
        fprintf(fpX, "\n");
        fprintf(fpY, "\n");
        fprintf(fpXi, "\n");
        fprintf(fpYi, "\n");
    }
    fclose(fpX);
    fclose(fpY);
    fclose(fpXi);
    fclose(fpYi);


    return 0;
}



int main()
{

    double *x, *y, *x_initial, *y_initial;
    input();
    // memory allocation:

    int l = m * n;
    //2D:
    x =	(double *) malloc( l * sizeof(double) );
    y =	(double *) malloc( l * sizeof(double) );
    x_initial = (double *) malloc( l * sizeof(double) );
    y_initial = (double *) malloc( l * sizeof(double) );



    //printf("%d\n",imax);
    //boundary( x, y);
	initialize(x, y, x_initial, y_initial);

	output(x, y, x_initial, y_initial);

    return 0;
}

