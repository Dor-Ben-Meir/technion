import numpy as np
import math
import matplotlib.pyplot as plt

imax = 51 
jmax = 26
iTEL = 12 
iLE = 26;
iTEU = 40
t = 0.12
Delta_y = 0.02
XSF = 1.15 
YSF = 1.15
m = imax
n = jmax
max_ij = imax
cntrl_scheme = 0
r = 0.01
w = 1.5
Num_of_iteraion = 200

l = m * n
x = [None] * l
y = [None] * l
x_initial = [None] * l
y_initial = [None] * l
dx_dxi = [0] * l
dx_deta = [0] * l
dy_dxi = [0] * l
dy_deta = [0] * l
alpha =	[0] * l
beta = [0] * l
gamma = [0] * l
phi = [0] * l
psi = [0] * l
Sx =  [0] * l
Sy =  [0] * l
  
#A =	[None] * max_ij
#B =	[None] * max_ij
#C =	[None] * max_ij
#D =	[None] * max_ij
U =	[None] * max_ij
#Rx = [None] * max_ij
#Ry = [None] * max_ij
x_initial_2 = []
y_initial_2 = []


#******************************************************************************************************

#index offset for arrays
def offset(i, j, m):
    return (i + j*m)


#******************************************************************************************************
    
#linear interpolation along eta or xi coordinate

# linear interpolation along  eta direction:
def interp_eta(vec):


    for i in range(1,imax-1): 
    
        for j in range(jmax): 
            #index = offset(i, j, m)
            #element = j*vec[ offset(i,jmax-1,m) ] + (jmax-1-j)*vec[ offset(i, 0, m) ]
            #element = element / (jmax-1)
            #vec.insert(index,element)
            vec[ offset(i, j, m) ] = j*vec[ offset(i,jmax-1,m) ] + (jmax-1-j)*vec[ offset(i, 0, m) ]
            vec[ offset(i, j, m) ] /= (jmax-1)
    
    return vec 


# linear interpolation along xi direction:
def interp_xi(vec):
    for j in range(1,jmax-1): 
    
        for i in range(imax): 
            
            vec[ offset( i, j, m ) ] = i * vec[ offset( imax-1, j, m ) ] + (imax-1-i) * vec[ offset(0,j,m) ]
            vec[ offset( i, j, m ) ] /= (imax-1)  
            
    
   
    return vec

#******************************************************************************************************
    

# Profile coordinates

def YpX(x):

     arg1 = 1 * x
     arg2 = arg1 * arg1
     arg3 = arg2 * arg1
     arg4 = arg2 * arg2
     
     return ( 5 * t * (0.2969 * np.sqrt(arg1) - 0.126 * (arg1) - 0.3516 * (arg2) + 0.2843 * (arg3) - 0.1015 * (arg4)))

#******************************************************************************************************

# Boundary Conditions on the NACA profile:
def profile_boundary(x, y):

    Delta_x = float(1 / ( iLE - iTEL ))
    itrex = 0
	
    for i in range((iTEL-1),iLE): 
	
        index = offset(i, 0, m)
        #element_x =  1 - math.cos( (np.pi)/2 * ( (iLE-1) - i) * Delta_x )
        #element_y = -YpX( x[index] )
        #x.insert(index,element_x )
        #element_y = -YpX( element_x)
        #y.insert(index,element_y )
        x[index] = 1 - math.cos( (np.pi)/2 * ( (iLE-1) - i) * Delta_x )
        y[index] = -YpX( x[index] )
		

        # mirroring:
        index_m = (iTEU - 1)  - itrex 
        #index_m = index + 2*( ( (iLE-1) - (iTEL-1) - 1 ) - ( index - (iTEL-1) ) ) + 2;
        #x.insert(index_m,x[index] )
        #y.insert(index_m,-y[index] )
        x[index_m] = x[index]
        y[index_m] = -y[index]
        itrex  = itrex  + 1
    return(x,y)




#Boundary Conditions on the  wake:
def wake_boundary(x, y):
	
	#for the boundary j = j_min:
	for i in range(iTEU,imax): 
		
		index = offset(i, 0, m)
		x[index] = x[index-1] + ( x[index-1] - x[index-2] ) * XSF
		y[index] = 0
		#x.insert(index,element_x )
		#y.insert(index,element_y )
        
	for i in range(iTEL-1): 
		
		index = offset(i, 0, m)
		x[index] = x[ offset(imax-i-1, 0, m) ]
		y[index] = 0
		
    
	return(x,y)
    
    

#Boundary Conditions on exit boundary:
def exit_boundary(x, y):
	
	index = offset(imax-1, 1, m)
	y[index] = Delta_y
	x[index] = x[ offset(imax-1, 0, m)]
		
	# mirroring:
	index_m = offset(0, 1, m)
	y[index_m] = -y[index]
	x[index_m] = x[ offset(0, 0, m)]
		
	for j in range(2,jmax):
		
		index = offset(imax-1, j, m)
		index_j = offset(imax-1, j-1, m)
		index_2j = offset(imax-1, j-2, m)
		y[index] = y[index_j] + ( y[index_j] - y[index_2j] ) * YSF
		x[index] = x[ offset(imax-1, 0, m) ]
			
		# mirroring:
		index_m = offset(0, j, m)
		y[index_m] = -y[index]
		x[index_m] = x[ offset(0, 0, m) ]
		

	return(x,y)
	
	
	
 #Boundary Conditions on outer boundary:
def outer_boundary(x, y):
	
		Y_max = y[ offset(imax-1, jmax-1, m) ]
		X_max = x[ offset(imax-1, 0, m) ]
		Length = 2 * X_max + np.pi*Y_max
		Delta_s = float(Length /  ( imax - 1))
		Straight_Sections = math.floor(X_max / Delta_s)
		Round_Sections = math.floor((np.pi*Y_max/2)/Delta_s)
		 

    # outer boundary on the straight section:
		N_Straight_Sections = 1
		index_u = imax - 2
		index_il = 1
		
		while N_Straight_Sections <= Straight_Sections:
		    
			index = offset(index_u, jmax-1, m)
			x[index] = X_max - N_Straight_Sections * Delta_s
			y[index] = Y_max
			
			# mirroring:
			index_m = offset(index_il, jmax-1, m)
			x[index_m] = x[index];
			y[index_m] = -Y_max;
			
			N_Straight_Sections = N_Straight_Sections + 1
			index_u  = index_u  - 1
			index_il = index_il + 1

		for N_Round_Sections in range(Round_Sections + 1): 
			i = (iLE - 1) + N_Round_Sections
			index = offset(i, jmax-1, m)
			angele = N_Round_Sections * Delta_s / Y_max
			x[index] = -Y_max * math.cos(angele)
			y[index] = Y_max * math.sin(angele)
		
			# mirroring:
			if (N_Round_Sections > 0):
				i = (iLE - 1) - N_Round_Sections
				index_m = offset(i, jmax-1, m)
				x[index_m] = x[index]
				y[index_m] = -y[index]


		return(x,y)


def boundry(x,y):
	
		(x,y) = profile_boundary(x, y)
		(x,y) = wake_boundary(x, y)
		(x,y) = exit_boundary(x, y)
		(x,y) = outer_boundary(x, y)
		
		return(x,y)

#******************************************************************************************************

def initialize(x, y,x_initial, y_initial,x_initial_2, y_initial_2):

		boundry(x,y)
		x = interp_eta(x)
		y = interp_eta(y)
		for i in range(imax):
			for j in range(jmax):
				index = offset(i, j, m)
				#index_2 = offset(j, i, m)
				x_initial[index] = x[index]
				y_initial[index] = y[index]
				x_initial_2.append(x_initial[index])
				y_initial_2.append(y_initial[index])
                
			#for k in range(jmax):
				#index_2 = offset(k, i, m)
				#x_initial_2[index] = x_initial[offset(i, k, m)]
				#y_initial_2[index_2] = y_initial[offset(i, k, m)]
		return
		#return(x_initial, y_initial,x_initial_2, y_initial_2)
        
 #******************************************************************************************************

def initial_plot(x, y, x_initial, y_initial):       
    plt.plot( x_initial, y_initial)
    plt.grid(True)
    plt.show()
   
#initial_plot(x, y, x_initial, y_initial)



#******************************************************************************************************

 #  Derivatives calculation: if dx_dxi  or dy_dxi is calculated than x_eflag is 1 and else
 #dx_deta or dy_deta are calculated.

# calculating first derivatives.
def calc_diff(vec,  i,  j,  x_eflag):
	
    if(x_eflag):	#xi
    
        return ( (vec[offset(i+1, j, m)] - vec[offset(i-1, j, m)]) / 2 )
    
    else:	#eta
    
        return ( (vec[offset(i, j+1, m)] - vec[offset(i, j-1, m)]) / 2 )
    




# calculating second derivatives
def calc_2nd_diff(vec,  i,  j,  x_eflag):
	
    if(x_eflag):	#xi
   
        return (vec[offset(i+1, j, m)] - 2*vec[offset(i, j, m)] + vec[offset(i-1, j, m)])
    
    else:	#eta
    
        return (vec[offset(i, j+1, m)] - 2*vec[offset(i, j, m)] + vec[offset(i, j-1, m)])



#******************************************************************************************************

def fill_diffs(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta):

     xi = 1
     eta = 0
    
     for i in range(1, imax - 1):
         
        dx_dxi[offset( i, 0, m )] = calc_diff(x, i, 0, xi)
        dy_dxi[offset( i, 0, m )] = calc_diff(y, i, 0, xi)
        dx_dxi[offset( i, jmax - 1, m )] = calc_diff(x, i, jmax - 1, xi)
        dy_dxi[offset( i, jmax - 1, m )] = calc_diff(y, i, jmax - 1, xi)
         
        for j  in range(1, jmax -1):
            
            o = offset(i, j, m)
            dx_dxi[o] = calc_diff(x, i, j, xi)
            dx_deta[o] = calc_diff(x, i, j, eta)
            dy_dxi[o] = calc_diff(y, i, j, xi)
            dy_deta[o] = calc_diff(y, i, j, eta)
            
            if (i == 1):       
                dx_deta[offset( 0, j, m )] = calc_diff(x, 0, j, eta)
                dy_deta[offset( 0, j, m )] = calc_diff(y, 0, j, eta)
                dx_deta[offset( imax - 1, j, m )] = calc_diff(x, imax - 1, j, eta)
                dy_deta[offset( imax - 1, j, m )] = calc_diff(y, imax - 1, j, eta)



#******************************************************************************************************


	
# These functions calculate alpha, beta and  gamma.


#  Calculate alpha.

def calc_alpha( dx_deta, dy_deta):

    return dx_deta*dx_deta + dy_deta*dy_deta;


#  Calculate beta.

def calc_beta( dx_dxi,  dx_deta, dy_dxi,  dy_deta):

    return dx_dxi*dx_deta + dy_dxi*dy_deta;


# Calculate gamma.

def calc_gamma( dx_dxi, dy_dxi):

    return dx_dxi*dx_dxi + dy_dxi*dy_dxi;



#******************************************************************************************************



def fill_alpha_beta_gamma(alpha, beta, gamma, dx_dxi, dx_deta, dy_dxi, dy_deta):

    
    for i in range(1, imax -1):
        for j  in range(1, jmax -1):
        
            index = offset(i, j, m)
            alpha[index] = calc_alpha(dx_deta[index], dy_deta[index]);
            beta[index] = calc_beta(dx_dxi[index], dx_deta[index], dy_dxi[index], dy_deta[index]);
            gamma[index] = calc_gamma(dx_dxi[index], dy_dxi[index]);
        
    #return(alpha, beta, gamma)



#******************************************************************************************************	

def fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi):

    
    eta = 0
    xi = 1

    if ( (cntrl_scheme  == 2 ) or ( cntrl_scheme == 3 ) ):
	
		# xi=const
        for j  in range(1, jmax -1):	
        
			#xi = xi_min:
            o = offset(0, j, m)
            if ( abs( dy_deta[o] ) > abs( dx_deta[o] ) ):
                psi[o] = -calc_2nd_diff(y, 0, j, eta) / dy_deta[o]
            else:
                psi[o] = -calc_2nd_diff(x, 0, j, eta) / dx_deta[o]
				
			#xi = xi_max:
            o = offset(imax-1, j, m)
            if ( abs( dy_deta[o] ) > abs( dx_deta[o] ) ):
                psi[o] = -calc_2nd_diff(y, imax-1, j, eta) / dy_deta[o]
            else:
                psi[o] = -calc_2nd_diff(x, imax-1, j, eta) / dx_deta[o]
        
   

    if ((cntrl_scheme == 1) or (cntrl_scheme == 3)):
	
		# eta=const
        for i in range(1, imax -1):	
        
        #eta = eta_min:
            o = offset(i, 0, m)
            if (abs(dx_dxi[o]) > abs(dy_dxi[o])):
                phi[o] = -calc_2nd_diff(x, i, 0, xi) / dx_dxi[o]
            else:
                phi[o] = -calc_2nd_diff(y, i, 0, xi) / dy_dxi[o]
                
        #eta = eta_max:
            o = offset(i, jmax-1, m)
            if (abs(dx_dxi[o]) > abs(dy_dxi[o])):
                phi[o] = -calc_2nd_diff(x, i, jmax-1, xi) / dx_dxi[o]
            else:
                phi[o] = -calc_2nd_diff(y, i, jmax-1, xi) / dy_dxi[o]
       
   
    #interpolation 
    interp_xi(psi)
    interp_eta(phi)



#******************************************************************************************************


def fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi):

    fill_diffs(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta)
    fill_alpha_beta_gamma(alpha, beta, gamma, dx_dxi, dx_deta, dy_dxi, dy_deta)
    fill_phi_psi(x, y, phi, psi, dy_deta, dx_deta, dx_dxi, dy_dxi)	


    
#******************************************************************************************************    

    
def L_i_j_n( i,  j, alpha, beta, gamma, x_or_y, phi, psi):

	part_1 = alpha[offset(i, j, m)] * ((x_or_y[offset(i+1, j, m)] - 2*x_or_y[offset(i, j, m)] + x_or_y[offset(i-1, j, m)]) + 0.5*phi[offset(i, j, m)] * (x_or_y[offset(i+1, j, m)] - x_or_y[offset(i-1, j, m)]))
	part_2 = - 0.5*beta[offset(i, j, m)] * (x_or_y[offset(i+1, j+1, m)] - x_or_y[offset(i+1, j-1, m)]- x_or_y[offset(i-1, j+1, m)]+ x_or_y[offset(i-1, j-1, m)])
	part_3 = gamma[offset(i, j, m)] * (x_or_y[offset(i, j+1, m)] - 2*x_or_y[offset(i, j, m)] + x_or_y[offset(i, j-1, m)] + 0.5*psi[offset(i, j, m)] * (x_or_y[offset(i, j+1, m)] - x_or_y[offset(i, j-1, m)]))
	
	return part_1 + part_2 + part_3 

 

 
 
def RHS( i,  j, alpha, beta, gamma, x, phi, psi):

    return r*w*L_i_j_n(i, j, alpha, beta, gamma, x, phi, psi)  
    


    

def LHS( i, j, A, B, C,vec):

     o = offset(i, j, m)
     a = -vec[o]
     b = r + 2*vec[o]
     c = -vec[o]
     A.append(a)
     B.append(b)
     C.append(c)

#******************************************************************************************************

# Algorithm for 3-diag matrix solution
def tridiag(a, b, c, d, u, i_s,  ie):

    for  i in range(i_s + 1,ie +1):
    
        if(b[i-1] == 0.):
            return(0)
			
        beta = a[i] / b[i-1]
        b[i] = b[i] - c[i-1] * beta
        d[i] = d[i] - d[i-1] * beta
    

    u[ie] = d[ie] / b[ie]
    i = ie - 1
    while(i >=  i_s):
    
        u[i] = (d[i] - c[i] * u[i+1]) / b[i]
        i = i - 1
        
#******************************************************************************************************        



def start_SX_SY(Sx, Sy, alpha, beta, gamma, x, phi, psi):
    # In all the  of the domain:
    for  i in range(1,imax-1):
    
        for j in range(1,jmax-1):
        
            o = offset(i, j, m)
            Sx[o] = RHS(i, j, alpha, beta, gamma, x, phi, psi)
            Sy[o] = RHS(i, j, alpha, beta, gamma, y, phi, psi)
            
            
#******************************************************************************************************        

def fx_fy(alpha, phi, psi, U, Sx, Sy):

	 for j in range(1,jmax-1):
			# fx sweep:
			
			A = [0]
			B = [1]
			C = [0]
			D = [0]
		
			for  i in range(1,imax-1):
        
				LHS(i, j, A, B, C, alpha)
				D.append(Sx[offset(i, j, m)])
				
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)
			
			
			tridiag(A, B, C, D, U, 0, imax-1)
            
			for  i in range(imax):
				o = offset(i, j, m)
				Sx[o] = U[i]
        
		
		
        # fy sweep:
		
			A = [0]
			B = [1]
			C = [0]
			D = [0]
			
			for  i in range(1,imax-1):
        
				LHS(i, j, A, B, C, alpha)
				D.append(Sy[offset(i, j, m)])
        
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)

			tridiag(A, B, C, D, U, 0, imax-1)
			
			for  i in range(imax):
				o = offset(i, j, m)
				Sy[o] = U[i]
				
#******************************************************************************************************				
  
def Cx_Cy(gamma, phi, psi, U, Sx, Sy):

	 for i in range(1,imax-1):
			# Cx sweep:
			
			A = [0]
			B = [1]
			C = [0]
			D = [0]
		
			for  j in range(1,jmax-1):
        
				LHS(i, j, A, B, C, gamma)
				D.append(Sx[offset(i, j, m)])
				
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)
			
			
			tridiag(A, B, C, D, U, 0, jmax-1)
            
			for  j in range(jmax):
				o = offset(i, j, m)
				Sx[o] = U[j]
        
		
		
        # Cy sweep:
		
			A = [0]
			B = [1]
			C = [0]
			D = [0]
			
			for  j in range(1,jmax-1):
        
				LHS(i, j, A, B, C, gamma)
				D.append(Sy[offset(i, j, m)])
        
			A.append(0)
			B.append(1)
			C.append(0)
			D.append(0)

			tridiag(A, B, C, D, U, 0, jmax-1)
			
			for  j in range(jmax):
				o = offset(i, j, m)
				Sy[o] = U[j]


#******************************************************************************************************



def advancement_x_y(x, y, Sx, Sy):
    for  i  in range(1, imax -1):
    
        for j in range(1,jmax-1): 
       
            o = offset(i, j, m)
            x[o] += Sx[o]
            y[o] += Sy[o]


#******************************************************************************************************

			
				
def step_func(alpha, beta, gamma, x, y, phi, psi, U, Sx, Sy, dx_dxi, dx_deta, dy_dxi, dy_deta):	
	#fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi)	
	for iter in range(Num_of_iteraion):			
		start_SX_SY(Sx, Sy, alpha, beta, gamma, x, phi, psi)
		fx_fy(alpha, phi, psi, U, Sx, Sy)
		Cx_Cy(gamma, phi, psi, U, Sx, Sy)
		advancement_x_y(x, y, Sx, Sy)
		fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi)
        
    
#******************************************************************************************************
        
def finel_plot(x,y):       
    plt.plot(x,y)
    plt.grid(True)
    plt.show()        
        
#******************************************************************************************************
                
initialize(x, y,x_initial, y_initial,x_initial_2, y_initial_2)
initial_plot(x, y, x_initial, y_initial)
fill_metrics(x, y, dx_dxi, dx_deta, dy_dxi, dy_deta, alpha, beta, gamma, phi, psi)
step_func(alpha, beta, gamma, x, y, phi, psi, U, Sx, Sy, dx_dxi, dx_deta, dy_dxi, dy_deta)       
finel_plot(x,y)        