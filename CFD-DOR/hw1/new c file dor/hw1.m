% Computational Fluid Dynamics - HW-1 - Shahar Wollmark - 201478591 
clear all
close all
clc

% impporting the data from the c. file

% Error 
Rx_data = importdata('Rx.txt');
Rx = Rx_data(:,1);
Ry_data = importdata('Ry.txt');
Ry = Ry_data(:,1);
n = max(length(Rx),length(Ry));
% C-Mesh grid
x_data = importdata('GridX.txt');
gridx = x_data(:,:);
x_data_i = importdata('GridXi.txt');
gridxi = x_data_i(:,:);
y_data = importdata('GridY.txt');
gridy = y_data(:,:);
y_data_i = importdata('GridYi.txt');
gridyi = y_data_i(:,:);

for index = 1:51
        x= x(index,:);
        y= y(index,:);
        plot(x,y,'.-','MarkerSize',10);
    end
    for index = 1:26
        x= x(:,index);
        y= y(:,index);
        plot(x,y);
    end
    
    
    
%     
% % plotting the data
% figure()
% hold all
% plot (Rx,'r','linewidth',3)
% plot (Ry,'g','linewidth',3)
% grid on;
% xlabel('n');
% ylabel('Log_1_0(|L|_m_a_x)');
% title('Log(|R|) - \phi and \psi Control Functions');
% legend('Log(|Rx|)','Log(|Ry|)');
% 
% figure()
% hold all
% plot (x_data,y_data,'g')
% plot (x_data',y_data','g');
% xlabel('x');
% ylabel('y');
% title('C-Mesh Grid - \phi and \psi Control Functions');
% 
% figure()
% hold all
% plot (x_data,y_data,'g')
% plot (x_data',y_data','g')
% axis ([-0.25 1.3 -0.5 0.5]);
% xlabel('x');
% ylabel('y');
% title('C-Mesh Grid - \phi and \psi Control Functions - close up');
% 
% figure()
% hold all
% plot (x_data_i,y_data_i,'b')
% plot (x_data_i',y_data_i','b')
% xlabel('x');
% ylabel('y');
% title('C-Mesh Grid - \phi and \psi Control Functions - initial grid');
% 
% figure()
% hold all
% plot (x_data_i,y_data_i,'b')
% plot (x_data_i',y_data_i','b')
% axis ([-0.25 1.3 -0.5 0.5]);
% xlabel('x');
% ylabel('y');
% title('C-Mesh Grid - \phi and \psi Control Functions - initial grid - close up');

