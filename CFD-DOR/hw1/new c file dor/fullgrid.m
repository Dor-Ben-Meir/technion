
Rx_data = importdata('Rx.txt');
Rx = Rx_data(:,1);
Ry_data = importdata('Ry.txt');
Ry = Ry_data(:,1);
n = max(length(Rx),length(Ry));




figure(1)
hold all
for i = 1 : length(x(:,1))
    hold all
    axis ([-0.25 1.3 -0.5 0.5]);
    plot(x(i,:),y(i,:),'b-')
     scatter(x(i,:),y(i,:),'.')
end

for j = 1 : length(x(1,:))
    hold all
    axis ([-0.25 1.3 -0.5 0.5]);
    plot(x(:,j),y(:,j),'b-')
     scatter(x(:,j),y(:,j),'.')
end
title('phi and xi , r=0.01, w=1.5')
xlabel('x axis')
ylabel('y axis')


figure(2)
hold all
for i = 1 : length(x(:,1))
    hold all
    %axis ([-0.25 1.3 -0.5 0.5]);
    plot(x(i,:),y(i,:),'b-')
     scatter(x(i,:),y(i,:),'.')
end

for j = 1 : length(x(1,:))
    hold all
    %axis ([-0.25 1.3 -0.5 0.5]);
    plot(x(:,j),y(:,j),'b-')
     scatter(x(:,j),y(:,j),'.')
end
title('phi and xi , r=0.01, w=1.5')
xlabel('x axis')
ylabel('y axis')




figure(3)
hold all
plot (Rx,'r','linewidth',3)
plot (Ry,'g','linewidth',3)
grid on;
xlabel('n');
ylabel('Log_1_0(|L|_m_a_x)');
title('phi and xi , r=0.01, w=1.5')
legend('Log(|Rx|)','Log(|Ry|)');
