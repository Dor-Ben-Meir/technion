#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define ERROR 1
#define NOERROR 0
#define PI  3.14159265358979323846264338327950288419716939937510


double h, y_0, y_n, y_tag0, y_tagn; /* these variables are for the boundary conditions */
int i; /* iteration step */
int n;  /* number of iterations */
int func_type; /* Dirichlet or Neumann conditions */





/*  ******************************************************************************* */


void input_output ()
{
    printf ( "please enter the number of steps to use in the program - \n");
    scanf ("%d", &n);
    printf ("please define the boundary condition - \n for Dirichlet press 1, for Neumann press 2\n");
    scanf ("%d", &func_type);
    /*  boundary conditions */
    if (func_type==1)
    {

        y_0 = 0.0;
        y_n = 1.0;
    }
    if (func_type==2)
    {
        y_tag0 = 1.0;
        y_tagn = -1.0;
    }
}



/*  ******************************************************************************* */







/* The initialize function
create the x vector in the equation */
double initialize (int n, double x[], int j)
{
    h = 1.0/n;
    for (i=0; i<=n; i++)
    {
        x[i]=i*h; /* Creating the x vector */
    }
    if (j == 0)
    {
        return x[n];
    }

    if (j == 1)
    {
        return h;
    }

}





/*  ******************************************************************************* */


/* Calculates the a b c d  vector */

/* Calculates the a   vector */
double a_func (int n, double x[], double a[])
{
    for (i=0; i<=n; i++)
    {
        a[i]=1;
    }
    return a[n];
}
/* Calculates the b vector */
double b_func (int n, double x[], double b[])
{
    for (i=0; i<=n; i++)
    {
        b[i]=x[i];
    }
    return b[n];
}
/* Calculates the c vector */
double c_func (int n, double x[], double c[])
{
    for (i=0; i<=n; i++)
    {
        c[i]=x[i]*x[i];
    }
    return c[n];
}
/* Calculates the d vector */
double d_func (int n, double x[], double d[])
{
    for (i=0; i<=n; i++)
    {
        d[i]=sin(2*PI*x[i])+cos(2*PI*x[i]);
    }
    return d[n];
}




/*  ******************************************************************************* */





/*  The A B C functions from the a b c functions */

/* calculates the A vector */
double A_fun (int n, double a[], double b[], double h, double A[])
{
    for (i=0; i<=n; i++)
    {
        A[i]=a[i]/(h*h)-b[i]/(2*h);
    }
    return A[n];
}
/* calculates the B vector */
double B_fun (int n, double a[], double c[], double h, double B[])
{
    for (i=0; i<=n; i++)
    {
        B[i]=-2*a[i]/(h*h)+c[i];
    }
    return B[n];
}
/* calculates the C vector */
double C_fun (int n, double a[], double b[], double h, double C[])
{
    for (i=0; i<=n; i++)
    {
        C[i]=a[i]/(h*h)+b[i]/(2*h);
    }
    return C[n];
}

/* calculates the D vector */
double D_fun (int n, double d[], double D[])
{
    for (i=0; i<=n; i++)
    {
        D[i]=d[i];
    }
    return D[n];
}



/*  ******************************************************************************* */




/* The right hand side -RHS */
double RHS (int func_type, int n, double D[], double D_RHS[], double h, double A[], double C[])

{		D_RHS[0]=0;
    D_RHS[n]=0;

    if (func_type==1)
    {
        D[1] = D[1] - A[1]*y_0;
        D[n-1] = D[n-1] - C[n-1]*y_n;
        for (i=1; i<n; i++)
        {
            D_RHS[i]=D[i];
        }

    }
    if (func_type==2)
    {
        D[0] = D[0] + 2*h*A[0]*y_tag0;
        D[n] = D[n] - 2*h*C[n]*y_tagn;
        for (i=0; i<=n; i++)
        {
            D_RHS[i]=D[i];
        }
    }

    return D_RHS[n];
}


/*  Left hand side of the matrix - LHS */
double LHS (int func_type, int n, int j, double A[], double B[], double C[], double A_LHS[], double B_LHS[], double C_LHS[])
{
    for (i=0; i<=n; i++)
    {
        A_LHS[i]=0;
        B_LHS[i]=0;
        C_LHS[i]=0;
    }

    if (func_type==1)
    {
        for (i=2; i<n; i++)
        {
            A_LHS[i]=A[i];
        }
        for (i=1; i<n; i++)
        {
            B_LHS[i]=B[i];
        }
        for (i=1; i<(n-1); i++)
        {
            C_LHS[i]=C[i];
        }

    }
    if (func_type==2)
    {
        for (i=1; i<n; i++)
        {
            A_LHS[i]=A[i];
        }
        A_LHS[n]=A[n]+C[n];

        for (i=0; i<=n; i++)
        {
            B_LHS[i]=B[i];
        }

        C_LHS[0]=A[0]+C[0];
        for (i=1; i<n; i++)
        {
            C_LHS[i]=C[i];
        }
    }

    if (j==1)
    {
        return A_LHS[n];
    }

    if (j==2)
    {

        return B_LHS[n];
    }

    if (j==3)
    {
        return C_LHS[n];
    }
}




/*  ******************************************************************************* */



int tridiag(double a[], double b[], double c[], double d[], double u[], int is, int ie)
{

    int i;
    double  beta;


    for (i = is + 1; i <= ie; i++)
    {
        if(b[i-1] == 0.) return(ERROR);
        beta = a[i] / b[i-1];
        b[i] = b[i] - c[i-1] * beta;
        d[i] = d[i] - d[i-1] * beta;
    }

    u[ie] = d[ie] / b[ie];
    for (i = ie - 1; i >= is; i--)
    {
        u[i] = (d[i] - c[i] * u[i+1]) / b[i];
    }


    return 0;
}



/*  ******************************************************************************* */




/*  step function - repeats the iteration for the numerical solution*/

double step (int n, double h, double A[], double B[], double C[], double D[], double A_LHS[], double B_LHS[], double C_LHS[], double D_RHS[], double u[])
{
    D_RHS[n] = RHS (func_type, n, D, D_RHS,h,A,C);
    A_LHS[n] = LHS (func_type, n, 1, A, B, C, A_LHS, B_LHS, C_LHS);
    B_LHS[n] = LHS (func_type, n, 2, A, B, C, A_LHS, B_LHS, C_LHS);
    C_LHS[n] = LHS (func_type, n, 3, A, B, C, A_LHS, B_LHS, C_LHS);

    if (func_type==1)
    {
        tridiag (A_LHS, B_LHS, C_LHS, D_RHS, u, 1, n-1);
        u[0]=y_0;
        u[n]=y_n;
    }

    if (func_type==2)
    {
        tridiag (A_LHS, B_LHS, C_LHS, D_RHS, u, 0, n);
    }

    return 0;

}





/*  ******************************************************************************* */



/* this program generates two vectors and saves them in the folder - u vector and x vector */
int output_file (double x[], double u[])
{
    FILE *hw0_x, *hw0_u;
    hw0_x=fopen("hw0_x_d_1000.txt", "w");
    hw0_u=fopen("hw0_u_d_1000.txt", "w");
    for (i=0; i<=n; i++)
    {
        fprintf(hw0_x, "%f ", x[i]);
    }
    for (i=0; i<=n; i++)
    {
        fprintf(hw0_u, "%f ", u[i]);
    }
    fclose(hw0_x); fclose(hw0_u);
}

/*  ******************************************************************************* */


int main()
{

    double *x, *a, *b, *c, *d;
    double *A, *B, *C, *D, *u;
    double *A_LHS, *B_LHS, *C_LHS, *D_RHS;

    input_output();

    x =	(double *) malloc( n * sizeof(double) );
    a =	(double *) malloc( n * sizeof(double) );
    b =	(double *) malloc( n * sizeof(double) );
    c =	(double *) malloc( n * sizeof(double) );
    d =	(double *) malloc( n * sizeof(double) );
    A =	(double *) malloc( n * sizeof(double) );
    B =	(double *) malloc( n * sizeof(double) );
    C =	(double *) malloc( n * sizeof(double) );
    D =	(double *) malloc( n * sizeof(double) );
    u =	(double *) malloc( n * sizeof(double) );
    A_LHS =	(double *) malloc( n * sizeof(double) );
    B_LHS =	(double *) malloc( n * sizeof(double) );
    C_LHS =	(double *) malloc( n * sizeof(double) );
    D_RHS =	(double *) malloc( n * sizeof(double) );

    x[n] = initialize(n, x, 0);
    h = initialize(n, x, 1);
    a[n] = a_func(n, x, a);
    b[n] = b_func(n, x, b);
    c[n] = c_func(n, x, c);
    d[n] = d_func(n, x, d);
    A[n] = A_fun(n, a, b, h, A);
    B[n] = B_fun(n, a, c, h, B);
    C[n] = C_fun(n, a, b, h, C);
    D_fun (n,d,D);
    step (n, h, A, B, C, D, A_LHS, B_LHS, C_LHS, D_RHS, u);

    for (i=0; i<=n; i++)
    {
        printf ( "%f\n",u[i]);

    }


    for (i=0; i<=n; i++)
    {
        printf ( "%f\n",x[i]);

    }

    output_file(x, u);
    return 0;
}
