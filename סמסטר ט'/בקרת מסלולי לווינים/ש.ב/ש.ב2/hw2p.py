######################################################################################
#Python code for homework 2 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
Re=6371 #[Km]
h = 400
a = Re + h
n = np.sqrt(mu/a**3)



x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0

xDot0 = n*y0/2
yDot0 = -2*n*x0

tau = 1787.45

def phi11Func(tau):
    phi = np.array( [[(4 - 3*np.cos(n*tau)) , 0] , [(6*(np.sin(n*tau) - n*tau)), 1]])
    return phi

def phi12Func(tau):
    phi = np.array([[np.sin(n*tau) , (2*(1 - np.cos(n*tau)))], [(2*(np.cos(n*tau) - 1)), (4*np.sin(n*tau) - 3*n*tau)]])
    return (1/n)*phi
    
def phi21Func(tau):
    phi = np.array([ [(np.sin(n*tau)) , 0] , [(2*(np.cos(n*tau) - 1)), 0]])
    return 3*n*phi
    
def phi22Func(tau):
    phi = np.array([[(np.cos(n*tau)) , (2*np.sin(n*tau))],[(-2*np.sin(n*tau)), (4*np.cos(n*tau) - 3)]])
    return phi

def PHI(tau):
    qq = np.concatenate((phi11Func(tau), phi12Func(tau)),axis=1)
    qq2 = np.concatenate((phi21Func(tau), phi22Func(tau)),axis=1)
    qq3 = np.concatenate((qq, qq2), axis=0)   
    return qq3

def KFunc(tau):
    C =  np.matmul(-inv(phi12Func(tau)),phi11Func(tau))
    K = -np.column_stack((C,-np.eye(2)))
    return (C,K)

def deltaV1Func(tau, x0, y0, xDot0, yDot0,K):
    a = np.matmul(-K , np.array([[x0], [y0], [xDot0], [yDot0]]))
    return a

def deltaV2Func(tau,C):
    a = np.matmul(phi21Func(tau),np.array([[x0], [y0]])) + np.matmul(phi22Func(tau),np.matmul(C,np.array([[x0], [y0]])))
    return -a

(C,K) = KFunc(tau)
deltaV1 = deltaV1Func(tau, x0, y0, xDot0, yDot0,K) * 10**3
deltaV2 = deltaV2Func(tau,C) * 10**3