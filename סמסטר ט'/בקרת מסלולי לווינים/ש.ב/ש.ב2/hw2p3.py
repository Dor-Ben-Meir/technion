######################################################################################
#Python code for homework 2 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
Re=6371 #[Km]
h = 400
a = Re + h
n = np.sqrt(mu/a**3)



x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0

xDot0 = n*y0/2
yDot0 = -2*n*x0
X0 = np.array([[x0], [y0], [xDot0], [yDot0]])


#worst case measurement errors [km]
deltaXm = 1e-3
deltaYm = 1e-3
deltaXDotm = 1e-5
deltaYDotm = 1e-5


def phi11Func(t):
    tau = 80*60 - t
    phi = np.array( [[(4 - 3*np.cos(n*tau)) , 0] , [(6*(np.sin(n*tau) - n*tau)), 1]])
    return phi

def phi12Func(t):
    tau = 80*60 - t
    phi = np.array([[np.sin(n*tau) , (2*(1 - np.cos(n*tau)))], [(2*(np.cos(n*tau) - 1)), (4*np.sin(n*tau) - 3*n*tau)]])
    return (1/n)*phi
    
def phi21Func(t):
    tau = 80*60 - t
    phi = np.array([ [(np.sin(n*tau)) , 0] , [(2*(np.cos(n*tau) - 1)), 0]])
    return 3*n*phi
    
def phi22Func(t):
    tau = 80*60 - t
    phi = np.array([[(np.cos(n*tau)) , (2*np.sin(n*tau))],[(-2*np.sin(n*tau)), (4*np.cos(n*tau) - 3)]])
    return phi

def PHI(t):
    t = -t + 80*60
    qq = np.concatenate((phi11Func(t), phi12Func(t)),axis=1)
    qq2 = np.concatenate((phi21Func(t), phi22Func(t)),axis=1)
    qq3 = np.concatenate((qq, qq2), axis=0)
    X0V = np.dot(qq3, X0)
    return X0V
    
def CFunc(t):
    C =  np.dot(-inv(phi12Func(t)),phi11Func(t))
    return C
    
def KFunc(t):
    C =  np.dot(-inv(phi12Func(t)),phi11Func(t))
    K = -np.concatenate((C,-np.eye(2)),axis=1)
    return K

def deltaV1Func(t):
    a = np.dot(-KFunc(t) , PHI(t))
    return a * (10**3)

def deltaV2Func(t):
    a = np.dot(phi21Func(t),np.array([PHI(t)[0], PHI(t)[1]])) + np.dot(phi22Func(t),np.dot(CFunc(t),np.array([PHI(t)[0], PHI(t)[1]])))
    return -a * (10**3)

from scipy.optimize import fsolve
def func(t):
    F = np.empty((2))
    F[0] = deltaV1Func(t)


    return F

zGuess = np.array([1,1])            
root = fsolve(func, zGuess)

#t = 0.01
#while((np.abs(deltaV1Func(t)[1]) > 0.000001) and (np.abs(deltaV1Func(t)[1]) > 0.000001)):
#    t = t + 0.1

#deltaPositionMax = np.dot(np.abs(phi11Func(t)) , np.array([[deltaXm], [deltaYm]])) + np.dot(abs(phi12Func(t)) ,np.array([[deltaXDotm], [deltaYDotm]]))
#Missdistance = LA.norm(deltaPositionMax)
