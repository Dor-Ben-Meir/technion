
######################################################################################
#Python code for homework 10 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit
from scipy import signal
from control.matlab import *
#from control import *
from control.statefbk import ctrb, obsv, place, place_varga, lqr, gram, acker
from pyproj import Proj
import pyproj
from astropy.coordinates import EarthLocation
from functools import reduce
import pandas as pd
import matplotlib.pyplot as plt




mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
T = 86164.09 #[sec]
n = 2*np.pi/T
fMax = (2 * (10**-5)) *(10**-3) # [km/s^2]

Req = 6378.1363 #[km] 
a = 42164.16 #[km]
K = 1.68 * (10**-3) #[deg/day^2]
lambda_n = 10 #[deg]
delta_lambda = -0.05 #[deg]
lambda_s = min([abs(75.1), abs(lambda_n - 255.1)])
lambda_2dot_n = -K * np.sin(np.deg2rad(2*(lambda_n - lambda_s)))
lambda_0 = lambda_n - delta_lambda
lamda_dot_0 = -2 * np.sign(lambda_2dot_n) * np.sqrt(-(lambda_2dot_n)  * delta_lambda)
#lamda_dot_01 = np.deg2rad(lamda_dot_0) / day2sec
tb = abs(lamda_dot_0 / lambda_2dot_n)
tm = 2 * tb
a_dot = 0.132 * np.sin(np.deg2rad(2 * (lambda_n - lambda_s) * tm))
delta_a0 = -tb * a_dot


#Transformation between Position and Velocity Vectors in ECI system to calsical elements:
def RVvectors_to_Clasical(r,v):
 
    R = r    
    V = v
    
    
    
    x = r[0]
    y = r[1]
    z = r[2]

    v1 = v[0]
    v2 = v[1]
    v3 = v[2]
    
    dot_rv = x*v1 + y*v2 + z*v3
    
    R_syze = np.sqrt(x**2 + y**2 + z**2)
    V_syze = np.sqrt(v1**2 + v2**2 + v3**2)
    
    h = np.transpose(np.cross(np.transpose(R),np.transpose(V)))
    #h_size = np.sqrt(h[:,0]**2 + h[:,1]**2  + h[:,2]**2)
    h_size = LA.norm(h)
    e = R
    e[0] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[0] - (dot_rv * V[0]))
    e[1] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[1] - (dot_rv * V[1]))
    e[2] = (1 / mu) * (((V_syze**2) - (mu / R_syze))*R[2] - (dot_rv * V[2]))
    e_size = LA.norm(e)
    p = (h_size**2) / mu 
    a = p / (1 - e_size**2)
    n = np.cross([0,0,1],np.transpose(h))
    n_size = LA.norm(n)
    dot_ne = np.dot(n,e)
    #dot_re = np.dot(r,e)
    i = np.arccos(h[2]/h_size)
    Omega = np.arccos(n.T[0]/n_size)
    w =  np.arccos(dot_ne / (n_size * e_size))
    th = np.arccos(((a*(1-e_size**2)-R_syze)/(R_syze*e_size)))

    n = np.transpose(n)
    ny = n[1]
    ek = e[2]
    

    if(ny < 0 ):
        Omega = 2 * np.pi - Omega
            
    if(ek < 0 ):
        w = 2 * np.pi - w
            
    if(dot_rv < 0 ):       
        th = 2 * np.pi - th 
            
            

    return(a,e,i,Omega,w,th,e_size,R,V) 

# x = EarthLocation.from_geodetic(lambda_n - delta_lambda,0,height=(aa - Req)*u.km)
# y = EarthLocation.from_geodetic(lambda_n + delta_lambda,0,height=(aa - Req)*u.km)
# Xtols = np.array([x.geocentric[0].value,x.geocentric[1].value,x.geocentric[2].value])
# Ytols = np.array([y.geocentric[0].value,y.geocentric[1].value,y.geocentric[2].value])
# tols = LA.norm(Xtols - Ytols) 



# x = EarthLocation.from_geodetic(lambda_0, 0,height=(aa - Req)*u.km)
# x0 = x.geocentric[0].value
# y0 = x.geocentric[1].value 
# z0 = x.geocentric[2].value 
# r = np.array([x0,y0,z0])

# x_dot = EarthLocation.from_geodetic(lamda_dot_0,0,height=(aa - Req)*u.km)
# x0_dot = x.geocentric[0].value 
# y0_dot = x.geocentric[1].value 
# z0_dot = x.geocentric[2].value 

# v = np.array([x0_dot,y0_dot,z0_dot])


# (a,e,i,Omega,w,th,e_size,R,V) = RVvectors_to_Clasical(r,v)

y0 = a * np.deg2rad(lambda_0) 
y0_dot = a * np.deg2rad(lamda_dot_0)
tols = -a * (2*np.deg2rad(delta_lambda))
tols = 6 * 10**-6





# x2 = EarthLocation.from_geocentric(x0,y0,z0,unit = u.km)
# long= x2.geodetic[0].value
# long = x2.geodetic[1].value
# alt = x2.geodetic[2].value


# F and G calculation:
F = np.array([ [0 , 1 , 0, 0],[3*(n**2), 0 , 0, 2*n],[0 , 0 , 0, 1],[0 , -2*n, -0, 0]])
#F = np.array([ [0 , 1 , 0, 0], [3*(n**2), 0 , 0, 2*n],[0 , 0 , 0, 1],[0 , 0, -2*n, 0]])
#F = np.array([ [0 , 0 , 1, 0],[0 , 0 , 0, 1], [3*(n**2), 0 , 0, 2*n],[0 , 0,-2*n, 0]])
#G = np.array([[0, 0],[0, 0],[0, 0],[0, 1]])


C calculation:
lam = [-0.5*n,-0.5*n + 1j*n,-0.5*n-1j*n]
a = np.real(lam[1]) 
abs_z = np.sqrt(np.real(lam[1]) + np.imag(lam[1]))
d = lam[0]
L2 = -2*a - d
L3 = abs_z + 2*a*d
L4 = -abs_z*d
C = np.zeros(4) 
C[0]=(L3+3*n**2)/2/n
C[1]=-L4/3/n**2
C[2]=(L2-C[1])/2/n
C[3]=1

C = np.zeros(4) 
C[0] = 0.0024871
C[1] = -2.1817e-04
C[2] = 0.8542
C[3]=1
 
X = np.array([0.0, 0.0, y0, y0_dot])
s = np.matmul(C, X)
K = LA.norm(reduce(np.matmul, [C, F, X]))

ans1 = dict(y0 = y0, y0_dot = y0_dot, tols = tols, K = K)



def orbitA(X,t):
     
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]   
    X7 = X[6:7]     
    
    X = np.array([X1, X2, X2, X5])
    # X = np.array([0.0, 0.0, y0, y0_dot])
    # s = np.matmul(C, X)
    # K = LA.norm(reduce(np.matmul, [C, F, X])) 
    
   # x = EarthLocation.from_geocentric(X1,X2,X3,unit = u.km)
    
    if(abs(s) < tols):
        
        fX,fY,fZ = 0,0,0
        
    else:
        fX = 0    
        fY = -K * np.sign(s)
        fZ = 0
    if(fY > fMax):
        fY = fMax
    if(fY < -fMax):
        fY = fMax
        
    #print(fY)
    dX1 = X4
    dX2 = X5
    dX3 = X6
    dX4 = 2*n*X5 + 3*(n**2)*X1 + fX
    dX5 = - 2*n*X4 + fY 
    dX6 = - n**2*X3 + fZ
    dX7 = fY
    #derivs = np.concatenate((dX1,dX2,dX3,dX4,dX5,dX6,dX7))
    derivs = np.column_stack((dX1,dX2,dX3,dX4,dX5,dX6,dX7))
    derivs = np.reshape(derivs, (7, ))
    return derivs    



X0 = np.array([0.0, y0, 0.0, 0.0, y0_dot, 0.0, 0.0])
t = np.arange( 0, T*2 + 1   ,1)   
sol = odeint(orbitA,X0,t) 

Vc = sol[:,5]
x = sol[:,0]
y = sol[:,1]
Vx = sol[:,3][-1]
Vy = sol[:,4][-1]
V_f = LA.norm([Vx,Vy,0])
fy = sol[:,6]*1e3


for i in range(len(t)):
    Vc[i] = LA.norm([sol[i,3],sol[i,4],0])


Lambtda = pd.DataFrame(data={'time[s]':t})
Lambtda['X[Km]'] = sol[:,0]
Lambtda['Y[Km]'] = sol[:,1]

Lambtda['lon[deg]'] = np.rad2deg(y / a)
Lambtda['alt[km]'] = x + a
Lambtda['fy[m/s^2]'] = fy
Lambtda['Delta_v[m/s]'] = Vc


#Plot of the relative trajectory in the altitude-longitude plane:
ax = plt.gca()
ax.grid()
ax.set_title('Relative Trajectory', fontsize=20)
Lambtda.plot(kind='line',x='Y[Km]',y='X[Km]' ,ax=ax)
ax.set_ylabel('X[Km]')
plt.show()

#Plot of the relative trajectory in the altitude-longitude plane:
ax = plt.gca()
ax.grid()
ax.set_title('Relative Trajectory', fontsize=20)
Lambtda.plot(kind='line',x='alt[km]',y='lon[deg]' ,ax=ax)
ax.set_ylabel('lon[deg]')
plt.show()


#Altitude and longitude vs. time:
ax = plt.gca()
ax.grid()
ax.set_title('Longitude vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[s]',y='lon[deg]' ,ax=ax)
ax.set_ylabel('lon[deg]')
plt.show()


ax = plt.gca()
ax.grid()
ax.set_title('Altitude vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[s]',y='alt[km]' ,ax=ax)
ax.set_ylabel('alt[km]')
plt.show()


#Thrust acceleration vs. time.
ax = plt.gca()
ax.grid()
ax.set_title('Thrust acceleration vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[s]',y='fy[m/s^2]' ,ax=ax)
ax.set_ylabel('fy[m/s^2]')
plt.show()


#Accumulated ΔV vs. time
ax = plt.gca()
ax.grid()
ax.set_title('Accumulated ΔV vs. time', fontsize=20)
Lambtda.plot(kind='line',x='time[s]',y='Delta_v[m/s]' ,ax=ax)
ax.set_ylabel('Delta_v[m/s]')
plt.show()