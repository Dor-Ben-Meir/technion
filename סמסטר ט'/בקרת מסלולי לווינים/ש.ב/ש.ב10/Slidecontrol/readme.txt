To add the libray to Simulink ensure the folder 'SMC Library' is on your matlab path and run 'slblocks.m'.

Instructions are provided in 'SMC Instructions.pdf'. A breif overview of Sliding Mode Control is provided however for more detailed information please go to the supplied references

This work was produced and the associated library was produced by Liam Vile 28/03/2019 � and is based on previous works completed in \cite{Edwards1998a} and \cite{Alwi2008a}. All rights of the library are reserved to the referenced authors. All distribution and use is limited to Academic study and any use for profit is strictly forbidden. Contact lev201@exeter.ac.uk for any queries.
  