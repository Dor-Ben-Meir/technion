######################################################################################
#Python code for homework 3 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
#a = 6778
#n = np.sqrt(mu/a**3)
#T = 2*np.pi/n
T = 100*60
n = 2*np.pi/T
fMax = 4 * (10**-6) # [km/s^2]
x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0
tf = 80*60
xDot0 = n*y0/2
yDot0 = -2*n*x0
zDot0 = 0
vXMax = x0 / tf
vX = vXMax

def phi11Func(t):
    tau = 80*60 - t
    phi = np.array( [[(4 - 3*np.cos(n*tau)) , 0] , [(6*(np.sin(n*tau) - n*tau)), 1]])
    return phi

def phi12Func(t):
    tau = 80*60 - t
    phi = np.array([[np.sin(n*tau) , (2*(1 - np.cos(n*tau)))], [(2*(np.cos(n*tau) - 1)), (4*np.sin(n*tau) - 3*n*tau)]])
    return (1/n)*phi
    
    
def CFunc(t):
    C =  np.dot(-inv(phi12Func(t)),phi11Func(t))
    return C

def init_orbit(X,t):
     
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]  
        
    dX1 = X4
    dX2 = X5
    dX3 = X6
    dX4 = 2*n*X5 + 3*n**2*X1 
    dX5 = - 2*n*X4 
    dX6 = - n**2*X3  
    derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6))
    return derivs 



def orbitA(X,t):
     
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]   
    X7 = X[6:7]     
    
    fX = -3*n**2*X1
    fY = -2*n*vX
    fZ = 0
    
    a = LA.norm(np.array([fX, fY, fZ]))
    
    dX1 = X4
    dX2 = X5
    dX3 = X6
    dX4 = 2*n*X5 + 3*n**2*X1 + fX
    dX5 = - 2*n*X4 + fY 
    dX6 = - n**2*X3 + fZ
    dX7 = a
    derivs = sci.concatenate((dX1,dX2,dX3,dX4,dX5,dX6,dX7))
    return derivs
    
def orbitB(X,t):
     
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]   
    X7 = X[6:7]     

    aT = fMax
    CStar = CFunc(t)
#    vg = CStar*np.array([[X1], [X2]]) - np.array([ [X4], [X5]])
#    vg = vg[0]
    vg = np.matmul(CStar,np.array([X1, X2])) - np.array([ X4, X5])
    vgHat = vg/LA.norm(vg)
    qq = np.matmul(-CStar , vg)
    q = np.sqrt(aT**2 - mit.dotproduct(qq,qq) + (mit.dotproduct(qq,vgHat))**2)
    
#    PPhi = phi11Func(t)
#    aai = np.array([X1,X2])
    
    deltaPositionMax = np.matmul((phi11Func(t)) , np.array([X1,X2])) + np.matmul((phi12Func(t)) ,np.array([X4,X5]))
    Missdistance = LA.norm(deltaPositionMax)
    if(Missdistance > (2* (10**-3))):
        aTVec = qq + (q - mit.dotproduct(qq,vgHat))*vgHat
        fX = aTVec[0]
        fY = aTVec[1]
        fZ = 0
        a = LA.norm(np.array([fX, fY, fZ]))
        dX7 = a
    else:
        fX = 0
        fY = 0
        fZ = 0
        a = LA.norm(np.array([fX, fY, fZ]))
        dX7 = [a]
    #a = LA.norm(np.array([fX, fY, fZ]))
    dX1 = X4
    dX2 = X5
    dX3 = X6
    dX4 = 2*n*X5 + 3*n**2*X1 + fX
    dX5 = - 2*n*X4 + fY 
    dX6 = - n**2*X3 + fZ
    #dX7 = a
    derivs = sci.concatenate([dX1,dX2,dX3,dX4,dX5,dX6,dX7])
    return derivs

def missdis(solB,t):
    fX = np.zeros(len(t))
    fY = np.zeros(len(t))
    a = np.zeros(len(t))
    #aTVec = np.zeros(len(t))
    for i in range(len(t)):
        X1 = solB[i][0]
        X2 = solB[i][1]
        X3 = solB[i][2]
        X4 = solB[i][3]
        X5 = solB[i][4]
        X6 = solB[i][5]
        X7 = solB[i][6]  
            
        aT = fMax
        CStar = CFunc(t[i])
        vg = np.matmul(CStar,np.array([X1, X2])) - np.array([ X4, X5])
        vgHat = vg/LA.norm(vg)
        qq = np.matmul(-CStar , vg)
        q = np.sqrt(aT**2 - mit.dotproduct(qq,qq) + (mit.dotproduct(qq,vgHat))**2)
            

            
        deltaPositionMax = np.matmul((phi11Func(t[i])) , np.array([X1,X2])) + np.matmul((phi12Func(t[i])) ,np.array([X4,X5]))
        Missdistance = LA.norm(deltaPositionMax)
        if(Missdistance > (2* (10**-3))):
            aTVec = qq + (q - mit.dotproduct(qq,vgHat))*vgHat
            fX[i] = aTVec[0]
            fY[i] = aTVec[1]
            fZ = 0
            a[i] = LA.norm([fX[i], fY[i]])

        else:
            break
    return(fX,fY,a)   
    
tfAnalytical = x0/vXMax
t = np.arange( 0,T + 0.1 ,0.1) 
X0 = np.array([x0, y0, z0, xDot0, yDot0, zDot0])
X0 = X0.flatten()
solint = odeint(init_orbit,X0,t)
#
xDot0Transfer = - vXMax
yDot0Transfer = 0
zDot0Transfer = 0
#t = np.arange( 0,tfAnalytical*2 + 0.1 ,0.1)
t = np.linspace(0.0, tfAnalytical, num=121) 
X0 = np.array([x0, y0, z0, xDot0Transfer, yDot0Transfer, zDot0,0])
X0 = X0.flatten()

sol = odeint(orbitA,X0,t)
solB = odeint(orbitB,X0,t)

(FX,FY,F) = missdis(solB,t)
#fX = np.zeros(len(t))
#fY = np.zeros(len(t))
#a = np.zeros(len(t))
##aTVec = np.zeros(len(t))
#for i in range(len(t)):
#    X1 = solB[i][0]
#    X2 = solB[i][1]
#    X3 = solB[i][2]
#    X4 = solB[i][3]
#    X5 = solB[i][4]
#    X6 = solB[i][5]
#    X7 = solB[i][6]  
#        
#    aT = fMax
#    CStar = CFunc(t[i])
#    vg = np.matmul(CStar,np.array([X1, X2])) - np.array([ X4, X5])
#    vgHat = vg/LA.norm(vg)
#    qq = np.matmul(-CStar , vg)
#    q = np.sqrt(aT**2 - mit.dotproduct(qq,qq) + (mit.dotproduct(qq,vgHat))**2)
#        
#
#        
#    deltaPositionMax = np.matmul((phi11Func(t[i])) , np.array([X1,X2])) + np.matmul((phi12Func(t[i])) ,np.array([X4,X5]))
#    Missdistance = LA.norm(deltaPositionMax)
#    if(Missdistance > (2* (10**-3))):
#        aTVec = qq + (q - mit.dotproduct(qq,vgHat))*vgHat
#        fX[i] = aTVec[0]
#        fY[i] = aTVec[1]
#        fZ = 0
#        a[i] = LA.norm([fX[i], fY[i]])
#
#    else:
#        break
##        fX[i] = 0
##        fY[i] = 0
##        fZ = 0
##        
##        a[i] = LA.norm([fX[i], fY[i]])
np.savetxt('output.txt', (solint))
np.savetxt('output1.txt', (sol))
np.savetxt('output2.txt', (solB))
np.savetxt('tpy.txt', (t))
np.savetxt('FX.txt', (FX))
np.savetxt('FY.txt', (FY))
np.savetxt('F.txt', (F))
