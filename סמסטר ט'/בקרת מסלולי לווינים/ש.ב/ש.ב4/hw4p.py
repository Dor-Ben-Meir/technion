######################################################################################
#Python code for homework 4 in SPACE MECHANICS Control, Dor Ben Meir 305702490
######################################################################################

import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from numpy.linalg import inv
import more_itertools as mit
from scipy import signal
from control.matlab import *
#from control import *
from control.statefbk import ctrb, obsv, place, place_varga, lqr, gram, acker

mu = 398601.2 #[km^3/s^2]
J2 = 0.0010826
T = 100*60
n = 2*np.pi/T
fMax = 4 * (10**-6) # [km/s^2]
x0 = 1
y0 = 0
z0 = 0
xf = 0
yf = 0
tf = 80*60
xDot0 = n*y0/2
yDot0 = -2*n*x0
zDot0 = 0
vXMax = x0 / tf
vX = vXMax
#n =  0.0011
#T = 2*np.pi/n
F = np.array([ [0 , 1 , 0, 0],[3*(n**2), 0 , 0, 2*n],[0 , 0 , 0, 1],[0 , -2*n, 0, 0]])
G = np.array([[0, 0],[0, 0],[0, 0],[0, 1]])

tau = 0.25*T
dominantReal = n

#np.conjugate(1+2j)

poles = np.zeros((4),dtype=complex)
poles[0] = -dominantReal + 1j*dominantReal
poles[1] = np.conjugate(poles[0])
poles[2] = -dominantReal*4 + 1j*dominantReal
poles[3] = np.conjugate(poles[2])
control_K = place_varga(F, G,  0.6*poles)
#control_K = place(F, G,  0.6*poles)
#control_K = np.array([[0,	0,	0,	0,],[1.84330992547141e-05,	0.00739305012650359,	-1.88017612398282e-06,	0.00678842751576333]])
def init_orbit(X,t):
     
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]  
        
    dX1 = X4
    dX2 = X5
    dX3 = X6
    dX4 = 2*n*X5 + 3*n**2*X1 
    dX5 = - 2*n*X4 
    dX6 = - n**2*X3  
    derivs = np.concatenate((dX1,dX2,dX3,dX4,dX5,dX6))
    return derivs 

    
def orbitA(X,t):
     
    X1 = X[0:1]
    X2 = X[1:2]
    X3 = X[2:3]    
    X4 = X[3:4]
    X5 = X[4:5]
    X6 = X[5:6]   
    X7 = X[6:7]     
    
    
    f = np.matmul(-control_K, np.array([X1, X4, X2, X5]))
    f = np.array(f.T)[0]
    fX = f[0]
    fY = f[1]
    fZ = 0
        
    
    dX1 = X4
    dX2 = X5
    dX3 = X6
    dX4 = 2*n*X5 + 3*(n**2)*X1 + fX
    dX5 = - 2*n*X4 + fY 
    dX6 = - n**2*X3 + fZ
    dX7 = np.array([LA.norm(f)])
    derivs = np.concatenate((dX1,dX2,dX3,dX4,dX5,dX6,dX7))
    return derivs    

t = np.arange( 0,T + 0.1 ,0.1)   
X0 = np.array([x0, y0, z0, xDot0, yDot0, zDot0])
X0 = X0.flatten()
solint = odeint(init_orbit,X0,t)

#t2 = np.arange( 0,2*T + 5 ,5) 
t2 = np.linspace(0.0, 2.5*T, num=5441)   
X02 = np.array([x0, y0, z0, xDot0, yDot0, zDot0, 0.0])
#X02 = X0.flatten()
sol = odeint(orbitA,X02,t2)

np.savetxt('output.txt', (solint))
np.savetxt('output1.txt', (sol))
np.savetxt('contorlk.txt', (control_K))
np.savetxt('tpy.txt', (t2))


# control_K[:,3] = control_K[:,3] * 0.5
# sol2 = odeint(orbitA,X02,t2)
# np.savetxt('output2.txt', (sol2))
# np.savetxt('contorlk2.txt', (control_K))
