######################################################################################
#Python code for homework 1 Heat, Dor Ben Meir 305702490
######################################################################################


import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci


def q1():
    K = 0.05
    n = 5
    d = 2
    A = d**2 * n
    Tout = 35 + 273.15
    Tin = -10 + 273.15
    q = 400

    t = (K * A * (Tout - Tin)) / q 
    t = t * u.m
    return t 
    
    
def q2():
    epsi = 0.7
    Sigma = 5.67 * (10**-8)
    C = 4.2
    n = 4
    L = 10 * (10**-3)
    Tinf = 25 + 273.15
    Ts = 85  + 273.15
    q = (L**2) * (C * ((Ts - Tinf)**(5/4)) + epsi*Sigma*((Ts**4) - (Tinf**4)))
    P = n * q
    
    h = 300
    q2 = (L**2) * (h * (Ts - Tinf) + epsi*Sigma*((Ts**4) - (Tinf**4)))
    return dict(q = q, P = P, qchip = q2) 
    
def q4():
    K = 40
    t = 0.05
    a = 200
    b = -2000
    qdot = -2*K*b
    
    q0 = 0
    qt = -K*2*b*t
    E = q0 - qt + qdot*t
    return dict(qdot = qdot,E = E)
q1 = q1()
q2 = q2()
q4 = q4()