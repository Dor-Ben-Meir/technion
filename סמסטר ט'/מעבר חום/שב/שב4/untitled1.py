

# https://www.wkcgroup.com/tools-room/logarithmic-addition-sound-pressure-levels/
#https://onlinelibrary.wiley.com/doi/pdf/10.1002/9781119038122.app3
#https://www.omnicalculator.com/physics/distance-attenuation


import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import fsolve
from scipy import special
import pandas
from sympy import Symbol
import pandas as pd
import astropy.units as un

#Q12:
rho_0 = 1.2
C0 = 343
Pref = 2 * (10**-5)
Lp = 40 #[dB]
Prms = Pref * 10**(Lp / 20) #[Pa]
rho0C01 = 415
u = Prms / (rho_0 * C0)
#rho_tag = u * rho_0/C0
rho_tag = Prms/(C0**2)

C02 = 1500 #[m/sec]
rho0C02 = 1.48 * (10**6)
rho02 = rho0C02 / C02
rho_tag = (u * rho02) / C02
P_rms2 = u * rho02 * C02

import astropy.units as un
ans12 = dict(Prms = Prms * un.Pa, u = u * (un.m / un.s), rho_tag = rho_tag * (un.kg / (un.m**3)) , P_rms2 = P_rms2 * un.Pa)

########################################
#Q5:
Pref = 2 * (10**-5)
Lp1 = 90 #[dB]
Lp2 = 93 #[dB]
Prms1 = Pref * 10**(Lp1 / 20) #[Pa]
Prms2 = Pref * 10**(Lp2 / 20) #[Pa]
Prms_sqr_total = Prms1**2 + Prms2**2
Lp_total = 10 * np.log10(Prms_sqr_total / (Pref**2))
ans5 = dict(Prms1 = Prms1* un.Pa, Prms2 = Prms2* un.Pa, Prms_sqr_total = Prms_sqr_total* un.Pa, Lp_total = Lp_total)


########################################
#Q6:
Pref = 2 * (10**-5)
Lp1 = 95 #[dB]
Lp2 = 90 #[dB]
Prms1 = Pref * 10**(Lp1 / 20) #[Pa]
Prms2 = Pref * 10**(Lp2 / 20) #[Pa]
Prms_sqr_Delta = Prms1**2 - Prms2**2
Lp_machine = 10 * np.log10(Prms_sqr_Delta / (Pref**2))
ans6 = dict(Prms1 = Prms1* un.Pa, Prms2 = Prms2* un.Pa, Prms_sqr_Delta = Prms_sqr_Delta* un.Pa, Lp_machine = Lp_machine)


########################################

#Q7:
Pref = 2 * (10**-5)
Lp1 = 63 #[dB]
Lp2 = 60 #[dB]
Prms1 = Pref * 10**(Lp1 / 20) #[Pa]
Prms2 = Pref * 10**(Lp2 / 20) #[Pa]
Prms_sqr_Delta = Prms1**2 - Prms2**2
Lp_machine = 10 * np.log10(Prms_sqr_Delta / (Pref**2))
Lp_2machines = 10 * np.log10(2*(10**(Lp_machine / 10)))
ans7 = dict(Prms1 = Prms1* un.Pa, Prms2 = Prms2* un.Pa, Prms_sqr_Delta = Prms_sqr_Delta* un.Pa, Lp_machine = Lp_machine,Lp_2machines = Lp_2machines)


########################################



# #Q4:
# Iref = 10**-12
# Pref = 2 * (10**-5)
# P = 2 #[pa]
# C0 = 343
# rho_0 = 1.2
# f = 100
# S = P / (rho_0 * C0 * (2 * np.pi * f) ) # Displacment
# u = S / (2 * np.pi * f) 
# Prms = u * rho_0 * C0
# I = Prms**2 / (rho_0 * C0) #Intensity 
# Lp = 20 * np.log10(Prms / Pref)
# LI = 10 * np.log10(I/Iref) #Sound Intensity Level

# ans4 = dict(u  =  u * (un.m / un.s), Prms = Prms* un.Pa, I = I, Lp = Lp, LI = LI)
########################################


########################################



#Q4:
Iref = 10**-12
Pref = 2 * (10**-5)
P = 2 #[pa]
C0 = 343
rho_0 = 1.2
f = 100
Lambda = C0 / f
K = 2 * np.pi / Lambda
Pmax = 2
Prms = Pmax / np.sqrt(2)
umax = Pmax / (rho_0 * C0)
Imax  = Pmax * umax
Irms = Imax / np.sqrt(2)
I = (Prms**2) / (rho_0 * C0) #Intensity 
Lp = 20 * np.log10(Prms / Pref)
LI = 10 * np.log10(I/Iref) #Sound Intensity Level

ans4 = dict(umax  =  u * (un.m / un.s), Prms = Prms* un.Pa, I = I, Lp = Lp, LI = LI)




# gamma = 1.4
# R = 287
# T = 25 + 273
# f = 250

# C0 = np.sqrt(gamma * R * T)
# Lambda = C0 / f
# K = 2 * np.pi / Lambda
########################################
# Pref = 2 * (10**-5)
# f = 1000
# Lp = 100 #[dB]
# Prms = Pref * 10**(Lp / 20) #[Pa]
# rho0C0 = 415
# P_tag = 0.00002 #[Pa]
# u = Prms / rho0C0
# Displacment = u / (2 * np.pi * f)

########################################


# Prms1 = 10
# Prms2 = 2 * Prms1
# Pref = 2 * (10**-5)
# Lp1 = 20 * np.log10(Prms1 / Pref)
# Lp2 = 20 * np.log10(Prms2 / Pref)
# DP = Lp1 / Lp2