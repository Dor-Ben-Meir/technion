
######################################################################################
#Python code for homework 4 in Heat Transfer, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import fsolve
from scipy import special
import pandas
from sympy import Symbol
import pandas as pd

K = 0.15 
RhoCp = 1.5 * (10**6)
alpha = K / RhoCp

Xm = 4 * (10**-3)
Ti = 150 + 273
Ts = 25 + 273

t = np.arange(0, 500000   ,0.1)
T = special.erfc(Xm / (2 * np.sqrt(alpha * t))) * (Ti - Ts) + Ts
T = T - 273


flight_conditions = pd.DataFrame(data={'Time[s]':t})
flight_conditions['T[K]'] = T
ax = plt.gca()
flight_conditions.plot(kind='line',x='Time[s]',y='T[K]' ,ax=ax)
plt.show()


# exelf = 'Question_5_Data.xlsx'
# sheetname1 = 'f = 000 Hz'
# sheetname2 = 'f = 240 Hz'
# rho_air = 1.2041 

# #Properties of air:
# u = 13.5 #[m/s]
# Ka = 0.029 #W/(mK)
# mu = 2.051 * (10**-5) #Ns/m2
# Pr = 0.701


# #Properties of acrylic wall:
# rho_w = 1190 #kg/m3
# CP_w = 1470 #J/(kgK)
# Kw = 0.19 #W/(mK)

# Pix_size = 0.333 * (10**-3) #[m]

# def dftomatrix(exelname,f): 
#     excel_data_df = pandas.read_excel(exelname, sheet_name = f)
#     ls = excel_data_df.columns.ravel()
#     M = np.zeros((1500,8))

#     i = 0
#     for name in ls:
#         ls2 = excel_data_df[name].tolist()
#         del ls2[0]
#         M[:,i] = np.array(ls2)
#         i = i + 1
        
#     return M
    
    
# Re_f = lambda x: (rho_air * u * x) / mu
# Nu_f = lambda x: 0.332 * (Re_f(x)**0.5) * (Pr**0.33)
# h_f = lambda x: (Nu_f(x) * Ka) / x


   
# EXP1 = dftomatrix(exelf,sheetname1)
# EXP2 = dftomatrix(exelf,sheetname2)

# Tz1 = EXP1[:,1]
# Tz2 = EXP2[:,1]

# T = 37.1 +  273.15 #[K]
# t = EXP1[:,7]
# x = EXP1[:,6] * Pix_size
# h = h_f(x)



# ai = np.arange( 2,600 + 1 ,1)
# TTi = np.zeros(700)
# TTg = np.zeros(700)


# # i = 3
# # def equations1(z):
# #     # z[0] = T2, z[1] = P2
# #     #i = 0
# #     Ti,Tg = z
# #     t1 = t[i]
# #     t2 = t[i + 1]
# #     # T1 = T[i]
# #     # T2 = T[i+1]
# #     T1 = 37.1 +  273.15
# #     T2 = 37.1 +  273.15
# #     h1 = h[i]
# #     h2 = h[i + 1]
# #     X1 = ((h1**2) * t1) / (Kw*rho_w*CP_w)
# #     X2 = ((h2**2) * t2) / (Kw*rho_w*CP_w)
# #     eq1 = 1 - np.exp(X1) *  special.erfc(np.sqrt(X1)) - ((T1 - Ti) / (Tg - Ti))
# #     eq2 = 1 - np.exp(X2) *  special.erfc(np.sqrt(X2)) - ((T2 - Ti) / (Tg - Ti))
    
# #     return(eq1,eq2) 
# Ti0 = 295
# Tg0 = 325

# i  = 1
# def equations2(z):
#     # z[0] = T2, z[1] = P2
#     #i = 0
#     h =  h_f(x[0])
#     Ti,Tg = z
#     t1 = t[i]
#     t2 = t[i + 1]
#     #t3 = t[i + 2]
#     T1 = 31.7 +  273.15
#     T2 = 31.7 +  273.15
#     #T3 = Tz1[i + 2] +  273.15
#     X1 = ((h**2) * t1) / (Kw*rho_w*CP_w)
#     X2 = ((h**2) * t2) / (Kw*rho_w*CP_w)
#     #X3 = ((h**2) * t3) / (Kw*rho_w*CP_w)
#     eq1 = 1 - np.exp(X1) *  special.erfc(np.sqrt(X1)) - ((T1 - Ti) / (Tg - Ti))
#     eq2 = 1 - np.exp(X2) *  special.erfc(np.sqrt(X2)) - ((T2 - Ti) / (Tg - Ti))
#     #eq3 = 1 - np.exp(X3) *  special.erfc(np.sqrt(X3)) - ((T3 - Ti) / (Tg - Ti))
#     return(eq1,eq2) 
    
# TTi[0],TTg[0]   = fsolve(equations2,(Ti0,Tg0))
# print(TTi,TTg,)

# # TTi[0],TTg[0]   = fsolve(equations1,(300,310))




# def EEwuation1(i,Ti0,Tg0):
# #    z = Symbol('z')
#     def equations2(z):
#         # z[0] = T2, z[1] = P2
#         #i = 0
#         h =  h_f(x[0])
#         Ti,Tg = z
#         t1 = t[i]
#         t2 = t[i + 1]
#         #t3 = t[i + 2]
#         T1 = Tz1[i] +  273.15
#         T2 = Tz1[i+1] +  273.15
#         #T3 = Tz1[i + 2] +  273.15
#         X1 = ((h**2) * t1) / (Kw*rho_w*CP_w)
#         X2 = ((h**2) * t2) / (Kw*rho_w*CP_w)
#         #X3 = ((h**2) * t3) / (Kw*rho_w*CP_w)
#         eq1 = 1 - np.exp(X1) *  special.erfc(np.sqrt(X1)) - ((T1 - Ti) / (Tg - Ti))
#         eq2 = 1 - np.exp(X2) *  special.erfc(np.sqrt(X2)) - ((T2 - Ti) / (Tg - Ti))
#         #eq3 = 1 - np.exp(X3) *  special.erfc(np.sqrt(X3)) - ((T3 - Ti) / (Tg - Ti))
#         return(eq1,eq2) 
#     return(fsolve(equations2,(Ti0,Tg0)))


# count = -1
# #z = Symbol('z')
# for i in ai:
#     # Ti = Symbol('Ti')
#     # Tg = Symbol('Tz')
#     count = count + 1
#     TTi[count+1],TTg[count+1]  = EEwuation1(i,TTi[count],TTg[count])
    

  
# Tz1 = np.zeros(len(EXP1[:,1]))
# Tz2 = np.zeros(len(EXP1[:,1]))        
# for i in range(len(EXP1[:,1])):
#     Tz1[i] = np.average(EXP1[i,1:5])
#     Tz2[i] = np.average(EXP2[i,1:5])
    
    
    
# flight_conditions = pd.DataFrame(data={'Time[s]':EXP1[:,0]})
# flight_conditions['T1[K]'] = Tz1
# flight_conditions['T2[K]'] = Tz2
# Xcr = x[440]
# Timecr = x[440]
# ax = plt.gca()
# ax.grid()
# flight_conditions.plot(kind='line',x='Time[s]',y='T1[K]' ,ax=ax)
# flight_conditions.plot(kind='line',x='Time[s]',y='T2[K]' ,ax=ax)
# #ax.set_ylabel('Mach Number')
# plt.show()

