######################################################################################
#Python code for Test learning in Heat Transfer, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import fsolve
from scipy import special
import pandas
from sympy import Symbol
import pandas as pd



#q = (762.05 / (4000**4)) * (3500**4)

# F47 = -(0.007790 - 0.140256)
# F7intf = 1 -  0.140256
# e = 0.2 * 0.007790 + 0.8 * F47 + 0.2 * F7intf
D = 90 * (10**-3)
l = 135 * (10**-3)
T1 = 550 
T2 = 300 
J1 = 7142.7
A1 = 0.08





epsi1 = 0.8
Sigma = 5.67*(10**-8)
Eb1 = Sigma * (T1**4)
Eb2 = Sigma * (T2**4)
#Re1 = (1 - epsi1) / (epsi1 * A1)
#q1 = (Eb1 - J1) / Re1



def q_one(Eb1,Eb2,epsi1,epsi2,F12,F1R,FR2,A1,A3):
    a = (1 - epsi1) / (epsi1 * A1)
    b = 1 / ((A1*F13) + (( (1/(A1*F12)) + (1/(A3*FR2)))**-1)) 
    c = (1 - epsi2) / (epsi2 * A2)
    q = (Eb1 - Eb2) / (a + b )
    return q

A1 = 0.08
A2 = 0.3
A3 = 0.010

F12 = 0.1096
F13 = 0.1096
F32 = 0.123

epsi1 = 0.8
epsi2 = 0.6
epsi3 = 1

q1 = q_one(Eb1,Eb2,epsi1,epsi2,F12,F13,F32,A1,A3)


# h = 45.4
# Too = 30 + 273
# Ts = 65 + 273
# A = 0.12
# q = A*h*(Ts - Too)



# Sigma = 5.67*(10**-8)
# # T = 60 + 273
# # Q = Sigma * (T**4)



# T1 = 100 + 273
# T2 = 500 + 273
# T3 = T2

# epsi1 = 0.15
# epsi2 = 0.5
# epsi3 = 0.5

# A1 = 0.5
# A2 = 0.3
# A3 = 0.4

# Eb1 = Sigma * (T1**4)
# Eb2 = Sigma * (T2**4)
# Eb3 = Sigma * (T3**4)

# F12 = (A1 + A2 - A3) / (2*A1)
# F13 = (A1 + A3 - A2) / (2*A1)
# F23 = (A2 + A3 - A1) / (2*A2)


# F31 = (A1/A3) * F13
# F21 = (A1/A2) * F12
# F32 = (A2/A3) * F23

# Re1 = (1 - epsi1) / (epsi1 * A1)
# Re2 = (1 - epsi2) / (epsi2 * A2)
# Re3 = (1 - epsi3) / (epsi3 * A3)

# B1 = (A1*F12) + A1*F13
# B2 = (A2*F21) + A2*F23
# B3 = (A3*F31) + A3*F32

# C1 = -1 - Re1*B1
# C2 = -1 - Re2*B2
# C3 = -1 - Re3*B3


# Eb1 = Sigma * (T1**4)
# Eb2 = Sigma * (T2**4)
# Eb3 = Sigma * (T3**4) 


# Sigma = 5.67*(10**-8)

# epsi1 = 0.7
# epsi2 = 1
# epsi3 = 1

# T1 = 1000 #[K]
# #T2 = 400 #[K]

# # R = 1 #[m]
# # K = 150 #[W/mK]
# # Too = 27 + 273.13




# def q_one(Eb1,Eb2,epsi1,epsi2,F12,F1R,FR2,A1,A3):
#     a = (1 - epsi1) / (epsi1 * A1)
#     b = 1 / ((A1*F12) + (( (1/(A1*F1R)) + (1/(A3*FR2)))**-1)) 
#     c = (1 - epsi2) / (epsi2 * A2)
#     q = (Eb1 - Eb2) / (a + b + c)
#     return q
    
# def q_two(Eb1,epsi1,epsi2,F12,F1R,FR2,A1,A3,q):
#     a = (1 - epsi1) / (epsi1 * A1)
#     b = 1 / ((A1*F12) + (( (1/(A1*F1R)) + (1/(A3*FR2)))**-1)) 
#     c = (1 - epsi2) / (epsi2 * A2)
#     Eb2 = Eb1 -  q * (a + b + c)    
#     return Eb2


# D1 = 0.040 #[m]
# D2 = 0.020 #[m]

# A1 = np.pi * (D1**2 / 4)
# A2 = np.pi * (D2**2 / 4)


# # A3 = R * (np.pi / 2)
# # A4 = np.sqrt(2)


# F12 = 0.03348
# F21 = 0.1339
# F13 = 0.9665
# F23 = 0.8661

# Eb1 = Sigma * (T1**4)
# # Eb2 = Sigma * (T2**4)
# q = 2.76

# Eb2 = q_two(Eb1,epsi1,epsi2,F12,F13,F23,A1,A2,q)
# Eb22 = 45.30  

# q2 = q_one(Eb1,Eb22,epsi1,epsi2,F12,F13,F23,A1,A2)



# # q = 21400
# # Re1 = (1 - epsi1) / (epsi1 * A1)
# # J1 = Eb1 - Re1*q1

# # q2 = -q
# # Re2 = (1 - epsi2) / (epsi2 * A2)
# # Re2 = 0.583j
# # J2 = Eb2 - Re2*q2

# # J3 = (( (1/(A1*F13)) + (1/(A3*F32)))**-1) * ((J1 / (1/(A1*F13)) ) + (J2 / (1/(A3*F32)))) 
# # Eb3 = J3
# # T3 = (Eb3 / Sigma)**(0.25)


















# R = 287
# gamma = 1.4
# bartopa = 100 * (10**3)
# Pr = 0.71
# Too = 288 #[K]
# Poo = 101325 #[pa]
# uoo = 3402 

# rl = np.sqrt(Pr)
# rt = Pr**(1/3)
# Moo = uoo / np.sqrt(gamma*R*Too) 
# Tr_l = Too * (1 + rl * ((gamma - 1) / 2) * (Moo**2))
# Tr_t = Too * (1 + rt * ((gamma - 1) / 2) * (Moo**2))

# Taw = rl*(Tr_l - Too) + Too

# mu = 1.002 * (10**-3)
# v = 3.22 * (10**-7)
# Cp = 4.183 * (10**3)
# K = 0.603
# Pr = mu * Cp / K

# v = 3.22 * (10**-7)
# rho = 965
# Cp = 4208
# K = 0.676
# mu = v * rho
# Pr = mu * Cp / K

# P = 1 * bartopa
# T = 20 + 273.13
# R = 287
# rho = P / (T * R)
# v = 1.56 * (10**-5)
# Cp = 1005
# K = 0.026
# mu = v * rho
# Pr = mu * Cp / K

# mu = 8.36 * (10**-2)
# v = 3.22 * (10**-7)
# Cp = 2035
# K = 0.141
# Pr = mu * Cp / K

# mu = 4.63 * (10**-5)
# v = 3.22 * (10**-7)
# Cp = 1175
# Pr = 0.71
# K = mu * Cp / Pr
# h_bar = 1000
# L = 40 * (10**-3)
# Nu = h_bar * L / K

# h = 100
# L = 438.7 * (10**-3)
# K = 0.025
# Pr = 0.71
# Nu = h * L / K
# Re_lam =  (Nu / (0.644 * (Pr**(1/3))))**2
# Re_turb =  (Nu / (0.037 * (Pr**(0.43))))**(10/8)
# Re_crtic = 1 * (10**7)
# Re_lam - Re_crtic

