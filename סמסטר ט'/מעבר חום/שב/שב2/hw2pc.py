######################################################################################
#Python code for homework 2 in Heat Transfer, Dor Ben Meir 305702490
######################################################################################

import numpy as np

Sigma = 5.67 * (10**-8)
Ta = 480 #[k]
h_rad = 4 * Sigma * (Ta**3)

Lair = 1 * (10**-3) #[m]
Ls = 0.8 * (10**-3) #[m]
Lmb = 0.55 * (10**-3) #[m]
Ltl = 3.5 * (10**-3) #[m]

Kair = 0.038 # As funtion of Ta
Ks = 0.047
Kmb = 0.012
Ktl = 0.038

a = Ls / Ks
b = (h_rad + (Kair/Lair))**-1
c = Lmb / Kmb
d = b
e = Ltl / Ktl

Tb = 66 + 273 #[K]
qin_rad = 0.25 * (10**4) #[W/m^2]
Rth = a + b + c + d + e

T_surf = Tb + qin_rad*Rth - 273
