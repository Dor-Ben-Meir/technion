######################################################################################
#Python code for homework 5 in Heat Transfer, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import fsolve
from scipy import special
import pandas
from sympy import Symbol
import pandas as pd




import math
import numpy as np
from numpy import linalg as LA
from scipy.integrate import odeint
import astropy.units as u
import scipy as sci
from scipy.optimize import fsolve
import pandas as pd
import matplotlib.pyplot as plt


Voo = 5.5 
kgas = 0.05 
vgas = 50 * (10**-6)
Pr_gas = 0.69
D = 10**-3 #[m]
As = np.pi * (D**2)
kTh = 100 
Cp_Th = 385 
rho_Th = 8920 
Sigma = 5.67 * (10**-8)

Re_f = lambda V,Ni: (V * D) / Ni
Nu_f = lambda V,Ni,Pr: 2 + (0.6 * (Re_f(V,Ni)**0.5) * (Pr**(1/3)))
h_f = lambda V,Ni,K,Pr: (Nu_f(V,Ni,Pr) * K) / D
t_f = lambda V,Ni,K,Pr: np.log(1/0.02) * (rho_Th * Cp_Th * (D/6) / h_f(V,Ni,K,Pr)) 

Re1 = Re_f(Voo,vgas)
Nu1 = Nu_f(Voo,vgas,Pr_gas)
h1 = h_f(Voo,vgas,kgas,Pr_gas)
t1 = t_f(Voo,vgas,kgas,Pr_gas)

ans1 = dict(t1 = t1 * u.s)




V = np.arange(1, 25 + 0.1   ,0.1) 
T_vec = np.zeros(len(V))
h_vec =  h_f(V,vgas,kgas,Pr_gas)

Too = 1000
Tc = 400 
epsi_TH = 0.5
T0 = 910

for i, h in enumerate(h_vec):
    def equations(T):
        eq1 = h * As * (Too - T) - (epsi_TH * As * Sigma * (T**4 - Tc**4))
        return(eq1) 
        
    T_vec[i] = fsolve(equations,(T0))

Heat_conf = pd.DataFrame(data={'V[m/s]':V})
Heat_conf['Ts'] = T_vec
ax = plt.gca()
ax.grid()
ax.set_title('Thermo-Couple Ts as function of Velocity', fontsize=20)
Heat_conf.plot(kind='line',x='V[m/s]',y='Ts' ,ax=ax)
ax.set_ylabel('Ts[K]')
plt.show()


Epsi = np.arange(0.1, 1 + 0.01   ,0.01) 
T_vec2 = np.zeros(len(Epsi))
T0 = 980
h = h_f(Voo,vgas,kgas,Pr_gas)
for i, epsi_TH in enumerate(Epsi):
    def equations(T):
        eq1 = h * As * (Too - T) - (epsi_TH * As * Sigma * (T**4 - Tc**4))
        return(eq1) 
        
    T_vec2[i] = fsolve(equations,(T0))

Heat_conf2 = pd.DataFrame(data={'epsi':Epsi})
Heat_conf2['Ts'] = T_vec2
ax = plt.gca()
ax.grid()
ax.set_title('Thermo-Couple Ts as function of Emissivity', fontsize=20)
Heat_conf2.plot(kind='line',x='epsi',y='Ts' ,ax=ax)
ax.set_ylabel('Ts[K]')
plt.show()
















def qq1():
    D = 10**-3 #[m]
    
    Pr_air = 0.706
    Pr_water = 4.85
    Pr_Oil = 4000 
    
    K_air = 0.0269
    K_water = 0.625
    K_Oil = 0.145
    
    
    v_air = 16.69 * (10**-6)
    v_water = 0.729 * (10**-6)
    v_Oil = 340 * (10**-6)
    
    Ts = 60 + 273
    Too = 20 + 273
    
    V = np.arange(0.5, 10 + 0.1   ,0.1) 
    
    Re_f = lambda V,Ni: (V * D) / Ni
    Nu_f = lambda V,Ni,Pr: 0.3 + (0.62 * (Re_f(V,Ni)**0.5) * (Pr**(1/3))) * ((1 + ((Re_f(V,Ni) / 282000)**(5/8)))**(4/5)) / ((1 + (0.4/Pr)**(2/3))**(1/4))
    h_f = lambda V,Ni,K,Pr: (Nu_f(V,Ni,Pr) * K) / D
    q_f = lambda V,Ni,K,Pr: h_f(V,Ni,K,Pr) * np.pi * D * (Ts - Too)
    
    
    Heat_conf = pd.DataFrame(data={'V[m/s]':V})
    Heat_conf['q_Air'] = q_f(V,v_air,K_air,Pr_air) 
    Heat_conf['q_Water'] = q_f(V,v_water,K_water,Pr_water)
    Heat_conf['q_Oil'] = q_f(V,v_Oil,K_Oil,Pr_Oil) 
    ax = plt.gca()
    ax.grid()
    ax.set_title('Heat flux as function of Velocity', fontsize=20)
    Heat_conf.plot(kind='line',x='V[m/s]',y='q_Air' ,ax=ax)
    Heat_conf.plot(kind='line',x='V[m/s]',y='q_Water' ,ax=ax)
    Heat_conf.plot(kind='line',x='V[m/s]',y='q_Oil' ,ax=ax)
    ax.set_ylabel('q''[W/m]')
    plt.show()
    























def q2():
    
    Clam = 8.845
    Cturb = 49.75
    Xcr = 0.55 #[m]
    x  = np.arange( 0.01, 1 + 0.01   ,0.01)  
    
    def h_Local(x):
        h = np.zeros(len(x))
        for i, xx in enumerate(x):
    
            if(xx < Xcr):
                h[i]= Clam * (xx**-0.5)
            else:
                h[i] = Cturb * (xx**-0.2)
        return(h)
        
    
    def h_Avg(x):
        h = np.zeros(len(x))
        for i, xx in enumerate(x):
    
            if(xx < Xcr):
                h[i]= (2*Clam/xx) * (xx**-0.5)
            else:
                h[i] = (1/xx) * (2 * Clam * (xx**0.5) + 1.25 * Cturb * ((xx**0.8) - (Xcr**0.8)))
        return(h)
    
    h_local = h_Local(x)
    h_avg = h_Avg(x)
    
    Heat_conf = pd.DataFrame(data={'X[m]':x})
    Heat_conf['h_local'] =  h_local
    Heat_conf['h_avg'] = h_avg
    ax = plt.gca()
    ax.grid()
    ax.set_ylim(0,150)
    Heat_conf.plot(kind='line',x='X[m]',y='h_local' ,ax=ax)
    Heat_conf.plot(kind='line',x='X[m]',y='h_avg' ,ax=ax)
    plt.show()