######################################################################################
#Python code for homework 3 in Heat Transfer, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation





def qq2():
    Too = 1200
    h = 250
    k = 20
    L = 50 * (10**-3)
    Ac = 6 * (10**-4)
    P = 110 * (10**-3)
    Tb = 300
    
    m = np.sqrt((h*P)/(k*Ac))
    Tmax = Too + (Tb - Too)/np.cosh(m*L)
    
    qf = -k * Ac * ((Tb - Too)/np.cosh(m*L)) * -m * np.sinh(m*L)
    qb = - qf
    ans1 = dict(Tmax = Tmax, qb = qb)
    return




D1 = 0.5 #[m]
t = 0.1 #[m]
z = 1.5 #[m]
Tsur = 0
Toil = 120

#Table A.3:
K_soil = 0.52
K_glass = 0.069

n_soil = np.log(4*z / (D1 + 2*t)) / K_soil
n_glass = np.log((D1 + 2*t) / D1) / K_glass
q = 2 * np.pi * (Toil - Tsur) / (n_soil + n_glass)











def q1():
    T1 = 50 #[c] 
    L = 2 #[m]
    T2 = 150 # [c]
    W = 1 #[m]
    
    x = 1 
    y = 0.5
    #Thetay = np.zeros(9)
    add = np.zeros(10)
    n = 1
    T9 = 0
    while(T9 < 5):
    
        A = ((((-1)**(n+1)) + 1) / n) * np.sin((n * np.pi * x) / L) * (np.sinh((n * np.pi * y) / L) / np.sinh((n * np.pi * W) / L))
    
        add[n-1] = A
        Thetay = (2/np.pi) * np.sum(add)
        T = Thetay * (T2 - T1) + T1 
        if T != 0:
            T9 = T9 + 1
    
        n = n + 1        
    Thetay = (2/np.pi) * np.sum(add)
    T5 = Thetay * (T2 - T1) + T1 
    
    
    add = np.zeros(10)
    n = 1
    T9 = 0
    while(T9 < 3):
    
        A = ((((-1)**(n+1)) + 1) / n) * np.sin((n * np.pi * x) / L) * (np.sinh((n * np.pi * y) / L) / np.sinh((n * np.pi * W) / L))
    
        add[n-1] = A
        Thetay = (2/np.pi) * np.sum(add)
        T = Thetay * (T2 - T1) + T1 
        if T != 0:
            T9 = T9 + 1
    
        n = n + 1
        
        
    Thetay = (2/np.pi) * np.sum(add)
    T3 = Thetay * (T2 - T1) + T1  
    
    
    epsi = (T5 - T3) / T5
    
    ans1 = dict(T5 = T5, T3 = T3, error = epsi)
    
    X = np.linspace(0.0, 2, num=100) 
    y = 0.5
    Thetax = np.zeros(100)
    add = np.zeros(9)
    
    
    
    n = 1
    i = -1 
    for x in X:    
        n = 1
        i = i + 1 
        while(n < 9):
            A = ((((-1)**(n+1)) + 1) / n) * np.sin((n * np.pi * x) / L) * (np.sinh((n * np.pi * y) / L) / np.sinh((n * np.pi * W) / L))
            add[n-1] = A
            n = n + 1
        Thetax[i] = (2/np.pi) * np.sum(add)
        
    Tx = Thetax * (T2 - T1) + T1 
    
    
    
    Thetay = np.zeros(100)
    add = np.zeros(100)
    Y = np.linspace(0.0, 1, num=100)
    x = 1
    
    n = 1
    i = -1 
    for y in Y:    
        n = 1
        i = i + 1 
        while(n < 9):
            A = ((((-1)**(n+1)) + 1) / n) * np.sin((n * np.pi * x) / L) * (np.sinh((n * np.pi * y) / L) / np.sinh((n * np.pi * W) / L))
            add[n-1] = A
            n = n + 1
        Thetay[i] = (2/np.pi) * np.sum(add)
        
    Ty = Thetay * (T2 - T1) + T1 
    
    
    
    
    X = np.linspace(0.0, 2, num=100) 
    #Plot the results:  
    plt.clf()
    plt.suptitle('T as function of x and y', fontsize=16)
    plt.subplot(2, 2, 1)
    plt.plot(X, Tx)
    plt.grid()
    plt.xlabel('x[m]')
    plt.ylabel('T(c)')
    plt.subplot(2, 2, 2)
    plt.plot(Y, Ty)
    plt.grid()
    plt.xlabel('y[m]')
    plt.ylabel('T[c]')
    plt.tight_layout()
    
    plt.show()
    return



# x = 1 
# y = 0.5

# Thetay = np.zeros(9)
# add = np.zeros(9)
# n = 1
# while(n < 9):
#     A = ((((-1)**(n+1)) + 1) / n) * np.sin((n * np.pi * x) / L) * (np.sinh((n * np.pi * y) / L) / np.sinh((n * np.pi * W) / L))
#     add[n-1] = A
#     n = n + 1
# Thetay = (2/np.pi) * np.sum(add)
# T = Thetay * (T2 - T1) + T1 



# #Operating Conditions
# Engine_Massflow = 75 #kg/s
# Tg =  1800 #K
# Tc1 =  700 #K
# hg  = 2000 #W/(m2K)

# #Blade geometry
# L  = 40 * (10**-3) #m
# c = 45 * (10**-3) #m
# Sg =  2.2 * c #m

# #Cooling system configuration
# nc =  15
# Sc =  6.28*(10**-3) #m
# Ac =  3.14 * (10**-6) #m2

# #Air properties
# mu  = 3.5 * (10**-5) #kg/(ms)
# k = 0.07 #W/(mK)
# Cp =  1100 #J/(kg K)

# mcc = (2/100)*(Engine_Massflow/(15*50))
# Re_d = (1 / mu ) * (mcc / Ac)
# hc = k * 0.013 * (Re_d**0.8)
# X = (nc * hc * Sc) / (hg * Sg)
# mcb = nc * mcc
# m_star = (mcb * Cp) / (hg * Sg * L) 
# Zeta = 1 - np.exp(-X/m_star)
# epsi = (m_star * Zeta) / (1 + m_star*Zeta)
# Tb = Tg - (Tg - Tc1)*epsi

# ans1 = dict(Tb = Tb, nc = nc, Sc = Sc)

# while (Tb > 1200):
#     mcc = (2/100)*(Engine_Massflow/(15*50))
#     Re_d = (1 / mu ) * (mcc / Ac)
#     hc = k * 0.013 * (Re_d**0.8)
#     X = (nc * hc * Sc) / (hg * Sg)
#     mcb = nc * mcc
#     m_star = (mcb * Cp) / (hg * Sg * L) 
#     Zeta = 1 - np.exp(-X/m_star)
#     epsi = (m_star * Zeta) / (1 + m_star*Zeta)
#     Tb = Tg - (Tg - Tc1)*epsi 
#     Sc = Sc + 1* (10**-3)
#     nc = nc + 1
    
# ans2 = dict(Tb = Tb, nc = nc, Sc = Sc)