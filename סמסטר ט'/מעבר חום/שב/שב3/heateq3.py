######################################################################################
#Python code for homework 3 in Heat Transfer, Dor Ben Meir 305702490
######################################################################################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation

Tb =  250 + 273.15 #[K]
Tinf = 20 + 273.15 #[K]
h = 40
K = 237
L = 6 * (10**-3) # [m]
tb = 2 *  (10**-3) # [m]
Ap = tb*L / 2
p = (L**1.5) * np.sqrt(h/(K*Ap))
Ni_f = np.tanh(p)/p

epsi_fin = 2 * Ni_f * np.sqrt(((L/tb)**2) + 0.25)

q = Ni_f * h * (Tb - Tinf) * np.sqrt(4 * (L**2) + (tb**2))

ans1 = dict(epsi_fin = epsi_fin, q = q)