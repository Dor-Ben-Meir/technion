


import cantera as ct
import numpy as np
import matplotlib.pyplot as plt
import math

T1 = 300 # K
P1 =   101325 # Pa
mech = 'gri30_highT.cti'
q = 'O2:0.21, N2:0.78, AR:0.01, CH4:1'

gas2 = ct.Solution(mech)
gas1 = ct.Solution(mech)
gas =ct.Solution(mech)
    #INTIAL CONDITIONS
gas.TPX = T1, P1, q
gas1.TPX =  T1, P1, q
gas2.TPX = T1, P1, q

# P2  =  np.arange( (6.907*(10**5)),(6.907*(10**5)) + 2 ,1)
# T2 = np.arange( 968.2,968.3+ 0.1 ,0.1) 
# flag = 0
# for t2 in T2:
#     for p2 in P2:
#         gas2.TP = t2, p2
#         gas2.equilibrate('TP')
#         gamma2 = gas2.cp/gas2.cv
#         R2 = gas2.cp - gas2.cv
#         R1 = gas1.cp - gas1.cv
#         h1 = gas1.enthalpy_mass
#         h2 = gas2.enthalpy_mass
#         delta_e = (h2 - h1) + (gas1.P/gas1.density) - (gas2.P/gas2.density)
#         if (abs(delta_e - ((R2 * gas2.T) / (2 * gamma2))) < 0.1):
#             rho1 = gas1.density
#             rho2 = gas2.density
#             RHO12 = rho2 / rho1
#             A = (1 / gamma2) + 1 - (R1*gas1.T/ (R2*gas2.T))
#             C = (R1*gas1.T)/ (R2*gas2.T)
#             if(abs(((RHO12**2) - A*RHO12 - C)) < 0.1):
#                 if(abs(gas2.P - RHO12*(1/C) * gas1.P) < 0.1):
#                     Ud = RHO12 * np.sqrt(gamma2*R2*gas2.T)
#                     flag = 1
#                     break
#         if(flag):
#             break


p2  =  (690672)
t2 = 968.263
gas2.TPX = t2, p2,q
gas2.equilibrate('TP')
gamma2 = gas2.cp/gas2.cv
#R2 = gas2.cp - gas2.cv
R2 = 448.244627
R1 = gas1.cp - gas1.cv
h1 = gas1.enthalpy_mass
h2 = gas2.enthalpy_mass
rho1 = gas1.density
rho2 = gas2.density
RHO12 = rho2 / rho1
A = (1 / gamma2) + 1 - (R1*gas1.T/ (R2*gas2.T))
C = (R1*gas1.T)/ (R2*gas2.T)
Ud = RHO12 * np.sqrt(gamma2*R2*gas2.T)