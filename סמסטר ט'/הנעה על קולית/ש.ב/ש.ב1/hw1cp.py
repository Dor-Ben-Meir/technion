
import cantera as ct
import numpy as np
import matplotlib.pyplot as plt
import math
# # Get all of the Species objects defined in the GRI 3.0 mechanism
# species = {S.name: S for S in ct.Species.listFromFile('gri30_highT.cti')}

# # Create an IdealGas object with species representing complete combustion
# complete_species = [species[S] for S in ('CH4','O2','N2','CO2','H2O')]
# gas1 = ct.Solution(thermo='IdealGas', species=complete_species)

# phi = np.linspace(0.5, 1.0, 100)
# T_complete = np.zeros(phi.shape)
# for i in range(len(phi)):
#     gas1.TP = 300, ct.one_atm
#     gas1.set_equivalence_ratio(phi[i], 'CH4', 'O2:1, N2:3.76')
#     gas1.equilibrate('HP')
#     T_complete[i] = gas1.T
    

gas = ct.Solution('gri30_highT.cti')

# Stream A (air)
A = ct.Quantity(gas, constant='HP')
A.TPX = 300.0, ct.one_atm, 'O2:0.21, N2:0.78, AR:0.01'

# Stream B (methane)
B = ct.Quantity(gas, constant='HP')
B.TPX = 300.0, ct.one_atm, 'CH4:1'

# Set the molar flow rates corresponding to stoichiometric reaction,
# CH4 + 2 O2 -> CO2 + 2 H2O
A.moles = 1
nO2 = A.X[A.species_index('O2')]
B.moles = nO2 * 0.5

# Compute the mixed state
M = A + B
#print(M.X)
print(M.report())
# Show that this state corresponds to stoichiometric combustion
M.equilibrate('TP')
print(M.report())




T1 = 300 # K
P1 =   101325 # Pa
mech = 'gri30_highT.cti'
q = 'O2:0.21, N2:0.78, AR:0.01, CH4:1'

gas2 = ct.Solution(mech)
gas1 = ct.Solution(mech)
gas =ct.Solution(mech)
    #INTIAL CONDITIONS
gas.TPX = T1, P1, q
gas1.TPX =  T1, P1, q
gas2.TPX = T1, P1, q


def equilSoundSpeeds(gas):

    """
    equilSoundSpeeds
    Calculates equilibrium and frozen sound speeds. For the equilibrium sound speed, the gas is equilibrated holding entropy and specific volume constant.
    FUNCTION
    SYNTAX
    [aequil,afrozen] = equilSoundSpeeds(gas)
    INPUT
    gas = working gas object (modified inside function)
    OUTPUT
    aequil = equilibrium sound speed (m/s)
    afrozen = frozen sound speed (m/s)
    """

    # set the gas to equilibrium at its current T and P
    gas.equilibrate('TP')

    # save properties
    s0 = gas.entropy_mass
    p0 = gas.P
    r0 = gas.density

    # perturb the density
    r1 = r0*1.0001
 
    # set the gas to a state with the same entropy and composition but
    # the perturbed density
    gas.SV = s0, 1.0/r1

    # save the pressure for this case for the frozen sound speed
    pfrozen = gas.P
        
    # now equilibrate the gas holding S and V constant
    gas.equilibrate("SV")
    
    p1 = gas.P

    # equilibrium sound speed
    aequil = math.sqrt((p1 - p0)/(r1 - r0));

    # frozen sound speed
    afrozen = math.sqrt((pfrozen - p0)/(r1 - r0));    
    return (aequil, afrozen)
   
def FHFP_CJ2(gas,gas1,gas2):

    """
    FHFP_CJ2
    Uses the momentum and energy conservation equations and the equilibrium sound speed to calculate error in current pressure and enthalpy guesses.  In this case, state 2 is in equilibrium.
    
    FUNCTION
    SYNTAX
    [FH,FP,cj_speed] = FHFP_CJ2(gas,gas1,gas2)
    INPUT
    gas = working gas object
    gas1 = gas object at initial state 
    gas2 = dummy gas object (for calculating numerical derivatives)
    
    OUTPUT
    FH,FP = error in enthalpy and pressure
    cj_speed = CJ detonation speed (m/s)
    
    """
    
    P1 = gas1.P
    H1 = gas1.enthalpy_mass
    r1 = gas1.density
    P2 = gas.P
    H2 = gas.enthalpy_mass
    r2 = gas.density
     
    speeds = equilSoundSpeeds(gas2)
    w2s=(speeds[0])**2
    w1s = w2s*(r2/r1)**2
    FH = H2 + 0.5*w2s - (H1 + 0.5*w1s)
    FP = P2 + r2*w2s - (P1 + r1*w1s)
    return [FH, FP, np.sqrt(w1s)]
    
def CJspeed2(P1, T1):

    
    # gas2 = ct.Solution(mech)
    # gas1 = ct.Solution(mech)
    # gas =ct.Solution(mech)
    # #INTIAL CONDITIONS
    # gas.TPX = T1, P1, q
    # gas1.TPX =  T1, P1, q
    # gas2.TPX = T1, P1, q
    gas2 = M
    gas1 = M
    gas = M

    #INITIALIZE ERROR VALUES & CHANGE VALUES    ERRFT = 1.0*10**-4;  ERRFV = 1.0*10**-4;
    ERRFT = 1.0*10**-4;  ERRFV = 1.0*10**-4;
    r1 = gas1.density_mass; V1 = 1/r1;
    P1 = gas1.P; T1 = gas1.T;
    i = 0;
    #PRELIMINARY GUESS
    Vg = V1/10; rg = 1/Vg; 
    gas.TD = T1, rg
    gas.equilibrate('UV')
    Tg = gas.T
    gas2.TDX = Tg, rg, gas.X

    
    #SAVE STATE
    V = Vg; r = rg;
    T = Tg;
    deltaT = 1000; deltaV = 1000; cj_speed = 0;
    #START LOOP
    while(abs(deltaT) > ERRFT*T or abs(deltaV) > ERRFV*V):
        i = i + 1
        if i == 500:
            #print "CJ speed 2 calc did not converge"
            return gas
 
        #CALCULATE FH & FP FOR GUESS 1
        [FH,FP,cj_speed] = FHFP_CJ2(gas,gas1,gas2)


        #TEMPERATURE PERTURBATION
        DT = T*0.01; Tper = T + DT;
        Vper = V; Rper = 1/Vper;
        
        gas.TD = Tper, Rper
        gas.equilibrate('TV',2)
        gas2.TDX = Tper, Rper, gas.X

        #CALCULATE FHX & FPX FOR "IO" STATE
        [FHX,FPX,cj_speed] = FHFP_CJ2(gas,gas1,gas2)
        #ELEMENTS OF JACOBIAN
        DFHDT = (FHX-FH)/DT; DFPDT = (FPX-FP)/DT;

        #VOLUME PERTURBATION
        DV = 0.01*V; Vper = V + DV;
        Tper = T; Rper = 1/Vper;
        
        gas.TD = Tper, Rper
        gas.equilibrate('TV',2)
        gas2.TDX = Tper, Rper, gas.X
        
        #CALCULATE FHX & FPX FOR "IO" STATE
        [FHX,FPX,cj_speed] = FHFP_CJ2(gas,gas1,gas2)
        #ELEMENTS OF JACOBIAN
        DFHDV = (FHX-FH)/DV; DFPDV = (FPX-FP)/DV;

        #INVERT MATRIX
        J = DFHDT*DFPDV - DFPDT*DFHDV
        b = [DFPDV, -DFHDV, -DFPDT, DFHDT]
        a = [-FH, -FP]
        deltaT = (b[0]*a[0]+b[1]*a[1])/J; deltaV = (b[2]*a[0]+b[3]*a[1])/J;

        #CHECK & LIMIT CHANGE VALUES
        #TEMPERATURE
        DTM = 0.2*T
        if abs(deltaT) > DTM:
            deltaT = DTM*deltaT/abs(deltaT)
        #VOLUME
        V2X = V + deltaV
        if V2X > V1:
            DVM = 0.5*(V1 - V)
        else:
            DVM = 0.2*V
        if abs(deltaV) > DVM:
            deltaV = DVM*deltaV/abs(deltaV)
        #MAKE THE CHANGES
        T = T + deltaT; V = V + deltaV; r = 1/V;
        gas.TD = T, r
        gas.equilibrate('TV',2)
        gas2.TDX = T, r, gas.X

    [FH,FP,cj_speed] = FHFP_CJ2(gas,gas1,gas2)
    
    return [gas,cj_speed]


CJspeed2(P1, T1)

    
# gas2 = ct.Solution(mech)
# gas1 = ct.Solution(mech)
# gas = ct.Solution(mech)
#     #INTIAL CONDITIONS
# gas.TPX = T1, P1, q
# gas1.TPX =  T1, P1, q
# gas2.TPX = T1, P1, q