% Shock and Detonation Toolbox Demo Program
% 
% Calculates the solution to ideal shock tube problem. Three cases possible: 
%     conventional nonreactive driver (gas), 
%     constant volume combustion driver (uv),
%     CJ detonation (initiate at diaphragm) driver (cj)
% 
% Creates P-u relationship for shock wave and expansion wave, plots results
% and interpolates to find shock speed and states 2 and 3 given states 1
% and 4.  Creates tabulation of relevant states for solution.
%  
% ################################################################################
% Theory, numerical methods and applications are described in the following report:
% 
%     Numerical Solution Methods for Shock and Detonation Jump Conditions, S.
%     Browne, J. Ziegler, and J. E. Shepherd, GALCIT Report FM2006.006 - R3,
%     California Institute of Technology Revised September, 2018.
% 
% Please cite this report and the website if you use these routines. 
% 
% Please refer to LICENCE.txt or the above report for copyright and disclaimers.
% 
% http://shepherd.caltech.edu/EDL/PublicResources/sdt/
% 
% ################################################################################ 
% Updated September 2018
% Tested with: 
%     MATLAB 2017b and 2018a, Cantera 2.3 and 2.4
% Under these operating systems:
%     Windows 8.1, Windows 10, Linux (Debian 9)
%%
clear;clc;
%% Select case for the driver operations
%CASE_DRIVER = 'gas';   % nonreactive gas driver
CASE_DRIVER = 'uv';    % constant pressure gas driver
%CASE_DRIVER = 'cj';    % detonation driver (initiation at diaphragm)
%% Select case for the driven gas operations (use equilibrium only for very strong shocks
CASE_DRIVEN = 'frozen';  %non reactive driven gas
%CASE_DRIVEN = 'equilibrium';  %reactive driven gas
disp(['demo_ShockTube, driver case: ',CASE_DRIVER,' driven case: ',CASE_DRIVEN]);
%% set initial state, composition, and gas object for driven section
P1 = oneatm; T1 = 298.; 
q1 = 'O2:1.0 N2:3.76'; 
%q1 = 'CO2:1.';
driven_mech = 'airNASA9noions.cti';
driven_gas = Solution(driven_mech);
set(driven_gas,'Temperature',T1,'Pressure',P1,'MoleFractions',q1);
%% Evaluate initial state 
rho1 = density(driven_gas);
a1 = soundspeed_fr(driven_gas);
%% Evaluate post-shock state (frozen) for a range of shock speeds 
i = 1;
disp(['Generating points on shock P-u curve']);
for U = a1*1.01:25:8*a1
    if strcmp(CASE_DRIVEN,'frozen')
        [shocked_gas] = PostShock_fr(U, P1, T1, q1, driven_mech);
    elseif strcmp(CASE_DRIVEN,'equilibrium')
        [shocked_gas] = PostShock_eq(U, P1, T1, q1, driven_mech);
    end
    a2(i) = soundspeed_fr(shocked_gas);
    P2(i) = pressure(shocked_gas);
    T2(i) = temperature(shocked_gas);
    rho2(i) = density(shocked_gas);
    w2 = rho1*U/density(shocked_gas);
    u2(i) = U - w2;
    Us(i) = U;
    i = i +1;
end
ismax = i-1;
if strcmp(CASE_DRIVER,'gas')
%% Set initial state for driver section - pressurized gas and no reaction
% for nonreacting driver, use frozen expansion
    EQ_EXP = false;
    P_driver = 30.*oneatm; T_driver = 298.; 
    q4 = 'He:1.0';    
    driver_mech = 'Mevel2017.cti';
    driver_gas = Solution(driver_mech);
    set(driver_gas,'Temperature',T_driver,'Pressure',P_driver,'MoleFractions',q4);
    P4 = pressure(driver_gas);
    T4 = temperature(driver_gas);
    a3(1) = soundspeed_fr(driver_gas);
    u3(1) = 0.;
elseif  strcmp(CASE_DRIVER,'uv') 
%% Set initial state for driver section - CV combustion driven shock tube 
% using constant volume state approximation 
% for reacting driver, use equilibrium expansion
    EQ_EXP = true;
    P_driver_fill = P1; T_driver_fill = T1;
    q4 = 'C2H2:1.0 O2:2.5';    
    driver_mech = 'gri30_highT.cti';
    driver_gas = Solution(driver_mech);
    set(driver_gas,'Temperature',T_driver_fill,'Pressure',P_driver_fill,'MoleFractions',q4);
    equilibrate(driver_gas,'UV');
    P4 = pressure(driver_gas);
    T4 = temperature(driver_gas);
    a3(1) = soundspeed_eq(driver_gas);
    u3(1) = 0.;
elseif strcmp(CASE_DRIVER,'cj')
%% Detonation driver with CJ wave moving away from diaphragm
    EQ_EXP = true;
    P_driver_fill = P1; T_driver_fill = T1;
    q4 = 'C2H2:1.0 O2:2.5';
    driver_mech = 'gri30_highT.cti';
    driver_gas = Solution(driver_mech);
    set(driver_gas,'Temperature',T_driver_fill,'Pressure',P_driver_fill,'MoleFractions',q4);
    rho4 = density(driver_gas);
    [cj_speed] = CJspeed(P_driver_fill, T_driver_fill, q4, driver_mech);
    [driver_gas] = PostShock_eq(cj_speed,P_driver_fill, T_driver_fill, q4, driver_mech);
    P4 = pressure(driver_gas);
    T4 = temperature(driver_gas);
    a3(1) = soundspeed_eq(driver_gas);
    w3 = cj_speed*rho4/density(driver_gas);
    u3(1) = w3-cj_speed;
end
%% Evaluate initial state for expansion computation
rho3(1) = density(driver_gas);
P3(1) = pressure(driver_gas);
T3(1) = temperature(driver_gas);
S4 = entropy_mass(driver_gas);
%% compute unsteady expansion (frozen)
disp(['Generating points on isentrope P-u curve']);
i = 1;
vv = 1/rho3(1);
while (u3(i)<u2(ismax)) 
    i = i+1;
    vv = vv*1.01;
    sv = [S4, vv];
    setState_SV(driver_gas,sv);
     if EQ_EXP
         %required for equilibrium expansion
         equilibrate(driver_gas,'SV');   
         a3(i) = soundspeed_eq(driver_gas);  
     else
         %use this for frozen expansion
         a3(i) = soundspeed_fr(driver_gas);
     end
    P3(i) = pressure(driver_gas);
    T3(i) = temperature(driver_gas);
    rho3(i) = density(driver_gas);
    u3(i) = u3(i-1) - 0.5*(P3(i)-P3(i-1))*(1./(rho3(i)*a3(i)) + 1./(rho3(i-1)*a3(i-1)));
end
%  Input limits for finding intersection of polars
pmin = 1.01;
pmax = max(P3)/P1;
% define intersection function
fun = @(x) (interp1(P2/P1,u2,x,'pchip')-interp1(P3/P1,u3,x,'pchip'))^2;  
% solve for intersection of P-u curves to find pressure at contact surface
P_contact = fminbnd(@(x) fun(x), pmin, pmax);
% find shock speed
U_shock = interp1(P2/P1,Us,P_contact,'pchip');
M_shock = U_shock/a1;
% find velocity and states on driven side of contact surface
u2_contact = interp1(P2/P1,u2,P_contact,'pchip');
T2_contact = interp1(P2/P1,T2,P_contact,'pchip');
a2_contact = interp1(P2/P1,a2,P_contact,'pchip');
rho2_contact = interp1(P2/P1,rho2,P_contact,'pchip');
M2 = u2_contact/a2_contact;
% find velocity and states on driver side of contact surface
u3_contact = interp1(P3/P1,u3,P_contact,'pchip');
T3_contact = interp1(P3/P1,T3,P_contact,'pchip');
a3_contact = interp1(P3/P1,a3,P_contact,'pchip');
rho3_contact = interp1(P3/P1,rho3,P_contact,'pchip');
M3 = u3_contact/a3_contact;
%%
% Display results on screen
disp(['Driven State (1)']);
disp(['   Fill gas ',q1]);
disp(['   Pressure ',num2str(P1/1E3),' (kPa)']);
disp(['   Temperature ',num2str(T1),' (K)']);
disp(['   Sound speed ',num2str(a1),' (m/s)']);
disp(['   Density ',num2str(rho1),' (kg/m3)']);
if strcmp(CASE_DRIVER,'cj')
    disp(['Driver State (CJ)']);
    disp(['   Fill gas ',q4]);
    disp(['   Fill Pressure ',num2str(P_driver_fill/1E3),' (kPa)']);
    disp(['   Fill Temperature ',num2str(T_driver_fill),' (K)']);
    disp(['   CJ speed ',num2str(cj_speed),' (m/s)']);
elseif strcmp(CASE_DRIVER,'uv')
    disp(['Driver State (UV)']);
    disp(['   Fill gas ',q4]);
    disp(['   Fill Pressure ',num2str(P_driver_fill/1E3),' (kPa)']);
    disp(['   Fill Temperature ',num2str(T_driver_fill),' (K)']);
elseif strcmp(CASE_DRIVER,'gas')
    disp(['Driver State (4)']);
    disp(['   Fill gas ',q4]);
end
disp(['   Pressure ',num2str(P4/1E3),' (kPa)']);
disp(['   Temperature ',num2str(T4),' (K)']);
disp(['   Sound speed ',num2str(a3(1)),' (m/s)']);
disp(['   Density ',num2str(rho3(1)),' (kg/m3)']);
disp(['Solution matching P-u for states 2 & 3']);
disp(['Shock speed ',num2str(U_shock),' (m/s)']);
disp(['Shock Mach number ',num2str(M_shock),' ']);
disp(['Shock Pressure ratio ',num2str(P_contact),' ']);
disp(['Postshock state (2) in driven section']);
disp(['   Pressure ',num2str(P1*P_contact/1E3),' (kPa)']);
disp(['   Velocity ',num2str(u2_contact),' (m/s)']);
disp(['   Temperature ',num2str(T2_contact),' (K)']);
disp(['   Sound speed ',num2str(a2_contact),' (m/s)']);
disp(['   Density ',num2str(rho2_contact),' (kg/m3)']);
disp(['   Mach number ',num2str(M2),' ']);
set(shocked_gas,'Temperature',T2_contact,'Pressure',P1*P_contact,'MoleFractions',q1);
disp(['Expansion state (3) in driver section']);
disp(['   Pressure ',num2str(P1*P_contact/1E3),' (kPa)']);
disp(['   Velocity ',num2str(u3_contact),' (m/s)']);
disp(['   Temperature ',num2str(T3_contact),' (K)']);
disp(['   Sound speed ',num2str(a3_contact),' (m/s)']);
disp(['   Density ',num2str(rho3_contact),' (kg/m3)']);
disp(['   Mach number ',num2str(M3),' ']);
Z = rho2_contact*a2_contact/(rho3_contact*a3_contact);
disp(['Impedance ratio at contact surface (rho a)_2/(rho a)_3 ',num2str(Z),' ']);
%%
% plot pressure-velocity diagram
	figure(1); clf;
    hold on
    axis([u3(1) u2(ismax) 0 P4/P1]);
	plot(u2(:),P2(:)/P1,'LineWidth',2);  
	plot(u3(:),P3(:)/P1,'LineWidth',2);  
	title(['Shocktube P-u relations'],'FontSize', 12);
	xlabel('Velocity (m/s)','FontSize', 12);
	ylabel('pressure P/P1','FontSize', 12);
	set(gca,'FontSize',12,'LineWidth',2);
    legend('Driven shock','Driver expansion','location','North');
    hold off
