%% MATLAB FILE FOR READING THE RECORDINGS OF THE SERVO SIMULATION 
clc
clear all
close all

%% plots and graphs and whatever
load('5degservoresults');
lw = 2.1;
% data = px4_read_binary_file('pwm.bin');
ans = data;
figure(1)
plot(ans(1,:), ans(2,:));
title('Recording of the signals from the PIXHAWK');
xlabel('Time [sec]'); ylabel('PWM signals'); grid;

figure(2)
plot(ans(1,:), ans(3,:));

v = (ans(3,:)-1.5)*(4800/3); % Number of ticks
angle = (360/4800)*v+58.245; % Angle from number of ticks
pwm2angle = -(ans(2,:)-1500)/10.1512; % The PWM commands to the command of the angles
figure(3)
plot(ans(1,:),angle);
hold on
plot(ans(1,:), pwm2angle);
grid;
legend('output', 'input');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% evaluating the transfer function

output10 = angle(37:2501); % bias for the first 10 seconds 
avg_output10 = sum(output10)/length(output10); 


figure;
plot(ans(1,:),angle-avg_output10-0.6, 'LineWidth', lw);
hold on
plot(ans(1,:), pwm2angle, 'LineWidth', lw);
grid;
legend('output', 'input');
time=ans(1,:);
f = 0.25:0.25:11.75;

for i = 2:48
    output(i,:) = (angle((2501)*(i-1) : (2501)*(i-1)+2500))-avg_output10;
    input(i,:) = pwm2angle((2501)*(i-1) : (2501)*(i-1)+2500);  
    t(i,:) = time((2501)*(i-1) : (2501)*(i-1)+2500);
%     figure(i+2)
%     plot(t(i,:),output(i,:));
%     hold
%     plot(t(i,:),input(i,:));
end

for ii=2:size(output,1)
[frq(ii),amp(ii)] = sinfapm(output(ii,:),250,'Xtol',1e-4);
end

amp = amp(2:48);
k_dB = 20*log10(amp/5); %[dB]
w = 2*pi*f; %[rad/s]

figure(5)
semilogx(w,k_dB);
% hold on
% s = tf('s');
% % w = 34.749; %[rad/sec]
% % % w = 60.479; %[rad/sec]
% % z = 0.819677; % damping ratio
% % H = w^2/(w^2+2*z*w*s+s^2); % Servo transfer function
% 
% H = 4019/(s^2+86.06*s+4019);
% bode(H);
% title('servo savox 1232MG bode gain graph');
% grid;

