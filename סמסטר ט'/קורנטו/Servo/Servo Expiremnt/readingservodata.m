%% FRESH START OF THE SERVO EXPIREMENT
% MATLAB FILE FOR READING THE RECORDINGS OF THE SERVO SIMULATION 
clc
clear 
close all

%% plots and graphs and whatever
lw = 2.1;
data = px4_read_binary_file('pwm.bin'); % Reading the data recorded from the SD card of the PX2.1Cube
ans = data;

% ans(1,:) - time[sec]
% ans(2,:) - PWM signals from the pixhawk
% ans(3,:) - analog input from the encoder [volts]


figure(1)
plot(ans(1,:), ans(2,:));
title('Recordings of the PWM signals from the PIXHAWK');
xlabel('Time [sec]'); ylabel('PWM signals'); grid;

figure(2)
plot(ans(1,:), ans(3,:));
grid; title('Recordings of the analog signals from the encouder');
xlabel('Time [sec]'); ylabel('analog signals [volts]');

N = (ans(3,:)-1.5)*(4800/3); % Number of ticks (encoder property)
angle = (360/4800)*N+58.245; % Angle from number of ticks (output - [deg])
pwm2angle = -(ans(2,:)-1500)/10.1512; % PWM commands to angle commands [deg]


final_data.PWM = pwm2angle;
final_data.Time = data(1,:);
final_data.output = angle;


output10 = angle(37:2502); % bias for the first 10 seconds 
avg_output10 = sum(output10)/length(output10); 

figure(3)
plot(ans(1,:), pwm2angle);
hold on
plot(ans(1,:), angle-avg_output10);
grid;
legend('input', 'output');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %% evaluating the transfer function

% figure;
% plot(ans(1,:),angle-avg_output10, 'LineWidth', lw);
% hold on
% plot(ans(1,:), pwm2angle, 'LineWidth', lw);
% grid;
% legend('output', 'input');

% output_cut=angle(2502:120001);



time = ans(1,:);
for i = 2:48
    output(i,:) = (angle( (2502)+(i-2)*2500 : (2502)+(i-2)*2500+2499)) - avg_output10;
    input(i,:) = pwm2angle( (2502)+(i-2)*2500 : (2502)+(i-2)*2500+2499 );  
    t(i,:) = time( (2502)+(i-2)*2500 : (2502)+(i-2)*2500+2499 );
    smoo(i,:)=smoothdata(output(i,:));
    [frq(i),amp(i)] = sinfapm(smoo(i,:),250);
% % %     figure(i+2)
% % %     plot(t(i,:),output(i,:));
% % %     hold
% % %     plot(t(i,:),input(i,:));
end

K = (amp(2:48))/2; % finding the ratio between the output-input (calculating gain)
K_dB = 20*log10(K); % gain [dB]
f = 0.25:0.25:11.75; % [Hz]
w = 2*pi*f; % [rad/sec]

figure (52)
semilogx(w,K_dB);
grid on;


% figure (51)
% plot(time(2502:120001),smoo);
% hold on
% plot(time(2502:120001),angle(2502:120001)-avg_output10, '--');

