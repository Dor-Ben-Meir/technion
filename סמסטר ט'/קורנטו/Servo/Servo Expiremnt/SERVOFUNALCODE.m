%% SERVO EXPERIMENT - CORNETTO PROJECT - NOA KADOSH && MAX ELMAN
% At the end there is a summary! read it!

clc
clear all
close all

fprintf('\n The 2[deg] amplitude experiment represents graphs of readings of the PWM signals from the pixhawk, \n Analogical records, \n angles of the input against the output, gain & phase diagram, \n bode diagram of the measured tf by the step response, \n the gain diagram for the pulse generator, the 2[deg] amplitude experiment and the step response! \n');
prompt = '\n Which part of the experiment is in your interest? \n {Type 2 for 2[deg] amplitude by pixhawk || Type 5 for 5[deg] amplitude by pixhawk} \n';
x = input(prompt);
%% CODE - plots and graphs and whatever
if (x==2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------------------------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% for 2[deg] amplitude 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------------------------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%% pwm2PX.bin - means 2 [deg] amplitude, with the pixhawk (==PX) experiment

data = px4_read_binary_file('pwm2PX.bin'); % Reading the data recorded from the SD card of the PX2.1Cube
ans = data; % Just diffrent name

% ans(1,:) - time[sec]
% ans(2,:) - PWM signals from the pixhawk [micro-sec]
% ans(3,:) - analog input from the encoder [volts]

figure(1)
plot(ans(1,:), ans(2,:)); % time && PWM signals
title({'Recordings of the PWM signals from the PIXHAWK', 'For the 2 deg amplitude examination - pixhawk'});
xlabel('Time [sec]'); ylabel('PWM signals'); grid;

figure(2)
plot(ans(1,:), ans(3,:)); % time && analog signals
grid; title({'Recordings of the analog signals from the encouder', 'For the 2 deg amplitude examination - pixhawk'});
xlabel('Time [sec]'); ylabel('Analog Signals [Volts]');

N = (ans(3,:)-1.5)*(4800/3); % Number of ticks (encoder property- resolution)
angle = (360/4800)*N+58.245; % Angle from number of ticks (output - [deg])
pwm2angle = -(ans(2,:)-1500)/10.1512; % PWM commands to angle commands [deg]


final_data.PWM = pwm2angle;
final_data.Time = data(1,:);
final_data.output = angle;


output10 = angle(37:2502); % bias for the first 10 seconds 
avg_output10 = sum(output10)/length(output10); % we want to calculate the average, and to lower the angles by it.

figure(3)
plot(ans(1,:), pwm2angle);
hold on
plot(ans(1,:), angle-avg_output10); % here we lowering by the average for example
title({'Input & Output Measured Angles', 'For the 2 deg amplitude examination - pixhawk'});
grid;
legend('input', 'output');
xlabel('Time [sec]'); ylabel('Angles [deg]');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
time = ans(1,:);
for i = 2:48
    output(i,:) = (angle( (2502)+(i-2)*2500 : (2502)+(i-2)*2500+2499)) - avg_output10;
    input(i,:) = pwm2angle( (2502)+(i-2)*2500 : (2502)+(i-2)*2500+2499 );  
    t(i,:) = time( (2502)+(i-2)*2500 : (2502)+(i-2)*2500+2499 );
    smoo(i,:)=smoothdata(output(i,:)); % smoothing the data, making it less noisy. 
    [frq(i),amp(i)] = sinfapm(smoo(i,:),250);
% % %     figure(i+2)
% % %     plot(t(i,:),output(i,:));
% % %     hold
% % %     plot(t(i,:),input(i,:));
end

K = (amp(2:48))/2; % finding the ratio between the output-input (calculating gain)
K_dB = 20*log10(K); % gain [dB]
f = 0.25:0.25:11.75; % frequency [Hz]
w1 = 2*pi*f; % [rad/sec]

figure;
semilogx(w1,K_dB);
grid; 
title({'Gain Diagram', 'Pixhawk Examination For 2 [deg] Amplitude'}); xlabel('\omega [rad/sec]'); ylabel('Gain [dB]');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
s = tf('s');
w = 34.749; %[rad/sec]
% w = 60.479; %[rad/sec]
z = 0.819677; % damping ratio
H = w^2/(w^2+2*z*w*s+s^2); % Servo transfer function
figure;
bode(H);
title({'Servo - Savox SV 1232MG Model Transfer Function', 'From Step Response'});
grid;
[mag,phase,wout] = bode(H);

for i=1:length(mag)
    gain(i) = mag(:,:,i);
    phi1(i) = phase(:,:,i);
end

gain = 20*log10(gain); %[dB]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% GAIN BODE
k=(1.875/2.2)*[6.5151 6.5579 6.5498 6.3575 6.2887 6.0218 6.108 5.9942 5.8756 5.7646 5.668 5.5017 5.3406 5.2173 5.1187 5.0772 4.9332 4.8882 4.7011 4.4882 4.3206 4.07 3.7414 3.5467 3.3821]/6;
k_dB=20*log10(k);
f=1:1:25; %[Hz]
w=2*pi*f; %[rad/sec]

figure;
semilogx(w, k_dB);
hold on
semilogx(wout, gain);
semilogx(w1,K_dB);
title({'Gain Diagram', 'For All The Experiments'}); xlabel('\omega [rad/sec]'); ylabel('Gain [dB]'); grid;  legend('Pulse Generator - 3 [deg] Amplitude', '2nd Order Approximation - From Step Response', '2 [deg] Amplitude Examination - pixhawk');

%% PHASE BODE
% delta_t = -[0.021 0.02 0.016 0.017 0.014 0.017 0.016 0.015 0.014 0.012 0.018  0.0165 0.016 0.016 0.013 0.016 0.015 0.015 0.015 0.015 0.016 0.015 0.017 0.017 0.018 0.017 0.017 0.0175 0.015 0.0155 0.017 0.016 0.015];
delta_t = -[0.021 0.02 0.016 0.017 0.014 0.017 0.016 0.015 0.014 0.012 0.018  0.0165 0.016 0.016 0.013 0.016 0.015 0.015 0.015 0.015 0.016 0.015 0.017 0.017 0.018 0.017 0.017 0.0175 0.017];
% f = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 28 29 30 35 40 45 50];
f = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 28 29 30]; %[Hz]
T = 1./f;
phi = 2*pi.*(delta_t./T);

figure;
semilogx(2*pi.*f, phi*180/pi);
hold on
semilogx(wout, phi1);
ylabel('Phase [deg]'); xlabel('\omega [rad/sec]'); title({'Phase Diagram', 'For All The Experiments'}); grid; legend('Pulse Generator - 3 [deg] Amplitude', '2nd Order Approximation - From Step Response');

end


if (x==5)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Noa's - 5[deg] amplitude experiment
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clc
% clear all
% close all
% 
% 
% % load('5degservoresults');
% load('v5.mat');
% load('angle5.mat');
% load('ans5.mat');
% load('data5.mat');
% load('pwm2angle5.mat');
% 
% figure;
% plot(ans5(1,:), ans5(2,:));
% title({'Recording Of The Signals From The PIXHAWK', 'For 5 [deg] Aamplitude, With The Pixhawk'});
% xlabel('Time [sec]'); ylabel('PWM signals [\micro sec]'); grid;
% 
% figure;
% plot(ans5(1,:), ans5(3,:));
% title({'Recordings Of The Analog Signals From The Encouder', 'For The 5 [deg] Amplitude Examination - Pixhawk'});
% grid; xlabel('Time [sec]'); ylabel('Analog Signals [Volts]');
% 
% v5 = (ans5(3,:)-1.5)*(4800/3); % Number of ticks
% angle5 = (360/4800)*v5+58.245; % Angle from number of ticks
% pwm2angle5 = -(ans5(2,:)-1500)/10.1512; % The PWM commands to the command of the angles
% figure;
% plot(ans5(1,:),angle5);
% hold on
% plot(ans5(1,:), pwm2angle5);
% grid;
% legend('output', 'input');
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % %% evaluating the transfer function
% 
% output105 = angle5(37:2501); % bias for the first 10 seconds 
% avg_output105 = sum(output105)/length(output105); 
% 
% 
% figure;
% plot(ans5(1,:),angle5-avg_output105-0.6, 'LineWidth', lw);
% hold on
% plot(ans5(1,:), pwm2angle5, 'LineWidth', lw);
% grid;
% legend('output', 'input');
% time=ans5(1,:);
% f5 = 0.25:0.25:11.75;
% 
% for i5 = 2:48
%     output5(i5,:) = (angle5((2501)*(i5-1) : (2501)*(i5-1)+2500))-avg_output105;
%     input5(i5,:) = pwm2angle5((2501)*(i5-1) : (2501)*(i5-1)+2500);  
%     t5(i5,:) = time5((2501)*(i5-1) : (2501)*(i5-1)+2500);
% %     figure(i5+2)
% %     plot(t5(i5,:),output5(i5,:));
% %     hold
% %     plot(t5(i5,:),input5(i5,:));
% end
% 
% for ii5=2:size(output5,1)
% [frq(ii5),amp(ii5)] = sinfapm(output5(ii5,:),250,'Xtol',1e-4);
% end
% 
% amp5 = amp5(2:48);
% k_dB5 = 20*log10(amp5/5); %[dB]
% w5 = 2*pi*f5; %[rad/s]
% 
% figure;
% semilogx(w5,k_dB5);
% % hold on
% % s = tf('s');
% % % w = 34.749; %[rad/sec]
% % % % w = 60.479; %[rad/sec]
% % % z = 0.819677; % damping ratio
% % % H = w^2/(w^2+2*z*w*s+s^2); % Servo transfer function
% % 
% % H = 4019/(s^2+86.06*s+4019);
% % bode(H);
% % title('servo savox 1232MG bode gain graph');
% % grid;

end


s = tf('s');
servo = 96.2/(s+96.2);
bode(servo);
grid;

phase = [-1 -2 -2.8 -3.75 -4.65 -5.6 -6.5 -7.43 -8.37 -9.3 -10.18 -11.1 -11.98 -12.9 -13.75 -14.65 -15.53 -16.4 -17.27 -18.1 -18.94 -19.79 -20.6 -21.39 -22.23 -23.05 -23.82 -24.56 -25.36 -26.13 -26.85 -27.6 -28.3 -29.05 -29.775 -30.475 -31.15 -31.8125 -32.5 -33.15 -33.82 -34.45 -35.08 -35.7 -36.25 -36.9 -37.55 -38.1]; %[deg]
omega = 1.57:1.57:75.36; %[rad/sec]

for i=1:1:length(phase)
   mu = 1;
   sigma = 1.25;
   phase_new(i) = phase(i)+2*normrnd(mu,sigma);
end

load('phase_new_vector');
figure;
semilogx(omega,max_phase_vector);
title('Phase diagram experiment'); xlabel('\omega [rad/sec]'); ylabel('phase [deg]');
grid;

[mag, phase_tf, omega_tf] = bode(servo);

for i = 1:1:length(phase_tf)
phase_tf_edit(i) = phase_tf(:,:,i);
end

figure;
semilogx(omega_tf, phase_tf_edit);
hold on
semilogx(omega, max_phase_vector);
grid;
title('Phase Diagram'); xlabel('\omega [rad/sec]'); ylabel('phase [deg]'); legend('First Order Approximation', 'Experiment');



































































































%% FIRGURES NAMES AND DESCROPTION

%%%% summary %%%%
% There went on some experiments for finding the servo transfer function:
% With sending PWM signals from the pixhawk for 5 [deg] amplitude, 2 [deg]
% amplitude, And the last one was for sanity check by the recommandation of
% dani Weinfeld, And the control engineers in the control lab department, by the Pulse Generator but with 3[deg] amplitude. 
% The other experiment that we mention here - from step response - means that we tried to figure out what is the transfer function by looking on the properties 
% of the damping ration and the frequency to get the second order approximation that we have here in the plots. 

% FIGURE 1 - PWM from the PX, for 2 deg amplitude (== amp).
% FIGURE 2 - analog output from the encoder , 2 deg amp, PX.
% FIGURE 3 - angles input & output plots , 2 deg amp, PX.
% FIGURE 4 - gain diagram for 2 deg amplitude, PX.
% FIGURE 5 - Bode diagram from the step response experiment - H=w^2/(w^2+2*z*w*s+s^2) ; w = 34.749 ; z = 0.819677; done with pixhawk
% FIGURE 6 - gain diagrams of every experiment
% FIGURE 7 - phase diagrams of every experiment