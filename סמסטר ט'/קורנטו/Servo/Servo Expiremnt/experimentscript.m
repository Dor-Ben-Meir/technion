%% MATLAB FILE FOR READING THE RECORDINGS OF THE SERVO SIMULATION 
clc
clear
close all

%% plots and graphs and whatever

load('5degservoresults');

%figure(1)
% plot(ans(1,:), ans(2,:));
% title('Recording of the signals from the PIXHAWK');
% xlabel('Time [sec]'); ylabel('PWM signals'); grid;
 
% figure(2)
% plot(ans(1,:), ans(3,:));

v = (ans(3,:)-1.5)*(4800/3); % Number of ticks
angle = (360/4800)*v+58.245; % Angle from number of ticks
pwm2angle = -(ans(2,:)-1500)/10.1512; % The PWM commands to the command of the angles

output10 = angle(37:2501); % bias for the first 10 seconds 
avg_output10 = sum(output10)/length(output10)+0.6; 

figure(3)
plot(ans(1,:),angle-avg_output10);
hold on
plot(ans(1,:), pwm2angle);
grid;
legend('output', 'input');
xlabel('Time [sec]'); ylabel('Angle [deg]'); title({'Angles Of The Input Signals', '& The Measured Output'});
ylim([-6 6]);
xlim([481 490]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% evaluating the transfer function

output10 = angle(37:2501); % bias for the first 10 seconds 
avg_output10 = sum(output10)/length(output10)+0.6; 

% figure;
% plot(ans(1,:),angle-avg_output10-0.6, 'LineWidth', lw);
% hold on
% plot(ans(1,:), pwm2angle, 'LineWidth', lw);
% grid;
% legend('output', 'input');

time = ans(1,:);
f = 0.25:0.25:11.75; %[Hz]
for i = 2:48
    output(i,:) = (angle((2501)*(i-1) : (2501)*(i-1)+2500))-avg_output10;
    input(i,:) = pwm2angle((2501)*(i-1) : (2501)*(i-1)+2500);  
    t(i,:) = time((2501)*(i-1) : (2501)*(i-1)+2500);  
    if i == 4
      figure(i)
      plot(t(i,:),movmean(output(i,:),3));
      hold
      plot(t(i,:),input(i,:));
    end 
end

% outputnew(:,:) = output(2:48,:);
% for p = 1:length(f)
%     calc_sine(i,:) = sinfapm(outputnew(i,:), f(i));
% end

for ii=2:size(output,1)
    [frq(ii),amp(ii)] = sinfapm(output(ii,:),250,'Xtol',1e-4);
    amp2(ii) = rms(output(ii,:))*sqrt(2);
end

amp = amp(2:48);
%amp = amp2(2:48);

% Pixhawk 5[deg] Experiment
k_dB = 20*log10(amp/max(amp)); %[dB]
w = 2*pi*f; %[rad/sec]

% Pulse Generator experiment

% k_PG=(1.875/2.2)*[6.5151 6.5579 6.5498 6.3575 6.2887 6.0218 6.108 5.9942 5.8756 5.7646 5.668 5.5017 5.3406 5.2173 5.1187 5.0772 4.9332 4.8882 4.7011 4.4882 4.3206 4.07 3.7414 3.5467 3.3821]/6;
k_PG=[6.5151 6.5579 6.5498 6.3575 6.2887 6.0218 6.108 5.9942 5.8756 5.7646 5.668 5.5017 5.3406 5.2173 5.1187 5.0772 4.9332 4.8882 4.7011 4.4882 4.3206 4.07 3.7414 3.5467 3.3821]/6.5579; %Cancelling out the weird factor, and deviding by the higher amplitude measured
k_dB_PG=20*log10(k_PG);
f_PG=1:1:25; %[Hz]
w_PG=2*pi*f_PG; %[rad/sec]

lw = 2.1;
figure(5)
semilogx(w,k_dB, 'LineWidth', lw);
hold on
w1 = 1:0.01:200;
fit.coeff = [-0.000253312867517007,-0.00702688976796014,0.0159616640088866];
y = fit.coeff(1).*w1.^2+fit.coeff(2).*w1+fit.coeff(3);  
semilogx(w1,y, 'LineWidth', lw);
grid; xlabel('\omega [rad/sec]'); ylabel('Gain [dB]');
title({'Gain Diagram','For The Pixhawk 5[deg] Amplitude'});
legend('Measured From Experiment - PX 5[deg] Amplitude', 'Quadratic Basic Fitting ');

figure(6);
wn = 96.2; 
zeta = 0.7;
s = tf('s');

H1 = wn/(s+wn); % First order approximation
H2 = wn^2/(s^2+2*wn*zeta*s+wn^2); % Second order approximation
% H2 = wn^2/(s+wn)^2;
myw = 0:0.01:20*2*pi; % New range of frequencies [rad/sec]

[mag,phase] = bode(H1,myw);
mag = 20*log10(squeeze(mag));
hold on
[mag2,phase2]=bode(H2,myw);
mag2 = 20*log10(squeeze(mag2));

semilogx((w/(2*pi)),k_dB);
plot(myw/(2*pi),mag); xlabel('Hz');
hold on
% plot(myw/(2*pi),mag2); xlabel('Hz');
plot(w_PG/(2*pi),k_dB_PG);


title('Servo Savox 1232MG Bode Gain Graph');
grid;
% legend('Experiment - 5 [deg] pixhawk','First Order Approximation','Second Order Approximation', 'Pulse Generator - Sanity Check 3[deg] Amplitude');
legend('Experiment - 5 [deg] pixhawk','First Order Approximation', 'Pulse Generator - Sanity Check 3[deg] Amplitude');
% xlabel('\omega [rad/sec]'); ylabel('Gain [dB]'); 
xlim([0 20]);


% Phase

for i=1:length(mag)
    phasenew(i) = phase(:,:,i); % Check the phase that is took here to consideration
end


% delta_t = -[0.021 0.02 0.016 0.017 0.014 0.017 0.016 0.015 0.014 0.012 0.018  0.0165 0.016 0.016 0.013 0.016 0.015 0.015 0.015 0.015 0.016 0.015 0.017 0.017 0.018 0.017 0.017 0.0175 0.015 0.0155 0.017 0.016 0.015];
delta_t = -[0.021 0.02 0.016 0.017 0.014 0.017 0.016 0.015 0.014 0.012 0.018  0.0165 0.016 0.016 0.013 0.016 0.015 0.015 0.015 0.015 0.016 0.015 0.017 0.017 0.018 0.017 0.017 0.0175 0.017];
% f = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 28 29 30 35 40 45 50];
f = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 28 29 30]; %[Hz]
T = 1./f;
phi = 2*pi.*(delta_t./T);

% for the 5[deg] amplitude experiment - with the pixhawk
f_fordata5 = 0.25:0.25:12; %[Hz]
delta_t_datafor5 = [-0.1 -0.05 -0.04 -0.05 -0.04 -0.04 -0.03 -0.05 -0.04 -0.05 -0.06 -0.05 -0.05 -0.04 -0.04 -0.04 -0.05 -0.05 -0.05 -0.04 -0.04 -0.06 -0.04 -0.05 -0.05 -0.05 -0.06 -0.04 -0.03 -0.03 -0.03 -0.03 -0.035 -0.03 -0.03 -0.0325 -0.0328 -0.025 -0.0325 -0.03 -0.035 -0.03 -0.028 -0.03 -0.03 -0.03 -0.0325 -0.025];
phase_datafor5 = 2*pi*delta_t_datafor5./(1./f_fordata5); %[rad]
phase_datafor5_deg = (180/pi)*phase_datafor5; %[deg]



figure;
semilogx(2*pi.*f, phi*180/pi);
hold on
semilogx(myw, phasenew); % The approximated phase diagram
semilogx(f_fordata5*2*pi, phase_datafor5_deg);
ylabel('Phase [deg]'); xlabel('\omega [rad/sec]'); title({'Phase Diagram', 'For All The Experiments'}); grid; legend('Pulse Generator - 3 [deg] Amplitude', '2nd Order Approximation');

%% New PHASE 

