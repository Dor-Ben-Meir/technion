%% fresh start for my experiments
clc
clear all
close all

%% TRANSFER FUNCTIONS

s = tf('s');
H1 = 16/(s+16);

figure;
bode(H1);
grid; title('Bode Diagram For The Examined Transfer Function');