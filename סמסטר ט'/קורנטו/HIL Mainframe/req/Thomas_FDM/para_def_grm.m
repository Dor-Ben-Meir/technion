%% Defines the parameters of the GRM Simulink model
%
%
% SUMMARY:
% Defines the parameters of the subsystems of the Guided Research Missile
% Airframe model. Loads two .mat-files.
%
% DESCRIPTION:
% Defines the parameters of the subsystems of the Guided Research Missile
% Airframe model. Loads two .mat-files containing the thrust of the solid
% fuel rocket motor over time and the massflow over time.
%
% DEPENDENCIES:
% prp_lookup.mat

%
% PROJECT:
% *** LOW COST MISSILE TESTBED ***
%
% RESOURCES:
% Modeling and Simulation of the Guided Research Missile, Lausenhammer
% Thomas; Semester Thesis, Technical University Munich, 2018
%
% CHANGE DIRECTORY
% Date          Name                    Description         Version
% 2018/11/12    Lausenhammer, Thomas    initial version     V 1.0
% 2019/05/07    Lausenhammer, Thomas    changed Header      V 1.1
% 2019/05/07    Lausenhammer, Thomas    added comments      V 1.2
% 2020/05/24    Lausenhammer, Thomas    New GRM Model       V 2.0
% 2020/08/19    Lausenhammer, Thomas    Sensor para-        V 2.1
%                                       metrization
% 2020/10/05    Lausenhammer, Thomas    Added GPS           V 2.2
%
%
%%  Load Parameters
%%  Propulsion
%Time and thrust data for Propulstion look-up table

load('prp_lookup.mat');           % Look-Up Table Data mass flow over time [kg/s]

pNPB = [-1.248;0;0];             %[m] position vector of the noozle

%%  Weight and Balance

mB_0 = 4.832;               % Missile structure mass without motor [kg]
m0 = mB_0;
mM_0 = 1.595;               % motor mass full [kg]
mB_e = 5.712;               % Missile mass after motor burn out [kg]
mM_e = 0.715;               % Motor mass empty [kg]

pSPB_x = -0.6777;             % x-position Missile center of gravity [m]
pMPB_x = -0.85415-0.215;      % x-position motor center of gravity [m]
pSPB_y = 0;                   % y-position Missile center of gravity [m]
pMPB_y = 0;                   % y-position motor center of gravity [m]
pSPB_z = 0;                   % z-position Missile center of gravity [m]
pMPB_z = 0;                   % z-position motor center of gravity [m]
pMPB = [pMPB_x;pMPB_y;pMPB_z];    %[m] position vector motor center of gravity
pSPB = [pSPB_x;pSPB_y;pSPB_z];    %[m] position vector missile structure center of gravity

% Moment of inertia tensor is defined in a coordinate system that is 45�
% rotated around the body fixed x-axis.
kg2g = 1e3;                 %[-] COnversion factor from kg to g
m22mm2 = 1e6;               %[m] Conversion factor from m^2 to mm^2

JSPP_0 = [6796155.46, 137001.04, -641003.58;137001.04,4212488212.36,1896.45;-641003.58,1896.45,4212530084.51]/kg2g/m22mm2;
JSPP_e = [6454046.52,136998.49,-641001.44;136998.49,3194577371.27,1905.91;-641001.44,1905.91,3194619249.93]/kg2g/m22mm2;
% tmp_al = pi/4;
% TBP = [1,0,0;0,cos(tmp_al),sin(tmp_al);0,-sin(tmp_al),cos(tmp_al)];
% J0PB_0 = TBP*J0PP_0*TBP';
% J0PB_e = TBP*J0PP_e*TBP';
JSPB_e = JSPP_e;

% JMMM_0 = TBP*J0PP_0*TBP';
% JMMM_e = TBP*J0PP_e*TBP';

JMMM_0 = [620072.46, -1.93, -0.17 ;-1.93,95174386.97,-5.90;-0.17,-5.90,95174421.28]/kg2g/m22mm2;
JMMM_e = [277963.52,0.87,0.07;0.87,42664380.37,-2.65;0.07,-2.65,42664395.75]/kg2g/m22mm2;

%%  Fins and Fin actuators
% position, velocity and acceleration limitations of the 2nd order actuator
% system
siB_max = deg2rad(30);              %[rad]
siB_min = deg2rad(-30);             %[rad]
d_siB_min = deg2rad(-1200);         %[rad/S]
d_siB_max = deg2rad(1200);          %[rad/s]
dd_siB_min = -inf;                  %[rad/s^2]
dd_siB_max = inf;                   %[rad/s^2]

act_omega = 59.9250;                %[rad/s] Sys-ID Technion
act_dmp = 0.6322;                   %[-] damping Sys-ID Technion
unc_act_bl = 0;                     %[rad] bias error
unc_act_scale = 1;                  %[-] scale factor error
unc_act_off = 0;                    %[rad] actuator offset
siB_0 = 0;                          %[rad] initial value
d_siB_0 = 0;                        %[rad/s] initial value
tRRR = 0;
Tr4r3 = [1,1,-1;1,1,1;1,-1,1;1,-1,-1];  % Control allocation matrix
Tr3r4 = [1,1,1,1;1,1,-1,-1;-1,1,1,-1]*0.25; %pseudo-inverse Control allocation matrix

%%  Sensors

%%  IMU
% IMU-Accelerometer
%settings for IMU time step, update rate and time offset have to be in
%compliance with the simulation time step

%   Parametrisierung BMI088
imu_dt = 0.001;             %[s] IMU time step (Blockschrittweite)
imu_UpdateRate = 400;       %[Hz] IMU Update rate
imu_delay = 0.001;         %[s] IMU time offset
imu_seed = 0;            %[-] initialization value for random number generator (either zero or positive integer)
imu_a_limits = [16*9.81];     %[m/s^2] accelerometer saturation
imu_a_sigmanoise = [1.7162e-3;1.7162e-3;1.7162e-3];       %[m/s/sqrt(s)] white sensor noise
imu_a_quantization = 9e-3; %[m/s^2] Quantization
imu_a_bias_static = [9.80665*20e-3;9.80665*20e-3;9.80665*20e-3];    %[m/s^2] static acc-imu bias
imu_a_bias_randomwalk = [1e-3;1e-3;1e-3];   %[m/s^2] standard deviation in run stability
imu_a_bias_timeconstant = 1;         %[s] dynamic in run stability
imu_a_flag_dyn = 1;                  %
imu_a_omega = [523;523;523];            %[rad/s] acc-imu dynamic frequency 5-523 [Hz]
imu_a_damping = [sqrt(2)/2; sqrt(2)/2;sqrt(2)/2];         %[-] damping
imu_a_ScaleFactor = [1000;1000;1000];      %[ppm] linear scale factor acc
imu_a_ScaleFactorN = zeros(3,1);     %[ppm] quadratic scale factor acc
imu_a_ScaleFactorA = zeros(3,1);     %[ppm] absolute scale factor acc
imu_a_misalignment = zeros(3,1);     %[rad] misalignment
imu_a_nonorthogonality = zeros(3,1); %[rad] deviation from ideal orthogonal projection
imu_a_CrossCoupling = zeros(3,1);    %[-] cross coupling
% IMU-Gyroscope
imu_w_limits = deg2rad(2000);         %[rad/s] gyroscope saturation
imu_w_sigmanoise = [2.443392e-4;2.443392e-4;2.443392e-4];       %[rad/sqrt(s)] gyroscope white sensor noise
imu_w_quantization = deg2rad(0.004); %[rad/s] quantization gyroscope
imu_w_bias_static = [deg2rad(1);deg2rad(1);deg2rad(1)];      %[rad/s] static bias gyroscope
imu_w_bias_randomwalk = zeros(3,1);  %[rad/s] standard deviation in run stability
imu_w_bias_timeconstant = 1;        %[s] dynamic in run stability
imu_w_bias_AccelerationEffect = [4.943723709009050e-07,0,0;0,4.943723709009050e-07,0;0,0,4.943723709009050e-07];  %[rad*s/m] ruck effect
imu_w_flag_dyn = 1;                  %
imu_w_omega = [523;523;523];          %[rad/s] Imu dynamic frequency
imu_w_damping = [sqrt(2)/2; sqrt(2)/2;sqrt(2)/2];          %[-] damping
imu_w_ScaleFactor = [400;400;400];      %[ppm] linear scale factor error
imu_w_ScaleFactorN = zeros(3,1);     %[ppm] quadratic scale factor error
imu_w_ScaleFactorA = zeros(3,1);     %[ppm] norm scale factor error
imu_w_misalignment = zeros(3,1);     %[rad] misalignment
imu_w_nonorthogonality = zeros(3,1); %[rad] deviation from ideal orthogonal projection
imu_w_CrossCoupling = zeros(3,1);    %[-] cross coupling

TUB = [-1,0,0;0,1,0;0,0,-1];          %IMU rotation Transformation matrix
TBU = TUB';
pUPB = [-0.136;0;0];                %[m] IMU position from the reference point

%%  GPS
%   It is assumed that the GPS is at the same positiona as the IMU in a
%   first iteration.

gps_sigH = 2.23;                    % Horizontal standard deviation [m] (typical value C/A based GPS receiver)
gps_sigV = 2.23;                    % Vertical Standard deviation [m] (typical value C/A based GPS receiver)
gps_T_sample = 1/10;                %   [s] Update Frequency of 10 Hz for GPS signal.

%%  Fin Encoder

%RMB20
enc_CLK_rate = 0.001;               %[s] Time periode of the SSI Bus Clock rate
enc_hys = 0.18;                     %{�] Encoder Hysteresis
enc_acc = 0.5;                      %[+-�] Encoder Accuracy
enc_reap = 0.07;                    %[�] Encoder repeatability
enc_res = 8192;                     %[bit] Encode resolution in positions per revolution
enc_rand_acc = 0.5;                 %[-] random initialization encoder accuracy

%%  Aerodynamics

aero.sref = 0.006362;
aero.lref = 0.0900;
aero.xcg  = 0.6195;

% TLU limits
Aerodynamics.ulim_alpha_rad = 10*pi/180;
Aerodynamics.llim_alpha_rad = -10*pi/180;
Aerodynamics.ulim_beta_rad  = 10*pi/180;
Aerodynamics.llim_beta_rad  = -10*pi/180;

% CX 
Aerodynamics.CX0        = -0.66845;
Aerodynamics.CX_alpha2  = 10.1204; 
Aerodynamics.CX_alpha   = -0.12128;
Aerodynamics.CX_beta2   = 10.0488;
Aerodynamics.CX_beta    = -0.08709;
Aerodynamics.CX_dele2   = -4.5959; 
Aerodynamics.CX_dela2   = -3.9394; 

% CZ
Aerodynamics.CZ0      = 0;
Aerodynamics.CZ_alpha = -28.2919; 
Aerodynamics.CZ_dele  = -5.1566; 

% CY
Aerodynamics.CY0      = 0;
Aerodynamics.CY_beta  = -27.724;
Aerodynamics.CY_delr = -Aerodynamics.CZ_dele; 

% CLL
Aerodynamics.CLL0 = 0;
Aerodynamics.CLL_dela = 3.7242;  

% CLM
Aerodynamics.CLM0      = 0;
Aerodynamics.CLM_alpha = -31.5283; 
Aerodynamics.CLM_dele  = -21.1994; 
Aerodynamics.CLM_dela  = 0.2177;

% CLN
Aerodynamics.CLN0      = 0;
Aerodynamics.CLN_beta  = 30.5576; 
Aerodynamics.CLN_delr  = Aerodynamics.CLM_dele; 

% Dynamic Derivatives
Aerodynamics.CLL_p = -104.988;
Aerodynamics.CLM_q = -479.557;
Aerodynamics.CLN_r = -479.557;


%%  GNC

guidc_aUIB_x_th = 40;
guidc_T_th = 0.3;

guidc_n_window = 10;
%%  Controller

t_sample_contr = 0.001;

% fc_aero = load('fc_aero.mat');

%   Output redefinition

% pUBB_x = 1.4;
pUBB_x = 1.2;

% Gains NDI-Inner-Loop

% kIi = 0;
% kIp = 1000;
wI_ref = 400;

% kOp = 0.0275;
% kOi = 0;
wO_ref = 200;

%%  Implemented uncertainties:

unc_fin_def_roll =          0;
unc_fin_def_pitch =         0;
unc_fin_def_yaw =           0;

%  Weight and Balance
unc_cg_x = 0;
unc_cg_y = 0;
unc_cg_z = 0;

unc_moi_Jxx = 0;
unc_moi_Jyy = 0;
unc_moi_Jzz = 0;
unc_mB_bl = 0;
unc_mB_scale = 1;

%   Propulsion
unc_prp_z = 0;
unc_prp_y = 0;
unc_prp_x = 0;
unc_prp_scale = 1;

%   Aerodynamic coefficients
unc_aero_Cx_scale = 1;
unc_aero_Cy_scale = 1;
unc_aero_Cz_scale = 1;
unc_aero_Cl_scale = 1;
unc_aero_Cm_scale = 1;
unc_aero_Cn_scale = 1;

%   Actuator Failure
unc_act_fail_flag_1 = 1;
unc_act_fail_position_1 = 0;
unc_act_fail_flag_2 = 1;
unc_act_fail_position_2 = 0;
unc_act_fail_flag_3 = 1;
unc_act_fail_position_3 = 0;
unc_act_fail_flag_4 = 1;
unc_act_fail_position_4 = 0;