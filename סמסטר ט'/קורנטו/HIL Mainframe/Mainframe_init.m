clear,clc

%% ----------> CHANGE ONLY THIS SECTION <---------- %%
% NOTE ADDITIONAL SETTINGS LOCATED IN:  \req\init\FDM\sophisticated\

% Specify Simulation Parameters

Enable_FlightGear = 0;    % ONLY ENABLE IN OFFLINE-MODE!! 

Enable_Pixhawk    = 0;    % <1> to enable, <0> to emulate
Enable_Gimbal     = 0;    % <1> to enable, <0> to disable
Enable_INS        = 0;    % <1> to use INS states, <0> to use FDM states
Measure_Servos    = 0;    % <1> to measure fin deflections, <0> to use tranfer functions
                          
FCC_items         = 18;   % Number of variables sent from FCC to HIL

%% Define Variant subsystems
VSS_GIMBAL_ON      = Simulink.Variant('HIL_Data.VSS.Gimbal==1');
VSS_GIMBAL_OFF     = Simulink.Variant('HIL_Data.VSS.Gimbal==0');
VSS_FCC_REAL       = Simulink.Variant('HIL_Data.VSS.FCC==1'); 
VSS_FCC_EMUL       = Simulink.Variant('HIL_Data.VSS.FCC==0');
VSS_FLIGHTGEAR_ON  = Simulink.Variant('HIL_Data.VSS .FlightGear==1');
VSS_FLIGHTGEAR_OFF = Simulink.Variant('HIL_Data.VSS.FlightGear==0');
VSS_SERVOS_ON      = Simulink.Variant('HIL_Data.VSS.MeasureServos==1');
VSS_SERVOS_OFF     = Simulink.Variant('HIL_Data.VSS.MeasureServos==0');
       
%% Add paths to the Current Directory
addpath(genpath([pwd,'\req\Thomas_FDM']));
addpath(genpath([pwd,'\req\library']));

%% Initialize Submodules
HIL_Data.FDM                = init_FDM;
HIL_Data.VSS.FCC            = Enable_Pixhawk;
HIL_Data.VSS.Gimbal         = Enable_Gimbal;
HIL_Data.VSS.FlightGear     = Enable_FlightGear;
HIL_Data.VSS.MeasureServos  = Measure_Servos;

Use_FDM_States              = not(Enable_INS); 
FCC_message_size            = FCC_items*4+3;
FCC_types                   = sprintf('%d*uint8',FCC_message_size);


%% Ready to Rock
%clc
disp("Ready!")