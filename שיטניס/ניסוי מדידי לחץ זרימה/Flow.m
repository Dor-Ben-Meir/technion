clc
clear
close all


%% Data

Pa = 101325; %[pa]
g = 9.81; %[kg*m/s^2]
am = deg2rad(45); %[deg]
rhom = 800; %[kg/m^3]
rhoa = 1.225; %[kg/m^3]
dP = @(L) g*L*(1e-2)*sin(am)*(rhom-rhoa);
V  = @(Ps,P0) sqrt(2*(P0-Ps)/rhoa);

%% prt 1
a1 = [0 5 15 20 25]; %[deg]
probe1 = {'cone' 'circ' 'invcone'};

for j = 1:length(probe1)
    switch probe1{j}
        case 'cone'
            dl(:,j) = [3.7 3.6 3.5 3.3 3]; %enetr measurments from pipes
        case 'circ'
            dl(:,j) = [3.7 3.6 3.5 3.4 3.2]; %enetr measurments from pipes
        case 'invcone'
            dl(:,j) = [3.7 3.6 3.6 3.6 3.5]; %enetr measurments from pipes
    end
end

P1 = Pa + dP(dl); 
for j = 1:length(probe1)
    err1(:,j) = abs(P1(1,j) - P1(:,j))/P1(1,j);
end

% Plot
% plt_ln= {'o-' '*--' 'x-.'};
% figure
% for j = 1:length(probe1)
%     plot(a1,err1(:,j),plt_ln{j},'LineWidth',2,'DisplayName',probe1{j})
%     hold on;
% end
% grid on;
% xlabel('\alpha')
% ylabel('Error')
% title('Error Vs. \alpha')
% legend show
% saveas(gcf,'Error1.jpg')


%% prt 2

D = [1 2 4 8];

dl = [0.5 0.2 0.1 0]; %enetr measurments from pipes
P2 = Pa + dP(dl);
err2 = abs(P2(4)-P2)/P2(4);

% figure
% plot(D,err2,'o-','LineWidth',2)
% grid on
% xlabel('x/D [mm]')
% ylabel('Error')
% title('Error Vs. Hole Distance Normalized by Probe Diameter')
% saveas(gcf,'Error2.jpg')


%% prt 3

degs = [-25 -20:10:20 25]; %enetr measurments from pipes
theta = ones(7,7);
phi = ones(7,7);

for j =1:7
    theta(:,j) = theta(:,j).*degs';
    phi(j,:) = phi(j,:).*degs;
end


dl_p{1} = -[-2 -2.8 -3 -3 -2.5 -2 -1.5; %enetr measurments from pipes
           -2 -2 -2.5 -2.3 -2 -1.5 -1;
           -1 -1.3 -1.5 -1 -1 0 -0.5;
           0 -0.2 -0.5 0 0.2 0.5 0.5;
           1 1 0.5 0.5 1 1.5 1.5;
           1.5 1.7 1.5 1.5 1.5 1.8 2;
           2 2 2 2 2 2 2];
dl_p{2} = -[-1.5 -1 0 1 1.8 2 2.2; %enetr measurments from pipes
           -2 -1.5 -0.5 0.5 1.5 2 2;
           -2.5 -2 -1 0 1 1.8 1;
           -3 -2.3 -1.5 0 0.5 1.5 1.8;
           -3 -2.3 -1.5 -0.5 0.5 1.5 1.8;
           -2.7 -2.5 -1.5 -1 0.5 1.2 2;
           -2.7 -2 -1.5 0 0.2 1.5 2];
dl_p{3} = -[2 2.3 2 2 2 2 2; %enetr measurments from pipes
           2 2 2 2 1.5 1.5 1.5;
           1.5 1.5 1 1 0.5 0.5 0.5;
           1 0.2 0 0 -0.2 -0.5 0;
           0 0 -1 -1 -2 -2 -1.5;
           -2 -1.2 -1.8 -2.5 -2.8 -3 -2;
           -1 -1.8 -2 -3 -3.2 -3.3 -2.5];
dl_p{4} = -[2 2 1.5 0 -1 -2 -2.5; %enetr measurments from pipes
           2.1 2 1 0 -1.5 -2 -3;
           2 2 1 0 -1.5 -2.8 -3;
           2 1.8 1 0 -1.2 -2.2 -2.8;
           2 2 1.2 0.2 -1 -1.8 -2.5;
           2 2 1.5 0.7 -0.5 -1 -1.8;
           2 2 1.8 1 0 -0.8 -1.5];
dl_p{5} = -[-1.2 -1.5 -2 -2.8 -2.5 -2 -2; %enetr measurments from pipes
           -1.5 -2 -2.5 -3 -3 -2.5 -2.5;
           -2 -2.5 -3.2 -3.5 -3.5 -3 -2.8;
           -2.5 -2.8 -3.5 -3.8 -3.7 -3.3 -3;
           -2 -2.5 -3 -3.4 -3.5 -3 -2.8;
           -1.5 -1.8 -2.5 -3 -3 -2.3 -2;
           -1 -1.5 -2 -2.5 -2.2 -1.8 -1.8];
     
for j = 1:5
    P_p{j} = (Pa + dP(dl_p{j}));
end

% for j =1:5
%     figure         
%     contourf(degs,degs,P_p{j})
%     xlabel('\Phi [deg]')
%     ylabel('\Theta [deg]')
%     xlim([-25 25])
%     ylim([-25 25])
%     h = colorbar;
%     h.Label.String = 'P [Pa]'; 
%     title(['Pressure as function of Phi and Theta, P',num2str(j)])
%     saveas(gcf,['Pressure as function of Phi and Theta, P',num2str(j),'.jpg'])
% end


P_ave = (P_p{1}+P_p{2}+P_p{3}+P_p{4})/4;
C_pt  = (P_p{3}-P_p{1})./(P_p{5}-P_ave);
C_pp  = (P_p{2}-P_p{4})./(P_p{5}-P_ave);
C_p5  = (P_p{5}-P2(4))./(P_p{5}(4,4)-P2(4));
C_pa  = (P_ave-P2(4))./(P_p{5}(4,4)-P2(4));

P1ex = 101406;
P2ex = 101320;
P3ex = 101280;
P4ex = 101338;
P5ex = 101458;

P_avex = (P1ex+P2ex+P3ex+P4ex)/4;
C_ptex = (P3ex-P1ex)/(P5ex-P_avex);
C_ppex = (P2ex-P4ex)/(P5ex-P_avex);

Ft = scatteredInterpolant(C_pt(:),C_pp(:),phi(:));
Fp = scatteredInterpolant(C_pt(:),C_pp(:),theta(:));

tex = Ft(C_ptex,C_ppex);
pex = Fp(C_ptex,C_ppex);

C_p5ex = interp2(degs,degs,C_p5,tex,pex);
C_pavex = interp2(degs,degs,C_pa,tex,pex);

Pex = (P5ex/C_p5ex - P_avex/C_pavex)/(1/C_p5ex - 1/C_pavex);
P0ex = (P5ex-Pex)/C_p5ex + Pex; 

Vex = V(Pex,P0ex);

 figure
 contourf(C_pt,C_pp,phi)
 xlabel('C_p_\Theta')
 ylabel('C_p_\Phi')
 title('\Phi')
 h = colorbar;
 h.Label.String = '\Phi [deg]';
 saveas(gcf,'Phi.jpg')
 figure
 contourf(C_pt,C_pp,theta)
 xlabel('C_p_\Theta')
 ylabel('C_p_\Phi')
 title('\Theta')
 h = colorbar;
 h.Label.String = '\Theta [deg]';
 saveas(gcf,'Theta.jpg')
 figure
 contourf(degs,degs,C_p5)
 xlabel('\Phi [deg]')
 ylabel('\Theta [deg]')
 title('C_P_5')
 h = colorbar;
 h.Label.String = 'C_P_5';
 saveas(gcf,'C_P_5.jpg')
 figure
 contourf(degs,degs,C_pa)
 xlabel('\Phi [deg]')
 ylabel('\Theta [deg]')
 title('C_P_A_v_e')
 h = colorbar;
 h.Label.String = 'C_P_A_v_e';
saveas(gcf,'C_P_A_v_e.jpg')


%% prt 4

dl04 = -[-11 -4 -0.5]; %enetr measurments from pipes
dls4 = -[-0.2 0 0]; %enetr measurments from pipes

P04 = Pa + dP(dl04);
Ps4 = Pa + dP(dls4);

V4 = V(Ps4,P04);
