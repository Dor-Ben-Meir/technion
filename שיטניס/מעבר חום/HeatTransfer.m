clc
clear 
close all

%% prt1 Conduction
%����� �� �������
k_stl  = 18;   %[W/(m*C)]
k_alm  = 250;  %[W/(m*C)]
k_grnt = 2.85; %[W/(m*C)]
k_gls  = 0.8;  %[W/(m*C)]

dplates = xlsread('holacha.csv');
%����� �������� �����
plts = dplates(10:end,2); %[C]


%���� �������
stl_h   = 5e-2;   %[m]
alm_h   = 9e-2;   %[m]
grnt_h  = 4.5e-2; %[m]
gls_h   = 3e-2;   %[m]���� �� ��� ��� ��� 3
plts_h  = linspace(0,21.5e-2,length(plts)); %[m]���� �� �� �������� �����

% ��� ��� ������ �����
Ap = 15e-2*15e-2;   %[m^2]

j_stl = find(abs(plts_h-stl_h)<1e-3,1);
j_alm = find(abs(plts_h-(stl_h+alm_h))<1e-3,1);
j_grnt = find(abs(plts_h-(stl_h+alm_h+grnt_h))<1e-3,1);

% figure
% plot(plts_h*1e2,plts); hold on; grid on;
% plot([plts_h(j_stl)*1e2 plts_h(j_stl)*1e2],[plts(end) plts(j_stl)])
% plot([plts_h(j_alm)*1e2 plts_h(j_alm)*1e2],[plts(end) plts(j_alm)])
% plot([plts_h(j_grnt)*1e2 plts_h(j_grnt)*1e2],[plts(end) plts(j_grnt)])
% ylim([plts(end) 280])
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')

stl = plts(find(plts==267.75):find(plts==240.23));
stl_x = plts_h(find(plts==267.75):find(plts==240.23));
a_stl = polyfit(stl_x,stl',1);
stl_dtdx = a_stl(1);
stl_eq = @(x) a_stl(1)*x*1e-2+a_stl(2);
alm = plts(find(abs(plts-227.45)<1e-9):find(abs(plts-221.77)<1e-9));
alm_x = plts_h(find(abs(plts-227.45)<1e-9):find(abs(plts-221.77)<1e-9));
a_alm = polyfit(alm_x,alm',1);
alm_dtdx = a_alm(1);
alm_eq = @(x) a_alm(1)*x*1e-2+a_alm(2);
grnt = plts(find(plts==178.72):find(plts==142.23));
grnt_x = plts_h(find(plts==178.72):find(plts==142.23));
a_grnt = polyfit(grnt_x,grnt',1);
grnt_dtdx = a_grnt(1);
grnt_eq = @(x) a_grnt(1)*x*1e-2+a_grnt(2);
gls = plts(find(abs(plts-121.64)<1e-9):find(plts==93.99,1));
gls_x = plts_h(find(abs(plts-121.64)<1e-9):find(plts==93.99,1));
a_gls = polyfit(gls_x,gls',1);
gls_dtdx = a_gls(1);
gls_eq = @(x) a_gls(1)*x*1e-2+a_gls(2);

% figure
% plot(stl_x*1e2,stl,'o','DisplayName','Measurement'); hold on; grid on;
% fplot(stl_eq,[stl_x(1)*1e2 stl_x(end)*1e2],'DisplayName','Linear Fit')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show
% figure
% plot(alm_x*1e2,alm,'o','DisplayName','Measurement'); hold on; grid on;
% fplot(alm_eq,[alm_x(1)*1e2 alm_x(end)*1e2],'DisplayName','Linear Fit')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show
% figure
% plot(grnt_x*1e2,grnt,'o','DisplayName','Measurement'); hold on; grid on;
% fplot(grnt_eq,[grnt_x(1)*1e2 grnt_x(end)*1e2],'DisplayName','Linear Fit')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show
% figure
% plot(gls_x*1e2,gls,'o','DisplayName','Measurement'); hold on; grid on;
% fplot(gls_eq,[gls_x(1)*1e2 gls_x(end)*1e2],'DisplayName','Linear Fit')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show


k_stl_norm  = stl_dtdx/stl_dtdx;
k_alm_norm  = stl_dtdx/alm_dtdx;
k_grnt_norm = stl_dtdx/grnt_dtdx;
k_gls_norm  = stl_dtdx/gls_dtdx;

k_alm_1  = k_stl*k_alm_norm;
k_grnt_1 = k_stl*k_grnt_norm;
k_gls_1  = k_stl*k_gls_norm;
err_alm_1 = abs((k_alm_1-k_alm)/k_alm);
err_grnt_1 = abs((k_grnt_1-k_grnt)/k_grnt);
err_gls_1 = abs((k_gls_1-k_gls)/k_gls);

Q_stl = -k_stl*Ap*(stl(end)-stl(1))/stl_h;


k_alm_2  = -Q_stl*alm_h/((alm(end)-alm(1))*Ap);
k_grnt_2 = -Q_stl*grnt_h/((grnt(end)-grnt(1))*Ap);
k_gls_2  = -Q_stl*gls_h/((gls(end)-gls(1))*Ap);
err_alm_2 = abs((k_alm_2-k_alm)/k_alm);
err_grnt_2 = abs((k_grnt_2-k_grnt)/k_grnt);
err_gls_2 = abs((k_gls_2-k_gls)/k_gls);
%% prt2 Fins

dfins = xlsread('fins.csv');

lng  = dfins(10:end,3); %[C]
med  = dfins(10:123,4); %[C]
srt  = dfins(10:67,2); %[C]

Af = 2.5e-2*0.5e-2; %[m^2]
Pf = 2*(2.5+0.5)*1e-2; %[m] ���� ��� ����
lng_h   = linspace(0,40e-2,length(lng)); %[m]
med_h   = linspace(0,20e-2,length(med)); %[m]
srt_h   = linspace(0,10e-2,length(srt(1:end))); %[m]

mu    = 1.846e-5; %[kg/(m*s)] ������ ������
Cp    = 1005;     %[J/9kg*K)] ����� ��� ������
g     = 9.81;     %[m/s^2]
k_air = 0.0262;   %[W/(m*C)] ������� ������
v     = 1.544e-5; %[m^2/s]������ ������� �� ������ 
Pr = mu*Cp/k_air; % ���� ������

%���� ������
Gr_lng = (g*lng_h(end)^3*(mean(lng)-26))/(v^2*(mean(lng)+273.15));
Gr_med = (g*med_h(end)^3*(mean(med)-26))/(v^2*(mean(med)+273.15));
Gr_srt = (g*srt_h(end)^3*(mean(srt)-26))/(v^2*(mean(srt)+273.15));

%���� �����
Ra_lng = Pr*Gr_lng;
Ra_med = Pr*Gr_med;
Ra_srt = Pr*Gr_srt;

%���� �����
Nu_lng = 0.59*Ra_lng^0.25;
Nu_med = 0.59*Ra_med^0.25;
Nu_srt = 0.59*Ra_srt^0.25;

%����� ���� �����
h_lng  = Nu_lng*k_air/lng_h(end);
h_med  = Nu_med*k_air/med_h(end);
h_srt  = Nu_srt*k_air/srt_h(end);

%���� ����� �� ��� ��� ������ ������ �� ��������� �� ������
m_lng = sqrt(h_lng*Pf/(k_stl*Af));
m_med = sqrt(h_med*Pf/(k_stl*Af));
m_srt = sqrt(h_srt*Pf/(k_stl*Af));

%����� �������� ���� ������
T_lng = (lng(1)-26)*(cosh(m_lng*(lng_h(end)-lng_h))+h_lng/(m_lng*k_stl)*sinh(m_lng*(lng_h(end)-lng_h)))/(cosh(m_lng*lng_h(end))+h_lng/(m_lng*k_stl)*sinh(m_lng*lng_h(end)))+26;
T_med = (med(1)-26)*(cosh(m_med*(med_h(end)-med_h))+h_med/(m_med*k_stl)*sinh(m_med*(med_h(end)-med_h)))/(cosh(m_med*med_h(end))+h_med/(m_med*k_stl)*sinh(m_med*med_h(end)))+26;
T_srt = (srt(1)-26)*(cosh(m_srt*(srt_h(end)-srt_h))+h_srt/(m_srt*k_stl)*sinh(m_srt*(srt_h(end)-srt_h)))/(cosh(m_srt*srt_h(end))+h_srt/(m_srt*k_stl)*sinh(m_srt*srt_h(end)))+26;


% figure
% plot(lng_h*1e2,lng,'DisplayName','Measurement'); hold on; grid on;
% plot(lng_h*1e2,T_lng,'--','DisplayName','Calculated')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show
% figure
% plot(med_h*1e2,med,'DisplayName','Measurement'); hold on; grid on;
% plot(med_h*1e2,T_med,'--','DisplayName','Calculated')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show
% figure
% plot(srt_h*1e2,srt,'DisplayName','Measurement'); hold on; grid on;
% plot(srt_h*1e2,T_srt,'--','DisplayName','Calculated')
% xlabel('Distance From Heated plate [cm]')
% ylabel('T [\circC]')
% legend show


%% prt3

Ar = 0.04; %[m^2]
h  = 8;    %[W/(m^2*C)]
sigma = 5.67e-8; %[W/(m^2*K^4)]
T_h = 300+273.15; %[K]
T_inf = 26+273.15; %[K]
F = [0.0743 0.143 0.343];% ���� ����� ���� �����

%������ ���� ���� ���� ���� ����� ������
p1 = [sigma 0 0 h -(F(1)*sigma*(T_h^4+T_inf^4)+sigma*T_inf^4+h*T_inf)];
p2 = [sigma 0 0 h -(F(2)*sigma*(T_h^4+T_inf^4)+sigma*T_inf^4+h*T_inf)];
p3 = [sigma 0 0 h -(F(3)*sigma*(T_h^4+T_inf^4)+sigma*T_inf^4+h*T_inf)];

T1 = roots(p1)-273.15;
T1 = T1(4);
T2 = roots(p2)-273.15;
T2 = T2(4);
T3 = roots(p3)-273.15;
T3 = T3(4);

T1c = 42; T2c = 47; T3c = 51.8;

err1 = abs(T1-T1c)/T1c;
err2 = abs(T2-T2c)/T2c;
err3 = abs(T3-T3c)/T3c;





